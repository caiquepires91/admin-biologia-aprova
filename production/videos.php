<?php require __DIR__.'/php/autentica.php';?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Vídeos</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <?php include './componentes/DataTableCSS.php' ?>

    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <!--TinyMCE-->
    <script src="../vendors/tinymce/tinymce.min.js"></script>


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <link href="./css/modules/video.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">


            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-play-circle"></i> Vídeos <small></small></h2>
                    <div class="panel_toolbox">
                    <button id="btn-novo-aluno" class="btn btn-primary" data-toggle="modal" data-target="#modal_cadastro">Cadastrar Novo Vídeo <i class="fa fa-plus-square"></i></button>
                  </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">

                    <table id="videos" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th><i class="fas fa-image"></i> Miniatura</th>
                              <th><i class="fas fa-file-alt"></i> Nome</th>
                              <th><i class="fas fa-list"></i> Categoria</th>
                              <th><i class="fas fa-clock-o"></i> Duração</th>
                              <th><i class="fas fa-calendar"></i> Data de Cadastro</th>
                              <th><i class="fas fa-calendar"></i> Data para Liberação</th>
                              <th style="width: 75px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                          </tr>
                      </thead>
                      
                  </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->

                  <!-- Small modal Cadastro-->
                  <div id="modal_cadastro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-user-plus"></i> Cadastrar Vídeo</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>
                          <!--Inserir formulario aqui-->
                          <form id="form-video" class="form-horizontal form-label-left">
                          <div id="error_cadastro"></div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="nome_video" name="nome-video" require class="form-control col-md-7 col-xs-12">
                              <span class="fas fa-list form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Descrição: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" name="desc-video" id="desc_video" rows="3" placeholder="Descrição do vídeo"></textarea>
                            
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Link: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="link_video" name="link-video" require class="form-control col-md-7 col-xs-12">
                              <span class="fas fa-list form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Duração</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="duracao_video" name="duracao" required="required" class="form-control col-md-7 col-xs-12 hora">
                                <span class="fas fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="select_categoria" name="id-categoria" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Data de Liberação</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="data_liberacao" name="data-liberacao" required="required" class="form-control col-md-7 col-xs-12 data hora">
                                <span class="fas fa-calendar form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 text-center">
                              <div class="flex-center">
                                <div class="hold-info">
                                  <img id="img_preview" class ="img img-preview info" src="./images/placeholder.png" width="150">
                                </div>
                              </div>
                              <div class="mt-7">
                                <label for="foto_video" class="btn btn-primary btn-xs">Selecionar imagem</label>
                                <small class="form-text text-muted d-block mt-7">Tamanho máximo de upload: 1MB.</small>
                                <input type="file" id="foto_video" name="foto-video" require class="col-md-7 col-xs-12 hidden">
                              </div>
                            </div>
                          </div>
                          <input type="hidden" name="action" value="novo">
                          
                          <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="cadastrar-video" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
                  <!-- /modal de leitura -->
                  <div id="read_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Detalhes</h4>
                        </div>
                        <div id="read_modal-body" class="modal-body">
                          <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>

                      </div>
                    </div>
                  </div>
            <!-- /modals leitura -->
            <!-- modals update -->
            <div id="update_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-edit"></i> Atualizar Vídeo</h4>
                  </div>
                  <div class="modal-body">
                    <h4>Preencha os campos abaixo</h4>
                    <!--Inserir formulario aqui-->
                    <form id="form-atualizar-video" class="form-horizontal form-label-left">
                          <div id="uerror_cadastro"></div>
                          
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="unome_video" name="nome-video" require class="form-control col-md-7 col-xs-12">
                              <span class="fas fa-list form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Descrição: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <textarea class="form-control descricao" name="desc-video" id="udesc_video" rows="3" placeholder="Descrição do vídeo"></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Link: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="ulink_video" name="link-video" require class="form-control col-md-7 col-xs-12">
                              <span class="fas fa-list form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Duração</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="uduracao_video" name="duracao" required="required" class="form-control col-md-7 col-xs-12 hora">
                                <span class="fas fa-clock-o form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="uselect_categoria" name="id-categoria" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Data de Liberação</label>
                              <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="udata_liberacao" name="data-liberacao" required="required" class="form-control col-md-7 col-xs-12 data hora">
                                <span class="fas fa-calendar form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 text-center">
                            <div class="flex-center">
                                <div class="hold-info">
                                <img id="uimg_preview" class ="img img-preview" src="">
                                </div>
                              </div>
                              <div class="mt-7">
                                <label for="ufoto_video" class="btn btn-primary btn-xs">Selecionar imagem</label>
                                <small class="form-text text-muted d-block mt-7">Tamanho máximo de upload: 1MB.</small>
                                <input type="file" id="ufoto_video" name="foto-video" require class="hidden">
                                <input type="hidden" id="usrc_img" name="img-atual">
                                <input type="hidden" id="id_video_update" name="id_video">
                              </div>
                              
                            </div>


                          </div>
                          
                          <div class="modal-footer">
                                <input type="hidden" name="action" value="atualizar">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="atualizar-video" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                </div>
              </div>
            </div>
          </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/crud_video.js"></script>


  </body>
</html>