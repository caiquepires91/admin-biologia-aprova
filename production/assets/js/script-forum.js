$(document).ready(function(){
    initTopicos()  

    $('#form-topico').submit(function(e){
      e.preventDefault();
      dados = {
        titulo: $('#form-topico input[name="titulo-topico"]').val(),
        resposta: $('#form-topico textarea[name="text-resposta"]').val(),
        id_user: id_usuario,
        action: 'novo_topico'
      }

      $.ajax({
        method: 'post',
        url: './ajax/ajax_forum.php',
        data: dados,
        dataType: 'json'	
      }).done(res=>{
        success = responseTreatment(res, false, [reloadTopicos, clearInputs], false, $('#novo-topico'));
        if(success) {
          window.location.href = `topico.php?id=${res.id_topico}&pagina=1`;
        }
      })
    })
})

function clearInputs(){
  $('#form-topico input[name="titulo-topico"]').val('');
  $('#form-topico textarea[name="text-resposta"]').val('');
}

function reloadTopicos(){
  $('#table-topicos').DataTable().ajax.reload();
  $('#table-destaques').DataTable().ajax.reload();
}

function initTopicos(){
    const table = $('#table-topicos').DataTable({
            processing: true,
            responsive: true,
            iDisplayLength: 15,
            lengthChange: false,
            aaSorting: [],
            ajax: {
              url: './ajax/ajax_forum.php',
              data: {action: 'ler_topicos'},
              dataSrc: ''
            },
            columns:[
              {className:"w-70",
              sort: "data_criacao", 
              render: function(data, type, row){
                  return `<div><a href="topico.php?id=${row.id}">${row.titulo}</a> ${row.deletado == 0? '':'<span style="color: #c02222; font-size: 12px">[Tópico desabilitado pelo administrador]</span>'}</div>
                  <div style="color: grey;" class="mt-3">Por ${row.criador}, <time datetime="${row.data_criacao}">${row.data_extenso}</time></div>` 
              }},

              {className:"w-10 text-right", 
              render: function(data, type, row){
                return `<div>${row.respostas} respostas</div>
                <div style="color: grey;" class="mt-3">${row.likes} likes</div>`
              }},

              {className:"w-40px", 
              render: function(data, type, row){
                if(row.ultimo_user)
                    return `<a href="#"><img class="img-icon-forum" src="./uploads/users/perfil/${row.ultimo_user.image}" width="34" height="34"></a>`
                else
                    return ``
              }},

              {className:"w-20 text-left", 
              render: function(data, type, row){
                  if(row.ultimo_user)
                    return `<div><a href="#">${row.ultimo_user.user.split(' ')[0]}</a></div><div style="color: grey;" class="mt-3">${row.ultimo_user.data_resposta}</div>`
                  else 
                    return ``
              }}
            ]
    })

    const tableDestaque = $('#table-destaques').DataTable({
      responsive: true,
      lengthChange: false,
      searching: false,
      paging: false,
      info: false,
      aaSorting: [],
      ajax: {
        url: './ajax/ajax_forum.php',
        data: {action: 'ler_topicos_destaque'},
        dataSrc: ''
      },
      columns:[
        {className:"w-70",
        sort: "data_criacao", 
        render: function(data, type, row){
            return `<div><a href="topico.php?id=${row.id}">${row.titulo}</a> ${row.deletado == 0? '':'<span style="color: #c02222; font-size: 12px">[Tópico desabilitado pelo administrador]</span>'}</div>
            <div style="color: grey;" class="mt-3">Por ${row.criador}, <time datetime="${row.data_criacao}">${row.data_extenso}</time></div>` 
        }},

        {className:"w-10 text-right", 
        render: function(data, type, row){
          return `<div>${row.respostas} respostas</div>
          <div style="color: grey;" class="mt-3">${row.likes} likes</div>`
        }},

        {className:"w-40px", 
        render: function(data, type, row){
          if(row.ultimo_user)
              return `<a href="#"><img class="img-icon-forum" src="./uploads/users/perfil/${row.ultimo_user.image}" width="34" height="34"></a>`
          else
              return ``
        }},

        {className:"w-20 text-left", 
        render: function(data, type, row){
            if(row.ultimo_user)
              return `<div><a href="#">${row.ultimo_user.user.split(' ')[0]}</a></div><div style="color: grey;" class="mt-3">${row.ultimo_user.data_resposta}</div>`
            else 
              return ``
        }}
      ]
})

    $("#searchbox").keyup(function() {
      table.search(this.value).draw();
   });

   $('#table-topicos tbody').on('click', 'tr', function () {
    var data = table.row( this ).data();
    window.location.href = `topico.php?id=${data.id}`;
  } );

  $('#table-destaques tbody').on('click', 'tr', function () { 
    var data = tableDestaque.row( this ).data();
    window.location.href = `topico.php?id=${data.id}`;
  } );

}