
/**
 * JAVASCRIPT FOR PLANS PAGE
 * 
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/

$(document).ready(function(){
    loadTable()
    resetarModal()
    getCupons()
    resetarForm($("#form-plano"), $("error_cadastro"))

    $(".valort").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: false,
    unmaskAsNumber: true,
    });

    $(".valord").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: false,
    unmaskAsNumber: true
    });

    $('#form-plano').submit(function(e){
        e.preventDefault()
        let formData={
            "plano-nome": $("#form-plano input[name='plano-nome']").val(),
            "plano-valor-total": $("#form-plano input[name='plano-valor-total']").val(),
            "plano-valor-desconto": $("#form-plano input[name='plano-valor-desconto']").val(),
            "plano-duracao": $("#form-plano input[name='plano-duracao']").val(),
            "plano-duracao-dim": $("#form-plano input[name='plano-duracao-dim']:checked").val(),
            "action": "novo"
        };

        $.ajax({
            method: 'post',
            url: './ajax/ajax_plano.php',
            data: formData,
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, $("#error_cadastro"), [reloadPlanos], false, $('#modal_cadastro'))
            if(success) resetarForm($("#form-plano"), $("#error_cadastro"));
        })
    })

    $('#form-atualizar-plano').submit(function(e){
        e.preventDefault()
        let formData={
            "plano-nome": $("#form-atualizar-plano input[name='plano-nome']").val(),
            "plano-valor-total": $("#form-atualizar-plano input[name='plano-valor-total']").val(),
            "plano-valor-desconto": $("#form-atualizar-plano input[name='plano-valor-desconto']").val(),
            "plano-duracao": $("#form-atualizar-plano input[name='plano-duracao']").val(),
            "plano-duracao-dim": $("#form-atualizar-plano input[name='plano-duracao-dim']:checked").val(),
            "id_plano": $("#form-atualizar-plano input[name='id_plano'] ").val(),
            "action": "atualizar"
        };

        $.ajax({
            method: 'post',
            url: './ajax/ajax_plano.php',
            data: formData,
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, $("#uerror_cadastro"), [reloadPlanos], false, $('#modal_update'))
            if(success) resetarForm($("#form-atualizar-plano"), $("#uerror_cadastro"));
        })
    })

    $("#vincular_cupom").submit(function(e){
        e.preventDefault()
        let plano_id = $('#id_plano').val()
        let cupom_id = $("#vincular_cupom select[name='cupom_id']").val()
        let formData={
            "cupom-id": cupom_id,
            "plano-id": plano_id,
            "action": "novo"
        };

        if(cupom_id == "") return;

        $.ajax({
            method: 'post',
            url: './ajax/ajax_desconto.php',
            data: formData,
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, $("#error_vinculo"), [reloadPlanos], false, false)
            if(success) {initTableCupom(plano_id), getCupons(plano_id)}
        })
    })
})

function resetarModal(){
    $('#modal_cadastro').on('hide.bs.modal', function (e) {
        resetarForm($("#form-plano"), $("error_cadastro"))
    })
    $('#modal_update').on('hide.bs.modal', function (e) {
        resetarForm($("#form-atualizar-plano"), $("uerror_cadastro"))
    })
}

function resetarForm(form, error){
        form[0].reset()
        error.html("");
}


function loadTable(){
    const table = $('#planos').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[4, "desc"]],
        "ajax": {
            "url": "./ajax/ajax_plano.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            {"data": "nome_plano"},
            {"data": "valor_total", render: function(valor_total){
                return formata_dinheiro(valor_total);
            }},
            {"data": "valor_desconto", render: function(valor_desconto){
                return formata_dinheiro(valor_desconto);
            }},
            {"data": "duracao",
            "render": function(duracao){
                if(duracao > 12){
                    meses = duracao % 12;
                    anos = parseInt(duracao/12)
                    if(meses != 0){
                        return `${anos} anos e ${meses} meses`
                    }else
                        return `${anos} anos`
                }else
                    return `${duracao} meses`
            }},
            {"data": "data_cadastro"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <button class="btn btn-primary btn-xs" onClick="readPlano(${id})" title="Detalhes" data-toggle="modal" data-target="#modal_read" data-tooltip="tooltip" data-placement="top"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="updatePlano(${id})" data-toggle="modal" data-target="#modal_update" title="Editar" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deletePlano(${id})" title="Excluir Registro" data-toggle="tooltip" data-placement="top" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}


function reloadPlanos(){
    $('#planos').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updatePlano(id){
    const hiddenid = $("#id_plano_update")
    hiddenid.val(id);

    $.ajax({
        url: './ajax/ajax_plano.php',
        data: {id_plano: id, action: "ler"}
    }).done(function(result){
        let response = JSON.parse(result)
        if(response.success == 1){
            $('#unome_plano').val(response.plano.nome_plano)
            $('#uvalor_total').val(response.plano.valor_total)
            $('#uvalor_desconto').val(response.plano.valor_desconto)
            $('#uduracao').val(response.plano.duracao)
        }else{
            new PNotify({
                title: 'Falha ao atualizar!',
                text: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
            //reloadCategorias()
        }
    })
}

function deletePlano(id){
    let confirmed = confirm("Deseja remover este Plano?")
    if(confirmed == true){

        $.ajax({
            method: 'post',
            url: './ajax/ajax_plano.php',
            data: {id_plano: id, action: "deletar"},
            dataType: 'json'	
        }).done(res=>{
            responseTreatment(res, false, [reloadPlanos], false, false)
        })
    }
}

// FUNÇÕES REFERENTES AO VINCULO DE CUPONS //
function initTableCupom(id_plano){
    const table = $('#plano_cupom')
    table.find('tbody').html("")
    $.ajax({
        url: './ajax/ajax_desconto.php',
        data: {id_plano: id_plano, action: 'ler_todos'},
        dataType: 'json'
    }).done(function(response){
        if(response.length > 0){
            response.forEach(cupom => {
                table.find('tbody').append(`<tr>
                                            <td>${cupom.codigo}</td>
                                            <td><button class="btn btn-primary btn-xs" onClick="desvincular(${cupom.id},${id_plano})" title="Desvincular cupom" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash-o"></i> Desvincular </button></td>
                                            </tr>`)
                });
        }else{
            table.find('tbody').append(`<tr><td colspan=2 style="text-align: center">Nenhum cupom foi vinculado a este plano!</td></tr>`)
        }
        
    })

}

function desvincular(id, idplano){
    let confirmed = confirm("Deseja desvincular este Cupom?")
    if(confirmed == true){
        $.ajax({
            method: 'post',
            url: './ajax/ajax_desconto.php',
            data: {id: id, action: "deletar"},
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, false, [] , false, false);
            if(success) {initTableCupom(idplano), getCupons(idplano)}
        })
    }
}

function getCupons(id_plano){
    const selectCupom = $('#select_cupom')
    selectCupom.html('<option value="">Selecionar Cupom de Desconto</option>')
    $.ajax({
        url: './ajax/ajax_cupom.php',
        data:{id_plano:id_plano, action: 'ler_todos_por_plano'},
        dataType: 'json'
    }).done(function(res){
        if(res.length == 0){
            selectCupom.html('<option value="">Não há mais cupons disponíveis</option>')
            return
        }
        res.forEach(cupom => {
            selectCupom.append(`<option value='${cupom.id}'>${cupom.codigo} R$ ${cupom.desconto} Válido até: ${cupom.data_validade}</option>`);
        })
    })
}

function readPlano(id){
    let table = $('#plan_view')
    $('#id_plano').val(id)
    getCupons(id)

    $.ajax({
        url: './ajax/ajax_plano.php',
        data: {id_plano: id, action: 'ler'},
        dataType: 'json'
    }).done(function(result){
        dur = result.plano.duracao
        
        html = `<tr>
                    <th> Nome</th>
                    <td>${result.plano.nome_plano}</td>
                </tr>
                <tr>
                    <th> Valor Total</th>
                    <td>${formata_dinheiro(result.plano.valor_total)}</td>
                </tr>
                <tr>
                    <th> Valor com Desconto</th>
                    <td>${formata_dinheiro(result.plano.valor_desconto)}</td>
                </tr>
                <tr>
                    <th> Duração</th>
                    <td>${dur>12? parseInt(dur/12)+' anos e ' + dur%12 + ' meses' : dur + ' meses'}</td>
                </tr>
                <tr>
                    <th> Data de Cadastro</th>
                    <td>${result.plano.data_cadastro}</td>
                </tr>`
                
        table.html(html)
        initTableCupom(id)
    })
}

// END FUNÇÕES REFERENTES AO VINCULO DE CUPONS //