<?php
  require __DIR__.'/php/autentica.php';
  require_once __DIR__ . '/php/functions.php';
  $simulado = validarSimulado();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Administração - <?php echo $simulado['nome']?></title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    

    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- datatables -->
    <?php include './componentes/DataTableCSS.php' ?>
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">

    <!--TinyMCE-->
    <script src="../vendors/tinymce/tinymce.min.js"></script>

    <!--jqueryUI-->
    <link href="./js/jqueryUI/jquery-ui.min.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <link href="./css/modules/prova.css" rel="stylesheet">
    <style>
      .flex-bottom{
        display: flex;
        align-items: end;
      }
      .flex-horizontal-center{
        display: flex;
        align-items: center;
      }
      </style>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Painel do Simulado<small></small></h2>

                    <div class="panel_toolbox">
                      <a href="simulados.php" class="btn btn-primary"><i class="fa fa-reply"></i> Visualizar todos simulados</a>
                    </div>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-2 col-sm-2 col-xs-12 profile_left">
                    <div class="panel-body">
                          <h3 class="green"><i class="fa fa-file-alt"></i> <?php echo $simulado['nome']?></h3>
                          <div class="project_detail">

                            <p class="title">Data de Cadastro</p>
                            <p><?php echo $simulado['data_cadastro']?></p>
                            <p class="title">Data de Liberação</p>
                            <p><?php echo $simulado['data_liberacao']?></p>
                            <p class="title">Data Final</p>
                            <p><?php echo $simulado['data_final']?></p>
                          </div>
                        </div>

                    </div>
                    <!--informações de simulado-->
                    <div class="col-md-10 col-sm-10 col-xs-12">
                    <!-- start TABS -->
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_info" role="tab" id="info-tab" data-toggle="tab" aria-expanded="false">Informações <i class="fa fa-file-alt"></i></a>
                          </li>
                          <li role="presentation" class="questao"><a href="#tab_question" id="home-tab" role="questao-tab" data-toggle="tab" aria-expanded="true">Questões <i class="fa fa-pencil"></i></a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_add_question" role="tab" id="add-questao-tab" data-toggle="tab" aria-expanded="false">Adicionar Questão <i class="fa fa-plus"></i></a>
                          </li>
                          <li role="presentation" class="edit" style="display: none;"><a href="#tab-edit-question" class="" role="tab" id="edit-questao-tab" data-toggle="tab" aria-expanded="false">Editar Questão <i class="fa fa-edit"></i></a>
                          </li>
                        </ul>

                            <!-- end TABS -->

                        <div id="myTabContent" class="tab-content">
                        <!-- start dados do simulado -->
                          <div role="tabpanel" class="tab-pane active fade in" id="tab_info" aria-labelledby="home-tab">
                            <input type="hidden" id="id" value=<?php echo $simulado['id']?> data-nome="<?php echo $simulado['nome']?>">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    
                                    <div class="x_content">
                                    <!-- top tiles -->
                                      <div class="row tile_count">
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                          <span class="count_top"><i class="fa fa-user"></i> Total de Simulados Realizados</span>
                                          <div class="count rlz"></div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                          <span class="count_top"><i class="fa fa-clock-o"></i> Número de Questões</span>
                                          <div class="count nq"></div>
                                          
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4 tile_stats_count">
                                          <span class="count_top"><i class="fa fa-user"></i> Média de Acertos</span>
                                          <div class="count green md"></div>
                                        </div>
                                      </div>
                                    
                                      <!-- /top tiles -->
                                    <!--SESSAO DE GRAFICO DE NOTAS-->
                                      <div class="row">
                                        <div class="x_panel">
                                          <div class="x_title">
                                            <h4>Gráfico de Notas</h4>
                                          </div>
                                          <div class="x_content">
                                            <div class="no-charts" style="display:none;">Nenhum Simulado Realizado até o momento</div>
                                            <div class="charts row">
                                            <div class="chart-container col-md-4 col-sm-4 col-xs-12" style="text-align:center;">
                                              <canvas class="" id="chartQuestoesCertas" height="200" width="200" style="margin: 0 auto;"></canvas>
                                            </div>
                                            <div class="chart-container col-md-4 col-sm-4 col-xs-12" style="text-align:center;">
                                              <canvas class="" id="chartQuestoesErradas" height="200" width="200" style="margin: 0 auto;"></canvas>
                                            </div>
                                            <div class="chart-container col-md-4 col-sm-4 col-xs-12" style="text-align:center;">
                                              <canvas class="" id="chartNotas" height="200" width="200" style="margin: 0 auto;"></canvas>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <!--END SESSAO DE GRAFICO-->
                                    <!--ATIVIDADES RECENTES-->
                                    <div class="row">
                                    <div class="x_panel">
                                      <div class="x_title">
                                        <h2>Atividades Recentes <small><!--inserir data--><?php echo date('d/m/Y H:i:s')?></small></h2>
                                        <div class="clearfix"></div>
                                      </div>
                                      <div class="x_content">
                                        <div class="dashboard-widget-content">
                                          <ul class="list-unstyled timeline widget">
                                          <div class="no-charts" style="display:none;">Nenhum Simulado Realizado até o momento</div>
                                          </ul>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                    <!--END ATIVIDADES RECENTES-->

                                      
                                    </div>
                                  </div>
                                </div>
                              </div>

                          </div>
                          <!-- end dados simulado -->

                         <!-- start questions -->
                          <div role="tabpanel" class="tab-pane fade" id="tab_question" aria-labelledby="profile-tab">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <div class="clearfix"></div>
                                      
                                    </div>
                                    <div class="x_content">
                                      <table id="questions" class="display table table-hover table-responsive" style="width: 100%">
                                        <thead>
                                          <tr>
                                            <th class="all"><i class="fas fa-file-alt"></i> Enunciado</th>
                                            <th class="none"><i class="fas fa-list-ol"></i> Código</th>
                                            <th class="none"><i class="fas fa-tag"></i> Assunto</th>
                                            <th class="none"><i class="fas fa-calendar"></i> Data de Cadastro</th>
                                            <th class="none"><i class="fas fa-mouse-pointer"></i> Ações</th>
                                          </tr>
                                        </thead>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <!-- end questions -->
                          <!-- start add questions -->
                          <div role="tabpanel" class="tab-pane fade" id="tab_add_question" aria-labelledby="profile-tab">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    <div class="x_title">
                                    <h4>Questão <strong id="number"></strong><!--Puxa quantidade de questões do banco para atribuir número--></h4>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                    <form id="form-questao" class="form-horizontal form-label-left">
                                    <div id="error_questao"></div>

                                        <div class="form-group">
                                            <label>ENUNCIADO: </label>
                                            <textarea class="form-control enunciado tinyEditor" name="enunciado" id="enunciado" rows="3" placeholder="Enunciado da questão"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>ASSUNTO: </label>
                                            <input type="text" class="form-control autocomplete assunto" name="assunto" placeholder="Assunto...">
                                            <input type="hidden" class="autocompleteId" name="assunto-id">
                                        </div>

                                        <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="a" checked/> ALTERNATIVA A: </label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt1" id="alt1" rows="3" placeholder="ALTERNATIVA"></textarea>
                                          </div>

                                          <div class="form-group">
                                          <label><input type="radio" class="" name="correct" value="b"/> ALTERNATIVA B: </label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt2" id="alt2" rows="3" placeholder="Alternativa"></textarea>
                                          </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="c"/> ALTERNATIVA C <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt3" id="alt3" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="d"/> ALTERNATIVA D <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt4" id="alt4" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="e"/> ALTERNATIVA E <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt5" id="alt5" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="clearfix"></div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" id="add-questao" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                                        </div>

                                    </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <!-- end add questions -->
                          <!-- TAB EDITAR QUESTAO-->
                          <div role="tabpanel" class="tab-pane fade" id="tab-edit-question" aria-labelledby="edit-tab">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content edit">
                                    <form id="form-edit-questao" class="form form-horizontal form-label-left">

                                        <div class="form-group">
                                            <label>ENUNCIADO: </label>
                                            <textarea class="form-control enunciado tinyEditor" name="enunciado" id="u_enunciado" rows="3" placeholder="Enunciado da questão"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label>ASSUNTO: </label>
                                              <input type="text" class="form-control autocomplete assunto" name="assunto" placeholder="Assunto...">
                                              <input type="hidden" class="autocompleteId" name="assunto-id">
                                        </div>

                                        <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="a" checked/> ALTERNATIVA A: </label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt1" id="u_alt1" rows="3" placeholder="Alternativa"></textarea>
                                          </div>

                                          <div class="form-group">
                                          <label><input type="radio" class="" name="correct" value="b"/> ALTERNATIVA B: </label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt2" id="u_alt2" rows="3" placeholder="Alternativa"></textarea>
                                          </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="c"/> ALTERNATIVA C <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt3" id="u_alt3" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="d"/> ALTERNATIVA D <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt4" id="u_alt4" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="form-group">
                                            <label><input type="radio" class="" name="correct" value="e"/> ALTERNATIVA E <small>(opcional)</small>:</label>
                                            <textarea class="form-control alternativa tinyEditor" name="alt5" id="u_alt5" rows="3" placeholder="Alternativa"></textarea>
                                            </div>

                                            <div class="clearfix"></div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="submit" id="add-questao" class="btn btn-primary"><i class="fa fa-save"></i> Salvar</button>
                                        </div>

                                        

                                    </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <!-- END TAB EDITAR QUESTAO -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- modal plano-->
          <div id="ver_questao" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Visualizar Questão</h4>
                        </div>
                        <div id="ver_questao_body" class="modal-body">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>

                      </div>
                    </div>
                  </div>
        
        <!-- /modals -->

        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI-->
    <script src="./js/jqueryUI/jquery-ui.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- Chart.js -->
    <script src="../vendors/Chart.js/dist/chart.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- datatables -->
    <?php include './componentes/DataTableJs.php' ?>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/config.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/simulado_question.js"></script>
    <script src="./js/simulado_info.js"></script>

  </body>
</html>