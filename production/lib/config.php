<?php
require_once __DIR__ . "/../config/environment.php";

date_default_timezone_set("America/Recife");
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');

// Versionamento de arquivos
define("ASSETS_VERSION", "0.1.14");

// ------------------------------------------------------------------------------------------------
// ------------------------------------------- DATABASE -------------------------------------------
// ------------------------------------------------------------------------------------------------
$mysqli = new mysqli(DATABASE_SERVER, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);

if (mysqli_connect_errno()) {
	header("HTTP/1.0 503 Service Unavailable", true, 503);
	exit();
}

mysqli_set_charset($mysqli,"utf8");

// ------------------------------------------------------------------------------------------------
// -------------------------------------------- Funções -------------------------------------------
// ------------------------------------------------------------------------------------------------
require_once __DIR__ . "/funcoes.php";

// ------------------------------------------------------------------------------------------------
// ------------------------------------------ Constantes ------------------------------------------
// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------
// ------------------------------------------ ERROR -----------------------------------------------
// ------------------------------------------------------------------------------------------------

function send_response ($response, $status = "success") {
	$response["status"] = $status;

	echo json_encode($response);
	exit();
}

function myErrorHandler ($errno, $errstr) {
	$data = array();
	$data["msg"] = $errstr;

	switch ($errno) {
		case E_USER_ERROR:
			send_response($data, "error");
			break;

		case E_USER_WARNING:
			send_response($data, "warning");
			break;

		case E_USER_NOTICE:
			send_response($data, "info");
			break;

		default:
			send_response("", "unknown");
			break;
	}
}

set_error_handler("myErrorHandler", E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE);

// Validação entradas
function valida_post ($parametros, &$mensagem_erro) {
	// Input checking
	$_funcoes_validacao_tipo = [
		// Texto
		// ['tipo' => "html", 'validar' => "validar_html", 'mensagem' => "Informe um nome válido"],
		['tipo' => "texto", 'validar' => "validar_texto", 'mensagem' => "Informe um nome válido"],
		['tipo' => "nome_limpo", 'validar' => "validar_nome_limpo", 'mensagem' => "Informe um nome válido"],
		['tipo' => "uppercase",  'validar' => "validar_uppercase",  'mensagem' => "Informe um texto válido"],
		// Numéricos
		['tipo' => "inteiro",    'validar' => "validar_inteiro",    'mensagem' => "Informe um valor inteiro"],
		['tipo' => "duplo",    'validar' => "validar_duplo",    'mensagem' => "Informe um valor duplo separado por underline"],
		['tipo' => "duplo_id",    'validar' => "validar_duplo",    'mensagem' => "ID inválido"],
		['tipo' => "dia",    'validar' => "validar_dia",    'mensagem' => "Informe um número válido, entre 1 e 31."],
		['tipo' => "quantidade", 'validar' => "validar_quantidade",    'mensagem' => "Informe um valor inteiro positivo"],
		['tipo' => "decimal",    'validar' => "validar_decimal",    'mensagem' => "Informe um valor decimal"],
		['tipo' => "decimal_en", 'validar' => "validar_decimal_en", 'mensagem' => "Informe um valor decimal"],
		['tipo' => "status", 'validar' => "validar_status", 'mensagem' => "Informe se o valor é 0 ou 1"],
		// Contatos
		['tipo' => "telefone", 'validar' => "validar_telefone", 'mensagem' => "Informe um telefone no formato (12) 1234-1234 ou (12) 12345-1234"],
		['tipo' => "email",    'validar' => "validar_email",    'mensagem' => "Informe um email válido"],
		['tipo' => "emails",    'validar' => "validar_multiplos_emails",    'mensagem' => "Informe emails válidos no formato Email1, Email2..."],
		['tipo' => "cep",      'validar' => "validar_cep",      'mensagem' => "Informe um CEP no formato 12345-123"],
		// Data en-EN
		['tipo' => "date",     'validar' => "validar_date",      'mensagem' => "Informe uma data no formato AAAA-MM-DD (ex: 2017-12-31)"],
		['tipo' => "datetime", 'validar' => "validar_datetime",  'mensagem' => "Informe uma data no formato AAAA-MM-DD HH:mm:ss (ex: 2017-12-31 23:59:59)"],
		['tipo' => "bithdate", 'validar' => "validar_birthdate", 'mensagem' => "Informe uma data no formato AAAA-MM-DD (ex: 2017-12-31) até o dia atual"],
		['tipo' => "schedule", 'validar' => "validar_schedule",  'mensagem' => "Informe uma data no formato AAAA-MM-DD (ex: 2017-12-31) posterior ao dia atual"],
		// Datas pt-BR
		['tipo' => "data",        'validar' => "validar_data",        'mensagem' => "Informe uma data no formato DD/MM/AAAA (ex: 31/12/2017)"],
		['tipo' => "datatempo",        'validar' => "validar_datatempo",        'mensagem' => "Informe uma data no formato DD/MM/AAAA hh:mm (ex: 31/12/2017 00:00)"],
		['tipo' => "hora",        'validar' => "validar_hora",        'mensagem' => "Informe um horário no formato HH:mm:ss (ex: 23:59:59)"],
		['tipo' => "hora2",        'validar' => "validar_hora2",        'mensagem' => "Informe um horário no formato HH:mm (ex: 23:59)"],
		['tipo' => "nascimento",  'validar' => "validar_nascimento",  'mensagem' => "Informe uma data no formato DD/MM/AAAA (ex: 31/12/2017) até o dia atual"],
		['tipo' => "agendamento", 'validar' => "validar_agendamento", 'mensagem' => "Informe uma data no formato DD/MM/AAAA (ex: 31/12/2017) posterior ao dia atual"],
		// Pessoas
		['tipo' => "cpf",        'validar' => "validar_cpf",        'mensagem' => "Informe um CPF válido"],
		['tipo' => "cnpj",        'validar' => "validar_cnpj",        'mensagem' => "Informe um CNPJ válido"],
		['tipo' => "login",      'validar' => "validar_login",      'mensagem' => "Informe um CPF válido"],
		['tipo' => "senha",      'validar' => "validar_senha",      'mensagem' => "Informe uma senha válida"],
	];

	$mensagem_erro = "";

	// Verifica se os esperados foram informados e atendem aos requisitos do tipo correspondente
	foreach($parametros as $parametro) {
		$nome_parametro = $parametro["parametro"];

        if (isset($parametro["ignorar"]) && $parametro["ignorar"] === true)
            continue;

		if (!isset($_POST[$nome_parametro])) {
			$mensagem_erro .= "Parâmetro não informado: " . $parametro["nome"] . "<br>";
			continue;
		}

		// Determina os valores a serem validados (array ou valor único)
		if (isset($parametro["is_array"]) && $parametro["is_array"] === true) {
			if (!is_array($_POST[$nome_parametro])) {
				$mensagem_erro .= "Parâmetro obrigatório não informado: " . $parametro["nome"] . "<br>";
				continue;
			}
			if ($parametro["requerido"] && empty($_POST[$nome_parametro])) {
				$mensagem_erro .= "Parâmetro obrigatório não informado: " . $parametro["nome"] . "<br>";
				continue;
			}

			$entradas = $_POST[$nome_parametro];
		}
		else {
			$entradas = [$_POST[$nome_parametro]];
		}

		// Valida os valores de acordo com o tipo de dado
		foreach ($entradas as $i => $valor) {
			$valor = trim($valor);

			if ($valor === "") { // Parâmetro não informado
				$entradas[$i] = null;
				if ($parametro["requerido"]) { // Parameto obrigatório não informado
					$mensagem_erro .= "Parâmetro obrigatório não informado: " . $parametro["nome"] . "<br>";
					break;
				}
			}
			else {
				// Verifica se valor informado atende ao tipo de entrada
				foreach($_funcoes_validacao_tipo as $tipo) {
					if ($tipo["tipo"] === $parametro["tipo"]) {
						$entradas[$i] = $tipo["validar"]($valor);
						break;
					}
				}

				if (is_null($entradas[$i])) { // Parâmetro informado não atende ao tipo
					$mensagem_erro .= $parametro["nome"] . " inválido(a). " . $tipo["mensagem"] . "<br>";
					break;
				}
			}
		}

		// Salva os valorel "limpos" em $_POST
		if (isset($parametro["is_array"]) && $parametro["is_array"] === true) {
			$_POST[$nome_parametro] = $entradas;
		}
		else {
			$_POST[$nome_parametro] = $entradas[0];
		}
	}

	if ($mensagem_erro !== "")
		return false;

	return true;
}


?>