<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';
//require '../models/Venda.php';

if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'add':
            add();
            break;

        case 'carregar_planos_aluno':
            carregar_planos_aluno();
        break;

        case 'deletar_venda':
            deletar_venda();
        break;

        case 'ler_venda':
            ler_venda();
            break;
        case 'historico_vendas':
            historico_vendas();
        break;
    }
}

if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'historico_ano':
        historico_ano();
            break;
        case 'historico_mes':
        historico_mes();
            break;
        case 'historico_dia':
        historico_dia();
            break;
        case 'historico_personalizado':
        historico_personalizado();
            break;

    }
}

function add(){
    global $mysqli;

    $id_aluno = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);
    $id_plano = filter_input(INPUT_POST, 'id_plano', FILTER_SANITIZE_NUMBER_INT);
    $valor = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $pagt = filter_input(INPUT_POST, 'pagt', FILTER_SANITIZE_NUMBER_INT);
    $parcelas = filter_input(INPUT_POST, 'parcelas', FILTER_SANITIZE_NUMBER_INT);

    if($valor == "0,00" || $valor === "0," || $valor === "0" || $valor == ""){
        $valor = 0.0;
    }

    if($valor < 0){
        echo json_encode(array("success" => 0, "msg" => "Valor inválido!"));
        die();
    }

    if($parcelas < 1 || $parcelas > 12){
        echo json_encode(array("success" => 0, "msg" => "Número de parcelas inválido!"));
        die();
    }

    $result = $mysqli->query("SELECT nome_plano, duracao FROM plano WHERE id = $id_plano");
    $result = $result->fetch_assoc(); //json_decode(Plano::read($venda['id_plano']), true);
    $duracao = $result['duracao'];
    $nome = $result['nome_plano'];

    $data_inicio = date("Y-m-d H:i:s");
    $data_cadastro = date("Y-m-d H:i:s");

    try{
        $valor_liquido = $valor;
        $mysqli->autocommit(FALSE);

        //buscando a ultima data do ultimo plano vinculado
        /*
        $result = $mysqli->query(
            "SELECT 
                DATE_ADD(data_inicio, INTERVAL duracao MONTH) as data_final, transacao.data_inicio, transacao.descricao, 
                transacao.valor, transacao.id 
            FROM 
                `transacao` 
            WHERE 
                tipo_transacao = 'plano' and 
                id_usuario = $id_aluno and 
                deletado = 0 
            ORDER BY 
                data_inicio DESC
        ")
        */
        if ($result = $mysqli->query(
            "SELECT
                up.id, up.fim AS data_final, up.inicio AS data_inicio, up.descricao
            FROM
                usuario_plano up
            WHERE
                up.id_usuario = $id_aluno AND
                up.inicio IS NOT NULL AND
                up.fim IS NOT NULL AND
                up.deletado = 0
            ORDER BY
                up.inicio DESC
            LIMIT 1
        ")) {
            $plano = $result->fetch_assoc();
            if(date("Y-m-d H:i:s", strtotime($plano['data_final'])) > date('Y-m-d H:i:s')){
                $data_inicio = $plano['data_final'];
            }
        }

        //Criando as parcelas com os valores do gateway
            switch($pagt){
                case '0': //boleto
                    $valor_liquido = $valor;

                    $stmt_result = $mysqli->prepare("INSERT INTO `transacao`(`id`, `id_usuario`,`id_plano`,`id_gateway`,`descricao`,`valor`, `valor_liquido`,`duracao`,`data_inicio`,`data_cadastro`,`deletado`,`tipo_transacao`,`tipo_pagamento`,`n_parcelas`) 
                    VALUES (default,?,?,NULL,?,?,?,?,?,?,default,'plano',2,1)");
                    $stmt_result->bind_param("iisddiss", $id_aluno,$id_plano,$nome,$valor,$valor_liquido,$duracao,$data_inicio,$data_cadastro);
                    $stmt_result->execute();
                    break;

                case '1': //cartao
                    if($parcelas == 1){
                        //Compra a vista realizada no cartão, numero de parcela 1
                        $valor_liquido = $valor;
                        
                        //adicionando plano
                        $stmt_result = $mysqli->prepare("INSERT INTO `transacao`(`id`, `id_usuario`,`id_plano`,`id_gateway`,`descricao`,`valor`, `valor_liquido`,`duracao`,`data_inicio`,`data_cadastro`,`deletado`,`tipo_transacao`,`tipo_pagamento`,`n_parcelas`) 
                        VALUES (default,?,?,NULL,?,?,?,?,?,?,default,'plano',1,1)");
                        $stmt_result->bind_param("iisddiss", $id_aluno,$id_plano,$nome,$valor,$valor_liquido,$duracao,$data_inicio,$data_cadastro);
                        $stmt_result->execute();

                    }else{
                        $valor_parcela = $valor/$parcelas; //calcula o valor das parcelas
                        //calcula valor liquido
                        $valor_liquido = $valor;
                        ///Adiciona transacao
                        $stmt_result = $mysqli->prepare("INSERT INTO `transacao`(`id`, `id_usuario`,`id_plano`,`id_gateway`,`descricao`,`valor`, `valor_liquido`,`duracao`,`data_inicio`,`data_cadastro`,`deletado`,`tipo_transacao`,`tipo_pagamento`,`n_parcelas`) 
                        VALUES (default,?,?,NULL,?,?,?,?,?,?,default,'plano',?,?)");
                        $stmt_result->bind_param("iisddissii", $id_aluno,$id_plano,$nome,$valor,$valor_liquido,$duracao,$data_inicio,$data_cadastro,$pagt,$parcelas);
                        $stmt_result->execute();

                        $id_venda = $mysqli->insert_id; //gera o id da transacao

                        //salva as parcelas
                        $data = date('Y-m-d');
                        for($i = 1; $i <= $parcelas; $i++){//loop para calcular o valor descontado mensal
                            $mysqli->query("INSERT INTO parcelas(id_venda, periodo, valor, juros, data_recebimento) VALUES($id_venda, $i, $valor_parcela,'0.0', DATE_ADD('$data', INTERVAL $i MONTH))");
                        }
                    }
                break;
            }


            // Cria assinatura de plano do usuário
            $data_inicio = date("Y-m-d", strtotime($data_inicio));
            $data_fim = date("Y-m-d", strtotime("+" . $duracao . " months", strtotime($data_inicio)));
            $horario_atual = date("Y-m-d H:i:s");
            $stmt = $mysqli->prepare("INSERT INTO usuario_plano (id_usuario, id_plano, duracao, inicio, fim, horario) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("iiisss", $id_aluno, $id_plano, $duracao, $data_inicio, $data_fim, $horario_atual);
            $stmt->execute();
        
        $mysqli->autocommit(TRUE);
        echo json_encode(array("success" => 1, "msg" => "Plano adicionado com sucesso!"));
    }catch(Exception $e){
        $mysqli->rollback();
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod"=>$e->getCode())));
    }
}

function carregar_planos_aluno(){
    global $mysqli;

    $id =  filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);//$_POST['id']; //id do aluno
    
    $vendas = array();

    /*
    $result = $mysqli->query("SELECT DATE_ADD(data_inicio, INTERVAL duracao MONTH) as data_final, data_inicio, descricao, 
    valor, duracao,tipo_pagamento, id FROM `transacao` WHERE id_usuario = $id and deletado = 0 ORDER BY data_inicio ASC")
    */

    if($result = $mysqli->query(
        "SELECT
            up.inicio AS data_inicio, up.fim AS data_final, up.duracao, up.id, up.descricao AS assinatura_descricao,
            p.nome_plano AS descricao, p.valor_total AS valor,
            TIMESTAMPDIFF(month, up.inicio, up.fim) AS duracao
        FROM
            usuario_plano up
            INNER JOIN plano p ON p.id = up.id_plano
        WHERE
            up.id_usuario = $id AND
            up.inicio IS NOT NULL AND
            up.fim IS NOT NULL AND
            up.deletado = 0
        ORDER BY
            up.inicio ASC
    ")){
        while($row = $result->fetch_assoc()){
            $row['metodo_pagamento'] = 0;

            $dataFinal = strtotime($row['data_final']);
            $dataInicio = strtotime($row['data_inicio']);
            $dataAtual = strtotime(date('Y-m-d H:i:s'));
            
            if ($dataInicio && $dataFinal)
                $row['progress'] = (($dataAtual - $dataInicio)/($dataFinal - $dataInicio))*100;
            else
                $row['progress'] = 0;

            $row['data_final'] = date('d/m/Y', strtotime($row['data_final']));
            $row['data_inicio'] = date('d/m/Y', strtotime($row['data_inicio']));

            $vendas[] = $row;
            }
        
        echo json_encode($vendas);
        
    }else{
        die(json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno)));
    }
}

function deletar_venda(){
    global $mysqli;

    $id = $_POST['id_venda'];

    $delete = "UPDATE usuario_plano SET deletado = 1 WHERE id = $id";
        if($mysqli->query($delete)){
            if($mysqli->affected_rows > 0){
                echo json_encode(array("success" => 1, "msg" => "O plano do aluno foi cancelado!"));
            }else{
                echo json_encode(array("success" => 0, "msg" => "Plano não encontrado, por favor atualize a página!"));
            }
        }else{
            echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}


function ler_venda(){
    global $mysqli;
    $id = $_POST['id_venda'];
    $select = "SELECT descricao, valor FROM `transacao` WHERE id = $id";
    
    if($result = $mysqli->query($select)){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => 1, "venda" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Plano não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function historico_vendas(){
    global $mysqli;

    try{
        // $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data_cadastro, '%d/%m/%Y') as data_cadastro FROM transacao WHERE tipo_transacao = 'plano' AND deletado = 0 GROUP BY data_cadastro");
        $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data, '%d/%m/%Y') as data_cadastro FROM vendas WHERE deletado = 0 GROUP BY DATE_FORMAT(data, '%d/%m/%Y')");
        $stmt->execute();
        $res = $stmt->get_result();
        $historical = array('datas'=>array(), 'valores'=>array(), 'max'=>0, 'min'=>0);

        while($row = $res->fetch_assoc()){
            array_push($historical['datas'], $row['data_cadastro']);
            array_push($historical['valores'], $row['amount']);
        }
        $historical['max'] = max($historical['valores']);
        $historical['min'] = min($historical['valores']);
        echo json_encode($historical);
    }catch(Exception $e){
        echo $e->getMessage();
    }

}

function historico_ano(){
    global $mysqli;
    $ano = filter_input(INPUT_GET, 'ano', FILTER_SANITIZE_NUMBER_INT);

    try{
        // $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data_cadastro, '%d/%m/%Y') as data_cadastro FROM transacao WHERE tipo_transacao = 'plano' AND YEAR(data_cadastro) = $ano AND deletado = 0 GROUP BY data_cadastro;");
        $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data, '%d/%m/%Y') as data_cadastro FROM vendas WHERE YEAR(data) = $ano AND deletado = 0 GROUP BY DATE_FORMAT(data, '%d/%m/%Y');");
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            die(json_encode(array('success'=>99, 'msg'=>'Não há dados no periodo selecionado.')));

        $historical = array('datas'=>array(), 'valores'=>array(), 'max'=>0, 'min'=>0);

        while($row = $res->fetch_assoc()){
            array_push($historical['datas'], $row['data_cadastro']);
            array_push($historical['valores'], $row['amount']);
        }
        $historical['max'] = max($historical['valores']);
        $historical['min'] = min($historical['valores']);
        echo json_encode(array('success'=>1, 'historico'=>$historical));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function historico_mes(){
    global $mysqli;
    $mes = filter_input(INPUT_GET, 'mes', FILTER_SANITIZE_NUMBER_INT);

    try{
        // $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data_cadastro, '%d/%m/%Y') as data_cadastro FROM transacao WHERE tipo_transacao = 'plano' AND MONTH(data_cadastro) = $mes AND deletado = 0 GROUP BY data_cadastro;");
        $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data, '%d/%m/%Y') as data_cadastro FROM vendas WHERE MONTH(data) = $mes AND deletado = 0 GROUP BY DATE_FORMAT(data, '%d/%m/%Y');");
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            die(json_encode(array('success'=>99, 'msg'=>'Não há dados no periodo selecionado.')));

        $historical = array('datas'=>array(), 'valores'=>array(), 'max'=>0, 'min'=>0);

        while($row = $res->fetch_assoc()){
            array_push($historical['datas'], $row['data_cadastro']);
            array_push($historical['valores'], $row['amount']);
        }
        $historical['max'] = max($historical['valores']);
        $historical['min'] = min($historical['valores']);
        echo json_encode(array('success'=>1, 'historico'=>$historical));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function historico_dia(){
    global $mysqli;
    $dia = filter_input(INPUT_GET, 'dia', FILTER_SANITIZE_NUMBER_INT);

    try{
        $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data, '%d/%m/%Y') as data_cadastro FROM vendas WHERE DAY(data) = $dia AND deletado = 0 GROUP BY data;");
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            die(json_encode(array('success'=>99, 'msg'=>'Não há dados no periodo selecionado.')));

        $historical = array('datas'=>array(), 'valores'=>array(), 'max'=>0, 'min'=>0);

        while($row = $res->fetch_assoc()){
            array_push($historical['datas'], $row['data_cadastro']);
            array_push($historical['valores'], $row['amount']);
        }
        $historical['max'] = max($historical['valores']);
        $historical['min'] = min($historical['valores']);
        echo json_encode(array('success'=>1, 'historico'=>$historical));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

function historico_personalizado(){
    global $mysqli;
    $data_inicial = filter_input(INPUT_GET, 'data_inicial', FILTER_SANITIZE_STRING);
    $data_final = filter_input(INPUT_GET, 'data_final', FILTER_SANITIZE_STRING);
    if(!$data_inicial || !$data_final)
        die(json_encode(array('success'=>0, 'msg'=>'Informe as duas datas.')));

    $data_inicial = str_replace('/', '-', $data_inicial);   
    $data_inicial = date('Y-m-d', strtotime($data_inicial));

    $data_final = str_replace('/', '-', $data_final);   
    $data_final = date('Y-m-d', strtotime($data_final));

    try{
        $stmt = $mysqli->prepare("SELECT sum(valor) as amount, count(*) as vendas, DATE_FORMAT(data, '%d/%m/%Y') as data_cadastro from vendas where data BETWEEN '$data_inicial' AND '$data_final' GROUP BY data;");
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            die(json_encode(array('success'=>99, 'msg'=>'Não há dados no periodo selecionado.')));

        $historical = array('datas'=>array(), 'valores'=>array(), 'max'=>0, 'min'=>0);

        while($row = $res->fetch_assoc()){
            array_push($historical['datas'], $row['data_cadastro']);
            array_push($historical['valores'], $row['amount']);
        }
        $historical['max'] = max($historical['valores']);
        $historical['min'] = min($historical['valores']);
        echo json_encode(array('success'=>1, 'historico'=>$historical));
    }catch(Exception $e){
        echo $e->getMessage();
    }
}

?>