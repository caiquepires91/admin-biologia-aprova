<?php require __DIR__.'/php/autentica.php'; ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Fórum</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css?v=<?php echo ASSETS_VERSION; ?>" rel="stylesheet">
    <link href="./css/modules/forum.css?v=<?php echo ASSETS_VERSION; ?>" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="x_panel">
            <div class="x_title">
              <button class="btn btn-primary" data-toggle="modal" data-target="#novo-topico">Novo Tópico</button>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="topicos-container">
                <table id="table-destaques" class="table table-striped table-hover" style="width: 100%; margin-bottom: 0;">
                <thead>
                  <tr>
                    <th colspan="4" class="bg-dark"><i class="fas fa-star"></i> Tópicos em destaque</th>
                  </tr>
                  <tr>
                    <th class="d-none">T</th>
                    <th class="d-none">R</th>
                    <th class="d-none">P</th>
                    <th class="d-none">N</th>
                  </tr>
                </thead>
                </table>
                <table id="table-topicos" class="table table-striped table-hover" style="width: 100%; margin-top: 0;">
                <thead>
                  <tr>
                    <th colspan="4" class="bg-dark">Tópicos <input style="float: right;" type="text" class="forum-filter" placeholder="Procurar..." id="searchbox"></th>
                  </tr>
                  <tr>
                    <th class="d-none">T</th>
                    <th class="d-none">R</th>
                    <th class="d-none">P</th>
                    <th class="d-none">N</th>
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /page content -->
        <!--modal container-->
        <div id="novo-topico" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus"></i> Criar Novo Tópico</h4>
                  </div>
                  <div class="modal-body">
                    <h4>Preencha os campos abaixo</h4>
                    <!--Inserir formulario aqui-->
                    <form id="form-topico" class="form-horizontal form-label-left">
                      <div id="error_cadastro"></div>

                      <div class="form-group">
                        <label>Título <span class="field-required">*</span>: </label>
                          <input type="text" name="titulo-topico" class="form-control col-md-7 col-xs-12 titulo-form" required>
                      </div>

                      <div class="form-group">
                            <label>Post <span class="field-required">*</span>: </label>
                            <textarea class="resposta-textarea form-control col-md-7 col-xs-12 resposta-form" name="text-resposta" id="userResposta" rows="5" required></textarea>
                      </div>
                      <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" id="cadastrar-topico" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
          <!--end modals-->
        
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script>
      const id_usuario = <?php echo $_SESSION['id_usuario'];?>
    </script>
    <script src="../build/js/custom.js?<?php echo ASSETS_VERSION; ?>="></script>
    <script src="./js/functions.js?<?php echo ASSETS_VERSION; ?>="></script>
    <script src="./js/script-forum.js?<?php echo ASSETS_VERSION; ?>="></script>


  </body>
</html>