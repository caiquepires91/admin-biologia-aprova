var _id = null;
$(document).ready(function(){
    console.log("Ready");

    $('#userResposta').val('');
    $('#form-responder input[name="action"]').val('');
    $('#form-responder input[name="id_resposta"]').val('');

    /*
    $(".hover-info").mouseover((e)=>{
        elemento = $(e.currentTarget);
        pos = elemento.offset()
        altura = elemento.height();
        $('.modal-info').css({'top': `${pos.top + altura + 15}px`, 'left': `${pos.left + 8}px`});
        $('.modal-info').addClass('fadein');
    }).mouseout((e)=>{
        $('.modal-info').removeClass('fadein');
    })
    */

    $('#btn-responder-topico').click(function(e){
        e.preventDefault();
        $("#form-responder .citacao").hide();
        $('#userResposta').focus();
        $('#form-responder input[name="id_resposta"]').val('');
        $('#form-responder input[name="action"]').val('resposta-topico');
    })

    $('.like-container').click(function(){

        $.ajax({
            url: './ajax/ajax_forum.php',
            data: {action: 'ler_likes_post', id: $(this).data('id')},
            dataType: 'json'	
        }).done(res=>{
            if(res.success == 1){
                html = "";
                res.likes.forEach(user => {
                    html +=
                    `<section>
                        <aside><img src="./uploads/users/perfil/${user.image}" alt="Imagem de perfil" class="img-rounded"></aside>
                        <div class="content">
                        <h3>
                            <a href="usuario.php?id=${user.id}">
                            <span>${user.nome}</span>
                            </a>
                        </h3>
                        <span><time datatime=${user.data_criacao}>${user.data_extenso}</time></span>
                        </div>
                    </section>`
                })
                $('.modal-likes-body').html(html);
                $('#modal-likes-label .n-likes').html(`(${res.likes.length})`);
            }
        })
    })

    $('#form-responder').submit(function(e){
        e.preventDefault();
        action = $('#form-responder input[name="action"]').val();
        dados = {
                resposta: $('#userResposta').val(),
                action: 'novo_post',
                id_usuario: id_usuario,
                id_resposta: $('#form-responder input[name="id_resposta"]').val() == ''? null: $('#form-responder input[name="id_resposta"]').val(),
                id_topico: $('#form-responder input[name="id_topico"]').val() 
            }
        
        $.ajax({
            method: 'post',
            url: './ajax/ajax_forum.php',
            data: dados,
            dataType: 'json'	
        }).done(res=>{
            if(responseTreatment(res, false, [], false, false)){
                $('#userResposta').val('');
                $('#form-responder input[name="action"]').val('');
                $('#form-responder input[name="id_resposta"]').val('');
                window.location.href = `topico.php?id=${dados.id_topico}&pagina=${Math.ceil(res.n/5)}`;
            }
        })
    })

    $('#form-edit').submit(function(e){
        console.log('form submit');
        e.preventDefault();
        switch($('#form-edit input[name=action]').val()){
            case '0':
                dados = {
                    texto_resposta: $('#form-edit .form-group.resp textarea').val(),
                    id: _id,
                    id_usuario: id_usuario,
                    action: 'editar_post'
                }

                $.ajax({
                    method: 'post',
                    url: './ajax/ajax_forum.php',
                    data: dados,
                    dataType: 'json'	
                }).done(res=>{
                    if(responseTreatment(res, false, [], false, false)){
                        _id = null;
                        $('#form-edit .form-group.resp').hide()
                        window.location.reload();
                    }  
                })
                break;

            case '1':
                    dados = {
                        titulo: $('#form-edit .form-group.title input').val(),
                        id: _id,
                        id_usuario: id_usuario,
                        action: 'editar_titulo'
                    }

                    $.ajax({
                        method: 'post',
                        url: './ajax/ajax_forum.php',
                        data: dados,
                        dataType: 'json'	
                    }).done(res=>{
                        if(responseTreatment(res, false, [], false, $('.modal-edit'))){
                            _id = null;
                            $('#form-edit .form-group.title').hide()
                            window.location.reload();
                        }  
                    })
                    break;
            default:
                break;
        }
    })
})

function editarResposta(id){
    $("#form-edit .form-group.title").hide();
    $('.modal-edit .modal-title').html('Editar Resposta');

    $.ajax({
        url: './ajax/ajax_forum.php',
        data: {action: 'ler_post', id: id},
        dataType: 'json'	
    }).done(res=>{
        $("#form-edit .form-group.resp").show();
        $("#form-edit .form-group.resp textarea").val(res.texto_resposta);
        $('#form-edit input[name=action]').val('0');
        _id = id;
    })
}

function editarTopico(id){
    $("#form-edit .form-group.resp").hide();
    $('.modal-edit .modal-title').html('Editar Título do Tópico');
    $.ajax({
        url: './ajax/ajax_forum.php',
        data: {action: 'ler_titulo_topico', id: id},
        dataType: 'json'	
    }).done(res=>{
        $("#form-edit .form-group.title").show();
        $("#form-edit .form-group.title input").val(res.titulo);
        $('#form-edit input[name=action]').val('1');
        _id = id;
    })
}

function citarResposta(id_resposta){
    $('#form-responder input[name="action"]').val('resposta-citar');
    $('#form-responder input[name="id_resposta"]').val(id_resposta);

    $.ajax({
        url: './ajax/ajax_forum.php',
        data: {action: 'ler_post', id: id_resposta},
        dataType: 'json'	
    }).done(res=>{
        $("#form-responder .citacao").html(`citando ${res.nome}, ${res.data_extenso}.`).show();
    })

    $('#userResposta').show().focus();
}

function likeTopico(id_topico, element){
    like(id_topico, id_usuario, 'like_topico', $(element));
}

function likeResposta(id_resposta, element){
    like(id_resposta, id_usuario, 'like_post', $(element));
}

function like(id_tabela, id_user, action, element){

    $.ajax({
        method: 'post',
        url: './ajax/ajax_forum.php',
        data: {action: action, id_tabela: id_tabela, id_user: id_user},
        dataType: 'json'	
    }).done(res=>{
        if(res.success == 1){
            n = parseInt(element.closest('.resposta-body').find('.n-likes').html());
            if(res.value==1){
                element.children().css('color', '#1ABB9C');
                element.attr("data-original-title", "like");
                element.closest('.resposta-body').find('.n-likes').html(n + 1);
            }else if(res.value==0){
                element.children().css('color', 'inherit');
                element.attr("data-original-title", "");
                element.closest('.resposta-body').find('.n-likes').html(n - 1);
            }
        }
    })
}

function disableTopico(id, element){
    console.log($(element))
    if($(element).data('ativo') == 0)
        desabilitar(id, 'desabilitar_topico', 1, $(element));
    else if($(element).data('ativo') == 1)
        desabilitar(id, 'desabilitar_topico', 0, $(element));
}

function disableResposta(id, element){
    if($(element).data('ativo') == 0)
        desabilitar(id, 'desabilitar_post', 1, $(element));
    else if($(element).data('ativo') == 1)
        desabilitar(id, 'desabilitar_post', 0, $(element));
}

function desabilitar(id, action, value, element){

    $.ajax({
        method: 'post',
        url: './ajax/ajax_forum.php',
        data: {action: action, id: id, value: value},
        dataType: 'json'	
    }).done(res=>{
        if(res.success == 1){
            if(value == 1 && element.data('ativo') == 0){
                element.children().css('color', 'red');
                element.attr("data-original-title", "Habilitar");

                if(action == 'desabilitar_topico'){
                    $('.titulo').append('<span class="desabilitado">[Tópico desabilitado pelo administrador]</span>');
                    $('.resposta-textarea').attr('disabled', true);
                    $('.btn-responder').attr('disabled', true);
                } 
                else element.closest('.resposta-body').find('.resposta-advice').append('<span class="desabilitado">[Resposta desativada pelo administrador]</span>');
                element.data("ativo", 1)
            }else
            if(value == 0 && element.data('ativo') == 1){
                element.children().css('color', 'inherit');
                element.attr("data-original-title", "Desabilitar");
                if(action == 'desabilitar_topico'){
                    $('.titulo').find('span').remove();
                    $('.resposta-textarea').attr('disabled', false);
                    $('.btn-responder').attr('disabled', false);
                }
                else element.closest('.resposta-body').find('.resposta-advice').find('span').remove();
                element.data("ativo", 0)
            }
        }
    })
}