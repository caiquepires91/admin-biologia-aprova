<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';
//require '../models/Venda.php';

if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler_transacao':
            ler_transacao();
        break;

        case 'carregar_transacoes':
            carregar_transacoes();
        break;
/*
        case 'atualizar_gateway':
            atualizar_gateway();
            break;
*/
        case 'calcula_valor_disponivel':
            calcula_valor_disponivel();
        break;
    }
}

if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'saque':
            saque();
            break;
    }
}

//-----------------FUNCOES AJAX--------------------------------///
function saque(){

    global $mysqli;
    $valor = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $data = date('Y-m-d H:i:s');
    $disponivel = calcula_valor_disponivel();

    if(($disponivel - $valor) < 0){
        die(json_encode(array("success"=>0, "msg"=>"Saldo insuficiente para saque. Valor disponível para saque R$ $disponivel")));
    }

    try{
        $stmt = $mysqli->prepare("INSERT INTO transacao(id,descricao, valor, valor_liquido, data_cadastro, tipo_transacao) VALUES(default, 'saque', ?,?,?,'saque')");
        $stmt->bind_param("dds", $valor, $valor, $data);
        $stmt->execute();
        echo json_encode(array("success" => 1, "msg" => "Saque realizado no valor de R$ $valor"));

    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=>$e->getMessage())));
    }
}

function ler_transacao(){
    global $mysqli;

    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT); //$_GET['id']; //id da transacao

    try{
        $stmt = $mysqli->prepare("SELECT u.nome, u.cidade, u.estado, u.id, v.descricao,v.duracao, v.n_parcelas, v.id as id_venda, v.tipo_pagamento as pagt, v.valor_liquido, v.valor as valor_pago, DATE_FORMAT(v.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_cadastro, p.valor_desconto, g.nome as gateway, g.id as id_gateway FROM transacao as v inner join usuario as u ON v.id_usuario = u.id inner JOIN plano as p ON v.id_plano = p.id LEFT JOIN gateway as g ON v.id_gateway = g.id WHERE v.id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $res = $stmt->get_result();

        $row = $res->fetch_assoc();
        extract($row);
        $valor_desconto = formata_dinheiro($valor_desconto);
        $valor_pago = formata_dinheiro($valor_pago);
        $valor_liquido = formata_dinheiro($valor_liquido);

        if($gateway)
            $tipo = $pagt == 1 ? $n_parcelas > 1 ? "Cartão parcelado" : "Cartão à vista" : "Boleto";
        else
            $tipo = $pagt == 1 ? $n_parcelas > 1 ? "Cartão parcelado" : "Cartão à vista" : "À vista";

        $gateway = $gateway == NULL? "Compra Direta" : $gateway;
        

        //$gateways = $mysqli->query("SELECT nome, id FROM gateway"); //comentado por enquanto, a ideia é poder realizar a troca do gateway para caso de antecipação de valor

        //gerando HTML
        $html = "<table class=\"table jambo_table table-bordered\">
        <caption class=\"table-caption\">Dados Básicos</caption>
        <thead>
            <tr>
                <th>Nome do Usuário</th>
                <th>Nome do Plano</th>
                <th>PAGT</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>$nome</td>
                <td>$descricao</td>
                <td>$tipo</td>
                <td>$data_cadastro</td>
            </tr>
        </tbody>
    </table>
    <table class=\"table jambo_table table-bordered\">
            <caption class=\"table-caption\">Valores da Transação</caption>
            <thead>
                <tr>
                    <th>Valor plano</th>
                    <th>Valor pago</th>
                    <th>Valor líquido</th>
                    <th>Gateway</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>$valor_desconto</td>
                        <td>$valor_pago</td>
                        <td>$valor_liquido</td>
                        <td>$gateway</td>
                    </tr>
                </tbody>
        </table>";
        
        if($pagt == 1 && $n_parcelas > 1){
            $html .= "<table class=\"table table-striped jambo_table table-bordered\">
        <caption class=\"table-caption\">Parcelas</caption>
        <thead>
            <tr>
                <th>Periodo</th>
                <th>Valor</th>
                <th>Juros</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>";

        $parcelas = $mysqli->query("SELECT periodo, valor, juros, DATE_FORMAT(data_recebimento, '%d/%m/%Y') as data_recebimento FROM parcelas  WHERE id_venda = $id_venda;");
        while($parcela = $parcelas->fetch_assoc()){
            extract($parcela);
            $html .= "<tr>
                        <td>$periodo x </td>
                        <td>R$ $valor</td>
                        <td>$juros %</td>
                        <td>$data_recebimento</td>
                    </tr>";
        }
        $html .= " </tbody></table>";
    }
    /*
    $html .= "<select name='id_gateway' id='select_gateway' class='select2_group form-control'>
        <option value=''>Selecionar novo gateway</option>";
    while($gw = $gateways->fetch_assoc()){
        if($id_gateway == $gw['id']){
            $html .= "<option value='$id_gateway' selected>".$gw['nome']."</option>";
            continue;
        }
        $html .= "<option value='".$gw['id']."'>".$gw['nome']."</option>";
    }

       
    $html .="</select></div>";
    */
    $stmt->close();
    echo json_encode(array('success'=>1, 'body'=>$html));

    }catch(Exception $e){
        die(json_encode(array('success'=>99, 'msg'=>$e->getMessage(), 'cod'=>$e->getCode())));
    }
}

function carregar_transacoes(){
    global $mysqli;
    $data_inicial = isset($_GET['data_inicial']) ? filter_input(INPUT_GET, 'data_inicial', FILTER_SANITIZE_STRING) : null;
    $data_final = isset($_GET['data_final']) ? filter_input(INPUT_GET, 'data_final', FILTER_SANITIZE_STRING) : null;
    
    $datas = ValidaData($data_inicial, $data_final);
    $data_inicial = $datas['inicio'];
    $data_final = $datas['final'];
    
    //DEVE PEGAR OS PLANOS DOS ALUNOS ATIVOS E INATIVOS, BEM COMO OS PLANOS?
    $select = "SELECT u.nome, u.cidade, u.estado, u.id, v.descricao, v.id as id_venda,v.duracao, v.tipo_pagamento as pagt, v.valor_liquido, v.valor as valor_pago, DATE_FORMAT(v.data_cadastro, '%d/%m/%Y') as data_cadastro, p.valor_desconto, g.nome as gateway FROM transacao as v inner join usuario as u ON v.id_usuario = u.id inner JOIN plano as p ON v.id_plano = p.id LEFT JOIN gateway as g ON v.id_gateway = g.id where v.tipo_transacao = 'plano' AND v.deletado = 0";
    $select2 = "SELECT u.nome, u.cidade, u.id, u.estado, v.descricao,v.id as id_venda, v.duracao, v.tipo_pagamento as pagt,v.valor_liquido,  v.valor as valor_pago, DATE_FORMAT(v.data_cadastro, '%d/%m/%Y') as data_cadastro, p.valor_desconto, g.nome as gateway FROM transacao as v inner join usuario as u ON v.id_usuario = u.id inner JOIN plano as p ON v.id_plano = p.id LEFT JOIN gateway as g ON v.id_gateway = g.id WHERE v.tipo_transacao = 'plano' and v.data_cadastro BETWEEN ? AND ? AND v.deletado = 0";
    $select3 = "SELECT u.nome, u.cidade, u.id, u.estado, v.descricao,v.id as id_venda, v.tipo_pagamento as pagt, v.duracao,v.valor_liquido, v.valor as valor_pago, DATE_FORMAT(v.data_cadastro, '%d/%m/%Y') as data_cadastro, p.valor_desconto g.nome as gateway FROM transacao as v inner join usuario as u ON v.id_usuario = u.id inner JOIN plano as p ON v.id_plano = p.id LEFT JOIN gateway as g ON v.id_gateway = g.id WHERE v.tipo_transacao = 'plano' and v.data_cadastro = ? AND v.deletado = 0";

    $estadosBrasileiros = array(
        'AC'=>'Acre',
        'AL'=>'Alagoas',
        'AP'=>'Amapá',
        'AM'=>'Amazonas',
        'BA'=>'Bahia',
        'CE'=>'Ceará',
        'DF'=>'Distrito Federal',
        'ES'=>'Espírito Santo',
        'GO'=>'Goiás',
        'MA'=>'Maranhão',
        'MT'=>'Mato Grosso',
        'MS'=>'Mato Grosso do Sul',
        'MG'=>'Minas Gerais',
        'PA'=>'Pará',
        'PB'=>'Paraíba',
        'PR'=>'Paraná',
        'PE'=>'Pernambuco',
        'PI'=>'Piauí',
        'RJ'=>'Rio de Janeiro',
        'RN'=>'Rio Grande do Norte',
        'RS'=>'Rio Grande do Sul',
        'RO'=>'Rondônia',
        'RR'=>'Roraima',
        'SC'=>'Santa Catarina',
        'SP'=>'São Paulo',
        'SE'=>'Sergipe',
        'TO'=>'Tocantins'
        );

    try{
        if(isset($data_final) && isset($data_inicial)){
            $stmt = $mysqli->prepare($select2);
            $stmt->bind_param("ss", $data_inicial, $data_final);
        }else if(isset($data_inicial) && !isset($data_final)){
            $stmt = $mysqli->prepare($select3);
            $stmt->bind_param("s", $data_inicial);
        }
        else 
            $stmt = $mysqli->prepare($select);
        $stmt->execute();
        $res = $stmt->get_result();
        $dados = array();
        while($row = $res->fetch_assoc()){
            $row['estado'] = strlen($row['estado']) > 2 ? array_search($row['estado'], $estadosBrasileiros) : $row['estado'];
            $dados[] = $row;
        }

        echo json_encode($dados);
    }catch(Exception $e){
        die(json_encode(array()));
    }
}

//------------FUNCOES AUXILIARES----------------------------//
function ValidaData($data_inicial, $data_fim){

    $datas = array('inicio'=>null, 'final'=>null);

    if($data_inicial == false && $data_fim == false) return $datas;
    if($data_inicial == null && $data_fim != null) return $datas;
    if($data_inicial != null && $data_fim == null) 
    {
        $datainicial = explode("/",$data_inicial);
        $res1 = $datainicial[0] ? checkdate($datainicial[1],$datainicial[0],$datainicial[2]): null;
        if ($res1 == 1){
            $datas['inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_inicial)));
            return $datas;
        } else {
           return $datas;
        }
    }

    $datainicial = explode("/",$data_inicial); // fatia a string $dat em pedados, usando / como referência
    $datafim = explode("/",$data_fim);

    $res1 = $datainicial[0] ? checkdate($datainicial[1],$datainicial[0],$datainicial[2]): null;
    $res2 = $datafim[0]? checkdate($datafim[1],$datafim[0],$datafim[2]) :null;
	// verifica se a data é válida!
	// 1 = true (válida)
    // 0 = false (inválida)

	if ($res1 == 1 && $res2 == 1){
        $datas['inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_inicial)));
        $datas['final'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_fim)));
        return $datas;
	} else {
	   return $datas;
	}
}

/*
function atualizar_gateway(){
    global $mysqli;

    $id = $_GET['id'];
    $id_gateway = filter_input(INPUT_GET, 'id_gateway', FILTER_SANITIZE_NUMBER_INT);

	try{
        $mysqli->autocommit(FALSE);
        $gateway = $mysqli->query("SELECT * FROM gateway WHERE id = $id_gateway");
        $transacao = $mysqli->query("SELECT n_parcelas, valor, tipo_pagamento as pagt, data_cadastro FROM transacao WHERE id = $id");
        extract($transacao->fetch_assoc()); // parcelas, valor, pagt
        $gateway = $gateway->fetch_assoc(); //array gateway
        $valor_liquido = $valor;

        switch($pagt){
            case '0': //boleto
                $valor_liquido = $valor*(1.00 - $gateway['taxa_boleto']*0.010) - $gateway['tarifa_boleto'];
                $mysqli->query("UPDATE transacao SET valor_liquido = $valor_liquido , id_gateway = $id_gateway WHERE id = $id");
                break;

            case '1': //cartao
                if($n_parcelas == 1){
                    //Compra a vista realizada no cartão, numero de parcela 1
                    $valor_liquido = $valor*(1.00 - $gateway['taxa_cartao']*0.010) - $gateway['tarifa_cartao'];
                    $mysqli->prepare("UPDATE transacao SET valor_liquido = $valor_liquido , id_gateway = $id_gateway WHERE id = $id");
                }else{
                    if($gateway['isTaxaFixa']){
                        $desconto_fixo = $valor*$gateway['taxa_cartao']*0.01 + $gateway['tarifa_cartao']; //calcula valor das taxas fixas
                        $valor_parcela = $valor/$n_parcelas; //calcula o valor das parcelas
                        $desconto_variavel = 0.0;

                        for($i = 1; $i <= $n_parcelas; $i++){//loop para calcular o valor descontado mensal
                            $desconto_variavel +=  $valor_parcela * $gateway["txp$i"]*0.01;
                        }
                        //calcula valor liquido
                        $valor_liquido = $valor - $desconto_fixo - $desconto_variavel;
                        ///atualiza transacao
                        $mysqli->query("UPDATE transacao SET valor_liquido = $valor_liquido , id_gateway = $id_gateway WHERE id = $id");

                        //atualiza as parcelas
                        $dias = $gateway['liberacao_cartao'];
                        for($i = 1; $i <= $n_parcelas; $i++){//loop para calcular o valor descontado mensal
                            $soma_dias = $i * (int)$dias;
                            $data = date('Y-m-d', strtotime("+$soma_dias days", strtotime($data_cadastro)));
                            
                            $valor_parcela_mensal = $valor_parcela * ( 1 - $gateway["txp$i"]*0.01 );
                            $mysqli->query("UPDATE parcelas SET valor = $valor_parcela_mensal, juros = ".$gateway["txp$i"].", data_recebimento = '$data' WHERE id_venda = $id AND periodo = $i");
                        }
                    }else{ //nao é taxa fixa
                        $desconto = $valor*$gateway["txp$n_parcelas"]*0.01 + $gateway['tarifa_cartao']; //calcula valor das taxas fixas
                        $valor_parcela = $valor/$n_parcelas; //calcula o valor das parcelas

                        //calcula valor liquido
                        $valor_liquido = $valor - $desconto;
                        ///atualiza transacao
                        $mysqli->query("UPDATE transacao SET valor_liquido = $valor_liquido , id_gateway = $id_gateway WHERE id = $id");

                        //atualiza as parcelas
                        $dias = $gateway['liberacao_cartao'];
                        for($i = 1; $i <= $n_parcelas; $i++){//loop para calcular o valor descontado mensal
                            $soma_dias = $i * (int)$dias;
                            $data = date('Y-m-d', strtotime("+$soma_dias days", strtotime($data_cadastro)));
                            $valor_parcela_mensal = $valor_parcela * ( 1 - $gateway["txp$n_parcelas"]*0.01 );
                            $mysqli->query("UPDATE parcelas SET valor = $valor_parcela_mensal, juros = ".$gateway["txp$n_parcelas"].", data_recebimento = '$data' WHERE id_venda = $id AND periodo = $i");
                        }
                    }
                }
            break;
        }
        $mysqli->autocommit(TRUE);
        echo json_encode(array("success" => 1, "msg" => "Gateway alterado com sucesso!"));
    }catch(Exception $e){
        $mysqli->rollback();
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod"=>$e->getCode())));
    }
}
*/
function calcula_valor_disponivel(){
    global $mysqli;
    $disponivel = 0.0;
        
    //carregar boletos cujo o dinheiro já foi liberado
    $boletos = $mysqli->query("SELECT SUM(t.valor_liquido) as total FROM transacao as t INNER JOIN gateway as g ON t.id_gateway = g.id WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW()");
    if($row = $boletos->fetch_assoc()){
            $disponivel += (float) $row['total'];
    }
    //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado
    $cartao_a_vista = $mysqli->query("SELECT SUM(t.valor_liquido) as total FROM transacao as t INNER JOIN gateway as g ON t.id_gateway = g.id WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW()");
    if($row = $cartao_a_vista->fetch_assoc()){
        $disponivel += (float)$row['total'];
    }
    //carregar cartao varias parcelas cujo o dinheiro ja foi liberado
    $parcelas = $mysqli->query("SELECT SUM(p.valor) as total FROM transacao as t INNER JOIN parcelas as p ON p.id_venda = t.id WHERE t.tipo_transacao = 'plano' and p.data_recebimento <= NOW()");
    if($row = $parcelas->fetch_assoc()){
        $disponivel += (float) $row['total'];
    }

    $debitos = $mysqli->query("SELECT SUM(valor_liquido) as total FROM transacao WHERE tipo_transacao = 'saque' OR tipo_transacao = 'despesa' and data_cadastro <= NOW()");
    if($row = $debitos->fetch_assoc()){
        $disponivel -= (float) $row['total']; 
    }

    return $disponivel;
}

?>