<?php 
  $logout = isset($_GET['logout']) ? $_GET['logout'] : null;
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/img/favicon.ico" type="image/x-icon">

    <title>Login - Administração - Curso Somma</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="../vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.min.css" rel="stylesheet">
    <style>
      .text-none{
        text-shadow: none !important;
      }
    </style>
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
          <div id="login-error" class="text-none">
            <?php if(is_numeric($logout) && $logout == 1) 
            echo '<div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Deslogado por inatividade: </strong>Faça login para continuar</div>'?>
          </div>
            <form id="form-login">
              <h1>Entrar</h1>
              <div>
                <input type="text" name="usuario" class="form-control" placeholder="E-mail ou CPF" required>
              </div>
              <div>
                <input type="password" name="senha" class="form-control" placeholder="Senha" required>
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">Entrar</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fas fa-shield-alt"></i> Área Administrativa</h1>
                  <p>© <?php echo date("Y"); ?>. Todos os direitos reservados.<br />Desenvolvido por <a href="http://www.re9agencia.com.br" target="_blank">Re9 Agência</a></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
  <!-- jQuery -->
  <script src="../vendors/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="./js/functions.js"></script>
  <script src="./js/login.js"></script>
</html>
