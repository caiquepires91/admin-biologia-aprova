$(document).ready(function(){
    //cria instancia do datatable
    loadTable();

    //reseta modal de cadastro quando inicia o documento
    resetarModal();

    //reseta formulario de criação
    resetarForm($("#form-documento"), $("error_cadastro"))

    //inicia select de categorias disponíveis
    loadCategorias($('#select_categoria'), null);

    //inicia select de vídeos disponíveis
    loadVideos($('#select_video'), null);

    //inicia mascaras de data e hora
    $('.data').inputmask({ mask: '99/99/9999' });
    $('.data.hora').inputmask({ mask: ['99/99/9999 99:99', '99/99/9999 99:99:99'] , placeholder: "__/__/____ 00:00:00"});

    //mostrando o nome do arquivo selecionado
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.file_name').html(fileName);
        ext = fileName.split('.');
        switch(ext[ext.length - 1].toLowerCase()){
            case 'pdf': $('#icon').attr('src', './images/icons/icon_pdf.png');
                break;
            case 'doc': $('#icon').attr('src', './images/icons/icon_doc.png');
                break;
            case 'jpg': 
            case 'jpeg': 
            case 'png': 
            case'gif': $('#icon').attr('src', './images/icons/icon_img.png');
                break;
            default: $('#icon').attr('src', './images/icons/icon_no.png');
                break;
        }
    });
    
    //salva documento
    $('#form-documento').submit(function(e){
        e.preventDefault()
        var formData = new FormData(this);
        $.ajax({
            method: 'post',
            url: './ajax/ajax_documento.php',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(result){
            if(responseTreatment(result, $("#error_cadastro"), [reloadDocumentos], false, $('#modal_cadastro'))){
                resetarForm($("#form-documento"), $("#error_cadastro"));
            }
        })
    })

    //atualiza documento
    $('#form-atualizar-documento').submit(function(e){
        e.preventDefault()
        var formData = new FormData(this);
        $.ajax({
            method: 'post',
            url: './ajax/ajax_documento.php',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(result){
            if(responseTreatment(result, $("#error_cadastro"), [reloadDocumentos], false, $('#modal_update'))){
                resetarForm($("#form-atualizar-documento"), $("#uerror_cadastro"));
            }
        })
    })
})

//atualiza dados da tabela
function reloadDocumentos(){
    $('#documentos').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

//cria a tabela
function loadTable(){
    const table = $('#documentos').DataTable({
        processing: true,
        responsive: true,
        order: [3, 'asc'],
        ajax: {
            url: "./ajax/ajax_documento.php",
            data: {action: 'ler_todos'},
            dataSrc: ""
        },
        columns: [
            {data: "nome"},
            {data: "caminho", render: function(caminho){
                path_array = caminho.split('.');
                return path_array[path_array.length - 1].toUpperCase();
            }},
            {data: "nome_categoria"},
            {data: "nome_video"},
            {data: "data_cadastro"},
            {data: "data_liberacao"},
            {data: "id",
                render: function(data, type, row){
                    let tags = `
                    <a class="btn btn-primary btn-xs" href="./uploads/documentos/${row.caminho}" download="${row.nome}" target="_blank" title="Baixar Documento" data-tooltip="tooltip" data-placement="top"><i class="fa fa-download"></i></a>
                    <button class="btn btn-primary btn-xs" onClick="updateDocumento(${row.id})" data-toggle="modal" data-target="#modal_update" title="Editar Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteDocumento(${row.id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function resetarModal(){
    $('#modal_cadastro').on('hide.bs.modal', function (e) {
        resetarForm($("#form-documento"), $("error_cadastro"))
    })
    $('#modal_update').on('hide.bs.modal', function (e) {
        resetarForm($("#form-atualizar-documento"), $("uerror_cadastro"))
    })
}

function resetarForm(form, error){
    form[0].reset();
    error.html("");
    $('.file_name').empty();
    $('#icon').attr('src', "");
}

function loadCategorias(element, categoria){
    $.ajax({
        url: './ajax/ajax_categoria.php',
        data: {action: 'ler_todos'},
        dataType: 'json'
    }).done(function(result){
        let options = "<option value='0'>Selecione uma Categoria</option>"
        result.forEach(categoria => {
            options += `<option value='${categoria.id}'>${categoria.nome}</option>`
        })
        element.html(options)
        if(categoria)
            element.val(categoria)
    })
}

function loadVideos(element, video){
    $.ajax({
        url: './ajax/ajax_video.php',
        data: {action: 'ler_todos'},
        dataType: 'json'
    }).done(function(result){
        let options = "<option value='0'>Selecione um Vídeo</option>"
        result.forEach(video => {
            options += `<option value='${video.id}'>${video.nome}</option>`
        })
        element.html(options)
        if(video)
            element.val(video)
    })
}

function deleteDocumento(id){
    let confirmed = confirm("Deseja remover este Documento?")
    if(confirmed == true){
        $.ajax({
            method: 'post',
            url: './ajax/ajax_documento.php',
            data: {id_documento: id, action: "deletar"},
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, false, [reloadDocumentos], false, false)
        })
    }
}

function updateDocumento(id){

    const hiddenid = $("#hidden_doc_id");
    hiddenid.val(id);

    $.ajax({
        url: './ajax/ajax_documento.php',
        data: {id_documento: id, action: "ler"},
        dataType: 'json'
    }).done(function(response){
        if(response.success == 1){
            loadCategorias($("#uselect_categoria"), response.documento.id_categoria);
            loadVideos($("#uselect_video"), response.documento.id_video);
            path = './uploads/documentos/' + response.documento.caminho;
            $('#unome-doc').val(response.documento.nome);
            $('#udata-liberacao').val(response.documento.data_liberacao);
            $('#file_atual').html(response.documento.file_name);
            $('#usrc_doc').val(response.documento.caminho);
            ext = response.documento.file_name.split('.');
            switch(ext[ext.length - 1].toLowerCase()){
                case 'pdf': $('#uicon').attr('src', './images/icons/icon_pdf.png');
                    break;
                case 'doc': $('#uicon').attr('src', './images/icons/icon_doc.png');
                    break;
                    case 'jpg': case 'jpeg': case 'png': case'gif': $('#uicon').attr('src', './images/icons/icon_img.png');
                    break;
                default: $('#uicon').attr('src', './images/icons/icon_no.png');
                    break;
            }
        }else{
            new PNotify({
                title: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
        }
    })
}