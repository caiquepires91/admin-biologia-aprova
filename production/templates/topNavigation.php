<div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right" style='height: 80px;'>
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="https://biologiaaprova.com.br/<?php echo $_SESSION["image"]?>" alt="Imagem do perfil"> <?php echo $_SESSION["nome_usuario"]?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right" style="width: 100%;">
                    <li><a href="<?php echo "usuario_admin.php?id=".$_SESSION['id_usuario']?>"> Perfil</a></li>
                    <li><a href="php/logout.php"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>

        <!-- Loading -->
        <div id="loading_overlay" style="position: fixed; top: 0; left:0; width: 100%; height: 100%; background-color: rgba(0,0,0,.7); z-index: 9999; display: none;">
            <div class="loading-img-animation-wrapper">
                <!-- <img src="assets/images/logo.png" class="loading-img-animation" alt="" style="top: 50%; width: 50%; transform: translate(-50%, -50%);"> -->
                <span class="loading-percent"></span>
            </div>
        </div>
        <!-- /Loading -->