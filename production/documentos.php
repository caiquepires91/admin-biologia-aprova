<?php require __DIR__.'/php/autentica.php'; ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Documentos</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">


            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-folder"></i> Documentos <small></small></h2>
                    <div class="panel_toolbox">
                    <button id="btn-novo-aluno" class="btn btn-primary" data-toggle="modal" data-target="#modal_cadastro">Cadastrar Documento <i class="fas fa-plus-square"></i></button>
                  </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">

                    <table id="documentos" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th><i class="fas fa-file-alt"></i> Nome</th>
                              <th><i class="fas fa-caret-down"></i> Extensão</th>
                              <th><i class="fas fa-list"></i> Categoria</th>
                              <th><i class="fas fa-play"></i> Vídeo</th>
                              <th><i class="fas fa-calendar"></i> Data de Cadastro</th>
                              <th><i class="fas fa-calendar"></i> Data de Liberação</th>
                              <th style="width: 75px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                          </tr>
                      </thead>
                      
                  </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->

                  <!-- Small modal Cadastro-->
                  <div id="modal_cadastro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus-square"></i> Cadastrar Documento</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>
                          <!--Inserir formulario aqui-->
                          <form id="form-documento" onsubmit="return ValidateDoc(this);" class="form-horizontal form-label-left">
                          <div id="error_cadastro">
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="nome-doc" name="nome" require class="form-control col-md-7 col-xs-12 nome">
                              <span class="fas fa-file-alt form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="select_categoria" name="id_categoria" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Vídeo: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="select_video" name="id_video" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Data de Liberação: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="data-liberacao" name="data_liberacao" require class="form-control col-md-7 col-xs-12 data hora">
                              <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Documento: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 text-center">
                              <div class="flex-center flex-dir-column">
                                <div class="">
                                  <img id="icon" class ="icon icon-preview" src="" width="50">
                                </div>
                                <label for="_documento" class="btn btn-primary btn-xs mt-7">Selecionar arquivo</label>
                                <input type="file" id="_documento" name="_documento" require class="col-md-7 col-xs-12 hidden">
                                <small class="mt-7 file_name"></small>
                              </div>
                            </div>
                          </div>
                            <div class="modal-footer">
                            <input type="hidden" name="action" value="novo">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="cadastrar-cat" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
            <!-- /modals -->
            <!-- /modal de leitura -->
                  
            <!-- /modals leitura -->
            <!-- modals update -->
            <div id="modal_update" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-edit"></i> Atualizar Documento</h4>
                  </div>
                  <div class="modal-body">
                    <h4>Preencha os campos abaixo</h4>
                    <!--Inserir formulario aqui-->
                    <form id="form-atualizar-documento" onsubmit="return ValidateDoc(this);" class="form-horizontal form-label-left">
                      <div id="uerror_cadastro"></div>
                      <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="unome-doc" name="nome" require class="form-control col-md-7 col-xs-12 nome">
                              <span class="fas fa-file-alt form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Categoria: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="uselect_categoria" name="id_categoria" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Vídeo: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                            <select id="uselect_video" name="id_video" class="form-control">
                          </select>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Data de Liberação: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" id="udata-liberacao" name="data_liberacao" require class="form-control col-md-7 col-xs-12 data hora">
                              <span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Documento: </label>
                            <div class="col-md-9 col-sm-9 col-xs-12 text-center">
                              <div class="flex-center flex-dir-column">
                                <div class="">
                                  <img id="uicon" class ="icon icon-preview" src="" width="50">
                                </div>
                                <label for="_udocumento" class="btn btn-primary btn-xs mt-7">Selecionar arquivo</label>
                                <input type="file" id="_udocumento" name="_documento" require class="hidden">
                                <input type="hidden" id="usrc_doc" name="_documento-atual">
                                <small class="mt-7 file_name" id="file_atual"></small>
                              </div>
                            </div>
                          </div>
                      <div class="modal-footer">
                          <input type="hidden" name="action" value="atualizar">
                          <input type="hidden" id="hidden_doc_id" name="id_documento">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/crud_documento.js"></script>


  </body>
</html>