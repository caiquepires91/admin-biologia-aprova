nome_simulado = ""
$(document).ready(function(){
    nome_simulado = $('#id').data('nome')
    initInfo()
})

function round5(x)
{
    return Math.ceil(x/5)*5;
}

function initInfo(){
    $.ajax({
        url: './ajax/ajax_simulado.php',
        data: {action: 'ler_info_simulado', id: id_simulado},
        dataType: 'json'	
    }).done(res=>{
         //iniciando tiles
         $('.tile_count .tile_stats_count .rlz').html(res.realizados)
         $('.tile_count .tile_stats_count .nq').html(res.total_questoes_simulado)
         $('.tile_count .tile_stats_count .md').html(`${res.media}/${res.total_questoes_simulado}`)
 
         //iniciando graficos e atividades recentes se algum simulado já foi realizado
         if(res.realizados != 0) {initAllCharts(res); initAtividadesRecentes(res);}
         //Se nenhum simulado foi realizado
         else{$('.x_content .charts').hide();$('.x_content .no-charts').show()}
    })
}

function initCharts(config, ctx){
    var chart = new Chart(ctx, config);
    //ctx.next().html(chart.generateLegend())
}

function initAllCharts(res){
        var chartNotasctx = document.getElementById('chartNotas').getContext('2d');
        var chartQEctx = document.getElementById('chartQuestoesErradas').getContext('2d');
        var chartQCctx = document.getElementById('chartQuestoesCertas').getContext('2d');

        var cores = [];
        var getCor = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };

        //Iniciando grafico de notas
        var pontos = res.dados;
        var nq = res.n_questoes_respondidas;
        var notas = {};
        pontos.forEach(p => {
            nota = round5((p/nq)*100); //PENSAR EM UM METODO PARA DIVIDIR EM INTERVALOS, parcialmente resolvido
            if(notas[nota])notas[nota]++;
                        else notas[nota]=1; 
            })

        labels1 = Object.keys(notas).map( label => { return `${label}% das Questões`});
        dataset1 = Object.values(notas);

        for(d in dataset1) cores.push(getCor());

        var config = {
            type: 'doughnut',
            data: {
                labels: labels1,
                datasets: [{label: 'Porcentagem de Notas', data: dataset1, backgroundColor: cores}]
                },
            options: {
                title: {
                    display: true,
                    text: 'Acertos(%) x Alunos'
                },
                elements: {
                    center: {
                        text: ' % ',
                color: '#CECECE', // Default is #000000
                fontStyle: 'Arial', // Default is Arial
                sidePadding: 20 // Defualt is 20 (as a percentage)
                    }
                },
                responsive: false
            }
        }
        initCharts(config, chartNotasctx);

        //iniciando gráfico de questoes certas

        var questoes_pontos = res.questoes_total_corretas;
        var labels2 = []
        var dataset2 = []
        var labels3 = []
        var dataset3 = []
        
        for(i=0, aux = nq - 1; i < 5; i++, aux--){
            if(questoes_pontos[i] && (Number.parseInt(questoes_pontos[i].certas) != 0)){
                labels2.push(`Q ${questoes_pontos[i].codigo}`)
                dataset2.push(Math.round(questoes_pontos[i].certas/res.realizados*100, 2))
            }
            if(questoes_pontos[aux] && (res.realizados - questoes_pontos[aux].certas != 0)){
                labels3.push(`Q ${questoes_pontos[aux].codigo}`)
                dataset3.push(Math.round((res.realizados - questoes_pontos[aux].certas)/res.realizados*100, 2))
            }
        }

        cores = []
        for(d in dataset2) cores.push(getCor());

        config = {
            type: 'bar',
            data: {
                labels: labels2,
                datasets: [{label: 'Acertos(%)', data: dataset2, backgroundColor: cores}]
                },
            options: {
                title: {
                    display: true,
                    text: 'Top 5 questões mais acertadas'
                },
                responsive: false,
                legend: false,
            }
        }

        initCharts(config, chartQCctx);
        //iniciando gráfic de questoes erradas

        for(d in dataset3) cores.push(getCor());
        config = {
            type: 'bar',
            data: {labels: labels3, datasets: [{label: 'Erros(%)', data: dataset3, backgroundColor: cores}]},
            options: {
                title: {
                    display: true,
                    text: 'Top 5 questões mais erradas'
                },
                responsive: false,
                legend: false,
            }
        }
        initCharts(config, chartQEctx);
}

function initAtividadesRecentes(res){
    var recentesHTML = "";
        res.atividades_recentes.forEach(atividade => { //pensar na query para puxar o nome do simulado
            recentesHTML += 
            `<li>
            <div class="block">
              <div class="block_content">
                <h2 class="title">
                    <a>O aluno ${atividade.nome_aluno} deu início a este Simulado!</a>
                </h2>
                <div class="byline">
                  <span>${atividade.data_realizacao}</span> por <a>${atividade.nome_aluno.split(' ')[0]}</a>
                </div>
                <!--<p class="excerpt">Preencher com alguma mensagem</a>-->
                </p>
              </div>
            </div>
          </li>`
        })
    $('.dashboard-widget-content .timeline').html(recentesHTML);
}