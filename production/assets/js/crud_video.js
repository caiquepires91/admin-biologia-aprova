/**
 * JAVASCRIPT FOR VIDEO PAGE
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/

$(document).ready(function(){

    loadTable()
    loadCategorias($("#select_categoria"), null)
    resetarModal()
    initTinyMCE()

    $('.data').inputmask({ mask: '99/99/9999' });
    $('.hora').inputmask({ mask: ['99:99', '99:99:99'] , placeholder: "hh:mm:ss"});
    $('.data.hora').inputmask({ mask: ['99/99/9999 99:99', '99/99/9999 99:99:99'] , placeholder: "DD/MM/AAAA 00:00:00"});


    $("#foto_video").change(function() {
        readURL(this, $('#img_preview'));
    });

    $("#ufoto_video").change(function() {
        readURL(this, $('#uimg_preview'));
    });

    $('#form-video').submit(function(e){
        e.preventDefault()
        tinyMCE.triggerSave()
        var formData = new FormData(this);
        $.ajax({
            method: 'post',
            url: './ajax/ajax_video.php',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(result){
            success = responseTreatment(result, $("#error_cadastro"), [reloadVideos], false, $('#modal_cadastro'))
            if(success) resetarForm($("#form-video"), $("#error_cadastro"));
        })
    })

    $('#form-atualizar-video').submit(function(e){
        e.preventDefault()
        tinyMCE.triggerSave()
        var formData = new FormData(this);
        $.ajax({
            method: 'post',
            url: './ajax/ajax_video.php',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(result){
            success = responseTreatment(result, $("#uerror_cadastro"), [reloadVideos], false, $('#update_modal'))
            if(success) resetarForm($("#form-atualizar-video"), $("#uerror_cadastro"));
        })
    })
})

function initTinyMCE(){
    tinymce.init({
        selector: 'textarea',
        language: 'pt_BR',
        height: 300,
        menubar: false,
        toolbar: 'styleselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        style_formats:[
            {
              title: "Headers",
              items: [
                {title: "Header 1",format: "h1"},
                {title: "Header 2",format: "h2"},
                {title: "Header 3",format: "h3"},
                {title: "Header 4",format: "h4"},
                {title: "Header 5",format: "h5"},
                {title: "Header 6",format: "h6"}
              ]
           },
           {
                title: "Inline",items: [{title: "Bold",icon: "bold",format: "bold"}, {title: "Italic",icon: "italic",format: "italic"}, 
               {title: "Sublinhado",icon: "underline",format: "underline"}, {title: "Strikethrough",icon: "strikethrough",format: "strikethrough"}, {title: "Superscript",icon: "superscript",format: "superscript"}, {title: "Subscript",icon: "subscript",format: "subscript"}, {title: "Code",icon: "code",format: "code"}]}, 
               {title: "Blocos",items: [{title: "Paragraph",format: "p"}, {title: "Blockquote",format: "blockquote"}, {title: "Div",format: "div"}, {title: "Pre",format: "pre"}]}, 
               {title: "Alinhamento",items: [{title: "À Esquerda",icon: "alignleft",format: "alignleft"}, {title: "Centralizado",icon: "aligncenter",format: "aligncenter"}, {title: "À Direita",icon: "alignright",format: "alignright"}, {title: "Justificado",icon: "alignjustify",format: "alignjustify"}]}, 
               {
                   title: "Fonte",
                   items: [
                       {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                       {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                       {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                       {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                       {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                       {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                       {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                       {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                       {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                       {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                       {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                       {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                       {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
                   ]
               },
       {title: "Tamanho da fonte", items: [
                                   {title: '8pt', inline:'span', styles: { fontSize: '12px', 'font-size': '8px' } },
                                   {title: '10pt', inline:'span', styles: { fontSize: '12px', 'font-size': '10px' } },
                                   {title: '12pt', inline:'span', styles: { fontSize: '12px', 'font-size': '12px' } },
                                   {title: '14pt', inline:'span', styles: { fontSize: '12px', 'font-size': '14px' } },
                                   {title: '16pt', inline:'span', styles: { fontSize: '12px', 'font-size': '16px' } },
                                   {title: '18pt', inline:'span', styles: { fontSize: '18px', 'font-size': '18px' } },
                                   {title: '20pt', inline:'span', styles: { fontSize: '20px', 'font-size': '20px' } },
                                   {title: '24pt', inline:'span', styles: { fontSize: '24px', 'font-size': '24px' } }
   ]
   }],
        content_css: [
            '../vendors/tinymce/css/codepen.min.css',
            '../vendors/tinymce/css/font.css'
        ]
      });
}

function resetarModal(){
    $('#modal_cadastro').on('hide.bs.modal', function (e) {
        resetarForm($("#form-video"), $("error_cadastro"))
    })
    $('#update_modal').on('hide.bs.modal', function (e) {
        resetarForm($("#form-atualizar-video"), $("uerror_cadastro"))
    })
}

function resetarForm(form, error){
        form[0].reset()
        error.html("");
        $('#img_preview').attr('src', "assets/images/placeholder.png")
}

function loadCategorias(element, categoria){
    $.ajax({
        url: './ajax/ajax_categoria.php',
        data: {action: 'ler_todos'},
        dataType: 'json'
    }).done(function(result){
        let options = "<option value='0'>Selecione uma Categoria</option>"
        result.forEach(categoria => {
            options += `<option value='${categoria.id}'>${categoria.nome}</option>`
        })
        element.html(options)
        if(categoria)
            element.val(categoria)
    })

}
function readURL(input, preview) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        preview.attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

function loadTable(){
    const table = $('#videos').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[ 4, "desc" ]],
        "ajax": {
            "url": "./ajax/ajax_video.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "foto",
                "render": function(foto){
                    let path = "./uploads/videos/img/"
                    return  `<div class="flex-center"><img src="${path + foto}" class="img img-miniatura" alt="${'Capa do vídeo'}"></div>` 
                }},
            {"data": "nome"},
            {"data": "categoria_nome"},
            {"data": "duracao"},
            {"data": "data_cadastro"},  
            {"data": "data_liberacao"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <button class="btn btn-primary btn-xs" onClick="readVideo(${id})" data-toggle="modal" data-target="#read_modal" title="Ver Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="updateVideo(${id})" data-toggle="modal" data-target="#update_modal" title="Editar Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteVideo(${id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function reloadVideos(){
    $('#videos').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updateVideo(id){

    const hiddenid = $("#id_video_update")
    hiddenid.val(id);

    $.ajax({
        url: './ajax/ajax_video.php',
        data: {id_video: id, action: "ler"},
        dataType: 'json'
    }).done(function(response){
        if(response.success == 1){
            loadCategorias($("#uselect_categoria"), response.video.id_categoria)
            imgpath = './uploads/videos/img/' + response.video.foto
            $('#unome_video').val(response.video.nome)
            tinymce.get("udesc_video").setContent(response.video.descricao)
            $('#ulink_video').val(response.video.link)
            $('#uduracao_video').val(response.video.duracao)
            $('#udata_liberacao').val(response.video.data_liberacao)
            $('#uimg_preview').attr("src",imgpath)
            $('#usrc_img').val(response.video.foto)
            new FormData($('form-atualizar-video')[0])
        }else{
            new PNotify({
                title: 'Falha ao atualizar!',
                text: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
            //reloadCategorias()
        }
    })
}

function readVideo(id){
    let body = $('#read_modal-body')
    $.ajax({
        url: './ajax/ajax_video.php',
        data: {id_video: id, action: 'ler'},
        dataType: 'json'
    }).done(function(result){
        let embed_video = result.video.link
        if(embed_video.includes('watch')){
            linkArray = embed_video.split('watch?v=')
            embed_video = linkArray[0] + 'embed/' + linkArray[1]
        }

        html = `<div class="container">
                    <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table">
                            <tr>
                                <th> Título</th>
                                <td>${result.video.nome}</td>
                            </tr>
                            <tr>
                                <th> Link</th>
                                <td><a href="${result.video.link}">${result.video.nome}</a></td>
                            </tr>
                            <tr>
                                <th> Categoria</th>
                                <td>${result.video.categoria_nome}</td>
                            </tr>
                            <tr>
                                <th> Data de Cadastro</th>
                                <td>${result.video.data_cadastro}</td>
                            </tr>
                            <tr>
                                <th> Data de Liberação</th>
                                <td>${result.video.data_liberacao}</td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="${embed_video}" allowfullscreen></iframe>
                    </div>
                    <div>
                        <p>${result.video.descricao}</p>
                    </div>
       
                    </div>
                    </div>
                </div>`
        body.html(html)

    })
}

function deleteVideo(id){
    let confirmed = confirm("Deseja remover este Vídeo?")
    if(confirmed == true){

        $.ajax({
            method: 'post',
            url: './ajax/ajax_video.php',
            data: {id_video: id, action: "deletar"},
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, false, [reloadVideos], false, false)
        })
    }

}

