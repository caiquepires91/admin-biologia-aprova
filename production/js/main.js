$("document").ready( function () {
    moment.locale("pt-BR");
  
    $("#menu_toggle").on("click", function(e){

        /* var layout = 0;

        if($('body').hasClass("nav-md"))
            layout = 0;
        else
            layout = 1;

        $.ajax({
            method: "POST",
            url: "ajax/sistema.php",
            data: {acao: "alterar_layout", layout: layout},
            dataType: "JSON",
        }).done(function (resposta) {
            //nada
        }).fail(function (q) {
            //console.log(q);
        });
 */
        
    });
  
 /*    $("[data-tipo=cpf]").mask("000.000.000-00");
    $("[data-tipo=cnpj]").mask("00.000.000/0000-00");
    $("[data-tipo=cep]").mask("00000-000");
    $("input[data-tipo=decimal]").mask("#.##0,00", {reverse: true});
    $("input[data-tipo=inteiro]").mask("#");
    $("input[data-tipo=float]").mask("000000#.#0000000");
    $("input[data-tipo=hora]").mask("00:00");
    $("input[data-tipo=ano]").mask("0000");
    $("input[data-tipo=celular]").mask("(00) 00000-0000");
    $("input[data-tipo=agencia], input[data-tipo=conta]").mask("#0-0", {reverse: true}); */
    //$("input[data-tipo=telefone]").mask(mask_telefone_behavior, mask_telefone_options);
    $("textarea[data-autoheight=true]").on("input", redimensionarTextarea);
    $('textarea').css('resize','none');
    $("[data-tipo=dias]").css('paddind-right', '24px').css('text-align', 'right').find('.sufix').css('margin-left', '-24px')
  
    $("input[data-tipo=data]").each(function(index){
        $(this).mask("00/00/0000");
        $(this).attr("autocomplete", "off").daterangepicker({
            parentEl: this.dataset.parent ? this.dataset.parent : 'body',
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY'
            },
        });
    });
  
    $("input[data-tipo=datatempo]").each(function(index){
        $(this).mask("00/00/0000 00:00");
        $(this).attr("autocomplete", "off").daterangepicker({
            parentEl: this.dataset.parent ? this.dataset.parent : 'body',  
            timePicker: true,
            singleDatePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            showDropdowns: true,
            locale: {
                format: 'DD/MM/YYYY HH:mm'
            },
        });
    })
      
  
    $("input[data-tipo=datapicker]").each(function(index){
        var lado = this.dataset.lado;
        $(this).attr("autocomplete", "off").daterangepicker({
            start: moment().startOf('month'),
            end: moment().endOf('month'),
            opens: lado,
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            autoApply: true,
            lang: 'pt-BR',
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Este Ano': [moment().startOf('year'), moment().endOf('year')],
            }
        });
    })

    

    $('input[data-tipo=datapicker]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('input[data-tipo=datapicker]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

    $("input[data-tipo=hora]").change(function(){
        let time = $(this).val();
        let pattern = /^([0-9]{2}:[0-9]{2})*$/;
        if(!pattern.test(time)){
            $(this).css('border-color','red');
        }else{
            $(this).css('border-color','#ccc');
        }
    })
  
      $("modal").on("hidden", function() {
          $.fn.modal.Constructor.prototype.enforceFocus = enforceModalFocusFn;
      });
      $("modal").modal({ backdrop : false });
  
      if ($("select[data-tipo=estado]").length > 0) {
          _carregar_selects_estado();
          $("select[data-tipo=estado]").on("change", _select_estado_change)
      }

      if($("input[data-tipo=placeholder]").length > 0){
        $("input[data-tipo=placeholder]").on("change", function(){
            console.log("change");
            readURL(this, $(this.dataset.target));
        });
      }

      if ($("select[data-tipo=cursos]").length > 0) {
        _carregar_selects_cursos();
    }

  
      // Menu
      var $SIDEBAR_MENU = $('#sidebar-menu');
      $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent('li').addClass('current-page');
  });

function isJson(item) {
    if (typeof item === "object" && item !== null) {
        return true;
    }
    console.log(item);
    return false;
}

function readURL(input, preview) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        preview.attr('src', e.target.result);
        preview.css('background-image', `url(${e.target.result})`);
      }
      
      reader.readAsDataURL(input.files[0]);
    }else{
        preview.attr('src', 'assets/images/placeholder.png');
        preview.css('background-image', `url(${e.target.result})`);
    }
  }
  

function responseTreatment(response, errorElement, callbackArray, clear, modalDOM){
    /**
     * @response pacote de resposta do servidor
     * @errorElement objeto para alerta de erro
     * @callback função para ser chamada quando ocorrer um sucesso
     * @clear limpa inputs e editores
     * @modal modal para esconder após sucesso
     */

     //checa se a response é json
    if(!isJson(response)) return false;
     
    var success = false;

    if(response.success == 1){
      success = true

      //limpa msgs de erro
      if(errorElement) errorElement.html("");

      //esconde o modal ativo
      if(modalDOM) modalDOM.modal('hide');

      //dispara uma notificação de sucesso
      new PNotify({
          title: response.msg,
          type: 'success',
          styling: 'bootstrap3'
      });

      //executa os callbacks
      for (var i = 0, len = callbackArray.length; i < len; i++) {
        callbackArray[i]();
      }

    } else if(response.success == 2){
        new PNotify({
            title: response.msg,
            type: 'info',
            styling: 'bootstrap3'
        });

    } else if(response.success == 99){
        if(response.cod){
            if(response.cod == 1062){
                let chave = response.msg.match(/key '(.*)'/)[1]
                let html = `<div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>${chave} já registrado!</strong>
                        </div>`
            errorElement.append(html)
            return success;
            }
        }
      new PNotify({
        title: response.msg,
        type: 'warning',
        styling: 'bootstrap3'
        });
    }
    else{
        new PNotify({
          title: response.msg,
          type: 'error',
          styling: 'bootstrap3'
      });
    }

    return success;
    
  }

  function requestFullScreen(element) {
    // suporte de browsers
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

function formata_dinheiro(n) {
    return "R$ " + parseFloat(n).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
    }

var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png", ".pdf", ".doc"];    
function ValidateDoc(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                  new PNotify({
                    title: "Formato do documento não aceito",
                    text: 'formatos aceitos: jpg, jpeg, gif, png, pdf, doc',
                    type: 'error',
                    styling: 'bootstrap3'
                    });
                    return false;
                }
            }
        }
    }
  
    return true;
}

function ValidateDoc(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                  new PNotify({
                    title: "Formato do documento não aceito",
                    text: 'formatos aceitos: jpg, jpeg, gif, png, pdf, doc',
                    type: 'error',
                    styling: 'bootstrap3'
                    });
                    return false;
                }
            }
        }
    }
  
    return true;
}

function parseVideo(url) {
    /*
    * - Supported YouTube URL formats:
    *   - http://www.youtube.com/watch?v=My2FRPA3Gf8
    *   - http://youtu.be/My2FRPA3Gf8
    *   - https://youtube.googleapis.com/v/My2FRPA3Gf8
    * - Supported Vimeo URL formats:
    *   - http://vimeo.com/25451551
    *   - http://player.vimeo.com/video/25451551
    * - Also supports relative URLs:
    *   - //player.vimeo.com/video/25451551
    */
    url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

    if (RegExp.$3.indexOf('youtu') > -1) {
      var type = 'youtube';
    } else if (RegExp.$3.indexOf('vimeo') > -1) {
      var type = 'vimeo';
    }

    return {
      type: type,
      id: RegExp.$6
    };
}

function getEmbedURL(url) {
    var embeds = {
        youtube: 'https://www.youtube.com/embed/',
        vimeo: 'https://player.vimeo.com/video/'
    };
    var obj = parseVideo(url);
    return embeds[obj.type] + obj.id;
}

async function getThumbnail(url) {
        /*
    * - Supported YouTube URL formats:
    *   - http://www.youtube.com/watch?v=My2FRPA3Gf8
    *   - http://youtu.be/My2FRPA3Gf8
    *   - https://youtube.googleapis.com/v/My2FRPA3Gf8
    * - Supported Vimeo URL formats:
    *   - http://vimeo.com/25451551
    *   - http://player.vimeo.com/video/25451551
    * https://vimeo.com/api/oembed.json?url=https://vimeo.com/426620607/513ce0977a
    * - Also supports relative URLs:
    *   - //player.vimeo.com/video/25451551
    */
   url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*\/[A-Za-z0-9._%-]*|[A-Za-z0-9._%-]*)(\&\S+)?/);

   if (RegExp.$3.indexOf('youtu') > -1) {
    /* return new Promise((resolve, reject) => {
        resolve(`http://i.ytimg.com/vi/${RegExp.$6}/mqdefault.jpg`);
    }) */
    return `http://i.ytimg.com/vi/${RegExp.$6}/mqdefault.jpg`;

   } else if (RegExp.$3.indexOf('vimeo') > -1) {
    if(RegExp.$6.includes('/')){
        let response = await fetch(`https://vimeo.com/api/oembed.json?url=https://vimeo.com/${RegExp.$6}`);
        let video = await response.json();
        return video.thumbnail_url;
    }else{
        let response = await fetch(`http://vimeo.com/api/v2/video/${RegExp.$6}.json`);
        let video = await response.json();
        return video[0].thumbnail_medium;   
    }
   }else{
       return "assets/images/placeholder.png";
   }
}

function redimensionarTextarea () {
	this.style.height = "auto";
	var height = this.scrollHeight;
	if (height < 44)
		height = 44;
	this.style.height = (height+5) + "px";
}

function _carregar_selects_estado () {
	$.ajax({
		method: "POST",
		url: "ajax/ajax_cidades.php",
		data: { acao: "listar_estados" },
		dataType: "JSON",
	}).done(function (resposta) {
        console.log(resposta)
		var html = "<option value=''>Selecione</option>";
		for (var i = 0; i < resposta.estados.length; i++)
			html += "<option value='" + resposta.estados[i].sigla + "'>" + resposta.estados[i].sigla + "</option>";

		$("select[data-tipo=estado]").each(function () {
			this.innerHTML = html;
            this.value = this.dataset.valor;
            console.log(this.dataset.valor);
			this.dataset.valor = "";
			$(this).trigger("change");
		})
	}).fail(err => console.log(err));
}

function _carregar_selects_cursos() {
	$.ajax({
		method: "POST",
		url: "ajax/ajax_cursos.php",
		data: { acao: "ler_todos" },
		dataType: "JSON",
	}).done(function (cursos) {
		var html = "<option value=''>Selecione o curso</option>";
		for (var i = 0; i < cursos.length; i++)
			html += "<option value='" + cursos[i].id + "'>" + cursos[i].nome + "</option>";

		$("select[data-tipo=cursos]").each(function () {
			this.innerHTML = html;
            this.value = this.dataset.valor;
			this.dataset.valor = "";
			$(this).trigger("change");
		})
	}).fail(err => console.log(err));
}

function _select_estado_change () {
	var id_select_cidade = this.dataset.cidade;
	if (!id_select_cidade)
		return;

	if (this.value) {
		$.ajax({
			method: "POST",
			url: "ajax/ajax_cidades.php",
			data: { estado: this.value, acao: "listar_cidades" },
			dataType: "JSON",
		}).done(function (resposta) {
			var html = "<option value=''>Selecione</option>";
			var $select_cidade = $(id_select_cidade)[0];
			var is_selected;

			for (var i = 0; i < resposta.cidades.length; i++) {
				is_selected = $select_cidade.dataset.valor.match(new RegExp(resposta.cidades[i].nome, "gi")) ? "selected" : "";
				html += "<option value='" + resposta.cidades[i].nome + "' " + is_selected + ">" + resposta.cidades[i].nome + "</option>";
			}

			$select_cidade.innerHTML = html;
			// $select_cidade.value = $select_cidade.dataset.valor || "";
			// $select_cidade.dataset.valor = "";
			$($select_cidade).trigger("change");
		}).fail(function (x) {
			alerta(x.responseText);
		});
	}
	else {
		var $select_cidade = $(id_select_cidade)[0];
		$select_cidade.innerHTML = "<option value=''>Selecione um estado</option>";
	}
}

function submeter_formulario_files (event, newWindow = null) {
	
	event.preventDefault();

	var success_callback = event.data.success_callback || null;
	var error_callback = event.data.error_callback || null;

	// Evita que formulário seja submetido múltiplas vezes
	if (this.disabled)
		return false;

	this.disabled = true;
	//loadingOn();

	var data = new FormData(this);
	if (!data) {
		alerta("Erro", "Não foi possível atender a sua solicitação", "error");
		return false;
	}

    // Efetua requisição ajax
    console.log(this.action);
	var form = this;
	$.ajax({
		method: "POST",
		url: this.action,
		data: data,
		dataType: "json",
		cache: false,
        crossDomain: true,
		contentType: false,
		processData: false,
		beforeSend: () => loadingOn(),
		xhr: function() {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);
					$(".loading-percent").show().html(percentComplete + "%");
				}
			}, false);
		return xhr;
		},
	}).done(function (retorno) {
		if (retorno.status == 1) {
			if (typeof(success_callback) === "function")
				success_callback(retorno, newWindow);
		}
		else {
			if (typeof(error_callback) === "function")
				error_callback(retorno.msg, retorno.status);
			else
				alerta("Erro", retorno.msg, "error");
		}
	}).fail(function (a, b, c, d) {
		if (a.status > 400 && a.status < 500 ){
			if(a.status == 403)
				alerta("Erro!", "Sessão expirada", "error");
			else
				alerta("Erro!", a.statusText, "error");
		}
		else
			alerta("Erro!", a.responseText, "error");
	}).always( function () {
		form.disabled = false;
		loadingOff();
	});

	return false;
}

function submeter_formulario_files_dinamico (form, success_callback = null, error_callback = null, newWindow = null) {
	//loadingOn();

	var data = new FormData(form);
	if (!data) {
		alerta("Erro", "Não foi possível atender a sua solicitação", "error");
		return false;
	}

	$.ajax({
		method: "POST",
		url: form.action,
		data: data,
		dataType: "json",
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: () => loadingOn(),
		xhr: function() {
			var xhr = new window.XMLHttpRequest();
			xhr.upload.addEventListener("progress", function(evt) {
				if (evt.lengthComputable) {
					var percentComplete = ((evt.loaded / evt.total) * 100).toFixed(0);
					$(".loading-percent").show().html(percentComplete + "%");
				}
			}, false);
		return xhr;
		},
	}).done(function (retorno) {
		if (retorno.status == 1) {
			if (typeof(success_callback) === "function")
				success_callback(retorno, newWindow);
		}
		else {
			if (typeof(error_callback) === "function")
				error_callback(retorno.mensagem_erro, retorno.status);
			else
				alerta(retorno.mensagem_erro);
		}
	}).fail(function (a, b, c) {
		alerta(a.responseText);
	}).always( function () {
		loadingOff();
	});
}

function submeter_formulario_dinamico (form, success_callback = null, error_callback = null, newWindow = null) {
	loadingOn();

	$.ajax({
		method: "POST",
		url: form.action,
		data: $(form).serialize(),
		dataType: "JSON"
	}).done(function (retorno) {
		if (retorno.status == 1) {
			if (typeof(success_callback) === "function")
				success_callback(retorno, newWindow);
		}
		else {
			if (typeof(error_callback) === "function")
				error_callback(retorno.mensagem_erro, retorno.status);
			else
				alerta(retorno.mensagem_erro);
		}
	}).fail(function (a, b, c) {
		alerta(a.responseText);
	}).always( function () {
		loadingOff();
	});
}

function initTinyMCE(size = null){
    tinymce.init({
      selector: 'textarea.tinyEditor',
      paste_data_images: true,
      entity_encoding : "raw",
      language: 'pt_BR',  
      height: size ? size : 200,
      plugins: [
        "advlist autolink lists link image code imagetools charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern",
      ],
      menubar: false,
    toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullscreen | image | link",
    image_advtab: true,
    content_css: [
      '../../vendors/tinymce/css/codepen.min.css',
      '../../vendors/tinymce/css/font.css'
    ],
    images_upload_url: './ajax/tinyMCEUpload.php',
    convert_urls: false,
    images_upload_base_path: './uploads/simulado/imgs',
  
      style_formats:[
        {
          title: "Headers",
          items: [
            {title: "Header 1",format: "h1"},
            {title: "Header 2",format: "h2"},
            {title: "Header 3",format: "h3"},
            {title: "Header 4",format: "h4"},
            {title: "Header 5",format: "h5"},
            {title: "Header 6",format: "h6"}
          ]
        },
        {
            title: "Inline",items: [{title: "Bold",icon: "bold",format: "bold"}, {title: "Italic",icon: "italic",format: "italic"}, 
            {title: "Sublinhado",icon: "underline",format: "underline"}, {title: "Strikethrough",icon: "strikethrough",format: "strikethrough"}, {title: "Superscript",icon: "superscript",format: "superscript"}, {title: "Subscript",icon: "subscript",format: "subscript"}, {title: "Code",icon: "code",format: "code"}]}, 
            {title: "Blocos",items: [{title: "Paragraph",format: "p"}, {title: "Blockquote",format: "blockquote"}, {title: "Div",format: "div"}, {title: "Pre",format: "pre"}]}, 
            {title: "Alinhamento",items: [{title: "À Esquerda",icon: "alignleft",format: "alignleft"}, {title: "Centralizado",icon: "aligncenter",format: "aligncenter"}, {title: "À Direita",icon: "alignright",format: "alignright"}, {title: "Justificado",icon: "alignjustify",format: "alignjustify"}]}, 
            {
                title: "Fonte",
                items: [
                    {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                    {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                    {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                    {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                    {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                    {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                    {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                    {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                    {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                    {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                    {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                    {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                    {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
                ]
            },
    {title: "Tamanho da fonte", items: [
                                {title: '8pt', inline:'span', styles: { fontSize: '12px', 'font-size': '8px' } },
                                {title: '10pt', inline:'span', styles: { fontSize: '12px', 'font-size': '10px' } },
                                {title: '12pt', inline:'span', styles: { fontSize: '12px', 'font-size': '12px' } },
                                {title: '14pt', inline:'span', styles: { fontSize: '12px', 'font-size': '14px' } },
                                {title: '16pt', inline:'span', styles: { fontSize: '12px', 'font-size': '16px' } },
                                {title: '18pt', inline:'span', styles: { fontSize: '18px', 'font-size': '18px' } },
                                {title: '20pt', inline:'span', styles: { fontSize: '20px', 'font-size': '20px' } },
                                {title: '24pt', inline:'span', styles: { fontSize: '24px', 'font-size': '24px' } }
  ]
  }],
  
      images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', './ajax/tinyMCEUpload.php');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
              console.log(xhr.responseText);
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
            console.log(json);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },
  
    });
  }

  
        // Alerta (PNotify)
        function alerta(titulo, texto, tipo, delay = 5000) {
            new PNotify({
                title: titulo,
                text: texto,
                type: tipo,
                hide: true,
                delay: delay,
                styling: 'bootstrap3'
            });
        }

        // Loading
        var cont_loading_overlay = 0;

        function loadingOn() {
            cont_loading_overlay++;
            if (cont_loading_overlay > 0)
                $("#loading_overlay").css("display", "block");
        }
        function loadingOff() {
            cont_loading_overlay--;
            if (cont_loading_overlay < 1) {
            $("#loading_overlay").css("display", "none");
            $(".loading-percent").hide().html("");
                cont_loading_overlay = 0;
            }
        }