<?php
/**
 * Lib @ Uploads
 * Métodos de envio de arquivos e imagens ao servidor.
 */

namespace Uploads;

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

function validar ($arquivos) {
    $arr = [];

	$total = count($arquivos["name"]);
	for ($i = 0; $i < $total; $i++) {
		if (strlen($arquivos["name"][$i])) {
			$arr[] = [
				"name" => validar_nome_limpo($arquivos["name"][$i]),
				"type" => $arquivos["type"][$i],
				"tmp_name" => $arquivos["tmp_name"][$i],
				"error" => $arquivos["error"][$i],
				"size" => $arquivos["size"][$i]
			];
		}
	}

	return $arr;
}

function enviar ($arquivo, $tipos = [], $diretorio = null) {
    $dir = $diretorio ? $diretorio : "upload/";

	$nome = $arquivo["name"];
	$tipo = strtolower(pathinfo($arquivo["name"], PATHINFO_EXTENSION));
	$nome = "RE9AGENCIA". date("YmdHis") . "." . $tipo;
	$url = $dir . $nome;

	if (!empty($tipos) && !in_array($tipo, $tipos))
		trigger_error("O tipo de arquivo .$tipo não é suportado.");

	if (move_uploaded_file($arquivo["tmp_name"], "../" . $url))
		return $url;
	else
		trigger_error("Ocorreu um erro ao fazer o upload do arquivo.", E_USER_ERROR);
}

function salvar_multiplos($diretorio, $files, $tipos = ["jpg", "jpeg", "png", "pdf", "doc", "docx", "mp4", "avi"]){
	$arquivos = array();

	// Upload dos arquivos
	$k = 0;
	foreach($files["name"] as $i => $file){ //LOOP PELA QUANTIDADE DE ITENS QUE FORAM INSERIDOS ARQUIVOS
		foreach($file as $j => $nfile){	
            if (!empty($nfile)){
                $name = $nfile;
                $tmp_name = $files["tmp_name"][$i][$j];
				$url = salvar_arquivo($diretorio, $name, $tmp_name, $tipos, [$i, $j]);
				//sleep(1); CASO HAJA CONFLITO COM NOMES IGUAIS
				/*$arquivos[$k][] = $url;*/
				$arquivos[$k][] = array("url" => $url, "nome" => pathinfo($name, PATHINFO_FILENAME), "ext" => strtolower(pathinfo($name, PATHINFO_EXTENSION)));
            }else{
                $arquivos[$k][] = NULL;
            }
		}
		$k++;
	}
	return $arquivos;

}

// create a version capeable of sending img to biologia aprova.
function salvar_arquivo ($diretorio, $nome , $name, $tmp_name, $tipos = []) {
    $dir = $diretorio;

	$tipo = strtolower(pathinfo($name, PATHINFO_EXTENSION));
		
	$url = $dir . $nome . "." . $tipo;

	if (!empty($tipos) && !in_array($tipo, $tipos))
		trigger_error("O tipo de arquivo .$tipo não é suportado.");

	if (move_uploaded_file($tmp_name, "../../../../../biologiaaprova.com.br/lumen/public/img/blog/" . $url))
		return $url;
	else
		trigger_error("Ocorreu um erro ao fazer o upload do arquivo.", E_USER_ERROR);
}
