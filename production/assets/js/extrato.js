$(document).ready(function(){
    loadTable();

    $('.date.initial').datepicker({format:'dd/mm/yyyy'})
    $('.date.end').datepicker({format:'dd/mm/yyyy'})

    $(".valor").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: false,
    unmaskAsNumber: true,
    });

    //inicializar saldos
    var balancos = $.ajax({
        url: './ajax/ajax_extrato.php?action=calcula_balancos',
        dataType: 'json'
    }).done(res=>{
        $("#entrada-geral").html(`${formata_dinheiro(res.entrada_geral)}`);
        $("#saida-geral").html(`${formata_dinheiro(res.saida_geral)}`);
        $("#total-geral").html(`${formata_dinheiro(res.total_geral)}`);
    })


    //saldos.done(res=>{ $('.disponivel').html(`<strong>Total Disponível: R$ ${res.valor_disponivel.toFixed(2)}</strong>`); $('.recebido').html(`<strong>Total Recebido: R$ ${res.valor_recebido.toFixed(2)}</strong>`); });

    $('#form-filter').submit(function(e){
        e.preventDefault();

        data_inicial = $('#form-filter input[name=data_inicial]').val()
        data_final = $('#form-filter input[name=data_final]').val()
        //id_gateway = $('#form-filter input[name=id-gateway]').val()
        
        if(data_inicial == false && data_final == false){
            //$("#extrato").DataTable().ajax.url(`./ajax/ajax_extrato.php?action=carregar_extratos`).load()
            $('#filter-info').html(`Exibindo transações dos últimos 30 dias`);
            dados = $(this).serialize() + '&action=carregar_extratos_filtro';
            $("#extrato").DataTable().ajax.url(`./ajax/ajax_extrato.php?${dados}`).load()
        }

        else{
            $('#filter-info').html(`Exibindo resultados entre ${data_inicial} e ${data_final}`);
            dados = $(this).serialize() + '&action=carregar_extratos_filtro';
            $("#extrato").DataTable().ajax.url(`./ajax/ajax_extrato.php?${dados}`).load()
        }
    })

    $('#form-saque').submit(e => {
        e.preventDefault();
        $.ajax({
            method: 'post',
            url: './ajax/ajax_extrato.php',
            data: 
                {action: 'saque', 
                valor: $('#form-saque input[name=valor-saque]').val(),
                descricao: $("textarea[name=descricao]").val(), 
                id_gateway: $('#form-saque select[name=id_gateway]').val(),
                id_usuario: parseInt(id_usuario)},
            dataType: 'json'
        }).done(res=>{
            if(responseTreatment(res, false, [reloadExtrato], false, $('#modal_saque'))){
                $.ajax({
                    url: './ajax/ajax_extrato.php?action=calcula_balancos',
                    dataType: 'text'
                }).done(res=>{
                    
                    $("#entrada-geral").html(`${formata_dinheiro(res.entrada_geral)}`);
                    $("#saida-geral").html(`${formata_dinheiro(res.saida_geral)}`);
                    $("#total-geral").html(`${formata_dinheiro(res.total_geral)}`);
                })
            }
        })
    })
})

function reloadExtrato(){
    $('#extrato').DataTable().ajax.reload();
}

function mostrar_saldos(){
    $.ajax({
        url: 'ajax/ajax_extrato.php?action=saldo_disponivel',
        dataType: 'json'
    }).done(res=>{
        $('#table_saldos tbody').html("");
        gateways = res.saldos;
        gateways.forEach( gateway => {
            $('#table_saldos tbody').append(`<tr><td>${gateway.gateway}</td><td>${formata_dinheiro(gateway.saldo)}</td></tr>`);
        })
    });
}

function loadTable(){
    $.fn.dataTable.moment('DD/MM/YYYY');
    var table = $('#extrato').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[ 1, "desc" ]],
        "ajax": {
            "method": 'get',
            "url": "./ajax/ajax_extrato.php?action=carregar_extratos",
            "dataSrc": ""
        },
        "columnDefs": [
            { "targets": 0, "className":"nosort text-center"},
            {"targets":0, "width": "20px"}
          ],
        "columns": [
            { "data": "id"},
            { "data": "data_recebimento"},
            {"data": "gateway"},
            { "data": "tipo", render: function(data, type, row){
                if(row.n_parcelas){
                    return `<strong>Venda (parcela <i>${row.periodo}/${row.n_parcelas}</i>)</strong>`;
                }else
                    return row.tipo == 'saque' ? "<strong>Saque</strong>": row.tipo == 'plano' ? "<strong>Venda</strong>" : "<strong>Despesa</strong>";
                
            }},
            { "data": "valor_liquido", render: function(data, type, row){
                return row.tipo == 'plano'? `<span style="color: green;">${formata_dinheiro(row.valor_liquido)}</span>` : `<span style="color: red;">${formata_dinheiro(row.valor_liquido)}</span>`;
            }}],
        "footerCallback": function(){
            var api = this.api();

            var intVal = function ( i ) {
                if(typeof i === 'string'){
                    return i.replace(/[\$,]/g, '')*1;
                }else if(typeof i === 'number'){
                    return i;
                }else{
                    return 0;
                }
            }
                    
            totalRecebido = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        v1 = intVal(a);
                        v1 = v1 < 0? 0 : v1;
                        v2 = intVal(b);
                        v2 = v2 < 0? 0 : v2;
                        return v1 + v2;
                    }, 0 );
            totalDisponivel = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    v1 = intVal(a);
                    v2 = intVal(b);
                    return v1 + v2;
                }, 0 );

            $( api.column( 4 ).footer() ).html(
                formata_dinheiro(totalDisponivel)
            );
            //$('.disponivel').html(`<strong>Total Disponível: ${totalDisponivel.toFixed(2)}</strong>`);
            //$('.recebido').html(`<strong>Total Recebido: ${totalRecebido.toFixed(2)}</strong>`);
        }
    }) 

    $('#extrato tbody').on('click', 'tr', function (e) {
        var data = table.row( this ).data();
            $.ajax({
                url: `./ajax/ajax_extrato.php?action=ver_transacao&id=${data.id}&tipo=${data.tipo}`,
                dataType:'json'
            }).done(res=>{
                if(res.status == 'success'){
                    $('#modal_ler_corpo').html(res.body);
                    $('#modal_ler_transacao').modal('show')
                }
                //reportar error
                
            })
        //ABRIR MODAL COM INFORMAÇÕES
    } );
}