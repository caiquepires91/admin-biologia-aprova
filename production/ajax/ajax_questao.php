<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;
    $questao = getQuestao();
    extract($questao);

    //salvando assunto, caso nao exista
    if(!$assunto_id){
        if($result = $mysqli->query("SELECT * FROM assunto WHERE nome LIKE _utf8 '$assunto' COLLATE utf8_unicode_ci;")){
            if($result->num_rows > 0){
                $assunto_id = $result->fetch_assoc()['id'];
            }else{
                if($mysqli->query("INSERT INTO assunto(id, nome) VALUES (default, '$assunto')")){
                    $assunto_id = $mysqli->insert_id;
                }
            }
        }
    }

    try{
        $mysqli->autocommit(FALSE);
        $acertos = null;
        $stmt_result = $mysqli->prepare("INSERT INTO `questao`(`id`, `id_simulado`, `id_assunto` ,`codigo`, `enunciado`, `acertos`, `alternativa_correta`, `data_criacao`) 
        VALUES (default,?,?,?,?,?,?,?)");
        $stmt_result->bind_param("iiissss", $id_simulado, $assunto_id, $codigo,  $enunciado, $acertos, $correta, $data_criacao);
        $stmt_result->execute();

        $current_id = $mysqli->insert_id;
    
        for($i = 0; $i < $count; $i++){
            $alternatives = "INSERT INTO `alternativa`(`id`, `id_questao`, `descricao`) VALUES (default,?,?)"; 
            $stmt_result = $mysqli->prepare($alternatives);   
            $stmt_result->bind_param("is",$current_id, $alternativas[$i]);
            $stmt_result->execute();
        }
        $mysqli->autocommit(TRUE);
        echo json_encode(array("success" => 1, "msg" => "Questao registrada com sucesso!"));
    }catch(Exception $e){
        $mysqli->rollback();
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function atualizar(){
    global $mysqli;
    $questao = getQuestao();
    extract($questao);

    try{
        $mysqli->autocommit(FALSE);
        //salvando assunto, caso nao exista
        if(!$assunto_id){
            if($result = $mysqli->query("SELECT * FROM assunto WHERE nome LIKE _utf8 '$assunto' COLLATE utf8_unicode_ci;")){
                if($result->num_rows > 0){
                    $assunto_id = $result->fetch_assoc()['id'];
                }else{
                    if($mysqli->query("INSERT INTO assunto(id, nome) VALUES (default, '$assunto')")){
                        $assunto_id = $mysqli->insert_id;
                    }
                }
            }
        }

        $stmt = $mysqli->prepare("UPDATE questao SET enunciado = ?, id_assunto = ? , alternativa_correta = ? WHERE id = ?");
        $stmt->bind_param("sisi", $enunciado, $assunto_id, $correta, $id);
        $stmt->execute();
        
        for($i = 0; $i < count($alternativas); $i++){
            $alternativa = $alternativas[$i];
            switch($alternativa['action']){
                case "insert":
                    $statment = $mysqli->prepare('INSERT INTO alternativa(id, id_questao, descricao) values(default,?,?);');
                    $statment->bind_param("is", $questao['id'], $alternativa['alt']);
                    $statment->execute();
                    break;

                case "update":
                    $statment = $mysqli->prepare('UPDATE alternativa SET descricao = ? WHERE id = ?');
                    $statment->bind_param("si", $alternativa['alt'], $alternativa['id_alt']);
                    $statment->execute();
                    break;

                case "delete":
                    $statment = $mysqli->prepare('DELETE FROM alternativa WHERE id = ?');
                    $statment->bind_param("i", $alternativa['id_alt']);
                    $statment->execute();
                    break;
            }
        }

        $retorno['count'] = count($alternativas);
        $retorno['success'] = 1;
        $retorno['msg'] = "Questão atualizada!";
        $mysqli->autocommit(TRUE);
        echo json_encode($retorno);
    }catch(Exception $e){
        $mysqli->rollback();
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id'];
    $delete = "UPDATE questao SET deletado = 1 WHERE id = $id";
    if($mysqli->query($delete)){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Questao removida do banco de dados!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Questao não encontrada, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 0, "msg" => "Erro ao remover questao!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id'];
	try{
        $result = $mysqli->query("SELECT q.id, q.enunciado, q.codigo, q.id_simulado, q.alternativa_correta, DATE_FORMAT(q.data_criacao, '%d/%m/%Y') as data_criacao, a.nome as assunto from questao as q INNER JOIN assunto as a ON q.id_assunto = a.id WHERE q.id = $id;"); //SELECT q.id, q.enunciado, a.nome from questao as q INNER JOIN assunto as a ON q.id_assunto = a.id WHERE q.id = 6; 
        $questao = $result->fetch_assoc();
        
        $result2 = $mysqli->query("SELECT * FROM alternativa WHERE id_questao = $id");
        $alternativas = $result2->fetch_all(MYSQLI_ASSOC);

        $retorno['questao'] = $questao;
        $retorno['alternativas'] = $alternativas;
        $retorno['success'] = 1;
    
    echo json_encode($retorno);

    }catch(Exception $e){
        echo json_encode(array("success" => 99, "msg" => $e->getMessage()));
    }
}

//-----------FUNCOES AUXILIARES --------------//

function getQuestao(){
    global $mysqli;
    $questao['enunciado'] = $_POST['enunciado'];
    $questao['assunto'] = filter_input(INPUT_POST, 'assunto', FILTER_SANITIZE_STRING);
    $questao['assunto_id'] = filter_input(INPUT_POST, 'assunto-id', FILTER_SANITIZE_NUMBER_INT);
    $questao['alternativas'] = array();
    $questao['correta'] = $_POST['correct'];
    $questao['id_simulado'] = $_POST['id_simulado'];
    $questao['data_criacao'] = date('Y-m-d H:i:s');
    $qt_alternativas = 0;
    $id_simulado = $_POST['id_simulado'];

    if($questao['enunciado'] == false || $questao['enunciado'] == ""){
        echo json_encode(array("success" => 0, "msg" => "O enunciado da questão é obrigatório!"));
        die();
    }

    if($questao['assunto'] == false || $questao['assunto'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um assunto!"));
        die();
    }
    
    if($_POST['action'] == 'novo'){
        $alt = "alt";
        for($i = 0; $i < 5; $i++){
            $n = $i + 1;
            if($_POST[$alt.$n] != false){
                $questao['alternativas'][$i] = $_POST[$alt.$n];
                $qt_alternativas++;
            }
        }
    }
    else if($_POST['action'] == 'atualizar'){ //Se for update, as alternativas devem ser tratadas 
        $questao['id'] = $_POST['id_questao'];
        $alt = "alt";
        for($i = 0; $i < count($_POST['alternativas']); $i++){
            $alternativa = $_POST['alternativas'][$i]; //salvo o array com a alternativa {alt: alternativa, id_alt: id da alternativa}

            if($alternativa['alt'] != false){
                $qt_alternativas++;
            }

            if(!($alternativa['alt'] == false && $alternativa['id_alt'] == false)){ //Só salva se pelo menos um dos atributosforem verdadeiros

                if($alternativa['alt'] == false && $alternativa['id_alt'] != false) //Se houver id, mais a alternativa é falsa, entao delete do banco a alternativa
                    $alternativa['action'] = 'delete';

                else if($alternativa['alt'] != false && $alternativa['id_alt'] != false) //Se houver id, e alternativa é verdadeira, atualize a alternativa
                    $alternativa['action'] = 'update';
                else
                    $alternativa['action'] = 'insert'; //se não houver id, mas tiver alernativa, crie uma nova alternativa

                $questao['alternativas'][$i] = $alternativa;
            }
        }
    }

    if($qt_alternativas < 2){
        echo json_encode(array("success" => 0, "msg" => "Informe pelo menos duas alternativas!"));
        die();
    }

    $questao['count'] = $qt_alternativas;
    //pegando quantidade de questoes
    $result = $mysqli->query("SELECT count(*) as total FROM questao where id_simulado = $id_simulado and deletado = 0");
    $questao['codigo'] = $result->fetch_assoc()['total'];

    return $questao;
}
?>