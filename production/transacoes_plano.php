<?php require __DIR__.'/php/autentica.php'; ?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Transações</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-datepicker -->
    <link href="./js/datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/modules/relatorio.css">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <?php include __DIR__.'/./componentes/menuFooter.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-file-invoice-dollar"></i> Venda de planos <small></small></h2>
                    <form id="form-filter" autocomplete="off" class="filters-content">
                    <div class="filter-data">
                        <label>Data inicial: </label>
                        <div class="input-group date initial">
                                 <input type="text" name="data_inicial" class="form-control">
                                <div class="input-group-addon">
                                    <span class="fas fa-calendar"></span>
                                </div>
                            </div>
                          <label>Data final: </label>
                          <div class="input-group date end">
                                <input type="text" name="data_final" class="form-control">
                              <div class="input-group-addon">
                                  <span class="fas fa-calendar"></span>
                              </div>
                          </div>
                          <button type="submit" class="btn btn-primary"><i class="fas fa-search-dollar"></i> Filtrar Data</button>
                      </div> 
                      </form>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <div class="totais no-btn">
                  <div class="pago"><strong>Total Pago: </strong></div>
                  <div class="liquido"><strong>Total Líquido: </strong></div>
                  </div>
                    <table id="transacoes" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <!-- por método de pagamento --><th>PAGT.</th>
                              <th><i class="fas fa-file-alt"></i> Nome</th>
                              <th><i class="fas fa-shopping-cart"></i> Plano</th>
                              <th><i class="fas fa-calendar"></i> Data</th>
                              <th><i class="fas fa-money-bill-wave"></i> Valor do Plano</th>
                              <th><i class="fas fa-money-bill-wave"></i> Valor Pago</th>
                              <th><i class="fas fa-money-bill-wave"></i> Valor Líquido</th>
                              <th><i class="fas fa-money-bill-wave"></i> Gateway</th>
                              <th><i class="fas fa-city"></i> Cidade</th>
                              <th style="width: 65px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                          </tr>
                      </thead>
                      <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="font-size: 12pt;"></th>
                        <th></th>
                        <th style="font-size: 12pt;"></th>
                        <th style="font-size: 12pt;"></th>
                        <th></th>
                        <th></th>
                        <th></th>
                      </tfoot>
                  </table>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->
                  <div id="modal-read-transaction" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Detalhes da Transação</h4>
                        </div>
                        <div id="read_modal_body" class="modal-body">

                        <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                          <input type="hidden" name="id_plano" id="id_plano">
                          <button type="button" id="update-gateway" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- bootstrap-datepicker -->    
    <script src="./js/moment/moment.min.js"></script>
    <script src="./js/datepicker/bootstrap-datepicker.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/relatorio.js"></script>


  </body>
</html>