<?php
  require __DIR__.'/php/autentica.php';

  //Validando id Aluno
  require_once __DIR__.'/php/functions.php';
  $usuario = validarAluno();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Administração - Alunos</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Switchery -->
    <link href="../vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- datatables -->
    <?php include './componentes/DataTableCSS.php' ?>
    <!-- NProgress -->
    <link href="../vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Perfil do Usuário <?php if($usuario['admin'] == 1):?>
                        <div class="admin-marker"><span><i class="fa fa-shield"></i> Administrador</span></div>
                        <?php endif;?></h2>

                        <div class="panel_toolbox">
                          <?php if ($usuario['admin'] == 1) { ?>
                          <a href="admins.php" class="btn btn-primary"><i class="fa fa-reply"></i> Visualizar todos usuários</a>
                          <?php } else { ?>
                          <a href="alunos.php" class="btn btn-primary"><i class="fa fa-reply"></i> Visualizar todos os alunos</a>
                          <?php } ?>
                        </div>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <label for="img-avatar" class="crop-avatar">
                          <img id="img-avatar" class="img-responsive avatar-view" src="./images/user.png" alt="Avatar">
                          <div class="edit-hover" data-toggle="modal" data-target=".profile-modal"></div>
                        </label>
                        <?php if($usuario['admin'] == 0):?>
                        <div class="">
                          <label>
                            <input type="checkbox" class="js-switch imgEnable" /> Desativar Imagem
                          </label>
                        </div>
                        <div class="" style="max-width: 200px;">
                          <label>
                            <input type="checkbox" class="js-switch imgPermission" /> Impedir que o usuário coloque imagem
                          </label>
                        </div>
                        <?php endif;?>
                      </div>
                      <h3 id="nome_perfil"></h3>

                      <ul class="list-unstyled user_data">
                        <li id="cpf_perfil"></li>

                        <li id="telefone_perfil"></li>

                        <li class="m-top-xs" id="email_perfil"></li>
                      </ul>

                      <!-- start skills -->
                      <!-- end of skills -->

                    </div>
                    <!--Dados pessoais e informações de planos-->
                    <div class="col-md-9 col-sm-9 col-xs-12">
                    <!-- start TABS -->
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_dados" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Dados de Cadastro <i class="fas fa-user"></i></a>
                          </li>
                          <?php if($usuario['admin'] == 0):?>
                          <li role="presentation" class=""><a href="#tab_planos" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Planos <i class="fas fa-shopping-cart"></i></a>
                          </li>
                          <?php endif;?>
                          <li role="presentation" class=""><a href="#tab_senha" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Alterar Senha <i class="fas fa-key"></i></a>
                          </li>
                        </ul>

                            <!-- end TABS -->

                        <div id="myTabContent" class="tab-content">
                        <!-- start dados cadastro -->
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_dados" aria-labelledby="home-tab">
                            <input type="hidden" id="id" value=<?php echo $_GET['id']?>>
                            <div id="update_error"></div>
                            <form id="form-atualizar" class="form-horizontal form-label-left">
                                <table class="table table-hover">
                                    <tr>
                                        <th>Nome Completo</th>
                                        <td>
                                            <div class="row">
                                                <input type="text" id="nome" name="nome" class="form-control col-md-7 col-xs-10 nome aluno-input">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>CPF</th>
                                        <td>
                                                <div class="row">
                                                        <input type="text" id="cpf" name="cpf" class="form-control col-md-7 col-xs-10 cpf aluno-input" data-inputmask="'mask' : '999.999.999-99'">
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>E-mail</th>
                                        <td>
                                                <div class="row">
                                                        <input type="email" id="email" name="email" class="form-control col-md-7 col-xs-10 email aluno-input">
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Estado</th>
                                        <td>
                                                <div class="row">
                                                        <select class="select2_group form-control select_estado col-md-5 col-xs-10 aluno-input" id="select_estado" name="estado">
                                                            </select>
                                                        <span id="lestado" class="col-md-5 col-xs-10"></span>
                                                          
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Cidade</th>
                                        <td>
                                                <div class="row">
                                                        <select class="select2_group form-control select_cidade col-md-5 col-xs-10 aluno-input" id="select_cidade" name="cidade">
                                                        </select>
                                                        <span id="lcidade" class="col-md-5 col-xs-10"></span>
                                                        
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>CEP</th>
                                        <td>
                                                <div class="row">
                                                        <input type="text" id="cep" name="cep" class="form-control col-md-7 col-xs-10 cep aluno-input" data-inputmask="'mask' : '99999-999'">
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Data de Nascimento</th>
                                        <td>
                                                <div class="row">
                                                        <input type="text" id="dataNascimento" name="dataNascimento" class="form-control col-md-7 col-xs-10 data aluno-input" data-inputmask="'mask' : '99/99/9999'">
                                                    </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Telefone</th>
                                        <td>
                                                <div class="row">
                                                        <input type="text" id="telefone" name="telefone" class="form-control col-md-7 col-xs-10 telefone aluno-input">
                                                    </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="modal-footer atualizar-btn collapse">
                                  <input type="hidden" id="id_usuario" name="id_usuario" value=<?php echo $_GET['id']; ?>>
                                        <button type="button" id="cancelar" class="btn btn-default">Cancelar</button>
                                        <button type="submit" id="atualizar" class="btn btn-primary"><i class="fa fa-save"></i> Salvar Alterações</button>
                                    </div>
                            </form>
                          </div>
                          <!-- end dados cadastro -->
                         <!-- start user plans -->
                          <?php if($usuario['admin'] == 0):?>
                          <div role="tabpanel" class="tab-pane fade" id="tab_planos" aria-labelledby="profile-tab">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    <div class="x_title">
                                      <div class="panel_toolbox">
                                        <button id="btn-add-plano" class="btn btn-primary" data-toggle="modal" data-target="#modal_plano">Adicionar Plano <i class="fa fa-plus-square"></i></button>
                                      </div>
                                      <div class="clearfix"></div>
                                      
                                    </div>
                                    <div class="x_content">
                                      <table id="aluno_planos" class="display table table-hover" style="width: 100%">
                                        <thead>
                                          <tr>
                                            <th><i class="fas fa-shopping-cart"></i> Plano</th>
                                            <th><i class="fas fa-money-bill-wave"></i> Valor</th>
                                            <th><i class="fas fa-calendar-day"></i> Início</th>
                                            <th><i class="fas fa-calendar-check"></i> Término</th>
                                            <th><i class="fas fa-info-circle"></i> Status</th>
                                            <th style="width: 45px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                                          </tr>
                                        </thead>
                                      </table>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <?php endif;?>
                          <!-- end user plans -->
                          <!-- start change password tab -->
                          <div role="tabpanel" class="tab-pane fade" id="tab_senha" aria-labelledby="profile-tab">
                              <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="x_panel">
                                    <div class="x_title">
                                    <h4>Preencha os campos abaixo </h4>
                                      <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                      <div class="xcontainer flex-center">
                                        
                                          <form id="form-pwd" class="form-horizontal form-label-left w-100">
                                          <div id="error_pwd"></div>
                                            <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nova Senha: </label>
                                              <div class="col-md-5 col-sm-5 col-xs-12">
                                                <input type="password" id="senhaNew" name="senha-new" required="required" class="form-control col-md-7 col-xs-12 senha">
                                                <span class="fas fa-key form-control-feedback right" aria-hidden="true"></span>
                                              </div>
                                            </div>

                                            <div class="form-group">
                                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Confirmar Senha: </label>
                                              <div class="col-md-5 col-sm-5 col-xs-12">
                                                <input type="password" id="senhaNewCfm" name="senha-new-cfm" required="required" class="form-control col-md-7 col-xs-12 senha">
                                                <span class="fas fa-key form-control-feedback right" aria-hidden="true"></span>
                                              </div>
                                            </div>

                                            <div class="modal-footer atualizar-pwd collapse">
                                              <input type="hidden" id="id_usuario" name="id_usuario" value=<?php echo $_GET['id']; ?>>
                                                <button type="button" id="cancelar-senha" class="btn btn-default">Cancelar</button>
                                                <button type="submit" id="atualizar-senha" class="btn btn-primary"><i class="fa fa-save"></i> Salvar Alterações</button>
                                            </div>

                                          </form>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <!-- end change passwd -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        <!-- modal plano-->
        <div id="modal_plano" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus-square"></i> Selecionar Plano</h4>
                        </div>
                        <div class="modal-body">
                          <!--Inserir formulario aqui-->
                          <div id="add_error"></div>
                          <form id="form-add-plano" class="form-horizontal form-label-left">
                          
                          <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                          <label>Selecione o Plano: </label>
                          <select id="select_plano" name="plano-id" class="form-control">
                          </select>
                            <div></div>
                          </div>

                          <div class="col-md-6 col-sm-6 col-xs-12 form-group valorp collapse">
                          <label>Editar Valor: </label>
                              <input type="text" class="form-control valor" id="valor_plano" name="plano-valor" placeholder="Valor do Plano">
                            <div></div>
                          </div>

                          <div class="col-md-6 col-sm-6 col-xs-12 form-group valorp collapse">
                          <label>Tipo de pagamento: </label>
                            <select id="select_pagt" name="plano-pagt" class="form-control">
                              <option value="0">À vista</option>
                              <option value="1">Cartão</option>
                            </select>
                            <div></div>
                          </div>

                          <div class="col-md-6 col-xs-12 float-right form-group parcelas collapse">
                          <label>Parcelas: </label>
                            <select id="select_parcelas" name="plano-parcelas" class="form-control">
                            </select>
                            <div></div>
                          </div>
                          
                          <div class="clearfix"></div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="add-plano" class="btn btn-primary"><i class="fa fa-save"></i> Adicionar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->

                  <div class="modal fade profile-modal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Editar Imagem</h4>
                        </div>
                        <div class="modal-body">
                          <form id="form-atualizar-img" class="form-horizontal form-label-left">

                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                            <div class="flex-center">
                                <div class="hold-perfil">
                                <img id="img_preview" class ="img img-perfil" src="">
                                </div>
                              </div>
                              <div class="mt-7">
                                <label for="foto_perfil" class="btn btn-primary btn-xs">Selecionar imagem</label>
                                <small class="form-text text-muted d-block mt-7">Formatos permitidos: JPG, PNG, GIF.</small>
                                <input type="file" id="foto_perfil" name="foto_perfil" require class="hidden">
                                <input type="hidden" id="foto-atual" name="foto_atual">
                                <input type="hidden" id="id_user" name="id_user">
                                <input type="hidden" name="action" value="alterar_imagem">
                              </div>
                            </div>
                            </div>
                          </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Aplicar</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->

        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- Switchery -->
    <script src="../vendors/switchery/dist/switchery.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- datatables -->
    <?php include './componentes/DataTableJs.php' ?>
    <!-- NProgress -->
    <script src="../vendors/nprogress/nprogress.js"></script>
    
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/functions.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/usuario.js?v=<?php echo ASSETS_VERSION; ?>"></script>

  </body>
</html>