<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '012992a0cea045b3cfebf3e9506c7f8cdbf992d4',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '012992a0cea045b3cfebf3e9506c7f8cdbf992d4',
    ),
    'mpdf/mpdf' => 
    array (
      'pretty_version' => 'v8.0.10',
      'version' => '8.0.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '1333a962cd2f7ae1a127b7534b7734b58179186f',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '776f831124e9c62e1a2c601ecc52e776d8bb7220',
      'replaced' => 
      array (
        0 => '1.10.2',
      ),
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.100',
      'version' => '9.99.100.0',
      'aliases' => 
      array (
      ),
      'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
    ),
    'pclzip/pclzip' => 
    array (
      'pretty_version' => '2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '19dd1de9d3f5fc4d7d70175b4c344dee329f45fd',
    ),
    'phpoffice/common' => 
    array (
      'pretty_version' => '0.2.9',
      'version' => '0.2.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edb5d32b1e3400a35a5c91e2539ed6f6ce925e4d',
    ),
    'phpoffice/phpword' => 
    array (
      'pretty_version' => '0.17.0',
      'version' => '0.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8346af548d399acd9e30fc76ab0c55c2fec03a5',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'setasign/fpdi' => 
    array (
      'pretty_version' => 'v2.3.6',
      'version' => '2.3.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '6231e315f73e4f62d72b73f3d6d78ff0eed93c31',
    ),
    'zendframework/zend-escaper' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3801caa21b0ca6aca57fa1c42b08d35c395ebd5f',
    ),
  ),
);
