<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Geral</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> Alunos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="alunos.php">Cadastrar Alunos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fas fa-user-shield"></i> Administradores <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="admins.php">Perfis Administrativos</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fas fa-file-invoice-dollar"></i> Relatório de Vendas <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="transacoes_plano.php">Planos</a></li>
                      <!-- <li><a href="gateways.php">Gateways</a></li> -->
                      <!-- <li><a href="extrato.php">Extrato</a></li> -->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-list"></i> Categorias <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="categorias.php">Cadastrar Categoria</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-play-circle"></i> Vídeos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="videos.php">Cadastrar Vídeos</a></li>
                    </ul>
                  </li>
                  <!-- <li><a><i class="fa fa-folder"></i> Documentos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="documentos.php">Cadastrar Documentos</a></li>
                    </ul>
                  </li> -->
                  <li><a><i class="fas fa-question-circle"></i> Faq <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="faq.php">Faq</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fas fa-file-invoice-dollar"></i> Blog <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="blog.php">Cadastrar Notícias</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fas fa-cart-plus"></i> Planos <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="planos.php">Cadastrar Planos</a></li>
                      <li><a href="cupons.php">Cadastrar Cupons</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fas fa-file-alt"></i> Simulados <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="simulados.php">Cadastrar Simulado</a></li>
                      <!--<li><a href="#">Informação</a></li>-->
                    </ul>
                  </li>
                 <!--  <li><a><i class="fas fa-comments"></i> Fórum <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="forum.php"> Tópicos</a></li>
                      <li><a href="#">Informação</a></li>
                    </ul>
                  </li> -->
                </ul>
              </div>
            </div>