<?php

namespace Blog;

function atualizar($args){
    global $mysqli;

    $stmt = $mysqli->prepare(
        "UPDATE 
            noticias 
            SET 
                titulo = ?,
                texto = ?,
                id_autor = ?,
                img = ?,
                categoria = ?,
                lingua = ?,
                nota = ?
            WHERE 
                id = ?
        "
    );
    $stmt->bind_param(
        "ssissssi",
        $args["titulo"],
        $args["texto"],
        $args["autor"],
        $args["img"],
        $args["categoria"],
        $args["lingua"],
        $args["nota"],
        $args["id"]
    );

    $stmt->execute() or die($stmt->error);

    return buscar($args["id"]);

}

function cadastrar($args){
    global $mysqli;

    $stmt = $mysqli->prepare(
        "INSERT INTO  noticias(titulo, texto, id_autor, img, categoria, lingua, nota, visualizacoes, data_publicacao) VALUES(?, ? , ? , ? , ? , ? , ? , ? , ? )"
    );

    $stmt->bind_param(
        "ssissssis",
        $args["titulo"],
        $args["texto"],
        $args["autor"],
        $args["img"],
        $args["categoria"],
        $args["lingua"],
        $args["nota"],
        $args["visualizacoes"],
        $args["data_publicacao"]
    );

    $stmt->execute() or die($stmt->error);

    return buscar($stmt->insert_id);
    
}

function listar(){
    global $mysqli;

    $result = $mysqli->query(
        "SELECT n.*, e.nome as autor FROM noticias as n LEFT JOIN equipe as e ON e.id = n.id_autor WHERE n.deletado = 0 ORDER BY data_publicacao DESC"
    );

    //var_dump($result->fetch_all(MYSQLI_ASSOC));

    return $result->fetch_all(MYSQLI_ASSOC);
}

function buscar($id){
    global $mysqli;

    $result = $mysqli->query(
        "SELECT n.*, e.nome as autor FROM noticias as n LEFT JOIN equipe as e ON e.id = n.id_autor WHERE n.id = $id"
    );

    return $result->fetch_assoc();
}

?>