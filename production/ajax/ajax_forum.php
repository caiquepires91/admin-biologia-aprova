<?php 
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){

        case 'novo_topico':
        novo_topico();
        break;

        case 'like_topico':
        like_topico();
        break;

        case 'like_post':
        like_post();
        break;

        case 'desabilitar_topico':
        desabilitar_topico();
        break;

        case 'desabilitar_post':
        desabilitar_post();
        break;

        case 'novo_post':
        novo_post();
        break;

        case 'editar_post':
        editar_post();
        break;

        case 'editar_titulo':
        editar_titulo();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){

        case 'ler_topicos':
        ler_topicos();
        break;

        case 'ler_topicos_destaque':
        ler_topicos_destaque();
        break;

        case 'ler_post':
        ler_post();
        break;

        case 'ler_titulo_topico':
        ler_titulo_topico();
        break;

        case 'ler_likes_post':
        ler_likes_post();
        break;
    }
}

//-----------FUNCOES AJAX POST-------------------//

function novo_topico(){
    global $mysqli;
    $topico = validar_topico();

    extract($topico);
    try{
        $mysqli->autocommit(FALSE);
        //INSERINDO REGISTRO NA TABELA TOPICO
        $create_topico = $mysqli->prepare("INSERT INTO topico(id, id_usuario, descricao, data_criacao) VALUES(default, ?,?,?)");
        $create_topico->bind_param("iss", $id_user,$titulo,$data_criacao);
        $create_topico->execute();
        $id_topico = $mysqli->insert_id;

        //GERANDO A PRIMEIRA RESPOSTA DO TÓPICO COM O ID GERADO
        $topico_resposta = $mysqli->prepare("INSERT INTO resposta(id,id_topico, id_usuario, id_resposta,texto_resposta, data_criacao) VALUES(default,?,?,null,?,?)");
        $topico_resposta->bind_param("iiss", $id_topico, $id_user, $post, $data_criacao);
        $topico_resposta->execute();
        $create_topico->close();
        $topico_resposta->close();
        $mysqli->autocommit(TRUE);

        echo json_encode(array("success" => 1, "msg" => "Tópico criado com sucesso!", "id_topico"=>$id_topico));
    }catch(Exception $e){
        $mysqli->rollback();
        echo json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode()));
    }   

}

function novo_topico_aluno(){
    global $mysqli;
    $topico = validar_topico();

    extract($topico);
    try{
        $mysqli->autocommit(FALSE);
        //INSERINDO REGISTRO NA TABELA TOPICO
        $create_topico = $mysqli->prepare("INSERT INTO topico(id, id_usuario, descricao, data_criacao) VALUES(default, ?,?,?)");
        $create_topico->bind_param("iss", $id_user,$titulo,$data_criacao);
        $create_topico->execute();
        $id_topico = $mysqli->insert_id;

        //GERANDO A PRIMEIRA RESPOSTA DO TÓPICO COM O ID GERADO
        $topico_resposta = $mysqli->prepare("INSERT INTO resposta(id,id_topico, id_usuario, id_resposta,texto_resposta, data_criacao) VALUES(default,?,?,null,?,?)");
        $topico_resposta->bind_param("iiss", $id_topico, $id_user, $post, $data_criacao);
        $topico_resposta->execute();
        $create_topico->close();
        $topico_resposta->close();
        $mysqli->autocommit(TRUE);

        echo json_encode(array("success" => 1, "msg" => "Tópico criado com sucesso!", "id_topico"=>$id_topico));
    }catch(Exception $e){
        $mysqli->rollback();
        echo json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode()));
    }   

}

function like_topico(){
    global $mysqli;

    $id_tabela = $_POST['id_tabela'];
    $id_usuario = $_POST['id_user'];
    $datetime = date('Y-m-d H:i:s');

    $select = $mysqli->query("SELECT * FROM likes_topico WHERE id_usuario = $id_usuario AND id_topico = $id_tabela");
    if($select->num_rows > 0)
    {
        $mysqli->query("DELETE FROM likes_topico WHERE id_usuario = $id_usuario AND id_topico = $id_tabela");
        echo json_encode(array("success" => 1, "value" => 0));
    }
    else
    {
        $mysqli->query("INSERT INTO likes_topico(id_usuario, id_topico, `data`) VALUES($id_usuario,$id_tabela,'$datetime')");
        echo json_encode(array("success" => 1, "value" => 1)); //RETORNAR NUMERO DE likes, OU ATUALIZAR DIRETO NO JS
    }
}
function like_post(){
    global $mysqli;

    $id_tabela = $_POST['id_tabela'];
    $id_usuario = $_POST['id_user'];
    $datetime = date('Y-m-d H:i:s');

    $select = $mysqli->query("SELECT * FROM likes_resposta WHERE id_usuario = $id_usuario AND id_resposta = $id_tabela");
    if($select->num_rows > 0)
    {
        $mysqli->query("DELETE FROM likes_resposta WHERE id_usuario = $id_usuario AND id_resposta = $id_tabela");
        echo json_encode(array("success" => 1, "value" => 0));
    }
    else
    {
        $mysqli->query("INSERT INTO likes_resposta(id_usuario, id_resposta, `data`) VALUES($id_usuario,$id_tabela,'$datetime')");
        echo json_encode(array("success" => 1, "value" => 1)); //RETORNAR NUMERO DE likes, OU ATUALIZAR DIRETO NO JS
    }
}
function desabilitar_topico(){
    global $mysqli;
    $id = $_POST['id'];
    $value = $_POST['value'];
    
    if(!$mysqli->query("UPDATE topico SET deletado = $value WHERE id = $id")){
        die(json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno)));
    }
    echo json_encode(array("success" => 1, "msg" => "Operação realizada!")); //RETORNAR NUMERO DE likes, OU ATUALIZAR DIRETO NO JS
}
function desabilitar_post(){
    global $mysqli;
    $id = $_POST['id'];
    $value = $_POST['value'];
    if(!$mysqli->query("UPDATE resposta SET deletado = $value WHERE id = $id")){
        die(json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno)));
    }
    echo json_encode(array("success" => 1, "msg" => "Operação realizada!"));
}
function novo_post(){
    global $mysqli;
    $post = validar_post();
    
    extract($post);
    $datanow = date('Y-m-d H:i:s');
    try{
        $stmt = $mysqli->prepare("INSERT INTO resposta(id, texto_resposta, data_criacao, id_topico, id_usuario, id_resposta) values(default,?,?,?,?,?)");
        $stmt->bind_param("ssiii", $texto_resposta, $datanow, $id_topico, $id_usuario, $id_resposta);
        $stmt->execute();

        $result = $mysqli->query("SELECT COUNT(*) as total_respostas FROM resposta WHERE id_topico = $id_topico");

        $res = $result->fetch_assoc()['total_respostas']; //NUMERO DE RESPOSTAS NO TÓPICO, SERVE PARA DIRECIONAR PARA A ULTIMA PAGINA
        echo json_encode(array("success" => 1, "msg" => "Resposta enviada ao tópico!", "n"=>(int)$res));

    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno)));
    }

}
function editar_post(){

    global $mysqli;
    
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
    $id_usuario = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);
    $texto_resposta = filter_input(INPUT_POST, 'texto_resposta', FILTER_SANITIZE_STRING);

    $usuario = validar_usuario($id_usuario);
    $post = get_post($id);
    
    $tempo_passado = strtotime(date('Y-m-d H:i:s')) - strtotime($post['data_criacao']);

    if($usuario['admin'] == 0){ //se o usuario não for admin, então haverá regras

        if($post['id_usuario'] !== $id_usuario)
            die(json_encode(array('success'=>0, 'msg'=>'O post não pode ser editado por outro usuario.')));

        if($post['n_respostas'])
            die(json_encode(array('success'=>0, 'msg'=>'O post não pode ser editado pois já foi respondido.')));

        if($tempo_passado > 900) //15 minutos para editar a resposta
            die(json_encode(array('success'=>0, 'msg'=>'O post não pode ser editado após 15 minutos.')));
    }

    if($texto_resposta == false){
        die(json_encode(array('success'=>0, 'msg'=>'A resposta não pode ser vazia!')));
    }

    try{
        $stmt = $mysqli->prepare("UPDATE resposta SET texto_resposta = ? WHERE id=?");
        $stmt->bind_param("si", $texto_resposta, $id);
        $stmt->execute();
        echo json_encode(array("success" => 1, "msg" => "Editado com sucesso!"));
    }catch(Exception $e){
        die(json_encode(array('success'=>99, 'msg'=>$e->getMessage())));
    }
}
function editar_titulo(){
    global $mysqli;
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
    $id_usuario = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);
    $titulo = filter_input(INPUT_POST, 'titulo', FILTER_SANITIZE_STRING);

    $usuario = validar_usuario($id_usuario);
    $topico = get_topico($id);
    
    $tempo_passado = strtotime(date('Y-m-d H:i:s')) - strtotime($topico['data_criacao']);

    if($usuario['admin'] == 0){ //se o usuario não for admin, então haverá regras

        if($topico['id_usuario'] !== $id_usuario)
            die(json_encode(array('success'=>0, 'msg'=>'O tópico não pode ser editado por outro usuario.')));

        if($topico['posts'] > 1)
            die(json_encode(array('success'=>0, 'msg'=>'O tópico não pode ser editado pois já foi respondido.')));

        if($tempo_passado > 900) //15 minutos para editar a resposta
            die(json_encode(array('success'=>0, 'msg'=>'O tópico não pode ser editado após 15 minutos.')));
    }

    if($titulo == false){
        die(json_encode(array('success'=>0, 'msg'=>'O título do tópico não pode ser vazio!')));
    }
    
    try{
        $stmt = $mysqli->prepare("UPDATE topico SET descricao = ? WHERE id=?");
        $stmt->bind_param("si", $titulo, $id);
        $stmt->execute();
        echo json_encode(array("success" => 1, "msg" => "Editado com sucesso!"));
    }catch(Exception $e){
        die(json_encode(array('success'=>99, 'msg'=>$e->getMessage())));
    }

}

//-----------FUNCOES AJAX GET-------------------//

function ler_topicos(){
    global $mysqli;
    $result = $mysqli->query("SELECT t.descricao as titulo, t.data_criacao, t.id, t.deletado, a.nome as criador, count(r.id) as respostas FROM topico as t INNER JOIN usuario as a ON t.id_usuario = a.id INNER JOIN resposta as r ON r.id_topico = t.id WHERE t.deletado = 0 GROUP BY r.id_topico ORDER BY t.data_criacao DESC;");
    $topicos = array();
    while($row = $result->fetch_assoc()){
        extract($row);
        $likes = $mysqli->query("SELECT count(*) as likes FROM likes_topico WHERE id_topico = $id");
        if($ultimo = $mysqli->query("SELECT r.id_usuario as id, a.nome as user, p.image, r.data_criacao as data_resposta FROM resposta as r INNER JOIN usuario as a ON a.id = r.id_usuario INNER JOIN perfil as p ON p.id = a.id_perfil WHERE id_topico = $id ORDER BY r.data_criacao DESC LIMIT 1;")){
            $row['likes'] = $likes->fetch_assoc()['likes'];
            $row['ultimo_user'] = $ultimo->fetch_assoc();
            $row['ultimo_user']['data_resposta'] = tempo_corrido($row['ultimo_user']['data_resposta']); 
            $row['data_extenso'] = tempo_corrido($row['data_criacao']);
        }
    $topicos[] = $row;
    }
    echo json_encode($topicos);
}
function ler_topicos_destaque(){
    global $mysqli;
    $result = $mysqli->query("SELECT t.descricao as titulo, t.data_criacao, t.id, t.deletado, a.nome as criador, count(r.id) as respostas FROM topico as t INNER JOIN usuario as a ON t.id_usuario = a.id INNER JOIN resposta as r ON r.id_topico = t.id WHERE t.deletado = 0 GROUP BY r.id_topico ORDER BY count(r.id) DESC LIMIT 3;");
    $topicos = array();
    while($row = $result->fetch_assoc()){
        extract($row);
        $likes = $mysqli->query("SELECT count(*) as likes FROM likes_topico WHERE id_topico = $id");
        if($ultimo = $mysqli->query("SELECT r.id_usuario as id, a.nome as user, p.image, r.data_criacao as data_resposta FROM resposta as r INNER JOIN usuario as a ON a.id = r.id_usuario INNER JOIN perfil as p ON p.id = a.id_perfil WHERE id_topico = $id ORDER BY r.data_criacao DESC LIMIT 1;")){
            $row['likes'] = $likes->fetch_assoc()['likes'];
            $row['ultimo_user'] = $ultimo->fetch_assoc();
            $row['ultimo_user']['data_resposta'] = tempo_corrido($row['ultimo_user']['data_resposta']);
            $row['data_extenso'] = tempo_corrido($row['data_criacao']);
        }
    $topicos[] = $row;
    }
    echo json_encode($topicos);
}
function ler_post(){
    global $mysqli;
    $id = $_GET['id'];
    try{
        $resposta = $mysqli->query("SELECT r.data_criacao, a.nome, r.texto_resposta from resposta as r LEFT JOIN usuario as a ON r.id_usuario = a.id WHERE r.id = $id;");
        $row = $resposta->fetch_assoc();
        $row['data_extenso'] = strftime('%d de %B de %Y às %H:%M', strtotime($row['data_criacao']));

        echo json_encode($row);
    }catch(Exception $e){
        die(json_encode(array('success'=>0, 'msg'=>$e->getMessage())));
    }

}
function ler_titulo_topico(){
    global $mysqli;
    $id = $_GET['id'];
    try{
        $topico = $mysqli->query("SELECT descricao as titulo from topico WHERE id = $id;");
        echo json_encode($topico->fetch_assoc());
    }catch(Exception $e){
        die(json_encode(array('success'=>99, 'msg'=>$e->getMessage())));
    }
}
function ler_likes_post(){
    global $mysqli;
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
    try{
        $result = $mysqli->query("SELECT u.nome, u.id, l.data, p.image FROM likes_resposta as l INNER JOIN usuario as u ON l.id_usuario = u.id INNER JOIN resposta as r ON l.id_resposta = r.id INNER JOIN perfil as p ON p.id = u.id_perfil WHERE l.id_resposta = $id ORDER BY r.data_criacao ASC;");
        $likes = array();
        while($row = $result->fetch_assoc()){
            $row['data_extenso'] = strftime('%d de %B de %Y às %H:%M', strtotime($row['data']));
            $likes[] = $row;
        }
        echo json_encode(array("success"=>1, "likes"=>$likes));
    }catch(Exception $e){
        echo json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno));
    }
}

//-----------FUNCOES AUXILIARES --------------//

function validar_topico(){
    $topico['id_user'] = filter_input(INPUT_POST, 'id_user', FILTER_SANITIZE_NUMBER_INT);
    $topico['titulo'] = filter_input(INPUT_POST, 'titulo', FILTER_SANITIZE_STRING);
    $topico['post'] =filter_input(INPUT_POST, 'resposta', FILTER_SANITIZE_STRING);
    $topico['data_criacao'] = date('Y-m-d H:i:s');
    $topico['id_resposta'] = null;

    if($topico['id_user'] == false){
        echo json_encode(array("success" => 0, "msg" => "Usuário inválido, por favor atualize a página!"));
        die();
    }

    if($topico['titulo'] == false){
        echo json_encode(array("success" => 0, "msg" => "Informe um título!"));
        die();
    }

    if($topico['post'] == false){
        echo json_encode(array("success" => 0, "msg" => "Post inválido!"));
        die();
    }

    return $topico;
}

function validar_post(){

    $resposta['id_topico'] = filter_input(INPUT_POST, 'id_topico', FILTER_SANITIZE_NUMBER_INT);//$_POST['id_topico'];
    $resposta['id_usuario'] = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);//$_POST['id_usuario'];
    //Se id da resposta é nulo então o post é no tópico
    $resposta['id_resposta'] = $_POST['id_resposta'] ? filter_input(INPUT_POST, 'id_resposta', FILTER_SANITIZE_NUMBER_INT) : null;
    $resposta['texto_resposta'] = filter_input(INPUT_POST, 'resposta', FILTER_SANITIZE_STRING);//$_POST['resposta'];
    if($resposta['texto_resposta'] == false){
        die(json_encode(array('success'=>0, 'msg'=>'Post inválido!')));
    }
    return $resposta;
}

function tempo_corrido($time) {
    $now = strtotime(date('m/d/Y H:i:s'));
    $time = strtotime($time);
    $diff = $now - $time;
    //echo $diff;exit;
    $seconds = $diff;
     $minutes = round($diff / 60);
     $hours = round($diff / 3600);
     $days = round($diff / 86400);
     $weeks = round($diff / 604800);
     $months = round($diff / 2419200);
     $years = round($diff / 29030400);
    
    if ($seconds <= 60) return"1 min atrás";
     else if ($minutes <= 60) return $minutes==1 ?'1 min atrás':$minutes.' min atrás';
     else if ($hours <= 24) return $hours==1 ?'1 hrs atrás':$hours.' horas atrás';
     else if ($days <= 7) return $days==1 ?'1 dia atras':$days.' dias atrás';
     else if ($weeks <= 4) return $weeks==1 ?'1 semana atrás':$weeks.' semanas atrás';
     else if ($months <= 12) return $months==1 ?'1 mês atrás':$months.' meses atrás'; //strftime('%d de %B às %H:%M', $time);
     else return $years == 1 ? 'um ano atrás':$years.' anos atrás';
}
function validar_usuario($id){
    global $mysqli;

    $usuario = NULL;
    if($res = $mysqli->query("SELECT id, cpf, admin, master FROM usuario WHERE id = $id AND deletado = 0"))
        $usuario = $res->fetch_assoc();

    return $usuario;
}
function get_post($id){
    global $mysqli;

    $post = NULL;
    $res = $mysqli->query("SELECT * FROM resposta WHERE id = $id AND deletado = 0");
    $post = $res->fetch_assoc();

    $res2 = $mysqli->query("SELECT COUNT(*) as respostas FROM RESPOSTA WHERE id_resposta = $id"); //verificando se o post foi respondido
    $post['n_respostas'] = $res2->fetch_assoc()['respostas'];

    return $post;
}

function get_topico($id){
    global $mysqli;

    $topico = NULL;
    $res = $mysqli->query("SELECT * FROM topico WHERE id = $id AND deletado = 0");
    $topico = $res->fetch_assoc();

    $res2 = $mysqli->query("SELECT COUNT(*) as posts FROM resposta WHERE id_topico = $id"); //verificando se o topico foi respondido
    $topico['posts'] = $res2->fetch_assoc()['posts'];

    return $topico;
}

?>