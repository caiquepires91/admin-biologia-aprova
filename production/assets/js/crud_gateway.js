var id_gateway = 0;

$(document).ready(function(){
    loadTable()

    $('.unico').change(function(){
        if(!$(this).is(':checked')) {
            $('#form-gateway input[name="taxas[]"]').val("");
            $('#form-gateway .first').css('border', '1px solid #ccc');
        }
        else{
            $('#form-gateway input[name="taxas[]"]').not('.first').val("");
            $('#form-gateway .first').css('border', '1px solid red');
        }
            
    })

    $('.txFixaCk').change(function(){
        if(!$(this).is(':checked')) {
            $('.first').attr("disabled", true);
        }
        else{
            $('.first').attr("disabled", false);
        }
            
    })

    $('.first').keyup(function(){
        if($('.unico').is(':checked') == true){
            var taxa = $(this).val();
            var parcela = taxa;
            var inputs = document.querySelectorAll('#form-gateway input[name="taxas[]"]');
            for(i = 1; i< inputs.length; i++){
                parcela += taxa;
                inputs[i].value = parcela;
            }
        }
    })

    $(".valor").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: true,
    unmaskAsNumber: true,
    });

    $(".percentage").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: '% ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: true,
    unmaskAsNumber: true,
    });


      $('#form-gateway').submit(function(event){
        event.preventDefault()
        let dados = {
            nome: $('#form-gateway input[name=nome]').val(),
            lBoleto: $('#form-gateway input[name=lBoleto]').val(),
            lCartao: $('#form-gateway input[name=lCartao]').val(),
            txBoleto: $('#form-gateway input[name=txBoleto]').val(),
            trBoleto: $('#form-gateway input[name=trBoleto]').val(),
            txCartao: $('#form-gateway input[name=txCartao]').val(),
            trCartao: $('#form-gateway input[name=trCartao]').val(),
            isTxFixa:  $(".txFixaCk").is(':checked'),
            isTxUnico: $(".unico").is(':checked'),
            txMensal: $('#form-gateway input[name="taxas[]"]').map(function(){return $(this).val();}).get(),
            action: "novo"
        };

        $.ajax({
            method: 'post',
            url: './ajax/ajax_gateway.php',
            data: dados,
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, false, [reloadGateways], false, $('#modal_cadastro'));
        })
    })
    
    $('#form-update-gateway').submit(function(event){
        event.preventDefault()
        let dados = {
            nome: $('#form-update-gateway input[name=nome]').val(),
            lBoleto: $('#form-update-gateway input[name=lBoleto]').val(),
            lCartao: $('#form-update-gateway input[name=lCartao]').val(),
            txBoleto: $('#form-update-gateway input[name=txBoleto]').val(),
            trBoleto: $('#form-update-gateway input[name=trBoleto]').val(),
            txCartao: $('#form-update-gateway input[name=txCartao]').val(),
            trCartao: $('#form-update-gateway input[name=trCartao]').val(),
            isTxFixa:  $("#form-update-gateway .txFixaCk").is(':checked'),
            isTxUnico: $("#formupdate-gateway .unico").is(':checked'),
            txMensal: $('#form-update-gateway input[name="taxas[]"]').map(function(){return $(this).val();}).get(),
            id: id_gateway,
            action: "atualizar"
        };

        $.ajax({
            method: 'post',
            url: './ajax/ajax_gateway.php',
            data: dados,
            dataType: 'text'
        }).done(res=>{
            responseTreatment(res, false, [reloadGateways], false, $('#modal_update'));
        })

    })

    $('#form-alterar-gateway').submit(function(e){
        e.preventDefault();

        id_gateway = $('#form-alterar-gateway #select_gateway').val();

        $.ajax({
            method: 'post',
            url: './ajax/ajax_gateway.php',
            data: {id_gateway: id_gateway, action: 'alterar_gateway'},
            dataType: 'json'
        }).done(res=>{
            responseTreatment(res, false, false,[],false,false);
            $('#gateway_ativo_nome').html(res.nome);
        })
    })
})

function loadTable(){
    const table = $('#gateways').DataTable({
        "processing": true,
        "responsive": true,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "order": [[2, 'desc']],
        "ajax": {
            "url": "./ajax/ajax_gateway.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "nome", render: function(nome){
                return `<div class="flex-center"><strong>${nome}</strong></div>`;
            }},
            { "data": "data_cadastro"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <button class="btn btn-primary btn-xs" onClick="readGateway(${id})" data-toggle="modal" data-target="#modal_read" title="Ver Gateway" data-tooltip="tooltip" data-placement="top"><i class="fa fa-eye"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="updateGateway(${id})" data-toggle="modal" data-target="#modal_update" title="Editar Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteGateway(${id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function reloadGateways(){
    $('#gateways').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updateGateway(id){
    id_gateway = id;

    $.ajax({
        url: './ajax/ajax_gateway.php',
        data: {id: id, action: "ler_objeto"},
        dataType: 'json'
    }).done(res=>{
        if(res.success == 1){
            $('#form-update-gateway input[name=nome]').val(res.gateway.nome);
            $('#form-update-gateway input[name=txBoleto]').val(res.gateway.taxa_boleto);
            $('#form-update-gateway input[name=trBoleto]').val(res.gateway.tarifa_boleto);
            $('#form-update-gateway input[name=lBoleto]').val(res.gateway.liberacao_boleto);
            $('#form-update-gateway input[name=txCartao]').val(res.gateway.taxa_cartao);
            $('#form-update-gateway input[name=trCartao]').val(res.gateway.tarifa_cartao);
            $('#form-update-gateway input[name=lCartao]').val(res.gateway.liberacao_cartao);
            $('#form-update-gateway input[name=fixa]').attr("checked", res.gateway.isTaxaFixa ? true: false);
            $('#form-update-gateway .first').attr("disabled" , res.gateway.isTaxaFixa ? false : true);
            $('#form-update-gateway input[name=unico]').attr("checked" , false);
            
            taxas = $('#form-update-gateway input[name="taxas[]"]');
    
            for(i = 0; i < 12; i++){
                taxas[i].value = res.gateway.taxas[i];
            }
        }else{
            new PNotify({
                title: 'Falha ao consultar dados!',
                text: res.msg,
                type: 'warning',
                styling: 'bootstrap3'
            });
        }
    })
}

function deleteGateway(id){
    let confirmed = confirm("Deseja remover este gateway?")
    if(confirmed == true){
        $.post('./ajax/ajax_gateway.php', {id: id, action: "deletar"}, function(res){
            success = responseTreatment(res, false, [reloadGateways], false, false)
        }, 'json');
    }
}

function readGateway(id){

    $.ajax({
        url: './ajax/ajax_gateway.php',
        data: {action: 'ler', id: id},
        dataType: 'json'
    }).done(res=>{
        if(res.success == 1)
            $('#read_modal_body').html(res.pagina);
        else
            console.log("Error");
    })
}