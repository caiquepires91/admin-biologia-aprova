$(document).ready(function() {
    $('#form-login').submit(function(e){
      e.preventDefault();
      dados = $(this).serialize();
      $.post('./ajax/ajax_login.php', dados, res=>{
          if(res.success == 1)
            window.location.href = "index.php";
        else{
            $("#login-error").html(`${res.success == 0 ? `<div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Erro: </strong>${res.msg}</div>`:`<div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Erro: </strong>${res.msg}</div>`}`)
        }
      },'json');
    });

});
