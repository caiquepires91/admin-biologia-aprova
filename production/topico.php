<?php 
// DIFERENCIAR RENDERIZAÇÃO DE BTNS CONFORME O TIPO DE USUARIO;
require __DIR__.'/php/autentica.php';
require_once __DIR__ . '/php/functions.php';

header('Content-Type: text/html; charset=utf-8');
$items_pagina = 5; //definir número de respostas por página

if(!isset($_GET['pagina']) || !is_numeric($_GET['pagina'])) $pc = 1;
else $pc = $_GET['pagina'];

$inicio = $pc - 1;
$inicio = $inicio * $items_pagina;

$response = getRespostas($items_pagina, $inicio, $_SESSION['id_usuario']);
$topico = $response['topico'];
$respostas = $response['respostas'];
$total_paginas = $response['total_paginas'];
$id = $response['id_pagina'];

if($pc <= 0) header("location: topico.php?id=$id&pagina=1");
//AQUI ONDE A MÁGICA ACONTECE, O PONTEIRO DO ARRAY VOLTA AO INICIO, E LOOPA ATÉ ATINGIR O INICIO DE ACORDO COM A PÁGINA SELECIONADA;
for(reset($respostas), $j=0; $j<(($pc-1)*$items_pagina); $j++, next($respostas));
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Tópico</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <link href="./css/modules/topico.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="col-md-12 topico-title">
                <a href=<?php echo "usuario.php?id=".$topico['id_criador'];?> class="user-link hover-info" data-idUser="<?php echo $topico['id_criador'];?>"><img class="img-user-topico" src="<?php echo "./uploads/users/perfil/".$topico['image'];?>" alt="Autor do Tópico"></a>
                <div class="description">
                    <h1 class="titulo"><?php echo $topico['descricao'];?><?php if($topico['deletado'] == 1) echo '<span class="desabilitado">[Tópico desabilitado pelo administrador]</span>';?></h1>
                    <p style="color: #8d9aa6;">Por <?php echo $topico['nome']?>, <time datetime="<?php echo $topico['data_criacao'];?>"><?php echo $topico['data_extenso'];?></time></p>
                </div>
                <div class="btn-group topico-actions">
                  <a id="btn-responder-topico" href="#div-bottom" class="btn btn-sm btn-primary" type="button"><i class="fa fa-reply"></i> Responder</a>
                  <?php 
                  if($_SESSION['admin'])
                    echo "<button class='btn btn-sm btn-default' type='button' onClick='editarTopico($topico[id])' data-toggle='modal' data-target='.modal-edit' data-placement='top'><i class='fas fa-edit'></i></button>";
                  else{
                    if($topico['id_criador'] == $_SESSION['id_usuario'])
                      echo "<button class='btn btn-sm btn-default' type='button' onClick='editarTopico($topico[id])' data-toggle='modal' data-target='.modal-edit' data-placement='top'><i class='fas fa-edit'></i></button>";
                  }

                  if($response['liked'])
                    echo "<button class='btn btn-sm btn-default' type='button' onClick='likeTopico($topico[id], this)' data-placement='top' data-toggle='tooltip' title='Votar'><i class='fas fa-thumbs-up' style='color: #1ABB9C;'></i></button>";
                  else
                    echo "<button class='btn btn-sm btn-default' type='button' onClick='likeTopico($topico[id], this)' data-placement='top' data-toggle='tooltip' title='Votar'><i class='fas fa-thumbs-up'></i></button>";
                  
                  if($topico['deletado'] == 0)
                    echo "<button class='btn btn-sm btn-default' type='button' onClick='disableTopico($topico[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Desabilitar' data-ativo=$topico[deletado]><i class='fa fa-ban'></i></button>";
                  else
                    echo "<button class='btn btn-sm btn-default' type='button' onClick='disableTopico($topico[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Habilitar' data-ativo=$topico[deletado]><i class='fa fa-ban' style='color: red;'></i></button>";
                  ?>
                </div>
            </div>
        <div class="clearfix"></div>
    <?php 
        echo "<section class='topico-respostas'>";
        for($post = current($respostas), $j=0; $j<$items_pagina && $post; $j++, $post = next($respostas)){

          echo "
          <article class='resposta' id='resposta$post[id]'>
          <aside class='resposta-user'>
            <h3 class='user-nome'>";

          echo $post['status']==0? '<span class="status off" title="offline"></span>' : '<span class="status on" title="online"></span>';
          $nome_array = explode(" ", $post['nome']);
          $nome = count($nome_array) > 1? $nome_array[0]." ".$nome_array[1]: $nome_array[0];
          echo " ".$nome;
          echo "</h3>";
          if($post['id_usuario'] == $topico['id_criador']){
            echo 
            "<div class='autor-marker'>
              <span><i class='fa fa-star'></i> Autor do tópico</span>
            </div>";
          }
          echo 
          "<a href=usuario.php?id=$post[id_usuario] class='user-img user-link hover-info' data-iduser=$post[id_usuario]>
            <img src='./uploads/users/perfil/$post[image]' alt='Imagem de perfil'>
          </a>";
        
        if($post['admin'] == 1){
          echo 
          "<div class='admin-marker'>
            <span><i class='fa fa-shield'></i> Administrador</span>
          </div>";
        }
        echo 
        "<p class='user-posts' style='color: #8d9aa6;'>$post[posts] posts'</p>
        <p class='user-endereco'>$post[cidade]</br>$post[estado]</p>
        <p class='user-date' style='color: #8d9aa6;'>desde $post[data_cadastro]</p>
      </aside>
      <div class='resposta-body'>
      <div class='top'><span class='resposta-advice' style='color: #8d9aa6; font-size: 12px;'>Postado <time datetime=$post[data_criacao]>$post[data_extenso]</time>";
      if($post['deletado'] == 1)
        echo ' <span class="desabilitado">[Resposta desativada pelo administrador]</span>';
        
        echo "</span>
        <div class='like-container' data-id=$post[id] data-toggle='modal' data-target='.modal-likes'><span><i class='fas fa-thumbs-up'></i></span><span class='n-likes'>$post[likes]</span></div></div>
        <div class='resposta-content'>";
      if($id_res = $post['id_resposta']){
        echo 
          "<blockquote class='resposta-blockquote'>
          <div class='resposta-citacao-title'> Em <time datetime=".$respostas[$id_res]['data_criacao'].">".$respostas[$id_res]['data_extenso']."</time>, <span><a href='#' data-idUser=".$respostas[$id_res]['id_usuario'].">";
        echo $respostas[$id_res]['id_usuario']? $respostas[$id_res]['nome']: "Administrador";
        echo "</a></span> disse: <a class='resposta-link' href='#resposta$id_res'><i class='fa fa-share'></i></a></div>
          <div class='resposta-citacao-body'>
            <p style='text-align: justify;white-space: pre-line;'>
            ".$respostas[$id_res]['texto_resposta']."
            </p>
          </div>
        </blockquote>";
      }
        echo 
        "<p style='text-align: justify;white-space: pre-line;'>
          $post[texto_resposta]
          </p>
        </div>
        
        <hr class='resposta-hr'>
        <div class='resposta-footer'>
        <div class='btn-group'>
          <button class='btn btn-sm btn-primary btn-citar' type='button' onClick='citarResposta($post[id])'><i class='fa fa-quote-right'></i> Responder</button>
        ";
        if($_SESSION['admin'] || ($post['id_usuario'] == $_SESSION['id_usuario']))
        echo "<button class='btn btn-sm btn-default' type='button' onClick='editarResposta($post[id])' data-toggle='modal' data-target='.modal-edit' data-placement='top'><i class='fas fa-edit'></i></button>";
        
        if(isset($response['likes']) && in_array($post['id'], $response['likes']))
          echo "<button class='btn btn-sm btn-default' type='button' onClick='likeResposta($post[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Votar'><i class='fas fa-thumbs-up' style='color: #1ABB9C;'></i></button>";
        else
          echo "<button class='btn btn-sm btn-default' type='button' onClick='likeResposta($post[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Votar'><i class='fas fa-thumbs-up'></i></button>";

        if($post['deletado'] == 0)
          echo "<button class='btn btn-sm btn-default' type='button' onClick='disableResposta($post[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Desabilitar' data-ativo=$post[deletado]><i class='fa fa-ban'></i></button>";
        else
          echo "<button class='btn btn-sm btn-default' type='button' onClick='disableResposta($post[id], this)' data-placement='top' data-toggle='tooltip' data-original-title='Habilitar' data-ativo=$post[deletado]><i class='fa fa-ban' style='color: red;'></i></button>";

        echo 
          "</div>
          </div>
          </div>
        </article>";
      }
    ?>
    <?php 
      $anterior = $pc -1; 
      $proximo = $pc +1; 
      $count=$total_paginas;
      
      echo "<nav aria-label='...'>
      <ul class='pagination'>";
      if($total_paginas == 1 || $total_paginas == 0);
      else if($pc>1 && $pc<$total_paginas)
      {
        echo 
          "<li class='page-item'>
            <a class='page-link' href='?id=$id&pagina=$anterior' tabindex='-1'>Anterior</a>
          </li>";
        
        for($i = 1; $i <= $total_paginas; $i++){
          if($pc==$i)
            echo "<li class='page-item active'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
          else
            echo "<li class='page-item'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
        }
        echo 
          "<li class='page-item'>
            <a class='page-link' href='?id=$id&pagina=$proximo'>Próximo</a>
          </li>";
        
      }
      else if($pc<=1)
      {
        for($i = 1; $i <= $total_paginas; $i++){
          if($pc==$i)
            echo "<li class='page-item active'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
          else
            echo "<li class='page-item'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
        }
        echo 
          "<li class='page-item'>
            <a class='page-link' href='?id=$id&pagina=$proximo'>Próximo</a>
          </li>";
      }else if($pc>=$total_paginas)
      {
        echo 
          "<li class='page-item'>
            <a class='page-link' href='?id=$id&pagina=$anterior' tabindex='-1'>Anterior</a>
          </li>";
        for($i = 1; $i <= $total_paginas; $i++){
          if($pc==$i)
            echo "<li class='page-item active'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
          else
            echo "<li class='page-item'><a class='page-link' href='?id=$id&pagina=$i'>$i</a></li>";
        }
      }
      echo 
        "</ul>
        </nav>
      </section>";

      echo 
      "<section id='responder' class='responder-section'>
        <aside>
        <img class='border-rounded' src='./uploads/users/perfil/$_SESSION[image]' alt='Imagem de perfil'>
        </aside>
        <form id='form-responder' class='resposta-content'>
        <span class='citacao'>
          </span>
          <textarea class='resposta-textarea' name='user-resposta' id='userResposta' placeholder='Responder...' ";
      echo $topico['deletado'] == 1? "disabled":"";
      echo "></textarea>
          <input type='hidden' name='id_topico' value=$topico[id]>
          <input type='hidden' name='action'>
          <input type='hidden' name='id_resposta'>
          <button class='btn btn-dark btn-responder' id='btn-responder' ";
      echo $topico['deletado'] == 1? "disabled":"";
      echo ">responder</button>
        </form>
      </section>
    </div>";
      ?>  
    
  <!-- /page content -->

        <!-- modal content -->
        <div class="modal fade modal-likes" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="modal-likes-label">Ver quem curtiu <span class="n-likes">(2)</span></h4>
              </div>
              <div class="modal-body modal-likes-body">
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Editar </h4>
              </div>
              <div class="modal-body">
              
              <form id="form-edit" class="form-horizontal form-label-left">
                <div id="error_cadastro"></div>
                <div class="form-group resp d-none">
                  <textarea class="form-control col-md-12 col-xs-12 resposta-form" name="text-resposta" rows="5"></textarea>
                </div>

                <div class="form-group title d-none">
                  <input type="text" name="titulo-topico" class="form-control col-md-12 col-xs-12 titulo-form">
                </div>

                <div class="modal-footer">
                    <input type="hidden" name="action">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="cadastrar-topico" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                </div>
              </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /modal content -->

        

        
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!--Special modal-->
    <div class="modal-info">
            <div class="modal-info-header">
              <h3>Weverton Bruno</h3>
              <img src="./uploads/users/perfil/132.jpg" alt="">
            </div>
            <div class="modal-info-body">
              <table class="table">
              </table>
            </div>
            <div class="modal-info-footer">
            </div>
          </div>
        <!--/Special modal-->

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <script>
      const id_usuario = <?php echo $_SESSION['id_usuario'];?>
    </script>
    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/script-topico.js"></script>
  </body>
</html>