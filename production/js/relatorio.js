var id_venda = 0;

$(document).ready(function(){

    $('.date.initial').datepicker({format:'dd/mm/yyyy'})
    $('.date.end').datepicker({format:'dd/mm/yyyy'})

    loadTable();
    $('[data-tooltip="tooltip"]').tooltip()

$('#form-filter').submit(function(e){
    e.preventDefault();
    dados = $(this).serialize() + '&action=carregar_transacoes';
    $("#transacoes").DataTable().ajax.url('./ajax/ajax_transacao.php?' + dados).load()
})

})

$('#update-gateway').click(function(e){
    e.preventDefault();
    if($("#read_modal_body #select_gateway").val() == "") return; //Não faz nada se nenhum gateway foi selecionado

    $.ajax({
        url: `./ajax/ajax_transacao.php?action=atualizar_gateway&id_gateway=${$("#read_modal_body #select_gateway").val()}&id=${id_venda}`,
        dataType: 'json'
    }).done(res=>{
        if(responseTreatment(res, false, [reloadVendas], false, false))
            readVenda(id_venda);
    })
})

function reloadVendas(){
    $('#transacoes').DataTable().ajax.reload();
}

function loadTable(){
    $.fn.dataTable.moment('DD/MM/YYYY');
    const table = $('#transacoes').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[ 3, "desc" ]],
        dom: 'lfBrtip',
        buttons: [{
            extend: 'print',
            text: 'imprimir',
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' )

                $(win.document.body).find('table')
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
            },
            exportOptions: {
                columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8]
            }
        }],
        //"cache": true,
        "ajax": {
            "method": 'get',
            "url": "./ajax/ajax_transacao.php?action=carregar_transacoes",
            "dataSrc": ""
        },
        "columnDefs": [
            { "targets": 0, "className":"nosort text-center"},
            {"targets":0, "width": "20px"}
          ],
        "columns": [
            {render: function(data, type, row ){
                return row.pagt == 1 ? `<i class="fab fa-cc-visa"></i><span style="display:none">Cartão</span>` : `<i class="fas fa-coins"></i><span style="display:none">Boleto</span>`
            }},
            { "data": "nome", render: function(nome){return `<div>${nome}</div>`}},
            { "data": "descricao"},
            { "data": "data_cadastro"},
            { "data": "valor_desconto", render: function(valor){
                return formata_dinheiro(valor);
            }},
            { "data": "valor_pago", render: function(valor){
                return formata_dinheiro(valor);
            }},
            { "data": "valor_liquido", render: function(valor){
                return formata_dinheiro(valor);
            }},
            { "data": "gateway", render: function(gateway){
                return gateway ? gateway : 'Compra direta'; //Se existia um gateway no momento da compra, ou não
            }},
            { "data": "cidade", render: function(data, type, row){
                return `${row.cidade}/${row.estado}`;
            }},
            { "data": "id_venda", render: function(id){
                return `<button class="btn btn-primary btn-xs" onClick="readVenda(${id})" title="Detalhes da transação" data-tooltip="tooltip" data-placement="top" data-toggle="modal" data-target="#modal-read-transaction"><i class="fas fa-eye"></i></button>`;
            }}
            ],

            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
     
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };
     
                // Total over all pages
                totalPago = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                    totalLiquido = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
     
                // Total over this page
     
                // Update footer
                $('.pago').html(`<strong>Total Pago: ${formata_dinheiro(totalPago)}</strong>`);
                $('.liquido').html(`<strong>Total Líquido: ${formata_dinheiro(totalLiquido)}</strong>`);
                
            }
    })

    $('#transacoes tbody').on('click', 'tr', function (e) {
        var data = table.row( this ).data();
        //ABRIR MODAL COM INFORMAÇÕES
    } );
}

function readVenda(id){
    id_venda = id;
    $.ajax({
        url: `./ajax/ajax_transacao.php?action=ler_transacao&id=${id_venda}`,
        dataType: 'json'
    }).done(res=>{
        if(res.success == 1){
            $('#read_modal_body').html(res.body);
        }
    })

}