/**
 * JAVASCRIPT FOR INFO ALUNO PAGE
 * THIS PAGE HAVE FUNCTIONS TO EDIT ALUNO'S INFO AND INSERT CLASS PLANS
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/
var ready = false;

$(document).ready(function(){
    //salva o id do aluno
    const id_usuario = $('#id').val();

    //flag para impedir que o usuário submeta o form enquanto estiver editando
    var edit = false;

    //iniciar informações da página
    initAluno(id_usuario)

    //carregar select de planos
    loadPlanos($("#select_plano"), null)

    //carregar os planos em que o aluno está vinculado
    loadTablePlanos(id_usuario)

    //trocar imagem quando selecionar
    $("#foto_perfil").change(function() {
        readURL(this, $('#img_preview'));
    });
    
    $('#select_pagt').change(function(){
        tipo = $(this).val();
        if(tipo == 0){
            $('.parcelas').hide();
        }else{
            $('.parcelas').show();
            
        }
    })
    $('#valor_plano').keyup(function(){
        valor = $(this).val();
        html=``;
        for(i=1; i<=12; i++){
            html+=`<option value=${i}>${i} x R$ ${valor/1?(valor/i).toFixed(2):0}</option>`;
        }
        $('#select_parcelas').html(html);
    })


    //tratamento de permissoes: desativar imagem de usuário
    $('.imgEnable').click(function(e){
        if(ready){
            if($(this).is(':checked')){
                changePermissoes('alterar_permissao_imagem', 'imgAtiva', 0, id_usuario);
            }
            else{
                changePermissoes('alterar_permissao_imagem', 'imgAtiva', 1, id_usuario);
            }
        }
    })

    //tratamento de permissoes: impedir que o usuário coloque imagens
    $('.imgPermission').click(function(e){
        if(ready){
            if($(this).is(':checked')){
                changePermissoes('alterar_permissao_imagem', 'imgPermissao', 0, id_usuario);
            }
            else{
                changePermissoes('alterar_permissao_imagem', 'imgPermissao', 1, id_usuario);
            }
        }
    })

    $("body").tooltip({ selector: '[data-toggle=tooltip]' })

    $(".valor").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: true,
    unmaskAsNumber: true,
    });

    $('.telefone').inputmask({ mask: ["(99) 9999-9999", "(99) 99999-9999"]});

    $('.aluno-input').click(function(){
        $('.atualizar-btn').show('collapse')
    })

	$('.aluno-input').blur(function() {
         if ($.trim(this.value) == ''){
			 this.value = (this.defaultValue ? this.defaultValue : '');
		 }
		 else{
			 $(this).val(this.value);
		 }
         edit = false;
        });

    

// TRATAMENTO DE ATUALIZAÇÃO DE DADOS CADASTRAIS //
    $("#form-atualizar").submit(function(e){
        e.preventDefault()
        if(!edit){
            let formData = $(this).serialize()
            $.ajax({
                method: 'post',
                url: './ajax/ajax_usuario.php',
                data: `${formData}&action=atualizar`,
                dataType: 'json'	
            }).done(res=>{
                success = responseTreatment(res, $('#update_error'), [], false, false)
                if(success){initAluno(id_usuario); $('.atualizar-btn').hide();}
            })
        }
    })

    $('#form-atualizar-img').submit(function(e){
        e.preventDefault();
        $('#id_user').val(id_usuario);
        var formData = new FormData(this);
        $.ajax({
            method: 'post',
            url: './ajax/ajax_usuario.php',
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function(result){
            success = responseTreatment(result, false, [], false, $('.profile-modal'))
            if(success) window.location.reload();
        })

    })
// END TRATAMENTO DE ATUALIZAÇÃO DE DADOS CADASTRAIS //

// TRATAMENTO DE VINCULO DE PLANOS //

    $("#form-add-plano").submit(function(e){
        e.preventDefault();
        let id_plano = $('#select_plano option:selected').val();
        let valor = $('#form-add-plano input[name="plano-valor"]').val();
        let pagt = $('#form-add-plano select[name=plano-pagt]').val();
        let parcelas = pagt == 1? $('#form-add-plano select[name=plano-parcelas]').val(): 1;

        let dadosVenda = {
            id_usuario: id_usuario,
            id_plano: id_plano,
            valor: valor,
            pagt: pagt,
            parcelas: parcelas,
            action: 'add'
        }
        console.log(dadosVenda);
        $.post('ajax/ajax_venda.php', dadosVenda, res => {
            success = responseTreatment(res, $('#add_error'), [reloadVendas], false, $('#modal_plano'))
        }, 'json')
    })
    // END TRATAMENTO DE VINCULO DE PLANOS //

    // TRATAMENTO DE ALTERAÇÃO DE SENHA //

    $('.senha').click(function(){
        $('.atualizar-pwd').show('collapse')
    })

    $('#cancelar-senha').click(function(){
        $('.atualizar-pwd').hide('collapse')
        $(".senha").val("")
    })

    $("#form-pwd").submit(function(e){
        e.preventDefault()
        let dados = {
            id: $('#form-pwd input[name="id_usuario"]').val(),
            senhaNew: $('#form-pwd input[name="senha-new"]').val(),
            senhaNewCfm: $('#form-pwd input[name="senha-new-cfm"]').val(),
            action: 'alterar_senha'
        }
    
        $.ajax({
            method: 'post',
            url: './ajax/ajax_usuario.php',
            data: dados,
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, $('#error_pwd'), [], false, false)
            if(success) {$(".senha").val(""); $('.atualizar-pwd').hide('collapse')}
        })
    })
    // END TRATAMENTO DE ALTERAÇÃO DE SENHA //
})

//carrega os planos disponíveis já cadastrados
function loadPlanos(element){
    $.ajax({
        url: './ajax/ajax_plano.php',
        data: {action: 'ler_todos'},    
        dataType: 'json'
    }).done(function(result){
        let options = "<option value=''>Escolha um Plano</option>"
        result.forEach(plano => {
            if(plano.duracao > 12){
                meses = plano.duracao % 12;
                anos = parseInt(plano.duracao/12)
                if(meses != 0){
                    options += `<option value='${plano.id}'>${plano.nome_plano} R$ ${plano.valor_desconto} ${anos + ' anos ' + meses + ' meses'}</option>`
                }else
                    options += `<option value='${plano.id}'>${plano.nome_plano} R$ ${plano.valor_desconto} ${anos + ' anos'}</option>`
            }else
                options += `<option value='${plano.id}'>${plano.nome_plano} R$ ${plano.valor_desconto} ${plano.duracao} meses</option>`
            
        })
        element.html(options)

        //ao escolher um plano o valor pode ser alterado
        element.change(function(){
            id_plano = element.val()
            if(id_plano == ""){
                $(".valorp").hide('collapse')
                $(".parcelas").hide('collapse')
                return
            }
            else{
                $.ajax({
                    url: './ajax/ajax_plano.php',
                    data: {id_plano: id_plano , action: "ler"},
                    dataType: 'json'
                }).done(function(res){
                    if(res.success == 1){
                        $(".valorp").show('collapse')
                        $('#form-add-plano #valor_plano').val(res.plano.valor_desconto)
                        html=``;
                        for(i=1; i<=12; i++){
                            html+=`<option value=${i}>${i} x R$ ${(res.plano.valor_desconto/i).toFixed(2)}</option>`;
                        }
                        $('#select_parcelas').html(html);
                    }
                })
            }
        })
    })

}

//CARREGA O DATATABLE DOS PLANOS VINCULADOS AO ALUNO
function loadTablePlanos(id_usuario){
    $.fn.dataTable.moment( 'DD-MM-YYYY HH:mm:ss' );
    $.fn.dataTable.moment('DD/MM/YYYY');
    $('#aluno_planos').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[ 2, "asc" ]],
        "ajax": {
            "method": 'post',
            "url": "./ajax/ajax_venda.php",
            "data": {action: 'carregar_planos_aluno', id: id_usuario},
            "dataSrc": ""
        },
        "columns": [
            { "data": "descricao",
            render: function ( data, type, row ) {
                var assinatura_descricao = row.assinatura_descricao ? '<br>' + row.assinatura_descricao : '';
                if(row.duracao > 12){
                    meses = row.duracao % 12;
                    anos = parseInt(row.duracao/12)
                    if(meses != 0){
                        return row.descricao + '<br>(' + anos + ' anos ' + meses + ' meses' + ')' + assinatura_descricao;
                    }else
                        return row.descricao + '<br>(' + anos + ' anos' + ')' + assinatura_descricao;
                }else
                    return row.descricao + ' (' + row.duracao + ' meses' + ')' + assinatura_descricao;
            }},
            { "data": "valor", 
                "render": function(valor){
                    if(valor != 0)
                        return `R$ ${valor}`
                    else
                        return `GRÁTIS`
                }},
            { "data": "data_inicio"},
            { "data": "data_final"},
            { "data": "progress",
            "className": "project_progress",
             "render": function(progress){
                if(progress >= 100){
                    return `<div class="progress">
                                <div class="progress-bar bg-red" role="progressbar" data-transitiongoal="${100}" style="width: ${100}%;" aria-valuenow="${100}">${100}%</div>
                            </div>
                            <small>Plano Expirado</small>`
                }else if(progress > 0 && progress < 100){
                    return `<div class="progress">
                                <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="${Math.round(progress)}" style="width: ${Math.round(progress)}%;" aria-valuenow="${Math.round(progress)}">${Math.round(progress)}%</div>
                            </div>
                            <small>Plano Atual</small>`
                }else{
                    return `<div class="status-symbol-next" data-toggle="tooltip" data-placement="top" title="Plano não Vigente"></div>
                            <small>Plano não Vigente</small>`
                }
                    
            }},
            { "data": "id",
                "render": function(id){
                    let tags = `<button class="btn btn-primary btn-xs" onClick="deletePlanoAluno(${id})" title="Excluir Plano" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash"></i> </button>`
                    return tags
                } },
            ]
    })
}

function changePermissoes(action, permissao, valor, id){
    $.ajax({
        method: 'post',
        url: './ajax/ajax_usuario.php',
        data: {action: action, permissao: permissao, valor: valor, id: id},
        dataType: 'json'	
    }).done(res=>{
        responseTreatment(res, false, [], false, false);
    })
}

//INICIA AS INFORMAÇÕES DO ALUNO
function initAluno(id){

    $.ajax({
        url: './ajax/ajax_usuario.php',
        data: {id_usuario: id, action: 'ler'},
        dataType: 'json'
    }).done(function (response) { 

        //inicia perfil lateral, com fotos e informações básicas
        initProfile(response)

        //BEGIN INIT INFO FIELDS
        $("#nome").prop('defaultValue', response.usuario.nome)
        $("#nome").val(response.usuario.nome)

        $("#cpf").prop('defaultValue', response.usuario.cpf)
        $("#cpf").val(response.usuario.cpf)

        $("#cep").prop('defaultValue', response.usuario.cep)
        $("#cep").val(response.usuario.cep)

        $("#email").prop('defaultValue', response.usuario.email)
        $("#email").val(response.usuario.email)

        loadEstadoCidade("#select_estado", "#select_cidade", [response.usuario.estado, response.usuario.cidade])

        let datanascimento = response.usuario.data_nascimento.split("-")
            datanascimento = `${datanascimento[2]}/${datanascimento[1]}/${datanascimento[0]}`

        $("#dataNascimento").prop('defaultValue', datanascimento)
        $("#dataNascimento").val(datanascimento)

        $("#telefone").prop('defaultValue', response.usuario.telefone)
        $("#telefone").val(response.usuario.telefone)

        $("#senha").prop('defaultValue', response.usuario.senha)
        $("#senha").val(response.usuario.senha)

        ready = true;
        //END

    })
}

function initProfile(res){

    image_profile = `./uploads/users/perfil/${res.usuario.image}`;

    $('#img-avatar').attr('src', image_profile);
    $('.img-perfil').attr('src', image_profile);
    $('#foto-atual').val(res.usuario.image);

    if(res.usuario.imgAtiva == 1){
        changeSwitchery($('.imgEnable'), false)
    }else{
        changeSwitchery($('.imgEnable'), true)
    }

    if(res.usuario.imgPermissao == 1){
        changeSwitchery($('.imgPermission'), false)
    }else{
        changeSwitchery($('.imgPermission'), true)
    }

    nome = res.usuario.nome.split(' ')
    nome.length > 1 ? $("#nome_perfil").html(`${nome[0]} ${nome[1]}`) : $("#nome_perfil").html(`${nome[0]}`);
    $("#cpf_perfil").html('<i class="fas fa-id-card user-profile-icon"></i> ' + res.usuario.cpf)
    $("#telefone_perfil").html('<i class="fa fa-phone user-profile-icon"></i> ' + res.usuario.telefone)
    $("#email_perfil").html('<i class="fa fa-envelope user-profile-icon"></i> ' + res.usuario.email)
}

function changeSwitchery(element, action){
    if(action == true){
        if(!element.is(':checked')){
            element.trigger('click');
        }
    }else{
        if(element.is(':checked')){
            element.trigger('click');
        }
    }
}

function loadEstadoCidade(idEstado, idCidade, adress){

    const selectEstado = $(idEstado)
    const selectCidade = $(idCidade)
    $.ajax({
        method: 'get',
        url: './js/json/estados-cidades.json',
        dataType: 'json'
    }).done(function(result){
        
        let options = ""
        options = "<option value=''>Selecione um estado</option>";

        result.estados.forEach(estado => {
            options += '<option value = "'+estado.sigla+'">'+estado.nome+'</option>'
        })
        selectEstado.html(options)

        selectEstado.change(function(){
            if(selectEstado.val() == 0){
                selectEstado.closest('.form-group').addClass('has-error');
                selectEstado.closest('.form-group').removeClass('has-success');
                selectCidade.closest('.form-group').addClass('has-error');
                selectCidade.closest('.form-group').removeClass('has-success');
                return
            }else{
                selectEstado.closest('.form-group').removeClass('has-error');
                selectEstado.closest('.form-group').addClass('has-success');
            }
            var estado = result.estados.find(function(estado){
                return selectEstado.val() === estado.sigla;
            })
            
            $("#iestado").val(estado.sigla)
            let options = ""
            options = "<option value=''>Selecione uma cidade</option>";

            $.each(estado.cidades, function(key, val){
                    options += "<option value='" + val + "'> " + val + "</option>";
            });

            selectCidade.html(options)
            selectCidade.show()
        })
        
        selectCidade.change(function(){
            $("#icidade").val(selectCidade.val())
        })

        changeIt(adress[0], adress[1])
        
    })

}

function reloadVendas(){
    $("#aluno_planos").DataTable().ajax.reload()
}

function deletePlanoAluno(id){
    let confirmed = confirm("Deseja remover este Plano?")
    if(confirmed == true){

        $.ajax({
            method: 'post',
            url: './ajax/ajax_venda.php',
            data: {id_venda: id, action: 'deletar_venda'},
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, false, [reloadVendas], false, false)
        })
    }
}

function changeIt(estado, cidade){ 
    $('#select_estado').val(estado).trigger('change');
    $('#select_cidade').val(cidade).trigger('change');
}