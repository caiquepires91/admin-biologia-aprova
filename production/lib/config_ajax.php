<?php
include_once __DIR__ . "/config.php";

function send_response ($response, $status = "success") {
	$response["status"] = $status;

	echo json_encode($response);
	exit();
}

function myErrorHandler ($errno, $errstr) {
	$data = array();
    $data["msg"] = $errstr;

	switch ($errno) {
		case E_USER_ERROR:
			send_response($data, "error");
			break;

		case E_USER_WARNING:
			send_response($data, "warning");
			break;

		case E_USER_NOTICE:
			send_response($data, "notice");
			break;

		default:
			send_response("", "unknown");
			break;
	}
}

set_error_handler("myErrorHandler", E_USER_ERROR | E_USER_WARNING | E_USER_NOTICE);

//Exemplo
//trigger_error("Usuário ($login) ou senha inválido(s).", E_USER_ERROR);

?>