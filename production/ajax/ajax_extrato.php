<?php 
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'carregar_extratos':
            carregar_extratos();
            break;

        case 'carregar_extratos_filtro':
            carregar_extratos_filtro();
        break;

        case 'ver_saldos':
            ver_saldos();
        break;

        case 'ver_transacao':
            ver_transacao();
        break;

        case 'calcula_balancos':
            calcula_balancos();
        break;

        case 'saldo_disponivel_por_gateway':
            saldo_disponivel_por_gateway();
        break;

        case 'saldo_disponivel':
            saldo_disponivel();
        break;
    }
}

//--------metodos post-------------//
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'saque':
            saque();
        break;
    }
}

function carregar_extratos(){
    global $mysqli;
    /**
     * Carrega as transações que ja liberaram dinheiro
     * 0 - venda
     * 1 - débito
     */
    $retorno = array();
        
    //carregar boletos cujo o dinheiro já foi liberado
    $direta = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo,t.valor_liquido ,DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t WHERE t.tipo_transacao = 'plano' AND t.id_gateway IS NULL AND t.data_cadastro BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0;");
    while($row = $direta->fetch_assoc()){
        $row['n_parcelas'] = NULL;
        $row['gateway'] = 'Compra direta';
        $retorno[] = $row;
    }
    /*compra direta parcelas
    $direta_parcelas = $mysqli->query(
        "SELECT p.id_venda, p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento, p.valor as valor_liquido from parcelas as p INNER JOIN transacao as t ON t.id = p.id_venda WHERE t.id_gateway is NULL AND p.data_recebimento BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0;");
    while($row = $direta_parcelas->fetch_assoc()){
        $row['gateway'] = 'Compra direta';
        $retorno[] = $row;
    }
    */

    $boletos = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido,g.nome as gateway ,DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0;");
    while($row = $boletos->fetch_assoc()){
        $row['n_parcelas'] = NULL;
        $retorno[] = $row;
    }
    //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado e a taxa é fixa
    $cartao_a_vista = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0");
    while($row = $cartao_a_vista->fetch_assoc()){
        $row['n_parcelas'] = NULL;
        $retorno[] = $row;
    }

    //todas as transacoes de cartao com gateway parcelada e com taxa fixa
    $parcela_tx_fixa = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0");
    while($row = $parcela_tx_fixa->fetch_assoc()){
        $retorno[] = $row;
    }
    //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
    $parcelas_tx_nao_fixa = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo, g.nome as gateway, p.valor as valor_liquido, p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento FROM transacao as t INNER JOIN parcelas as p ON p.id_venda = t.id INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 WHERE t.tipo_transacao = 'plano' AND p.data_recebimento BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0");
    while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
        $retorno[] = $row;
    }

    $debitos = $mysqli->query(
        "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, t.tipo_transacao, DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t INNER JOIN gateway as g on g.id = t.id_gateway WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND t.deletado = 0");
    while($row = $debitos->fetch_assoc()){
        $row['n_parcelas'] = NULL;
        $row['valor_liquido'] *= -1;
        $retorno[] = $row;
    }
    echo json_encode($retorno);
    exit;
}

function ver_transacao(){
    global $mysqli;

    try{
    $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING); //$_GET['id'];
    $tipo_transacao = filter_input(INPUT_GET, 'tipo', FILTER_SANITIZE_STRING); //$_GET['tipo'];

    if($tipo_transacao == 'plano'){
        $busca = $mysqli->query(
            "SELECT u.nome, u.cidade, u.estado, u.id, v.descricao,v.duracao, v.n_parcelas, v.id as id_venda, v.tipo_pagamento as pagt, v.valor_liquido, v.valor as valor_pago, DATE_FORMAT(v.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_cadastro, p.valor_desconto, g.nome as gateway, g.id as id_gateway 
                FROM transacao as v 
                inner join usuario as u ON v.id_usuario = u.id inner JOIN plano as p ON v.id_plano = p.id LEFT JOIN gateway as g ON v.id_gateway = g.id 
            WHERE v.id = $id"
            );
        if($row = $busca->fetch_assoc()){
            extract($row);

            $_valor = $valor_liquido;
            $valor_desconto = formata_dinheiro($valor_desconto);
            $valor_liquido = formata_dinheiro($valor_liquido);
            $valor_pago = formata_dinheiro($valor_pago);

            if($gateway == NULL){
                $tipo = $pagt == 1 ? $n_parcelas > 1 ? "Cartão $n_parcelas x" : "Cartão à vista" : "Dinheiro";
                $gateway = "Compra Direta";
                $gatewayIsNull = true;
            }else{
                $tipo = $pagt == 1 ? $n_parcelas > 1 ? "Cartão $n_parcelas x" : "Cartão à vista" : "Boleto";
                $gatewayIsNull = false;
            }

            $gateways = $mysqli->query("SELECT nome, id FROM gateway");

            //gerando HTML
            $html = "<table class=\"table jambo_table table-bordered\">
            <caption class=\"table-caption\">Dados Básicos</caption>
            <thead>
                <tr>
                    <th>Nome do Usuário</th>
                    <th>Descrição</th>
                    <th>PAGT</th>
                    <th>Data</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>$nome</td>
                    <td>$descricao</td>
                    <td>$tipo</td>
                    <td>$data_cadastro</td>
                </tr>
            </tbody>
        </table>
        <table class=\"table jambo_table table-bordered\">
                <caption class=\"table-caption\">Valores da Transação</caption>
                <thead>
                    <tr>
                        <th>Valor plano</th>
                        <th>Valor pago</th>
                        <th>Valor líquido</th>
                        <th>Gateway</th>
                    </tr>
                </thead>
                <tbody>
                        <tr>
                            <td>$valor_desconto</td>
                            <td>$valor_pago</td>
                            <td>$valor_liquido</td>
                            <td>$gateway</td>
                        </tr>
                    </tbody>
            </table>";
            
            if(!$gatewayIsNull && ($pagt == 1 && $n_parcelas > 1)){
                $html .= "<table class=\"table table-striped jambo_table table-bordered\">
            <caption class=\"table-caption\">Parcelas</caption>
            <thead>
                <tr>
                    <th>Periodo</th>
                    <th>Valor</th>
                    <th>Juros</th>
                    <th>Data</th>
                </tr>
            </thead>
            <tbody>";

            $parcelas = $mysqli->query(
                "SELECT periodo, valor, juros, DATE_FORMAT(data_recebimento, '%d/%m/%Y') as data_recebimento 
                    FROM parcelas  
                WHERE id_venda = $id_venda;"
                );
            while($parcela = $parcelas->fetch_assoc()){
                extract($parcela);
                $valor = formata_dinheiro($valor);
                $html .= "<tr>
                            <td>$periodo x </td>
                            <td>$valor</td>
                            <td>$juros %</td>
                            <td>$data_recebimento</td>
                        </tr>";
                }
            $html .= " </tbody></table>";

            }else if($gatewayIsNull && ($pagt == 1 && $n_parcelas > 1)){
                $html .= "<table class=\"table table-striped jambo_table table-bordered\">
            <caption class=\"table-caption\">Parcelas</caption>
            <thead>
                <tr>
                    <th>Periodo</th>
                    <th>Valor</th>
                </tr>
            </thead>
            <tbody>";
            $parcela = formata_dinheiro($_valor / $n_parcelas);
            
            for($i=1;$i<=$n_parcelas;$i++){
                $html .= "<tr>
                            <td>$i x </td>
                            <td>$parcela</td>
                        </tr>";
            }
            $html .= " </tbody></table>";
            }
        }
    }else{
        $busca = $mysqli->query(
            "SELECT u.nome, t.descricao ,t.valor_liquido, DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data 
                FROM transacao as t
                INNER JOIN usuario as u ON t.id_usuario = u.id
            WHERE t.id = $id"
            );
        if($row = $busca->fetch_assoc()){
            extract($row);
            $valor_liquido = formata_dinheiro($valor_liquido);
            $html = "<table class=\"table jambo_table table-bordered\">
            <caption class=\"table-caption\">Resumo</caption>
            <thead>
                <tr>
                    <th>Nome do usuário</th>
                    <th>Descrição</th>
                    <th>Valor</th>
                    <th>Data</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>$nome</td>
                    <td>$descricao</td>
                    <td>$valor_liquido</td>
                    <td>$data</td>
                </tr>
            </tbody>
        </table>";
        }
    }
        echo json_encode(array('status'=>'success', 'body'=>$html));
    }catch(Exception $e){
        echo json_encode(array('status'=>'error', 'msg'=>$e->getMessage()));
    }  
}

function ver_saldos(){
    global $mysqli;

    $retorno = array('valor_recebido'=>0, 'valor_disponivel'=>0);
        
    //carregar boletos cujo o dinheiro já foi liberado
    $boletos = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as valor 
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW()");
    if($row = $boletos->fetch_assoc()){
        $retorno['valor_recebido'] += $row['valor'];
    }
    //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado
    $cartao_a_vista = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as valor 
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW()");
    if($row = $cartao_a_vista->fetch_assoc()){
        $retorno['valor_recebido'] += $row['valor'];
    }
    //carregar cartao varias parcelas cujo o dinheiro ja foi liberado
    $parcelas = $mysqli->query(
        "SELECT SUM(p.valor) as valor 
            FROM transacao as t 
            INNER JOIN parcelas as p ON p.id_venda = t.id 
        WHERE p.data_recebimento <= NOW()");
    if($row = $parcelas->fetch_assoc()){
        $retorno['valor_recebido'] += $row['valor'];
    }

    $debitos = $mysqli->query(
        "SELECT -1*SUM(valor_liquido) as valor 
            FROM transacao 
        WHERE (tipo_transacao = 'saque' OR tipo_transacao = 'despesa') and data_cadastro <= NOW()");
    if($row = $debitos->fetch_assoc()){
        $retorno['valor_disponivel'] = $retorno['valor_recebido'] + $row['valor'];
    }
    echo json_encode($retorno);
}

function carregar_extratos_filtro(){
    global $mysqli;
    
    $data_inicial = filter_input(INPUT_GET, 'data_inicial', FILTER_SANITIZE_STRING);
    $data_final = filter_input(INPUT_GET, 'data_final', FILTER_SANITIZE_STRING);
    $id_gateway = filter_input(INPUT_GET, 'id-gateway', FILTER_SANITIZE_NUMBER_INT);
    $datas = valida_data($data_inicial, $data_final);

    extract($datas);
    $retorno = array();
    /*$direta = $mysqli->query(
            "SELECT t.id, t.valor_liquido ,t.data_cadastro as data_recebimento FROM transacao as t WHERE t.tipo_transacao = 'plano' AND t.id_gateway IS NULL AND t.n_parcelas <=1 AND t.data_cadastro <= NOW() AND t.data_cadastro BETWEEN '2019-01-01' AND '2050-01-01';");
        while($row = $direta->fetch_assoc()){
            $retorno[] = $row;
        }*/

    if($id_gateway === '0'){
        if($data_inicial && $data_final){
            //compra direta a vista e cartao 1 x
            $direta = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido ,DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t WHERE t.tipo_transacao = 'plano' AND t.id_gateway IS NULL AND t.data_cadastro <= NOW() AND t.data_cadastro BETWEEN '$inicio' AND '$final' AND t.deletado = 0;");
            while($row = $direta->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            /*compra direta parcelas
            $direta_parcelas = $mysqli->query(
                "SELECT p.id_venda, p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento, p.valor as valor_liquido from parcelas as p INNER JOIN transacao as t ON t.id = p.id_venda WHERE t.id_gateway is NULL AND p.data_recebimento <= NOW() AND p.data_recebimento BETWEEN '$inicio' AND '$final' AND t.deletado = 0;");
            while($row = $direta_parcelas->fetch_assoc()){
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            */
            echo json_encode($retorno);
                exit;
        }else{
            //compra direta a vista e cartao 1 x
            $direta = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido ,DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t WHERE t.tipo_transacao = 'plano' AND t.id_gateway IS NULL AND t.data_cadastro <= NOW() AND t.deletado = 0;");
            while($row = $direta->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            /*compra direta parcelas
            $direta_parcelas = $mysqli->query(
                "SELECT p.id_venda, p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento, p.valor as valor_liquido from parcelas as p INNER JOIN transacao as t ON t.id = p.id_venda WHERE t.id_gateway is NULL AND p.data_recebimento <= NOW() AND t.deletado = 0;");
            while($row = $direta_parcelas->fetch_assoc()){
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            */
            echo json_encode($retorno);
                exit;
        }
    }else if($id_gateway){
        if($data_inicial && $data_final){
            $boletos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido,g.nome as gateway ,DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final' AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $boletos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado e a taxa é fixa
            $cartao_a_vista = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final' AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $cartao_a_vista->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }

            //todas as transacoes de cartao com gateway parcelada e com taxa fixa
            $parcela_tx_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final' AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $parcela_tx_fixa->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
            $parcelas_tx_nao_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, g.nome as gateway, p.valor as valor_liquido,p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento FROM transacao as t INNER JOIN parcelas as p ON p.id_venda = t.id INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 WHERE t.tipo_transacao = 'plano' AND p.data_recebimento <= NOW() AND p.data_recebimento BETWEEN '$inicio' AND '$final' AND g.id = $id_gateway AND t.deletado = 0");
            while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
                $retorno[] = $row;
            }

            $debitos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, t.tipo_transacao, DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t INNER JOIN gateway as g on g.id = t.id_gateway WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro <= NOW() AND t.data_cadastro BETWEEN '$inicio' AND '$final' AND g.id = $id_gateway AND t.deletado = 0");
            while($row = $debitos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['valor_liquido'] *= -1;
                $retorno[] = $row;
            }
            echo json_encode($retorno);
            exit;
        }else{
            $boletos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido,g.nome as gateway ,DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $boletos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado e a taxa é fixa
            $cartao_a_vista = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $cartao_a_vista->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }

            //todas as transacoes de cartao com gateway parcelada e com taxa fixa
            $parcela_tx_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0;");
            while($row = $parcela_tx_fixa->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
            $parcelas_tx_nao_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, g.nome as gateway,p.periodo, t.n_parcelas, p.valor as valor_liquido, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento FROM transacao as t INNER JOIN parcelas as p ON p.id_venda = t.id INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 WHERE t.tipo_transacao = 'plano' AND p.data_recebimento <= NOW() AND g.id = $id_gateway AND t.deletado = 0");
            while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
                $retorno[] = $row;
            }

            $debitos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, t.tipo_transacao, DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t INNER JOIN gateway as g on g.id = t.id_gateway WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro <= NOW() AND g.id = $id_gateway AND t.deletado = 0");
            while($row = $debitos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['valor_liquido'] *= -1;
                $retorno[] = $row;
            }
            echo json_encode($retorno);
            exit;
        }
    }else{
        //NAO FOI SELECIONADO GATEWAY, FILTRO APENAS NA DATA
        if($data_inicial && $data_final){
            $direta = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido ,DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t WHERE t.tipo_transacao = 'plano' AND t.id_gateway IS NULL AND t.data_cadastro <= NOW() AND t.data_cadastro BETWEEN '$inicio' AND '$final';");
            while($row = $direta->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            /*compra direta parcelas
            $direta_parcelas = $mysqli->query(
                "SELECT p.id_venda, p.periodo, t.n_parcelas, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento, p.valor as valor_liquido from parcelas as p INNER JOIN transacao as t ON t.id = p.id_venda WHERE t.id_gateway is NULL AND p.data_recebimento <= NOW() AND p.data_recebimento BETWEEN '$inicio' AND '$final';");
            while($row = $direta_parcelas->fetch_assoc()){
                $row['gateway'] = 'Compra direta';
                $retorno[] = $row;
            }
            */

            $boletos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido,g.nome as gateway ,DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final';");
            while($row = $boletos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado e a taxa é fixa
            $cartao_a_vista = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final'");
            while($row = $cartao_a_vista->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }

            //todas as transacoes de cartao com gateway parcelada e com taxa fixa
            $parcela_tx_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, DATE_FORMAT(DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY), '%d/%m/%Y %H:%i:%s') as data_recebimento 
                    FROM transacao as t 
                    INNER JOIN gateway as g ON t.id_gateway = g.id 
                WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) BETWEEN '$inicio' AND '$final'");
            while($row = $parcela_tx_fixa->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $retorno[] = $row;
            }
            //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
            $parcelas_tx_nao_fixa = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, g.nome as gateway,p.periodo, t.n_parcelas, p.valor as valor_liquido, DATE_FORMAT(p.data_recebimento, '%d/%m/%Y') as data_recebimento FROM transacao as t INNER JOIN parcelas as p ON p.id_venda = t.id INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 WHERE t.tipo_transacao = 'plano' AND p.data_recebimento <= NOW() AND p.data_recebimento BETWEEN '$inicio' AND '$final'");
            while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
                $retorno[] = $row;
            }

            $debitos = $mysqli->query(
                "SELECT t.id,t.tipo_transacao as tipo, t.valor_liquido, g.nome as gateway, t.tipo_transacao, DATE_FORMAT(t.data_cadastro, '%d/%m/%Y %H:%i:%s') as data_recebimento FROM transacao as t INNER JOIN gateway as g on g.id = t.id_gateway WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro <= NOW() AND t.data_cadastro BETWEEN '$inicio' AND '$final'");
            while($row = $debitos->fetch_assoc()){
                $row['n_parcelas'] = NULL;
                $row['valor_liquido'] *= -1;
                $retorno[] = $row;
            }
            echo json_encode($retorno);
            exit;
        }else{
            carregar_extratos();
            exit;
        }
    }
}

function valida_data($data_inicial, $data_fim){

    $datas = array('inicio'=>null, 'final'=>null);

    if($data_inicial == false && $data_fim == false) return $datas;
    if($data_inicial == null && $data_fim != null) return $datas;
    if($data_inicial != null && $data_fim == null) 
    {
        $datainicial = explode("/",$data_inicial);
        $res1 = $datainicial[0] ? checkdate($datainicial[1],$datainicial[0],$datainicial[2]): null;
        if ($res1 == 1){
            $datas['inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_inicial)));
            return $datas;
        } else {
           return $datas;
        }
    }

    $datainicial = explode("/",$data_inicial); // fatia a string $dat em pedados, usando / como referência
    $datafim = explode("/",$data_fim);

    $res1 = $datainicial[0] ? checkdate($datainicial[1],$datainicial[0],$datainicial[2]): null;
    $res2 = $datafim[0]? checkdate($datafim[1],$datafim[0],$datafim[2]) :null;
	// verifica se a data é válida!
	// 1 = true (válida)
    // 0 = false (inválida)

	if ($res1 == 1 && $res2 == 1){
        $datas['inicio'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_inicial)));
        $datas['final'] = date('Y-m-d', strtotime(str_replace('/', '-', $data_fim)));
        return $datas;
	} else {
	   return $datas;
	}
}

function calcula_balancos(){
    global $mysqli;
    $balanco = array('entrada_geral'=>0.0, 'saida_geral'=>0.0, 'total_geral'=>0.0);
    
    //CALCULO BALANÇO GERAL TRANSAÇÕES COM GATEWAY
    $boletos = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as total
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW() AND t.deletado = 0"
        );
    if($row = $boletos->fetch_assoc()){
        $balanco['entrada_geral'] += (float) $row['total'];
    }
    //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado
    $cartao_a_vista = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as total
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND t.deletado = 0");
    if($row = $cartao_a_vista->fetch_assoc()){
        $balanco['entrada_geral'] += (float) $row['total'];
    }

    $parcela_tx_fixa = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as total
            FROM transacao as t 
            INNER JOIN gateway as g ON t.id_gateway = g.id 
        WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND t.deletado = 0");
    while($row = $parcela_tx_fixa->fetch_assoc()){
        $balanco['entrada_geral'] += (float) $row['total'];
    }
    //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
    $parcelas_tx_nao_fixa = $mysqli->query(
        "SELECT SUM(p.valor) as total
            FROM transacao as t 
            INNER JOIN parcelas as p ON p.id_venda = t.id 
            INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 
        WHERE t.tipo_transacao = 'plano' AND p.data_recebimento <= NOW() AND t.deletado = 0");
    while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
        $balanco['entrada_geral'] += (float) $row['total'];
    }

    $debitos = $mysqli->query(
        "SELECT SUM(t.valor_liquido) as total
            FROM transacao as t 
            INNER JOIN gateway as g on g.id = t.id_gateway 
        WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro <= NOW() AND t.deletado = 0"
        );
    if($row = $debitos->fetch_assoc()){
        $balanco['saida_geral'] += (float) $row['total'];
    }
    $balanco['total_geral'] = $balanco['entrada_geral'] - $balanco['saida_geral'];

    echo json_encode($balanco);
}

function saldo_disponivel_por_gateway($id_gateway = null){
        global $mysqli;

        if(!$id_gateway)
            if(!$id_gateway = filter_input(INPUT_GET, 'id_gateway', FILTER_SANITIZE_NUMBER_INT)){
                die(json_encode(array('success'=>0, 'msg'=>'Gateway inválido.')));
            }    

        $entradas = 0.0;
        $saidas = 0.0;
        $total = 0.0;
        
        //CALCULO BALANÇO GERAL TRANSAÇÕES COM GATEWAY
        $boletos = $mysqli->query(
            "SELECT SUM(t.valor_liquido) as total
                FROM transacao as t 
                INNER JOIN gateway as g ON t.id_gateway = g.id 
            WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 0 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_boleto DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0"
            );
        if($row = $boletos->fetch_assoc()){
            $entradas += (float) $row['total'];
        }
        //carregar cartoes 1 parcela cujo o dinheiro ja foi liberado
        $cartao_a_vista = $mysqli->query(
            "SELECT SUM(t.valor_liquido) as total
                FROM transacao as t 
                INNER JOIN gateway as g ON t.id_gateway = g.id 
            WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas = 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0");
        if($row = $cartao_a_vista->fetch_assoc()){
            $entradas += (float) $row['total'];
        }
    
        $parcela_tx_fixa = $mysqli->query(
            "SELECT SUM(t.valor_liquido) as total
                FROM transacao as t 
                INNER JOIN gateway as g ON t.id_gateway = g.id 
            WHERE t.tipo_transacao = 'plano' and t.tipo_pagamento = 1 AND t.n_parcelas > 1 AND DATE_ADD(t.data_cadastro, INTERVAL g.liberacao_cartao DAY) <= NOW() AND g.id = $id_gateway AND t.deletado = 0");
        while($row = $parcela_tx_fixa->fetch_assoc()){
            $entradas += (float) $row['total'];
        }
        //PARCELAS COM taxa do gateway nao fixa, ou parcelas de compra direta
        $parcelas_tx_nao_fixa = $mysqli->query(
            "SELECT SUM(p.valor) as total
                FROM transacao as t 
                INNER JOIN parcelas as p ON p.id_venda = t.id 
                INNER JOIN gateway as g ON t.id_gateway = g.id and g.isTaxaFixa != 1 
            WHERE t.tipo_transacao = 'plano' AND p.data_recebimento <= NOW() AND g.id = $id_gateway AND t.deletado = 0");
        while($row = $parcelas_tx_nao_fixa->fetch_assoc()){
            $entradas += (float) $row['total'];
        }
    
        $debitos = $mysqli->query(
            "SELECT SUM(t.valor_liquido) as total
                FROM transacao as t 
                INNER JOIN gateway as g on g.id = t.id_gateway 
            WHERE (t.tipo_transacao = 'saque' OR t.tipo_transacao = 'despesa') AND t.data_cadastro <= NOW() AND g.id = $id_gateway AND t.deletado = 0"
            );
        if($row = $debitos->fetch_assoc()){
            $saidas += (float) $row['total'];
        }
        $total = $entradas - $saidas;
        if(isset($_GET['id_gateway']))
            echo $total;
        else
            return $total;
}

function saldo_disponivel(){
    global $mysqli;

    $result = $mysqli->query("SELECT id, nome FROM gateway WHERE deletado = 0;");
    $saldos = array();
    while($row = $result->fetch_array()){
        $saldos[] = array('gateway'=>$row['nome'], 'saldo'=>saldo_disponivel_por_gateway($row['id']));
    }
    echo json_encode(array('success'=>1, 'saldos'=>$saldos));
}

function saque(){

    global $mysqli;
    $valor = filter_input(INPUT_POST, 'valor', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $descricao = filter_input(INPUT_POST, 'descricao', FILTER_SANITIZE_STRING);
    $data = date('Y-m-d H:i:s');
    $id_gateway = filter_input(INPUT_POST, 'id_gateway', FILTER_SANITIZE_NUMBER_INT);
    $id_usuario = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);
    
    $disponivel = saldo_disponivel_por_gateway($id_gateway);

    if(($disponivel - $valor) < 0){
        die(json_encode(array("success"=>0, "msg"=>"Saldo insuficiente para o gateway selecionado. Valor disponível para saque R$ ".formata_dinheiro($disponivel))));
    }

    try{
        if(!$descricao){
            $descricao = "Saque pelo sistema";
            $msg = "Saque cadastrado";
            $tipo_transacao = 'saque';
         }else{
             $msg = "Despesa cadastrada";
             $tipo_transacao = 'despesa';
         } //SE NÃO HOUVER DECRICAO, ENTÃO SE TRATA DE UMA DESPESA

            $stmt = $mysqli->prepare("INSERT INTO transacao(id,id_usuario,id_gateway,descricao,valor,valor_liquido,data_cadastro,tipo_transacao) VALUES(default, ? , ? , ? , ? , ? , ? , ?)");
            $stmt->bind_param("iisddss", $id_usuario , $id_gateway , $descricao , $valor , $valor , $data, $tipo_transacao);
            $stmt->execute();

            $_valor = formata_dinheiro($valor);
        echo json_encode(array("success" => 1, "msg" => "$msg no valor de R$ $_valor"));

    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=>$e->getMessage())));
    }
}

function cadastra_saque(){

}

?>