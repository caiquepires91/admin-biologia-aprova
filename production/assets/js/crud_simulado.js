/**
 * JAVASCRIPT DA PAGINA DE SIMULADO
 * 
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/
$(document).ready(function(){
    loadTable();
    loadCategorias($("#select_categoria"), null);

    // VALIDAÇÕES E INICIAÇLIZAÇÕES
    $('.data').inputmask({mask: '99/99/9999'})

    $('#form-simulado').submit(function(event){ //TRATAMENTO DE CADASTRO DE SIMULADOS
        event.preventDefault()
        let dados = {
            nome: $("#form-simulado input[name='nome-sim']").val(),
            id_categoria: $("#form-simulado select[name='id-categoria']").val(),
            data_liberacao: $("#form-simulado input[name='data-liberacao']").val(),
            data_final: $("#form-simulado input[name='data-final']").val(),
            action: 'novo'
        }

        $.ajax({
            method: 'post',
            url: './ajax/ajax_simulado.php',
            data: dados,
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, $("#error_cadastro"), [reloadSimulados], false, $('#modal_cadastro'))
        })
    })
    
    $('#form-atualizar-simulado').submit(function(event){ // TRATAMENTO DE ATUALIZAÇÃO DE SIMULADOS
        event.preventDefault()

        let dados = {
            nome: $("#form-atualizar-simulado input[name='nome-sim']").val(),
            id_categoria: $("#form-atualizar-simulado select[name='id-categoria']").val(),
            data_liberacao: $("#form-atualizar-simulado input[name='data-liberacao']").val(),
            data_final: $("#form-atualizar-simulado input[name='data-final']").val(),
            id: $("#form-atualizar-simulado input[name='id_simulado']").val(),
            action: 'atualizar'
        }

        $.ajax({
            method: 'post',
            url: './ajax/ajax_simulado.php',
            data: dados,
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, $("#uerror_cadastro"), [reloadSimulados], false, $('#update_modal'))
        })
    })
})

function loadTable(){
    const table = $('#simulados').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[2, "desc"]],
        "ajax": {
            "url": "./ajax/ajax_simulado.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "nome"},
            { "data": "categoria_nome" },
            { "data": "concluidos"},
            { "data": "data_cadastro"},
            { "data": "data_liberacao"},
            { "data": "data_final"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <a class="btn btn-primary btn-xs" href="./simulado.php?id=${id}" title="Detalhes do Simulado" data-toottip="tooltip" data-placement="top"><i class="fas fa-eye"></i></a>
                    <button class="btn btn-primary btn-xs" onClick="updateSimulado(${id})" data-toggle="modal" data-target="#update_modal" title="Editar" data-tootip="tooltip" data=placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteSimulado(${id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function reloadSimulados(){
    $('#simulados').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updateSimulado(id){
    const nome = $("#unome-sim")
    const data = $("#udata_liberacao")
    const dataF = $("#udata_final")
    const hiddenid = $("#hidden_sim_id")
    hiddenid.val(id);
    $.ajax({
        url: './ajax/ajax_simulado.php',
        data: {id: id, action: "ler"},
        dataType: 'json'
    }).done(function(response){
        if(response.success == 1){
            loadCategorias($("#uselect_categoria"), response.simulado.id_categoria);
            nome.val(response.simulado.nome);
            data.val(response.simulado.data_liberacao);
            dataF.val(response.simulado.data_final);
        }else{
            new PNotify({
                title: 'Falha ao atualizar!',
                text: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
            //reloadCategorias()
        }
    })
}

function deleteSimulado(id){
    let confirmed = confirm("Deseja remover este Simulado?")
    if(confirmed == true){

        $.ajax({
            method: 'post',
            url: './ajax/ajax_simulado.php',
            data: {id: id, action: "deletar"},
            dataType: 'json'	
        }).done(res=>{
            success = responseTreatment(res, false, [reloadSimulados], false, false)
        })
    }

}

function loadCategorias(element, categoria){
    $.ajax({
        url: './ajax/ajax_categoria.php',
        data: {action: 'ler_todos'},
        dataType: 'json'
    }).done(function(result){
        let options = "<option value='0'>Selecione uma Categoria</option>"
        result.forEach(categoria => {
            options += `<option value='${categoria.id}'>${categoria.nome}</option>`
        })
        element.html(options)
        if(categoria)
            element.val(categoria)
    })

}