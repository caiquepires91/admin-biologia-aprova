<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;
    $plano = getPlano();
    try{
        $query = "INSERT INTO `plano`(`id`, `nome_plano`,`valor_total`,`valor_desconto`, `duracao`, `data_cadastro`) 
        VALUES (default,?,?,?,?,?)"; 
        $stmt_result = $mysqli->prepare($query);   
        $stmt_result->bind_param("sddis", $plano['nome'],$plano['valorT'],$plano['valorD'], $plano['duracao'], $plano['data_cadastro']);
        $stmt_result->execute();
        echo json_encode(array("success" => 1, "msg" => "plano registrado com sucesso!"));
    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function atualizar(){
    global $mysqli;
    $plano = getPlano();
    $id = $_POST['id_plano'];
    extract($plano);

    $update = "UPDATE plano SET nome_plano = '$nome', valor_total = '$valorT', valor_desconto = '$valorD', duracao = '$duracao' WHERE id = $id";
        if($mysqli->query($update)){
            if($mysqli->affected_rows > 0){
                echo json_encode(array("success" => 1, "msg" => "Registro atualizado com sucesso!"));
            }else{
                echo json_encode(array("success" => 2, "msg" => "Não houve alterações no registro!"));
            }
        }else{
            echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
        }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id_plano'];
    $delete = "UPDATE plano set deletado = 1 WHERE id = $id";
    if($mysqli->query($delete)){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Plano removido com sucesso!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Plano não encontrado, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 0, "msg" => "Erro ao remover Plano!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id_plano'];
    $select = "SELECT plano.nome_plano, plano.valor_total, plano.valor_desconto, plano.duracao, DATE_FORMAT(plano.data_cadastro, '%d/%m/%Y') as data_cadastro
        FROM plano WHERE id = $id";

    if($result = $mysqli->query($select)){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => "1", "plano" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "plano não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    $select = "SELECT plano.nome_plano, plano.valor_total, plano.valor_desconto, plano.duracao, DATE_FORMAT(plano.data_cadastro, '%d/%m/%Y') as data_cadastro, plano.id FROM plano WHERE deletado = 0";
    $planos = array();
    if($result = $mysqli->query($select)){
        while($row = $result->fetch_assoc()){
            $planos[] = $row;
        }
        echo json_encode($planos);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

//-----------FUNCOES AUXILIARES --------------//
function getPlano(){
    $plano['nome'] = filter_input(INPUT_POST, 'plano-nome', FILTER_SANITIZE_STRING);//$_POST['plano-nome'];
    $plano['valorT'] = filter_input(INPUT_POST, 'plano-valor-total', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);//$_POST['plano-valor-total'];
    $plano['valorD'] = filter_input(INPUT_POST, 'plano-valor-desconto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);//$_POST['plano-valor-desconto'];
    $plano['duracao'] = filter_input(INPUT_POST, 'plano-duracao', FILTER_SANITIZE_NUMBER_INT);//$_POST['plano-duracao'];
    $plano['dim'] = filter_input(INPUT_POST, 'plano-duracao-dim', FILTER_SANITIZE_NUMBER_INT);//$_POST['plano-duracao-dim'];
    $plano['data_cadastro'] = date('Y-m-d');

    if($plano['nome'] == false || $plano['nome'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um nome!"));
        die();
    }

    if(!validarValor1($plano['valorT'])){
        echo json_encode(array("success" => 0, "msg" => "Informe um valor válido!"));
        die();
    }

    if($plano['valorD'] == "0,00" || $plano['valorD'] === "0," || $plano['valorD'] === "0"){
        $plano['valorD'] = 0.0;
    }

    if(!validaDuracao($plano['duracao'])){
        echo json_encode(array("success" => 0, "msg" => "Informe um tempo de duração válido (somente inteiros)!"));
        die();
    }

    if($plano['dim'] == 1)
        $plano['duracao'] = $plano['duracao']*12;

    return $plano;
}
function validarValor1($valor){
    if($valor == false || $valor == "")
        return false;
    
    if($valor == "0,00" || $valor == "0," || $valor == "0"){
        $plano['valorT'] = 0.0;
        return true;
    }
    return true;
}
function validarValor2($valor){

    if($valor == false)
        return false;
    
    return true;
}
function validaDuracao($duracao){
    if($duracao == false || $duracao == "" || $duracao == "0")
        return false;
    if(!preg_match("/^[0-9]+$/", $duracao)){
        return false;
    }
    return true;
}

?>