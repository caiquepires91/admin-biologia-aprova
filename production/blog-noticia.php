<?php 
require __DIR__ . '/php/autentica.php';
require __DIR__ . '/lib/blog.php';

  //verifica nivel de acesso
  //verifica_nivel([0]);

//noticias
if(isset($_GET["id"]) && $_GET["id"]){
    $post = Blog\buscar($_GET["id"]);
}

//autores
$result = $mysqli->query("SELECT * FROM equipe");
$equipe = $result->fetch_all(MYSQLI_ASSOC);

//categorias
$result = $mysqli->query("SELECT DISTINCT(UPPER(categoria)) as categoria FROM noticias");
$categorias = $result->fetch_all(MYSQLI_ASSOC);

?>

<!DOCTYPE html>
<html lang="pt-BR">
  <!-- scripts -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Vídeos</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>

  <!--TinyMCE-->
  <script src="../vendors/tinymce/tinymce.min.js"></script>

  <body class="nav-md">
    <style>
      .news{
        display: flex;
        justify-content: space-between;
      }

      .news > div > span{
        color: #747474;
        font-size: 10px;
      }

    </style>
    <div class="container body">
      <div class="main_container">
        
      <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <form id="form_post" action="http://sandbox.biologiaaprova.com.br/blog/post" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="acao" value="salvar">
                    <input type="hidden" name="id" value="<?php echo isset($post) ? $post["id"] : ""?>">

                    <!-- postagem -->
                    <div class="x_title">
                      <h2>Postagem</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="form-group">
                            <label for="title">Título*:</label>
                            <input type="text" class="form-control" name="titulo" value="<?php echo isset($post) ? $post["titulo"] : ""?>">
                        </div>
                        <div class="form-group">
                          <label for="">Texto*:</label>
                          <textarea id="texto" class="form-control tinyEditor" name="texto" rows="8"><?php echo isset($post) ? $post["texto"] : ""?></textarea>
                          <small>Obs.: Use as opções de alinhamento para alinhar as imagens no texto.</small>
                        </div>
                    </div>

                    <!-- informações -->
                    <div class="x_title">
                      <h2>Informações adicionais</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <div class="row">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                          <div class="form-group">
                              <label for="title">Autor</label>
                              <select name="autor" id="autor" class="form-control select2 select2-notags" style="width: 100%">
                              <option value="">Selecione autor do texto</option>
                                <?php
                                  foreach($equipe as $e){
                                    if(isset($post) && $post["id_autor"] == $e["id"])
                                      echo "<option value='$e[id]' selected>$e[nome]</option>";
                                    else 
                                      echo "<option value='$e[id]'>$e[nome]</option>";
                                  }
                                ?>
                              </select>
                          </div>
                          <div class="form-group">
                              <label for="title">Categoria*:</label>
                              <select name="categoria" id="categoria" class="form-control select2 select2-tags" style="width: 100%">
                                <option value="">Selecionar categoria</option>
                                
                                <?php 
                                  foreach($categorias as $c){
                                    if(isset($post) && mb_strtoupper($c["categoria"]) === $c["categoria"])
                                      echo "<option value='$c[categoria]' selected>$c[categoria]</option>";
                                    else 
                                      echo "<option value='$c[categoria]'>$c[categoria]</option>";
                                  }
                                ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label for="">Idioma*:</label>
                              <select name="lingua" id="lingua" class="form-control select2 select2-notags" style="width: 100%">
                                <option value="pt_BR" <?php echo (isset($post) && $post["lingua"] === "pt_BR") ? "selected" : ""?>>Português</option>
                                <option value="en" <?php echo (isset($post) && $post["lingua"] === "en") ? "selected" : ""?>>Inglês</option>
                              </select>
                            </div>

                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-12">
                              <div class="form-group">
                                  <label>Imagem de exibição*:</label>
                                  <label for="img" id="preview_img" style="cursor: pointer; width: 100%; min-height: 170px; background-color: #eee; background-repeat: no-repeat; background-position: center; background-size: contain; background-image: url(<?php echo isset($post) ? "http://biologiaaprova.com.br/img/blog/$post[img]" : "assets/images/placeholder.png"?>)"></label>
                                  <input data-tipo="placeholder" data-target="#preview_img" type="file" name="img" id="img" style="display: none;">
                              </div>
                          </div>
                        </div>

                        <!-- <div class="form-group">
                          <label for="">Nota:</label> -->
                          <textarea style="display: none" id="nota" class="form-control" name="nota" rows="3"><?php echo isset($post) ? $post["nota"] : ""?></textarea>
                        <!-- </div> -->

                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="ln_solid"></div>

                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <button id="submit" class="btn btn-success"><?php echo isset($post) ? "Atualizar" : "Cadastrar"; ?></button>
                          </div>
                        </div>
                      </div>

                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div id="modal_deletar" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-trash-o"></i> Apagar post</h4>
              </div>
              <form id="form-descontos" action="ajax/blog.php" method="POST" class="form-horizontal form-label-left">
                <div class="modal-body">
                  <input type="hidden" name="id_post" value="">
                  <p style="white-space: pre-line;">Deseja <b>REMOVER</b> o post selecionado do blog?
                    <div style="padding: 2px 0 2px 7px; border-left: 3px solid #d9534f; margin-top: 7px;">
                      <b>Título:</b> <span id="del-titulo"></span><br>
                      <b>Data de postagem:</b> <span id="del-data"></span><br>
                      <b>Autor:</b> <span id="del-autor"></span><br>
                    </div>
                  </p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-trash-o"></i> Apagar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /modals -->

         <!-- footer content -->
         <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- scripts -->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="./js/moment/moment.min.js"></script>
    <script src="./js/moment/datetime-moment.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/main.js"></script>

    <!-- <script src="assets/js/crud_admin.js?v=<?php echo ASSETS_VERSION; ?>"></script> -->
    <script>
      

      $(document).ready(function(){
        initTinyMCE(500);
        $("#form_post").on("submit", {success_callback: post_salvo}, submeter_formulario_files);

        $(".select2-tags").select2({
          theme: "classic",
          tags: true,
        });

        $(".select2-notags").select2({
          theme: "classic",
          language: {
            noResults: function(){
                return "Nenhum resultado encontrado";
              },
          },
        });

        

      });

      function post_salvo(res){
        console.log("blog: " + res);
        if (res.success) {
          window.location.href = "blog-noticia.php?id=" + res.noticia.id;
        } else {
          console.log("erro postar noticia: " + res.message)
        }
      }

    </script>
  </body>
</html>