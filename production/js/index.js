
const theme = {
    color: [
        '#26B99A', '#34495E', '#BDC3C7', '#3498DB',
        '#9B59B6', '#8abb6f', '#759c6a', '#bfd3b7'
    ],

    title: {
        itemGap: 8,
        textStyle: {
            fontWeight: 'normal',
            color: '#408829'
        }
    },

    dataRange: {
        color: ['#1f610a', '#97b58d']
    },

    toolbox: {
        color: ['#408829', '#408829', '#408829', '#408829']
    },

    tooltip: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        axisPointer: {
            type: 'line',
            lineStyle: {
                color: '#408829',
                type: 'dashed'
            },
            crossStyle: {
                color: '#408829'
            },
            shadowStyle: {
                color: 'rgba(200,200,200,0.3)'
            }
        }
    },

    dataZoom: {
        dataBackgroundColor: '#eee',
        fillerColor: 'rgba(64,136,41,0.2)',
        handleColor: '#408829'
    },
    grid: {
        borderWidth: 0
    },

    categoryAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },

    valueAxis: {
        axisLine: {
            lineStyle: {
                color: '#408829'
            }
        },
        splitArea: {
            show: true,
            areaStyle: {
                color: ['rgba(250,250,250,0.1)', 'rgba(200,200,200,0.1)']
            }
        },
        splitLine: {
            lineStyle: {
                color: ['#eee']
            }
        }
    },
    timeline: {
        lineStyle: {
            color: '#408829'
        },
        controlStyle: {
            normal: {color: '#408829'},
            emphasis: {color: '#408829'}
        }
    },

    k: {
        itemStyle: {
            normal: {
                color: '#68a54a',
                color0: '#a9cba2',
                lineStyle: {
                    width: 1,
                    color: '#408829',
                    color0: '#86b379'
                }
            }
        }
    },
    map: {
        itemStyle: {
            normal: {
                areaStyle: {
                    color: '#ddd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            },
            emphasis: {
                areaStyle: {
                    color: '#99d2dd'
                },
                label: {
                    textStyle: {
                        color: '#c12e34'
                    }
                }
            }
        }
    },
    force: {
        itemStyle: {
            normal: {
                linkStyle: {
                    strokeColor: '#408829'
                }
            }
        }
    },
    chord: {
        padding: 4,
        itemStyle: {
            normal: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            },
            emphasis: {
                lineStyle: {
                    width: 1,
                    color: 'rgba(128, 128, 128, 0.5)'
                },
                chordStyle: {
                    lineStyle: {
                        width: 1,
                        color: 'rgba(128, 128, 128, 0.5)'
                    }
                }
            }
        }
    },
    gauge: {
        startAngle: 225,
        endAngle: -45,
        axisLine: {
            show: true,
            lineStyle: {
                color: [[0.2, '#86b379'], [0.8, '#68a54a'], [1, '#408829']],
                width: 8
            }
        },
        axisTick: {
            splitNumber: 10,
            length: 12,
            lineStyle: {
                color: 'auto'
            }
        },
        axisLabel: {
            textStyle: {
                color: 'auto'
            }
        },
        splitLine: {
            length: 18,
            lineStyle: {
                color: 'auto'
            }
        },
        pointer: {
            length: '90%',
            color: 'auto'
        },
        title: {
            textStyle: {
                color: '#333'
            }
        },
        detail: {
            textStyle: {
                color: 'auto'
            }
        }
    },
    textStyle: {
        fontFamily: 'Arial, Verdana, sans-serif'
    },
};

$(document).ready(function(){
    const echartBar = echarts.init(document.getElementById('vendas_echart'), theme);
    $('.data').inputmask({ mask: '99/99/9999' });

    $.ajax({
        method: 'post',
        url: './ajax/ajax_venda.php',
        data: {action: 'historico_vendas'},
        dataType: 'json'
    }).done(res=>{
        monta_grafico(echartBar, res);
    })

    $('input[name=periodo_opt]').change(e=>{
        opt = $('input[name=periodo_opt]:checked').val();
        switch(opt){
            case '0':
                $("#form-filter-grafico .filtro").hide();
                $("#form-filter-grafico .ano").show();
                break;
            case '1':
                $("#form-filter-grafico .filtro").hide();
                $("#form-filter-grafico .mes").show();
                break;
            case '2':
                $("#form-filter-grafico .filtro").hide();
                $("#form-filter-grafico .dia").show();
                break;
            case '3':
                $("#form-filter-grafico .filtro").hide();
                $("#form-filter-grafico .personalizado").show();
                break;
        }
    })

    $("#form-filter-grafico").submit(e=>{
        e.preventDefault();
        opt = $('input[name=periodo_opt]:checked').val();
        var dados;
        switch(opt){
            case '0': //ano
                dados = {action: 'historico_ano', ano: $('select[name=ano-periodo]').val()}
                break;
            case '1': //mes
                dados = {action: 'historico_mes', mes: $('select[name=mes-periodo]').val()}
                break;
            case '2': //dia
                dados = {action: 'historico_dia', dia: $('select[name=dia-periodo]').val()}
                break;
            case '3': //personalizado
            dados = {
                action: 'historico_personalizado', 
                data_inicial: $('input[name=data-inicial]').val(),
                data_final: $('input[name=data-final]').val()
            }
                break;
        }
    if(!dados) return false;

    $.ajax({
        url: './ajax/ajax_venda.php',
        data: dados,
        dataType: 'json'
    }).done(res=>{
        if(res.success == 1)
            monta_grafico(echartBar, res.historico);
        else
            console.log(res.msg);
    })
        
    })
})	
    
function monta_grafico(grafico, res){
    grafico.setOption({
        title: {
          text: 'Histórico de transações',
          subtext: 'Venda de planos'
        },
        tooltip: {
          trigger: 'axis'
        },
        legend: {
          data: ['Vendas']
        },
        toolbox: {
          show: false
        },
        calculable: false,
        xAxis: [{
          type: 'category',
          data: res.datas
        }],
        yAxis: [{
          type: 'value'
        }],
        series: [{
          name: 'Vendas',
          type: 'bar',
          data: res.valores,
          markLine: {
            data: [{
              type: 'average',
              name: 'Média de vendas'
            }]
          }
        }]   
    });
}
    