<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';


//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;

        case 'ler_todos_por_plano':
            ler_todos_por_plano();
        break;
    }
}

function novo(){
    global $mysqli;
    $cupom = getCupom();
    try{
        $stmt_result = $mysqli->prepare(
            "INSERT INTO `cupom`(`id`, `codigo`, `desconto`, `duracao`, `data_cadastro`, `duracao_unidade`) 
            VALUES (default,?,?,?,?,?)"
            );
        $stmt_result->bind_param("sdiss", 
        $cupom['codigo'],
        $cupom['valor'],
        $cupom['duracao'],
        $cupom['data_cadastro'],
        $cupom['dim']);

        $stmt_result->execute();
        echo json_encode(array("success" => 1, "msg" => "Cupom cadastrado com sucesso!"));
    }catch(Exception $e){

        switch($e->getCode()){
            case 1062: 
                die(json_encode(array('success'=>99, 'msg'=>'Código já cadastrado no sistema')));
                break;
        }
    }
}

function atualizar(){
    global $mysqli;
    $cupom = getCupom();
    $id = $_POST['id'];
    extract($cupom);
   
    if($mysqli->query(
        "UPDATE cupom 
            SET codigo = '$codigo', desconto = $valor, duracao = $duracao, duracao_unidade = '$dim'
        WHERE id = $id"
        )){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Cupom atualizado com sucesso!"));
        }else{
            echo json_encode(array("success" => 2, "msg" => "Não houve alterações no cupom!"));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id'];

        if($mysqli->query(
            "DELETE 
            FROM cupom 
            WHERE id = $id"
            )){
            if($mysqli->affected_rows > 0){
                echo json_encode(array("success" => 1, "msg" => "Cupom removido do banco de dados!"));
            }else{
                echo json_encode(array("success" => 0, "msg" => "Cupom não encontrado, por favor atualize a página!"));
            }
        }else{
            echo json_encode(array("success" => 0, "msg" => "Erro ao remover cupom!"));
        }
}

function ler(){
    global $mysqli;
    $id = $_GET['id'];

    if($result = $mysqli->query(
        "SELECT * FROM cupons 
        WHERE id = $id"
        )){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => "1", "cupom" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Cupom não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    
    $cupons = array();

    if($result = $mysqli->query(
        "SELECT cupom, DATE_FORMAT(data_inicio, '%d/%m/%Y') as data_inicio, DATE_FORMAT(data_fim, '%d/%m/%Y') as data_fim, idPlano as plano, tipo_desconto, desconto, DATE_FORMAT(created_at, '%d/%m/%Y %H:%i') as data_cadastro, id 
        FROM cupons"
        )){
        while($row = $result->fetch_assoc()){
            $cupons[] = $row;
        }
        echo json_encode($cupons);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos_por_plano(){
    global $mysqli;
    $id_plano = $_GET['id_plano'];

    $cupons = array();

    if($result = $mysqli->query(
        "SELECT codigo, desconto, DATE_FORMAT(data_cadastro, '%d/%m/%Y %H:%i') as data_cadastro, duracao, duracao_unidade, data_cadastro as data_validade , id 
        FROM cupom 
        WHERE id NOT IN( 
            SELECT id_cupom 
            FROM plano_cupom 
            WHERE id_plano = $id_plano 
            );")){
        while($row = $result->fetch_assoc()){
            if($row['duracao_unidade'] == 'hora'){
                $date = strtotime("+".$row['duracao']." hours", strtotime($row['data_validade']));
                $row['data_validade'] = date("d/m/Y H:i:s", $date);
            }else if($row['duracao_unidade'] == 'dia'){
                $date = strtotime("+".$row['duracao']." days", strtotime($row['data_validade']));
                $row['data_validade'] = date("d/m/Y H:i:s", $date);
            }else if($row['duracao_unidade'] == 'mes'){
                $date = strtotime("+".$row['duracao']." month", strtotime($row['data_validade']));
                $row['data_validade'] = date("d/m/Y H:i:s", $date);
            }
            if($date > strtotime(date("Y-m-d H:i:s")) )
                $cupons[] = $row;
        }
        echo json_encode($cupons);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}


//--------------FUNCOES AUXILIARES -------------///

function getCupom(){
    $cupom['codigo'] = filter_input(INPUT_POST, 'cupom-codigo', FILTER_SANITIZE_STRING);//$_POST['cupom-codigo'];
    $cupom['valor'] = filter_input(INPUT_POST, 'cupom-valor', FILTER_SANITIZE_STRING); //$_POST['cupom-valor'];
    $cupom['duracao'] = filter_input(INPUT_POST, 'cupom-duracao', FILTER_SANITIZE_NUMBER_INT);//$_POST['cupom-duracao'];
    $cupom['dim'] = $_POST['cupom-duracao-dim'] == 0 ? 'hora' : ($_POST['cupom-duracao-dim'] == 1 ? 'dia' : 'mes');
    $cupom['data_cadastro'] = date('Y-m-d H:i:s');

    if($cupom['codigo'] == false || $cupom['codigo'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um código válido!"));
        die();
    }
    if(!preg_match('/^[a-zA-Z0-9]+$/', $cupom['codigo'])){
        echo json_encode(array("success" => 0, "msg" => "O código não pode conter espaços!"));
        die();
    }
    $cupom['codigo'] = strtoupper($cupom['codigo']);

    if($cupom['valor'] == "0,00" || $cupom['valor'] === "0," || $cupom['valor'] === "0" || $cupom['valor'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Valor de desconto não pode ser 0!"));
        die();
    }

    if(!preg_match('/^[0-9]*.[0-9]*$/', $cupom['valor'])){
        echo json_encode(array("success" => 0, "msg" => "Informe um valor válido ex.: 10.50!"));
        die();
    }

    if(!validaDuracao($cupom['duracao'])){
        echo json_encode(array("success" => 0, "msg" => "Informe um tempo de duração válido (somente inteiros)!"));
        die();
    }

    return $cupom;
}

function validaDuracao($duracao){
    if($duracao == false || $duracao == "" || $duracao == "0")
        return false;
    if(!preg_match("/^[0-9]+$/", $duracao)){
        return false;
    }
    return true;
}

?>