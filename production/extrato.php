<?php 
require __DIR__.'/php/autentica.php'; 
require __DIR__.'/php/functions.php';
?>

<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Extrato</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-datepicker -->
    <link href="./js/datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css?v=<?php echo ASSETS_VERSION; ?>" rel="stylesheet">
    <link rel="stylesheet" href="./css/modules/relatorio.css?v=<?php echo ASSETS_VERSION; ?>">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <?php include __DIR__.'/./componentes/menuFooter.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-search-dollar"></i> Extrato <small id="filter-info">Exibindo resultado dos últimos 30 dias</small></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="totais">
                    <button class="btn btn-primary" onClick="mostrar_saldos()" data-toggle="modal" data-target="#modal_saque"> Saque <i class="fas fa-coins"></i></button>
                    <div class="d-flex">
                    <!--
                      <div class="disponivel"><strong>Total Disponível: </strong></div>
                      <div class="recebido"><strong>Total Recebido: </strong></div>
                      
                      <div class="balanco mes">
                        <div class="balanco-box">
                          <div class="flex-div-space"><p style="color: green;">Entradas: </p><p style="color: green;" id="entrada-mes">R$ 0,00</p></div>
                          <div class="flex-div-space"><p style="color: red;">Saídas: </p><p style="color: red;" id="saida-mes">R$ 0,00</p></div>
                          <hr>
                          <div class="flex-div-space"><p style="color: green; font-weight: 600;">Total: </p><p style="color: green; font-weight: 600;" id="total-mes">R$ 0,00</p></div>
                        </div>
                      </div>-->

                      <div class="balanco geral">
                        <div class="balanco-box">
                          <div class="flex-div-space"><p style="color: green;">Entradas: </p><p style="color: green;" id="entrada-geral">R$ 0,00</p></div>
                          <div class="flex-div-space"><p style="color: red;">Saídas: </p><p style="color: red;" id="saida-geral">R$ 0,00</p></div>
                          <hr>
                          <div class="flex-div-space"><p style="color: green; font-weight: 600;">Disponível: </p><p style="color: green; font-weight: 600;" id="total-geral">R$ 0,00</p></div>
                        </div>
                      </div>
                      
                    </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-12">
                  <div class="profile_left">
                  <div class="x_title">
                    <h2>Filtrar resultados</h2>
                  </div>
                    <div class="x_content">
                      <form id="form-filter" autocomplete="off" class="">
                      <div class="">
                          <label>Data inicial: </label>
                          <div class="input-group date initial">
                                  <input type="text" name="data_inicial" class="form-control">
                                  <div class="input-group-addon">
                                      <span class="fas fa-calendar"></span>
                                  </div>
                              </div>
                            <label>Data final: </label>
                            <div class="input-group date end">
                                  <input type="text" name="data_final" class="form-control">
                                <div class="input-group-addon">
                                    <span class="fas fa-calendar"></span>
                                </div>
                            </div>
                            <div class="form-group">
                            <label>Gateway: </label>
                            <select name="id-gateway" class="form-control">
                            <?php
                              $result = $mysqli->query("SELECT id, nome FROM gateway");
                              $gateways = $result->fetch_all(MYSQLI_ASSOC);
                              reset($gateways);
                              
                              echo "<option value=''>Selecionar Gateway</option>
                                    <option value='0'>Compra direta</option>";
                              while($gateway = current($gateways)){
                                echo "<option value=".$gateway['id'].">".$gateway['nome']."</option>";
                                next($gateways);
                              }
                            ?>
                            </select>
                            </div>
                            
                            <button type="submit" class="btn btn-dark"><i class="fas fa-search-dollar"></i> Filtrar</button>
                        </div> 
                        </form>
                    </div>
                  </div>
                  </div>

                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <table id="extrato" class="display table table-hover table-bordered table-striped jambo_table" style="width:100%; cursor:pointer;">
                      <thead>
                          <tr>
                              <!-- por método de pagamento --><th>ID.</th>
                              <th><i class="fas fa-calendar"></i> Data</th>
                              <th><i class="fas fa-calendar"></i> Gateway</th>
                              <th><i class="fas fa-tag"></i> Categoria</th>
                              <th><i class="fas fa-money-bill-wave-alt"></i> Valor</th>
                          </tr>
                      </thead>
                      <tfoot>
                        <tr>
                          <th></th>
                          <th></th>
                          <th></th>
                          <th>Total: </th>
                          <th></th>
                        </tr>
                      </tfoot>
                    </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->
                  <div id="modal_ler_transacao" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Detalhes da Transação</h4>
                        </div>
                        <div id="modal_ler_corpo" class="modal-body">

                        <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- /modals -->

                  <!-- modals saque -->
                  <div id="modal_saque" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Realizar Saque</h4>
                        </div>
                        <div id="modal_saque_corpo" class="modal-body">
                          <table id="table_saldos" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>Gateway</th>
                                  <th>Saldo Disponível</th>
                                </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                          <form id="form-saque" class="form">
                            <div class="form-group">
                              <label>Valor: </label>
                              <input type="text" name="valor-saque" require class="form-control valor">
                            </div>
                            <div class="form-group mt-5">
                              <label>gateway: </label>
                                <select name="id_gateway" class="form-control">
                                  <?php
                                  reset($gateways);
                                  echo "<option value=''>Selecionar Gateway</option>";
                                  while($gateway = current($gateways)){
                                    echo "<option value=".$gateway['id'].">".$gateway['nome']."</option>";
                                    next($gateways);
                                  }
                                  ?>
                                </select>
                            </div>
                            <div class="form-group">
                              <label>Descrição: </label>
                                <textarea name="descricao" class="form-control" rows="3" style="resize: none;"></textarea>
                            </div>
                          </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary"> Sacar</button>
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- bootstrap-datepicker -->    
    <script src="./js/moment/moment.min.js"></script>
    <script src="./js/datepicker/bootstrap-datepicker.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script>
      const id_usuario = <?php echo $_SESSION['id_usuario'];?>
    </script>
    <script src="../build/js/custom.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/functions.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/extrato.js?v=<?php echo ASSETS_VERSION; ?>"></script>
  </body>
</html>