<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;
    $documento = getDocumento();
    try{
        $stmt_result = $mysqli->prepare(
            "INSERT INTO `documento`(`id`, `id_categoria`, `id_video`, `nome`, `data_cadastro`, `data_liberacao`, `caminho`) 
            VALUES (default,?,?,?,?,?,?)");
        $stmt_result->bind_param("iissss", $documento['id_categoria'], $documento['id_video'], $documento['nome'],  $documento['data_cadastro'],  $documento['data_liberacao'], $documento['caminho']);
        $stmt_result->execute();
        echo json_encode(array("success" => 1, "msg" => "Documento registrado com sucesso!"));        
    }catch(Exception $e){
        switch($e->getCode()){
            case 1062: 
                die(json_encode(array('success'=>99, 'msg'=>'Código já cadastrado no sistema')));
                break;
            default:
                die(json_encode(array('success'=>0, 'msg'=>$e->getMessage())));
            break;
        }
    }
        
}

function atualizar(){
    global $mysqli;
    $documento = getDocumento();
	extract($documento);
     
    if($mysqli->query(
        "UPDATE documento 
        SET id_categoria = $id_categoria, id_video = $id_video, nome = '$nome', data_liberacao = '$data_liberacao', caminho = '$caminho'
        WHERE id = $id"
        )){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Registro atualizado com sucesso!"));
        }else{
            echo json_encode(array("success" => 2, "msg" => "Não houve alterações no registro!"));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id_documento'];
    if($delete = $mysqli->query(
        "UPDATE documento 
        SET deletado = 1 
        WHERE id = $id")){
        echo json_encode(array("success" => 1, "msg" => "Documento removido!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id_documento'];

    if($result = $mysqli->query(
        "SELECT d.nome, d.id_categoria, d.id_video, d.caminho, c.nome as nome_categoria, v.nome as nome_video, DATE_FORMAT(d.data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(d.data_liberacao, '%d/%m/%Y %H:%i') as data_liberacao, d.id
            FROM documento as d 
            LEFT JOIN categoria as c ON c.id = d.id_categoria
            LEFT JOIN video as v ON v.id = d.id_video
        WHERE d.id = $id"
        )){
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();

            //incluindo o nome do arquivo salvo no servidor
            $re = '/^[0-9]*_(?<doc>.*)$/m';
            preg_match_all($re, $row['caminho'], $matches, PREG_SET_ORDER, 0);
            $row['file_name'] = $matches[0]['doc'];

            echo json_encode(array("success" => "1", "documento" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Documento não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    $documentos = array();

    if($result = $mysqli->query(
        "SELECT d.nome, d.caminho, c.nome as nome_categoria, v.nome as nome_video, DATE_FORMAT(d.data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(d.data_liberacao, '%d/%m/%Y %H:%i') as data_liberacao, d.id 
            FROM documento as d
            LEFT JOIN categoria as c ON c.id = d.id_categoria
            LEFT JOIN video as v ON v.id = d.id_video
        WHERE d.deletado = 0"
    )){
        while($row = $result->fetch_assoc()){
            $documentos[] = $row;
        }
        echo json_encode($documentos);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

//-----------FUNCOES AUXILIARES --------------//

function getDocumento(){
    $documento['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);//$_POST['nome'];
    $documento['id_categoria'] = filter_input(INPUT_POST, 'id_categoria', FILTER_SANITIZE_NUMBER_INT);//$_POST['id_categoria'];
    $documento['id_video'] = filter_input(INPUT_POST, 'id_video', FILTER_SANITIZE_NUMBER_INT);//$_POST['id_video'];
    if(isset($_POST['id_documento'])) $documento['id'] = $_POST['id_documento']; 

    if($documento['nome'] == false || $documento['nome'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um nome!"));
        die();
    }

    if(($documento['id_categoria'] == false || $documento['id_categoria'] == '0') && ($documento['id_video'] == false || $documento['id_video'] == '0')){
        echo json_encode(array("success" => 0, "msg" => "Selecione uma categoria ou um vídeo para o documento!"));
        die();
    }

    $data = filter_input(INPUT_POST, 'data_liberacao', FILTER_SANITIZE_STRING);//$_POST['data_liberacao'];
    if(!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4} ([01][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9]){0,1}$/", $data)){
        echo json_encode(array("success" => 0, "msg" => "Informe uma data de liberação válida!"));
        die();
    }
    $data_ajustado = str_replace('/', '-', $data);   
    $data_ajustado = date('Y-m-d H:i:s', strtotime($data_ajustado));

    if(!ValidaData($data_ajustado, $documento['id_categoria'])){
        echo json_encode(array("success" => 0, "msg" => "A data de liberação do documento deve ser superior ou igual à data da categoria selecionada!"));
        die();
    }
    $documento['data_liberacao'] = $data_ajustado;
    
    //tratando arquivos
    $file_name = "";
    $target_dir = "../uploads/documentos/";

    if($_POST['action'] == 'novo'){
    $file_name = time() .'_'. basename($_FILES["_documento"]["name"]);
    $target_file = $target_dir.$file_name;

    if($result = saveDoc($target_file)){
        echo $result;
        die();
    }
    $documento['caminho'] = $file_name;

    }
    else if($_FILES['_documento']['size'] !== 0 && $_POST['action'] == "atualizar"){ //se for ação de update e o arquivo for alterado
        $doc_atual = $target_dir.$_POST['_documento-atual'];
        unlink($doc_atual);
        $file_name = time() .'_'. basename($_FILES["_documento"]["name"]);
        $target_file = $target_dir.$file_name;
        if($result = saveDoc($target_file)){
            echo $result;
            die();
        }
        $documento['caminho'] = $file_name;
    }
    else if($_FILES['_documento']['size'] == 0 && $_POST['action'] == "atualizar"){ //se o arquivo bnao foi alterado
        $documento['caminho'] = $_POST['_documento-atual'];
    }

    $documento['data_cadastro'] = date('Y-m-d');
    return $documento;
}

function ValidaData($data, $id_categoria){
    global $mysqli;

    $categoria = $mysqli->query("SELECT * from categoria WHERE id = $id_categoria");
    $categoria = $categoria->fetch_assoc();

    $data_categoria = date('Y-m-d H:i', strtotime($categoria['data_liberacao'])); //busca data de liberacao da categoria
	if ($data < $data_categoria){
	   return false;
	} else {
	   return true;
	}
}

function saveDoc($target_file){
    $FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    if(!$_FILES["_documento"]["tmp_name"]){
        die(json_encode(array("success" => 0, "msg" => "Selecione um arquivo!")));
    }
    $check = filesize($_FILES["_documento"]["tmp_name"]);
    //checa se é imagem
    if($check == false) {
        die( json_encode(array("success" => 0, "msg" => "Arquivo inválido!")));
    }
    // Tipos permitidos
    if($FileType != "pdf" && $FileType != "doc" && $FileType != "jpg" && $FileType != "png" && $FileType != "jpeg"
    && $FileType != "gif") {
        die(json_encode(array("success" => 0, "msg" => "Formato não aceito!")));
    }
    // se tudo estiver ok, o arquivo é salvo
    if (!move_uploaded_file($_FILES["_documento"]["tmp_name"], $target_file)) {
        die(json_encode(array("success" => 0, "msg" => "Erro ao fazer upload de arquivo, por favor tente mais tarde!")));
    }
    return false;
}

?>