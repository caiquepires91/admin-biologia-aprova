<?php

    include_once __DIR__ . '/../lib/config.php';
    session_start();

    if(!$usuario = filter_input(INPUT_POST, 'usuario', FILTER_SANITIZE_STRING)){
        die(json_encode(array('success'=>0, 'msg'=>'Nome de usuário inválido!')));
    }
    if(!$senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING)){
        die(json_encode(array('success'=>0, 'msg'=>'Senha inválida!')));
    }
    $senha = md5($senha);


    if(!preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $usuario)){
        //pode ser que se trate de um cpf
        $cpf = cpf_transform($usuario);
        $cpf1 = substr($cpf, 0, 3);
        $cpf2 = substr($cpf, 3, 3);
        $cpf3 = substr($cpf, 6, 3);
        $cpf4 = substr($cpf, 9, 2);

        $usuario = "$cpf1.$cpf2.$cpf3-$cpf4";
    }

    try{
        $stmt = $mysqli->prepare("SELECT * FROM usuarios WHERE (cpf = ? OR email = ?) AND tipo = 1");
        //$perfil = $mysqli->prepare("SELECT p.image FROM usuario as u INNER JOIN perfil as p ON u.id_perfil = p.id WHERE u.id = ?");
        
        $stmt->bind_param("ss", $usuario, $usuario);
        $stmt->execute();
        $res = $stmt->get_result();
        if($res->num_rows > 0){
            $dados = $res->fetch_assoc();

            if(!strcmp($senha, $dados['senha'])){
                $id = $dados['id'];
                //$perfil->bind_param("i", $id);
                //$perfil->execute();

                //$res_perfil = $perfil->get_result();
                //$dados_perfil = $res_perfil->fetch_assoc();

                // Obtém nova sessão para o usuário
                $mysqli->query("UPDATE sessoes SET encerrada = 1 WHERE id_usuario = $dados[id] AND encerrada = 0");
                $data_corrente = date("Y-m-d H:i:s");
                $stmt_session = $mysqli->prepare("INSERT INTO sessoes (id_usuario, data, ultima_atividade, endereco, user_agent) VALUES (?, ?, ?, ?, ?)");
                $stmt_session->bind_param("issss", $dados["id"], $data_corrente, $data_corrente, $_SERVER["REMOTE_ADDR"], $_SERVER["HTTP_USER_AGENT"]);
                if (!$stmt_session->execute()){
                    die(json_encode(array('success'=>99, 'msg'=>'Falha ao autenticar usuário, tente mais tarde!')));
                }
                $id_sessao = $mysqli->insert_id;
                //die(json_encode(array('success'=>0, 'msg'=> json_encode($dados))));


                //$status = $mysqli->query("UPDATE perfil as p INNER JOIN usuario as u on p.id = u.id_perfil and u.id = $id SET p.status = 1;"); //muda o status
                // TUDO OK! Agora, passa os dados para a sessão e redireciona o usuário
                $_SESSION["id"] = $id_sessao; 
                $_SESSION["id_usuario"]= $dados["id"]; 
                $_SESSION["nome_usuario"] = $dados['nome']; 
                $_SESSION['image'] = $dados['foto'];
                if ($dados["admin"] == 1) {
                    $_SESSION["admin"] = 1;
                    if($dados["master"] == 1)
                        $_SESSION["master"] = 1;
                }
                $_SESSION['ultima_atividade'] = time(); 
                $_SESSION['timeout'] = 1*60*60;

                die(json_encode(array('success'=>1, 'msg'=>'LOGADO')));
            }else
                die(json_encode(array('success'=>0, 'msg'=>'Senha incorreta!')));
        }else 
            die(json_encode(array('success'=>0, 'msg'=>'Usuário inexistente!')));
    }catch(Exception $e){
        die(json_encode(array('success'=>0, 'msg'=>$e->getMessage(), 'cod'=>$e->getCode())));
    }

    function cpf_transform($cpf){
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        return $cpf;
    }

?>