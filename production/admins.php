<?php require __DIR__.'/php/autentica.php'; ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Admins</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
    <?php include __DIR__ . '/componentes/DataTableCSS.php' ?>


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css?v=<?php echo ASSETS_VERSION; ?>" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">


            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-user-shield"></i> Perfis Administrativos <small></small></h2>
                    <div class="panel_toolbox">
                    <button id="btn-novo-admin" class="btn btn-primary" data-toggle="modal" data-target="#modal_cadastro">Cadastrar Novo Perfil <i class="fas fa-user-plus"></i></button>
                  </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">

                    <table id="admins" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th><i class="fas fa-user"></i> Nome completo</th>
                              <th><i class="fas fa-id-card"></i> CPF</th>
                              <th><i class="fas fa-phone"></i> Telefone</th>
                              <th><i class="fas fa-calendar-check"></i> Data de cadastro</th>
                              <th style="width: 65px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                          </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->

                  <!-- Small modal Cadastro-->
                  <div id="modal_cadastro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-user-plus"></i> Cadastrar Administrador</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>

                          <!--Inserir formulario aqui-->
                          <form id="form-cadastro" class="form-horizontal form-label-left">
                            
                          <div id="error_cadastro">
                            
                            </div>
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Nome completo </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nome" name="nome" required="required" class="form-control col-md-7 col-xs-12 fm-nome">
                                <span class="fas fa-user form-control-feedback right" aria-hidden="true"></span>
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">CPF 
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cpf-fm" name="cpf" class="form-control col-md-7 col-xs-12 cpf-fm" data-inputmask="'mask' : '999.999.999-99'">
                                <span class="fas fa-id-card form-control-feedback right" aria-hidden="true"></span>
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_group form-control select_estado" id="select_estado" name="estado">
                                  
                                </select>
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" >Cidade</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_group form-control select_cidade" id="select_cidade" name="cidade">
                                </select>
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">CEP</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="cep-fm" name="cep" class="form-control col-md-7 col-xs-12 cep-fm" data-inputmask="'mask' : '99999-999'">
                                <span class="fas fa-city form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fr-email" name="email" required="required" class="form-control col-md-7 col-xs-12 fm-email">
                                <span class="fas fa-envelope form-control-feedback right" aria-hidden="true"></span>
                              </div>
                            </div>
      
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Data de Nascimento</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="fr-data" name="dataNascimento" class="form-control col-md-7 col-xs-12 fm-data" data-inputmask="'mask' : '99/99/9999'">
                                <span class="fas fa-birthday-cake form-control-feedback right" aria-hidden="true"></span>
                              
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-3">Telefone</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="telefone-fm" name="telefone" class="form-control telefone-fm">
                                <span class="fas fa-phone form-control-feedback right" aria-hidden="true"></span>
                              </div>
                            </div>
      
                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12">Senha</label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="senha-fm" name="senha" required="required" class="form-control col-md-7 col-xs-12 senha-fm">
                                <span class="fas fa-key form-control-feedback right" aria-hidden="true"></span>
                                <small class="form-text text-muted">Mínimo de 6 caracteres *</small>
                              </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="cadastrar-sub" class="btn btn-primary"><i class="fa fa-save"></i> Cadastrar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <?php include __DIR__ . '/componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/functions.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/crud_admin.js?v=<?php echo ASSETS_VERSION; ?>"></script>


  </body>
</html>