<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';
//require '../models/Desconto.php';

//--------------METODOS POST------------//
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'deletar':
            deletar();
        break;
    }
    
}

//--------------METODOS GET --------------//

if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler_todos':
            ler_todos();
        break;

    }
}

function novo(){
    global $mysqli;

    $id_plano = $_POST['plano-id'];
    $id_cupom = $_POST['cupom-id'];
    $data_cadastro = date('Y-m-d');

    try{
            $stmt_result = $mysqli->prepare(
                "INSERT INTO `plano_cupom`(`id`, `id_plano`,`id_cupom`,`data_vinculo`) 
                VALUES (default,?,?,?)"
                );
            $stmt_result->bind_param("iis", $id_plano, $id_cupom, $data_cadastro);
            $stmt_result->execute();

        echo json_encode(array("success" => 1, "msg" => "Cupom vinculado com sucesso!"));
    }catch(Exception $e){
        switch($e->getCode()){
            case 1062: 
                die(json_encode(array('success'=>99, 'msg'=>'Código já cadastrado no sistema')));
                break;
            default:
                die(json_encode(array('success'=>0, 'msg'=>$e->getMessage())));
            break;
        }
    }
	
}

function deletar(){
    global $mysqli;
    $id = $_POST['id'];

    if($mysqli->query(
        "DELETE 
        FROM plano_cupom 
        WHERE id = $id"
        )){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Cupom de desconto desvinculado!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Cupom não encontrado, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }

}

function ler_todos(){
    global $mysqli;
    
    $id_plano = $_GET['id_plano'];

        $descontos = array();

        if($result = $mysqli->query(
            "SELECT c.codigo,c.duracao,c.duracao_unidade,c.data_cadastro,pc.id 
                FROM plano_cupom as pc
                INNER JOIN cupom as c ON c.id = pc.id_cupom
            WHERE pc.id_plano = $id_plano;"
            )){ //verificando se o cupom já passou da validade, para exibir na lista de cupons vinculados
            while($row = $result->fetch_assoc()){
                if($row['duracao_unidade'] == 'hora'){ 
                    $date = strtotime("+".$row['duracao']." hours", strtotime($row['data_cadastro']));
                }else if($row['duracao_unidade'] == 'dia'){
                    $date = strtotime("+".$row['duracao']." days", strtotime($row['data_cadastro']));
                }else if($row['duracao_unidade'] == 'mes'){
                    $date = strtotime("+".$row['duracao']." month", strtotime($row['data_cadastro']));
                }
                if($date > strtotime(date("Y-m-d H:i:s")))
                    $descontos[] = $row;
            }
            
            echo json_encode($descontos);
            
        }else{
            echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
        }
}
?>