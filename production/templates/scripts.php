
    <!-- jQuery -->
    <script src="assets/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="assets/vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="assets/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="assets/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="assets/vendors/jQuery-Mask/dist/jquery.mask.min.js"></script>
    <script src="assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="assets/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="assets/vendors/pnotify/dist/pnotify.js"></script>
    <script src="assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>

    <!-- ECharts -->
    <script src="assets/vendors/echarts/dist/echarts.min.js"></script>
    <script src="assets/vendors/echarts/map/js/world.js"></script>
    
    <!-- Datatables -->
    <script src="assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="assets/vendors/jszip/dist/jszip.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="assets/vendors/moment/min/moment.min.js"></script>
    <script src="assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Switchery -->
    <script src="assets/vendors/switchery/dist/switchery.min.js"></script>

    
    <script src="assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="assets/js/moment/moment.min.js"></script>
    <script src="assets/js/moment/datetime-moment.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="assets/build/js/custom.js?v=<?php echo '1.1'; ?>"></script>
    <script src="assets/js/main.js?v=<?php echo '1.1' ?>"></script>

    <script>

        // Alerta (PNotify)
        function alerta(titulo, texto, tipo, delay = 5000) {
            new PNotify({
                title: titulo,
                text: texto,
                type: tipo,
                hide: true,
                delay: delay,
                styling: 'bootstrap3'
            });
        }

        // Loading
        var cont_loading_overlay = 0;

        function loadingOn() {
            cont_loading_overlay++;
            if (cont_loading_overlay > 0)
                $("#loading_overlay").css("display", "block");
        }
        function loadingOff() {
            cont_loading_overlay--;
            if (cont_loading_overlay < 1) {
            $("#loading_overlay").css("display", "none");
            $(".loading-percent").hide().html("");
                cont_loading_overlay = 0;
            }
        }
</script>
