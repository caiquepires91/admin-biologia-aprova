function isJson(item) {
    if (typeof item === "object" && item !== null) {
        return true;
    }
    console.log(item);
    return false;
}

function readURL(input, preview) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        preview.attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }

function responseTreatment(response, errorElement, callbackArray, clear, modalDOM){
    /**
     * @response pacote de resposta do servidor
     * @errorElement objeto para alerta de erro
     * @callback função para ser chamada quando ocorrer um sucesso
     * @clear limpa inputs e editores
     * @modal modal para esconder após sucesso
     */

     //checa se a response é json
    if(!isJson(response)) return false;
     
    var success = false;

    if(response.success == 1){
      success = true

      //limpa msgs de erro
      if(errorElement) errorElement.html("");

      //esconde o modal ativo
      if(modalDOM) modalDOM.modal('hide');

      //dispara uma notificação de sucesso
      new PNotify({
          title: response.msg,
          type: 'success',
          styling: 'bootstrap3'
      });

      //executa os callbacks
      for (var i = 0, len = callbackArray.length; i < len; i++) {
        callbackArray[i]();
      }

    } else if(response.success == 2){
        new PNotify({
            title: response.msg,
            type: 'info',
            styling: 'bootstrap3'
        });

    } else if(response.success == 99){
        if(response.cod){
            if(response.cod == 1062){
                let chave = response.msg.match(/key '(.*)'/)[1]
                let html = `<div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <strong>${chave} já registrado!</strong>
                        </div>`
            errorElement.append(html)
            return success;
            }
        }
      new PNotify({
        title: response.msg,
        type: 'warning',
        styling: 'bootstrap3'
        });
    }
    else{
        new PNotify({
          title: response.msg,
          type: 'error',
          styling: 'bootstrap3'
      });
    }

    return success;
    
  }

  function requestFullScreen(element) {
    // suporte de browsers
    var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen;

    if (requestMethod) { // Native full screen.
        requestMethod.call(element);
    } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
        var wscript = new ActiveXObject("WScript.Shell");
        if (wscript !== null) {
            wscript.SendKeys("{F11}");
        }
    }
}

function formata_dinheiro(n) {
    return "R$ " + parseFloat(n).toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
    }

var _validFileExtensions = [".jpg", ".jpeg", ".gif", ".png", ".pdf", ".doc"];    
function ValidateDoc(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                  new PNotify({
                    title: "Formato do documento não aceito",
                    text: 'formatos aceitos: jpg, jpeg, gif, png, pdf, doc',
                    type: 'error',
                    styling: 'bootstrap3'
                    });
                    return false;
                }
            }
        }
    }
  
    return true;
}

function ValidateDoc(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }
                
                if (!blnValid) {
                  new PNotify({
                    title: "Formato do documento não aceito",
                    text: 'formatos aceitos: jpg, jpeg, gif, png, pdf, doc',
                    type: 'error',
                    styling: 'bootstrap3'
                    });
                    return false;
                }
            }
        }
    }
  
    return true;
}