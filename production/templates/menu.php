<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0; height: 80px;">
                <a href="admins.php" style='height: 100%;' class="site_title"><div style="display: flex; height: 100%; justify-content: center; align-items: center;"><img class='width: 35%; ' src="assets/images/logo.png" alt="Biologia Aprova"></div></a>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                  <?php if(verifica_acesso([1])){?><li><a><i class="fa fa-users"></i> Usuários <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="admins.php"> Admins </a></li>
                      <li><a href="professores.php"> Autores </a></li>
                    </ul>
                  </li><?php } ?>
                  <?php if(verifica_acesso([0, 1])){?><li><a href="blog.php"><i class="fa fa-rss-square"></i> Blog </a></li><?php } ?>
                  <?php if(verifica_acesso([1])){?><li><a href="vendas.php"><i class="fa fa-dollar"></i> Vendas </a></li><?php } ?>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small" style="display: none;">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" onclick="requestFullScreen(document.body)" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>