<?php 
require __DIR__ . '/php/autentica.php';
require __DIR__ . '/lib/blog.php';

  //verifica nivel de acesso
 // verifica_nivel([0]);

//noticias
$noticias = Blog\listar();
?>

<!DOCTYPE html>
<html lang="pt-br">
  <!-- scripts -->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Vídeos</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>


  <body class="nav-md">
    <style>
      .news{
        display: flex;
        justify-content: space-between;
      }

      .news > div > span{
        color: #747474;
        font-size: 10px;
      }

    </style>
    <div class="container body">
      <div class="main_container">
        
      <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-rss-square"></i> Blog <small></small></h2>
                    <div class="panel_toolbox">
                    <a href="blog-noticia.php" class="btn btn-primary"><i class="fas fa-plus-square"></i> Novo post </a>
                  </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">

                    <table id="noticias" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th>POSTS</th>
                          </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- modals -->
        <div id="modal_deletar" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content">

              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-trash"></i> Apagar post</h4>
              </div>
              <form id="form_remover" action="ajax/blog.php" method="POST" class="form-horizontal form-label-left">
                <div class="modal-body">
                  <input type="hidden" name="id_post" id="id_post" value="">
                  <input type="hidden" name="acao" value="remover">
                  <p style="white-space: pre-line;">Deseja <b>REMOVER</b> o post selecionado do blog?
                    <div style="padding: 2px 0 2px 7px; border-left: 3px solid #d9534f; margin-top: 7px;">
                      <b>Título:</b> <span id="del-titulo"></span><br>
                      <b>Data de postagem:</b> <span id="del-data"></span><br>
                      <b>Autor:</b> <span id="del-autor"></span><br>
                    </div>
                  </p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Apagar</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- /modals -->
    
         <!-- footer content -->
         <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- scripts -->
    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="../vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="../vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>

    <script src="./js/moment/moment.min.js"></script>
    <script src="./js/moment/datetime-moment.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/main.js"></script>

    <!-- <script src="assets/js/crud_admin.js?v=<?php echo ASSETS_VERSION; ?>"></script> -->

    <script>

      var noticias = <?php echo json_encode(Blog\listar());?>;

      console.log(noticias);
 
      const columns_map = {
          array: [
              { 
                  name: "POSTS", 
                  data: (row, type, set, meta) => {
                      let checked = row.deletado == 0 ? "checked " : "";

                      let html = 
                      `<div class="news">
                        <div style="flex-grow: 1;">
                          <h2>${row.titulo}</h2>
                            
                          <span><i class="far fa-clock"></i> Postado em ${moment(row.data_publicacao).format('DD/MM/YYYY [às] HH:mm')} ${row.id_autor ? "por " + row.autor : ""}</span>
                          ${row.texto.replace(/\<figure[\s\S]+?\<\/figure\>/gm, "").replace(/\<img[\s\S]+?\/\>/gm, "").substring(0, 200)}...<br>
                        </div>
                        <div style="min-width: 200px; max-width:200px;">
                         <img style="width: 100%;" src="http://biologiaaprova.com.br/img/blog/${row.img}" alt="imagem noticia ${row.titulo}">
                        </div>
                      </div>
                      <div class="news-buttons">
                        <a href="blog-noticia.php?id=${row.id}" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Editar</a>
                        <button class="btn btn-danger btn-xs" onclick="apagar_post(${row.id})"><i class="fa fa-trash-o"></i> Apagar</button>
                      </div>`;
                      
                      return html;
                  }
              },
          ]
      };

      $(document).ready(function(){

          $('#noticias').DataTable({
            data: noticias,
            columns: columns_map["array"],
            order: [],
            fnDrawCallback: () => {
                var $switchs = $(this).find(".js-switch");
                $switchs.each((index, element) => {
                    if (element.closest("label").querySelector(".switchery"))
                        return;

                    new Switchery(element);
                });
            }
        });

        $("#form_remover").on("submit", {success_callback: success_remover}, submeter_formulario_files);
    });

    function apagar_post (id) {
        let post = get(id);
        let modal = $("#modal_deletar");

        $("#id_post").val(id);
        $("#del-titulo").html(post.titulo);
        $("#del-data").html(moment(post.data_publicacao).format('DD/MM/YYYY [às] HH:mm'));
        $("#del-autor").html(post.autor ? post.autor : "Não informado");

        modal.modal("show");
    }           

    function get(id){
      for(i in noticias){
        if(noticias[i].id == id)
          return noticias[i];
      }
      return null;
    }     

    function success_remover(res){
      let modal = $("#modal_deletar");
      alerta("Removido!", "O post foi removido do blog com sucesso!", "success");
      refresh();
      modal.modal('hide');
    }           

    function refresh (start, end) {
      $.ajax({
          method: "POST",
          url: "ajax/blog.php",
          data: { acao: "listar" },
          dataType: "JSON",
          beforeSend: () => loadingOn()
      })
      .done(res => {
          if (res.status != 1)
              alerta("Erro!", res.mensagem_erro);

          noticias = res.noticias;
          $("#noticias").DataTable().clear();
          $("#noticias").DataTable().rows.add(noticias).draw();
      })
      .error(x => console.log(x))
      .always(() => loadingOff());
    }          
    

    </script>
  </body>
</html>