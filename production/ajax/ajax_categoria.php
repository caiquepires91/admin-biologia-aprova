<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;
    }
}

function novo(){
    global $mysqli;
    $categoria = validarForm();
    try{
        $stmt_result = $mysqli->prepare(
            "INSERT INTO `categoria`(`id`, `nome`, `data_cadastro`, `data_liberacao`) 
            VALUES (default,?,?,?)"
            );  
        $stmt_result->bind_param("sss", $categoria['nome'], $categoria['data_cadastro'], $categoria['data_liberacao']);
        $stmt_result->execute();
        echo json_encode(array("success" => 1, "msg" => "Categoria registrada com sucesso!"));
    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function atualizar(){
    global $mysqli;
    $categoria = validarForm();
    $id = $_POST['id_cat'];
    extract($categoria);
    
    if($mysqli->query(
        "UPDATE categoria SET nome = '$nome', data_liberacao = '$data_liberacao'
        WHERE id = $id"
        )){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Registro atualizado com sucesso!"));
        }else{
            echo json_encode(array("success" => 2, "msg" => "Não houve alterações no registro!"));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id_cat'];
    if($mysqli->query(
        "UPDATE categoria 
            SET deletado = 1 
        WHERE id = $id")){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Categoria removida do banco de dados!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Categoria não encontrada, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 0, "msg" => "Erro ao remover categoria!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id_cat'];

    if($result = $mysqli->query(
        "SELECT nome, DATE_FORMAT(categoria.data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(categoria.data_liberacao, '%d/%m/%Y %H:%i') as data_liberacao, id
        FROM categoria 
        WHERE id = $id")){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => "1", "categoria" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Categoria não encontrada!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    $categorias = array();
    if($result = $mysqli->query(
        "SELECT area.nome, DATE_FORMAT(area.created_at, '%d/%m/%Y') as data_cadastro, area.id 
        FROM area 
        WHERE deletado = 0"
        )){
        while($row = $result->fetch_assoc()){
            $categorias[] = $row;
        }
        echo json_encode($categorias);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}


function validarForm(){
    $categoria['nome'] = filter_input(INPUT_POST, 'nome_cat', FILTER_SANITIZE_STRING);
    $data = filter_input(INPUT_POST, 'data_liberacao', FILTER_SANITIZE_STRING);

    if($categoria['nome'] == false || $categoria['nome'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe uma categoria válida!"));
        die();
    }

    if(!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4} ([01][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9]){0,1}$/", $data)){
        echo json_encode(array("success" => 0, "msg" => "Informe uma data de liberação válida!"));
        die();
    }

    $data_ajustado = str_replace('/', '-', $data);   
    $data_ajustado = date('Y-m-d H:i:s', strtotime($data_ajustado));

    if(!ValidaData($data_ajustado)){
        echo json_encode(array("success" => 0, "msg" => "A data de liberação deve ser superior a ".date('d/m/Y H:i:s')));
        die();
    }
    $categoria['data_liberacao'] = $data_ajustado;
    $categoria['data_cadastro'] = date('Y-m-d');

    return $categoria;
}

function ValidaData($data){
    $data_hoje = date('Y-m-d H:i:s');
	if ($data < $data_hoje){
	   return false;
	} else {
	   return true;
	}
}
?>