<?php
include_once __DIR__ . '/../lib/config.php';

$status = array(
    400 => '400 Requisição Inválida',
    404 => 'Recurso Não Encontrado',
    500 => '500 Erro no servidor'
    );

function validarAluno(){
    global $mysqli;
    if (isset($_GET['id']) && is_numeric($_GET['id'])){
        $id = (int) $_GET['id'];
        $result = $mysqli->query("SELECT usuario.nome, usuario.admin, usuario.cpf, usuario.email, usuario.cep, usuario.estado, usuario.cidade, usuario.telefone, usuario.data_nascimento, perfil.image, perfil.imgAtiva, perfil.imgPermissao FROM usuario INNER JOIN perfil ON perfil.id = usuario.id_perfil WHERE usuario.id = $id");
        if($result->num_rows > 0){
            $usuario = $result->fetch_assoc();
            $nomes = explode(" ", $usuario['nome']);
            $usuario['nome'] = count($nomes) > 1 ? $nomes[0].' '.$nomes[1] : $nomes[0];
            return $usuario;
        }
        else
            badRequest(404);
        }
      else{
        header("location: alunos.php");
        die(); //tratar erro caso o id não for válido
      }
}

function validarSimulado(){
    global $mysqli;
    if (isset($_GET['id']) && is_numeric($_GET['id'])){
        $id = (int) $_GET['id'];
        $result = $mysqli->query("SELECT nome, id, DATE_FORMAT(data_liberacao, '%d/%m/%Y') as data_liberacao, DATE_FORMAT(data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(data_final, '%d/%m/%Y') as data_final
        FROM simulado WHERE id = $id");
        if($result->num_rows > 0){
            $simulado = $result->fetch_assoc();
            return $simulado;
        }else
            badRequest(404);
      }else{
        header("location: simulados.php");
        die(); //tratar erro caso o id não for válido
      }
}


function getRespostas($items, $inicio,$id_usuario){
    global $mysqli;
    if (isset($_GET['id']) && is_numeric($_GET['id'])){
        $id = (int) $_GET['id'];
        $retorno = array();
        $retorno['id_pagina'] = $id;
        
        try{
            $topico = $mysqli->query("SELECT t.id, t.id_usuario as id_criador, t.descricao, t.deletado, t.data_criacao, a.nome, a.admin, a.data_cadastro, p.image, p.imgAtiva FROM topico as t INNER JOIN usuario as a ON t.id_usuario = a.id INNER JOIN perfil as p ON a.id_perfil = p.id WHERE t.id = $id;");
            if($topico->num_rows == 0)
                header("Location: forum.php");
            
            $respostas_topico = $mysqli->query("SELECT r.*, a.nome, a.admin, DATE_FORMAT(a.data_cadastro, '%d/%m/%Y') as data_cadastro, a.cidade, a.estado, p.image, p.status, p.imgAtiva from resposta as r LEFT JOIN usuario as a ON r.id_usuario = a.id LEFT JOIN perfil as p ON a.id_perfil = p.id WHERE id_topico = $id ORDER BY r.data_criacao ASC;");
            $likes_respostas = $mysqli->query("SELECT l.id_usuario, l.id_resposta from likes_resposta as l INNER JOIN resposta as r ON l.id_resposta = r.id INNER JOIN topico as t ON r.id_topico = t.id and t.id = $id WHERE l.id_usuario = $id_usuario");
            $likes_topico = $mysqli->query("SELECT * from likes_topico WHERE id_topico = $id and id_usuario = $id_usuario;");
            
            $topico = $topico->fetch_assoc();
            $topico['data_extenso'] = strftime('%A, %d de %B de %Y às %H:%M', strtotime($topico['data_criacao']));
            $topico['image'] = $topico['imgAtiva'] == 1? $topico['image'] : 'placeholder.png';
            $respostas_retorno = array();
            while($row = $respostas_topico->fetch_assoc()){
                $row['image'] = $row['imgAtiva'] == 1? $row['image'] : 'placeholder.png';
                extract($row);
                    $posts = $mysqli->query("SELECT count(*) as posts FROM resposta WHERE id_usuario = $id_usuario");
                    $likes = $mysqli->query("SELECT count(*) as likes FROM likes_resposta where id_resposta = $id");
                    $row['posts'] = $posts->fetch_assoc()['posts'];
                    $row['likes'] = $likes->fetch_assoc()['likes'];
                    $row['data_extenso'] = strftime('%d de %B de %Y às %H:%M', strtotime($row['data_criacao']));
                    $respostas_retorno[$row['id']] = $row;
            }
            
            while($row = $likes_respostas->fetch_assoc())
                $retorno['likes'][] = $row['id_resposta'];
            
            if($likes_topico->num_rows > 0)
                $retorno['liked'] = true;
            else $retorno['liked'] = false;
            
            
            $retorno['success'] = 1;
            $retorno['topico'] = $topico;
            $retorno['respostas'] = $respostas_retorno;
            $retorno['total_registros'] = (int)$respostas_topico->num_rows;
            $retorno['total_paginas'] = ceil($retorno['total_registros'] / $items);
            
            return $retorno;

        }catch(Exception $e){
            die($mysqli->error);
        }
        
      }else{
        header("location: forum.php");
        //die(); //tratar erro caso o id não for válido
      }
}

function getGateways(){
    global $mysqli;
    $result = $mysqli->query("SELECT id, nome, current FROM gateway");
    return $result->fetch_all(MYSQLI_ASSOC);
}

function validarErro(){
    if (isset($_GET['codigo']) && is_numeric($_GET['codigo'])){
        return (int) $_GET['codigo'];
      }else{
        header("location: index.php");
        die(); //tratar erro caso o id não for válido
      }
}

function badRequest($codigo = 400){
    header("location: error.php?codigo=".$codigo);
}

?>