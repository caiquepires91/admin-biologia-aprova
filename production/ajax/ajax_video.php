<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';


//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;

    $video = getVideo();

    try{
        $query = "INSERT INTO `video`(id, idArea, nome, src, descricao, duracao, img, created_at, data_liberacao) 
        VALUES (default,?,?,?,?,?,?,?,?)"; 
        $stmt_result = $mysqli->prepare($query);
        $stmt_result->bind_param("isssssss", $video['idArea'],$video['nome'],$video['src'],$video['descricao'],$video['duracao'],$video['img'],$video['data_cadastro'],$video['data_liberacao']);
        $stmt_result->execute();
        echo json_encode(array("success" => 1, "msg" => "Vídeo cadastrado com sucesso!"));
    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function atualizar(){
    global $mysqli;
    $video = getVideo();
    $id = $_POST['id_video'];
    $update = 'UPDATE video SET nome = \''.$video["nome"].'\', src = \''.$video["src"].'\', descricao = \''.$video["descricao"].'\', duracao = \''.$video["duracao"].'\',
            idArea = \''.$video["idArea"].'\', data_liberacao = \''.$video["data_liberacao"].'\', img = \''.$video["img"].'\'
            WHERE id = '.$id.'';
     
    if($mysqli->query($update)){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Registro atualizado com sucesso!"));
        }else{
            echo json_encode(array("success" => 2, "msg" => "Não houve alterações no registro!"));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id_video'];
    $delete = "UPDATE video SET deletado = 1 WHERE id = $id";
    if($mysqli->query($delete)){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Vídeo removido com sucesso!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Vídeo não encontrado, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 0, "msg" => "Erro ao remover vídeo!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id_video'];
    $select = "SELECT v.id, v.nome, v.src, v.img, v.idArea, v.duracao, v.descricao,  DATE_FORMAT(v.created_at, '%d/%m/%Y') as data_cadastro, 
        DATE_FORMAT(v.data_liberacao, '%d/%m/%Y %H:%i') as data_liberacao, area.nome as categoria_nome
        FROM videos as v
        INNER JOIN area on area.id = v.idArea
        WHERE v.id = $id";

    if($result = $mysqli->query($select)){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => "1", "video" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Vídeo não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    $select = "SELECT v.id, v.nome, v.src, v.img, v.idArea, v.duracao, v.descricao, DATE_FORMAT(v.created_at, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(v.data_liberacao, '%d/%m/%Y %H:%i') as data_liberacao, area.nome as categoria_nome 
        FROM videos as v 
        INNER JOIN area on area.id = v.idArea
        WHERE v.deletado = 0";
    $videos = array();
    if($result = $mysqli->query($select)){
        while($row = $result->fetch_assoc()){
            $videos[] = $row;
        }
        echo json_encode($videos);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

//-----------FUNCOES AUXILIARES --------------//

function getVideo(){
    $video['nome'] = filter_input(INPUT_POST, 'nome-video', FILTER_SANITIZE_STRING);//$_POST['nome-video'];
    $video['descricao'] = filter_input(INPUT_POST, 'desc-video', FILTER_SANITIZE_STRING);//$_POST['desc-video'];
    $video['src'] = filter_input(INPUT_POST, 'link-video', FILTER_SANITIZE_STRING);//$_POST['link-video'];
    $video['duracao'] = filter_input(INPUT_POST, 'duracao', FILTER_SANITIZE_STRING);//$_POST['duracao'];
    

    if($video['nome'] == false || $video['nome'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um nome!"));
        die();
    }
    if($video['descricao'] == false || $video['descricao'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe uma descrição válida!"));
        die();
    }
    if($video['src'] == false || $video['src'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um link válido!"));
        die();
    }

    $video['idArea'] = $_POST['id-categoria'];
    if($video['idArea'] == false || $video['idArea'] == '0'){
        echo json_encode(array("success" => 0, "msg" => "Selecione uma categoria!"));
        die();
    }

    $data = $_POST['data_liberacao'];
    if(!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4} ([01][0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9]){0,1}$/", $data)){
        echo json_encode(array("success" => 0, "msg" => "Informe uma data de liberação válida!"));
        die();
    }
    $data_ajustado = str_replace('/', '-', $data);   
    $data_ajustado = date('Y-m-d H:i:s', strtotime($data_ajustado));

    if(!ValidaData($data_ajustado, $video['idArea'])){
        echo json_encode(array("success" => 0, "msg" => "A data de liberação do vídeo deve ser superior ou igual à data da categoria selecionada!"));
        die();
    }

    //tratando arquivo de imagem
    $video['data_liberacao'] = $data_ajustado;
    $image_name = "";
    $target_dir = "../uploads/videos/img/";

    if($_POST['action'] == 'novo'){
    $image_name = time() .'_'. basename($_FILES["foto-video"]["name"]);
    $target_file = $target_dir.$image_name;

    if($result = saveImg($target_file)){
        echo $result;
        die();
    }
    $video['img'] = $image_name;

    }else if($_FILES['foto-video']['size'] !== 0 && $_POST['action'] == "atualizar"){
        $img_atual = $target_dir.$_POST['img-atual'];
        unlink($img_atual);
        $image_name = time() .'_'. basename($_FILES["foto-video"]["name"]);
        $target_file = $target_dir.$image_name;
        if($result = saveImg($target_file)){
            echo $result;
            die();
        }
        $video['img'] = $image_name;

    }
    else if($_FILES['foto-video']['size'] == 0 && $_POST['action'] == "atualizar"){
        $video['img'] = $_POST['img-atual'];
    }

    $video['data_cadastro'] = date('Y-m-d');
    return $video;
}

function ValidaData($data, $id_categoria){
    global $mysqli;
    $result = $mysqli->query("SELECT data_liberacao FROM categoria WHERE id = $id_categoria");
    $data_liberacao = $result->fetch_assoc()['data_liberacao'];

    $data_categoria = date('Y-m-d H:i', strtotime($data_liberacao)); //busca data de liberacao da categoria
	if ($data < $data_categoria){
	   return false;
	} else {
	   return true;
	}
}

function saveImg($target_file){
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    if(!$_FILES["foto-video"]["tmp_name"]){
        die(json_encode(array("success" => 0, "msg" => "Selecione uma imagem!")));
    }
    $check = getimagesize($_FILES["foto-video"]["tmp_name"]);
    //check se é imagem
    if($check == false) {
        die(json_encode(array("success" => 0, "msg" => "Imagem inválida!")));
    }
    // Check file size, 1 MB
    if ($_FILES["foto-video"]["size"] > 1000000) {
        die(json_encode(array("success" => 0, "msg" => "A imagem de capa não deve ter mais que 1MB!")));

    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        die(json_encode(array("success" => 0, "msg" => "Formatos de imagens aceitos: JPG, JPEG, PNG, GIF!")));
    }

    // if everything is ok, try to upload file

    if (!move_uploaded_file($_FILES["foto-video"]["tmp_name"], $target_file)) {
        die(json_encode(array("success" => 0, "msg" => "Erro ao fazer upload de arquivo, por favor tente mais tarde!")));
    }
    return false;
}
?>