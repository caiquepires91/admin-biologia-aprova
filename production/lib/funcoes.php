<?php 

function formata_dinheiro($n){
    return "R$ ".number_format($n, 2, ',', '.');
}

function verifica_acesso($niveis){
	return in_array($_SESSION["perfil_acesso"], $niveis);
}

function verifica_nivel($permitidos){
	if(!verifica_acesso($permitidos)){
		switch($_SESSION["perfil_acesso"]){
			case 0: header("Location: blog.php"); exit; break;
		}
	}
}

function gerar_randon_token ($size) {
	$size = intval($size);

	if ($size % 2 === 1)
		$size++;

	return bin2hex(openssl_random_pseudo_bytes($size/2));
}

function mysqli_fetch_by_index($object, $index){
	if(!$object)
		return NULL;
	$arr = $object->fetch_assoc();
	return $arr[$index];
}

function antisql($mysqli,$variavel) {
	return $mysqli->real_escape_string(strip_tags(trim($variavel)));
  }

  function tirarAcentos($string){
    return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/","/(Ã)/"),explode(" ","a A e E i I o O u U n N C c A"),$string);
}

function text_to_id ($valor) {
	if (empty($valor))
		return $valor;

	$valor = trim(validar_nome_limpo($valor));
	$valor = strtolower($valor);
	$valor = preg_replace("/[^a-z0-9_ ]/", "", $valor);
	$valor = str_replace(" ", "_", $valor);
	
	return $valor;
}

function validar_entradas ($recebidas, $esperadas) {
	$temp = [];

	foreach ($esperadas as $esperada) {
		$temp[$esperada] = "";
    }

	if (count(array_diff_key($temp, $recebidas)) > 0)
		return false;

	return true;
}


// ------------------------------------------------------------------------------------------------
// --------------------------------- Validações de tipos de dados ---------------------------------
// ------------------------------------------------------------------------------------------------


function validar_duplo($valor){
	if (preg_match("/^[0-9]+_[0-9]*$/", $valor)) {
		$duplo = explode('_', $valor);
		$duplo[1] = isset($duplo[1])? $duplo[1] : NULL;
		return $duplo;
	}
	else
		return null;
}

function validar_texto ($valor) {
	$valor = trim($valor);
	
	if (empty($valor))
		return null;
	else 
		return $valor;
}

function validar_nome_limpo ($valor) {
	$valor = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/","/(Ç)/","/(ç)/"), explode(" ", "a A e E i I o O u U n N C c"), $valor);

	return $valor ? $valor : null;
}
function validar_uppercase ($valor) {
	if (is_string($valor))
		return mb_strtoupper($valor);
	else
		return null;
}


// Numéricos
function validar_inteiro ($valor) {
	if (preg_match("/^[+-]?\d+$/", $valor)) {
		return intval($valor);
	}
	else
		return null;
}

function validar_dia ($valor) {
	if (preg_match("/^[+-]?\d+$/", $valor)) {
		$dia = intval($valor);
		if($dia >= 1 && $dia <= 31)
				return $dia;
		else
			return null;
	}
	else
		return null;
}

function validar_quantidade ($valor) {
	if (preg_match("/^\d+$/", $valor)) {
		return intval($valor);
	}
	else
		return null;
}
function validar_decimal ($valor) {
	$valor = str_replace(".", "", $valor);
	$valor = str_replace(",", ".", $valor);

	if (is_numeric($valor))
		return floatval($valor);
	else
		return null;
}
function validar_decimal_en ($valor) {
	$valor = str_replace(",", "", $valor);

	if (is_numeric($valor))
		return floatval($valor);
	else
		return null;
}

// Contatos
function validar_telefone ($valor) {
	if ( preg_match("/^\(\d{2}\) \d{4,5}-\d{4}$/", $valor) )
		return $valor;
	else
		return null;
}
function validar_email ($valor) {
	if (preg_match("/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/", $valor)) {
		return $valor;
	}
	else {
		return null;
	}
}

function trim_value(&$value) 
{ 
    $value = trim($value); 
}

function validar_multiplos_emails ($valor) {
	$emails = array_filter( explode(",", $valor), 'strlen' );
	array_walk($emails, 'trim_value');
	$emails = array_unique($emails);

	foreach($emails as $email){
		if(!validar_email($email))
			return null;
	}
	return $emails;
}

function validar_cep ($valor) {
	if ( preg_match("/^\d{5}-\d{3}$/", $valor) )
		return $valor;
	else
		return null;
}

// Data en-EN
function validar_date ($valor) {
	if ( preg_match("/^\d{4}-\d{2}-\d{2}$/", $valor) ) {
		$data = date("Y-m-d", strtotime($valor));

		if ($data === $valor)
			return $valor;
		else
			return null;
	}
	else
		return null;
}

function aplicar_mascara($texto){
	while(preg_match('/\*\*(.*?)\*\*/s', $texto, $matches)){
		if(isset($matches[1])){
			$replacement = "<b>".$matches[1]."</b>";
			$texto = preg_replace('/\*\*(.*?)\*\*/s', $replacement, $texto, 1);
		}
	}

	return $texto;
}

function validar_datetime ($valor) {
	if ( preg_match("/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$/", $valor) ) {
		$data = date("Y-m-d H:i:s", strtotime($valor));

		if ($data === $valor)
			return $valor;
		else
			return null;
	}
	else
		return null;
}

function validar_datatempo ($valor) {
	if ( preg_match("/^\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}$/", $valor) ) {
		$data = DateTime::createFromFormat('d/m/Y H:i', $valor);

		if ($data)
			return $data->format("Y-m-d H:i");
		else
			return null;
	}
	else if ( preg_match("/^\d{2}\/\d{2}\/\d{4} \d{2}:\d{2}:\d{2}$/", $valor) ) {
		$data = DateTime::createFromFormat('d/m/Y H:i:s', $valor);

		if ($data)
			return $data->format("Y-m-d H:i:s");
		else
			return null;
	}
	else
		return null;
}


function validar_birthdate ($valor) {
	$valor = validar_date($valor);

	if (is_null($valor))
		return null;
	
	if (strtotime($valor) > time())
		return null;
	
	return $valor;
}
function validar_schedule ($valor) {
	$valor = validar_date($valor);

	if (is_null($valor))
		return null;
	
	if (strtotime($valor) < strtotime("midnight"))
		return null;
	
	return $valor;
}

// Datas pt-BR
function validar_data ($valor) {
	if ( preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $valor) ) {
		$data = DateTime::createFromFormat('d/m/Y', $valor);

		if ($data)
			return $data->format("Y-m-d");
		else
			return null;
	}
	else
		return null;
}


function validar_hora ($valor) {
	if ( preg_match("/^\d{2}:\d{2}:\d{2}$/", $valor) ) {
		$temp = explode(":", $valor);

		if (intval($temp[0]) > 23)
			return null;
		if (intval($temp[1]) > 59)
			return null;
		if (intval($temp[2]) > 59)
			return null;

		return $valor;
	}
	else if ( preg_match("/^\d{2}:\d{2}$/", $valor) ) {
		$temp = explode(":", $valor);

		if (intval($temp[0]) > 23)
			return null;
		if (intval($temp[1]) > 59)
			return null;

		return $valor . ":00";
	}
	else
		return null;
}

function validar_hora2 ($valor) {
	if ( preg_match("/^\d{2}:\d{2}$/", $valor) ) {
		$temp = explode(":", $valor);

		if (intval($temp[0]) > 23)
			return null;
		if (intval($temp[1]) > 59)
			return null;

		return $valor;
	}
	else
		return null;
}

function validar_nascimento ($valor) {
	$valor = validar_data($valor);

	if (is_null($valor))
		return null;
	
	if (strtotime($valor) > time())
		return null;
	
	return $valor;
}
function validar_agendamento ($valor) {
	$valor = validar_data($valor);

	if (is_null($valor))
		return null;
	
	if (strtotime($valor) < strtotime("midnight"))
		return null;
	
	return $valor;
}

// Pessoas
function validar_cpf ($valor) {
	if ( !preg_match("/^\d{3}.\d{3}.\d{3}-\d{2}$/", $valor) && !preg_match("/^\d{11}$/", $valor))
		return null;

	$cpf = preg_replace("/[^0-9]/", "", $valor);

	if ($cpf == "00000000000"  || $cpf == "11111111111" || $cpf == "22222222222" || $cpf == "33333333333" || $cpf == "44444444444" || $cpf == "55555555555" || $cpf == "66666666666" || $cpf == "77777777777" || $cpf == "88888888888" || $cpf == "99999999999") {
		return null;
	} else {
		for ($t = 9; $t < 11; $t++) {
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}

			$d = ((10 * $d) % 11) % 10;

			if ($cpf{$c} != $d) {
				return null;
			}
		}

		return $cpf;
	}
}

function validar_cnpj($valor)
{	
	if ( !preg_match("/^\d{2}.\d{3}.\d{3}\/\d{4}-\d{2}$/", $valor) && !preg_match("/^\d{14}$/", $valor))
		return null;

	$cnpj = preg_replace('/[^0-9]/', '', (string) $valor);
	
	// Valida tamanho
	if (strlen($cnpj) != 14)
		return false;
	// Verifica se todos os digitos são iguais
	if (preg_match('/(\d)\1{13}/', $cnpj))
		return false;	
	// Valida primeiro dígito verificador
	for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
		return false;
	// Valida segundo dígito verificador
	for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
	{
		$soma += $cnpj{$i} * $j;
		$j = ($j == 2) ? 9 : $j - 1;
	}
	$resto = $soma % 11;
	if($cnpj{13} == ($resto < 2 ? 0 : 11 - $resto))
		return $cnpj;
	else
		return false;
}

function validar_login ($valor) {
	if ( !preg_match("/^\d{3}.\d{3}.\d{3}-\d{2}$/", $valor) && !preg_match("/^\d{11}$/", $valor))
		return null;

	$cpf = preg_replace("/[^0-9]/", "", $valor);

	if ($cpf == "00000000000"  || $cpf == "11111111111" || $cpf == "22222222222" || $cpf == "33333333333" || $cpf == "44444444444" || $cpf == "55555555555" || $cpf == "66666666666" || $cpf == "77777777777" || $cpf == "88888888888" || $cpf == "99999999999") {
		return $cpf;
	} else {
		for ($t = 9; $t < 11; $t++) {
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}

			$d = ((10 * $d) % 11) % 10;

			if ($cpf{$c} != $d) {
				return null;
			}
		}

		return $cpf;
	}
}
function validar_senha ($valor) {
	if ( strlen($valor) < 6)
		return null;
	
	return $valor;
}

function validar_status ($valor) {
	if (preg_match("/^[+-]?\d+$/", $valor)) {
		if(($valor == 0) || ($valor == 1))
			return intval($valor);
		else
			return null;
	}
	else
		return null;
}

// ------------------------------------------------------------------------------------------------
// --------------------------------- formatação de dados ---------------------------------
// ------------------------------------------------------------------------------------------------

function formata_cpf_cnpj($value)
{
  $cnpj_cpf = preg_replace("/\D/", '', $value);
  
  if (strlen($cnpj_cpf) === 11) {
    return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
  } 
  
  return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
}

// ------------------------------------------------------------------------------------------------
// --------------------------------- MANIPULAÇÃO DE DATAS -----------------------------------------
// ------------------------------------------------------------------------------------------------

function formata_hora($segundos){
	$segundos = (int)$segundos;
	$hora = floor($segundos / 3600);
	$segundos %= 3600;
	$minutos = floor($segundos / 60);
	return ($hora <= 9 ? "0" : "") . $hora . "h " . ($minutos <= 9 ? "0" : "") . $minutos ."min";
}

function secondsToTime($value){
	$value = (int)$value;

	$hora   = floor($value / 3600);
    $minutos = floor(($value - ($hora * 3600)) / 60);
    $segundos = $value - ($hora * 3600) - ($minutos * 60);

	if ($hora   < 10) {$hora   = "0".$hora;}
    if ($minutos < 10) {$minutos = "0".$minutos;}
	if ($segundos < 10) {$segundos = "0".$segundos;}
	
    return $hora.":".$minutos.":".$segundos;
}

function secToHR($seconds, $format = null) {
	$hours = floor($seconds / 3600);
	$minutes = floor(($seconds / 60) % 60);
	$seconds = $seconds % 60;
	if($format)
		return $format === 'H:i:s' ? "$hours:$minutes:$seconds" : "$hours:$minutes";
	return "$hours:$minutes";
  }

function days_diff($d1, $d2) {
    $x1 = days($d1);
    $x2 = days($d2);
   
    if ($x1 && $x2) {
        return abs($x1 - $x2);
    }
}

function add_days($data, $intervalo){
	return date('Y-m-d', strtotime($data . " +$intervalo days"));
}


function days($x) {
    if (get_class($x) != 'DateTime') {
        return false;
    }
   
    $y = $x->format('Y') - 1;
    $days = $y * 365;
    $z = (int)($y / 4);
    $days += $z;
    $z = (int)($y / 100);
    $days -= $z;
    $z = (int)($y / 400);
    $days += $z;
    $days += $x->format('z');

    return $days;
}

function tempo_corrido($time) {

	$now = strtotime(date('Y-m-d'));
	$time = strtotime($time);
	$diff = $now - $time;

	$seconds = $diff;
	$days = round($diff / 86400);
	$weeks = round($diff / 604800);
	$months = round($diff / 2419200);
	$years = round($diff / 29030400);

	if ($days <= 7) return $days==1 ?'1 dia atras':$days.' dias atrás';
	else if ($weeks <= 4) return $weeks==1 ?'1 semana atrás':$weeks.' semanas atrás';
	else if ($months <= 12) return $months == 1 ?'1 mês atrás':$months.' meses atrás';
	else return $years == 1 ? 'um ano atrás':$years.' anos atrás';
}

?>