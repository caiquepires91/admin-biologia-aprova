<?php 

  require __DIR__.'/php/autentica.php';
  include_once __DIR__ . '/./lib/config.php';

  $info = array();
  if($result = $mysqli->query("
  SELECT 
  (SELECT count(*) FROM usuarios WHERE usuarios.deletado = 0 and usuarios.admin = 0) as nAlunos,
  (SELECT count(*) FROM videos WHERE deletado = 0) as nVideos,
  (SELECT count(*) FROM simulado WHERE deletado = 0) as nSimulados,
  (SELECT count(*) FROM simulado_usuario WHERE deletado = 0) as nRealizados,
  (SELECT count(*) FROM usuarios WHERE usuarios.admin = 0 and usuarios.created_at BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND CURRENT_DATE AND usuarios.deletado = 0) as nAlunosSemana,
  (SELECT count(*) FROM videos WHERE videos.created_at BETWEEN DATE_SUB(CURRENT_DATE, INTERVAL 7 DAY) AND CURRENT_DATE AND deletado = 0) as nVideosSemana;")){
    $info = $result->fetch_assoc();
  }else{
    $info['nAlunos'] = "-";
    $info['nVideos'] = "-";
    $info['nSimulados'] = "-";
    $info['nTransac'] = "-";
    $info['nAlunosSemana'] = "-";
    $info['nVideosSemana'] = "-";
    $info['nTransacSemana'] = "-";
    $info['nRealizados'] = "-";
  }

  ?>
  

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Curso Somma</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <?php include './componentes/DataTableCSS.php' ?>


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <?php include __DIR__.'/./componentes/menuFooter.php'?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count"><?php echo $info['nAlunos']?></div>
                  <h3>Total de Alunos</h3>
                  <p><span class=''><?php echo $info['nAlunosSemana']?></span> registros na última semana</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-play-circle"></i></div>
                  <div class="count"><?php echo $info['nVideos']?></div>
                  <h3>Total de Vídeos</h3>
                  <p><span class=''><?php echo $info['nVideosSemana']?></span> registros na última semana</p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-file-alt"></i></div>
                  <div class="count"><?php echo $info['nSimulados']?></div>
                  <h3>Total de Simulados</h3>
                  <p><span class=''><?php echo $info['nRealizados']?></span> realizados no total</p>
                </div>
              </div>
             <!--  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fas fa-exchange-alt r-90  "></i></div>
                  <div class="count"><?php echo $info['nTransac']?></div>
                  <h3>Transações</h3>
                  <p><span class=''><?php echo $info['nTransacSemana']?></span> transações na última semana</p>
                </div>
              </div> -->
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-3 col-xs-12">
                  <a href="alunos.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-users"></i></div>
                    <div class="atalho-descricao"><span>Alunos</span></div>
                  </div>
                  </a>
                </div>

                <div class="col-md-4 col-sm-3 col-xs-12">
                  <a href="admins.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fas fa-user-shield"></i></div>
                    <div class="atalho-descricao"><span>Administradores</span></div>
                  </div>
                  </a>
                </div>
               
              <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="videos.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-play-circle"></i></div>
                    <div class="atalho-descricao"><span>Vídeos</span></div>
                  </div>
                  </a>
                </div>
                
              <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="categorias.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-list"></i></div>
                    <div class="atalho-descricao"><span>Categorias</span></div>
                  </div>
                  </a>
                </div>
                
             <!--  <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="planos.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-cart-plus"></i></div>
                    <div class="atalho-descricao"><span>Planos</span></div>
                  </div>
                  </a>
                </div> -->
                
              <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="cupons.php">
                <div class="atalho btn">
                  <div class="atalho-icon"><i class="fas fa-donate"></i></div>
                  <div class="atalho-descricao"><span>Cupons</span></div>
                </div>
                </a>
              </div>

              <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="blog.php">
                <div class="atalho btn">
                  <div class="atalho-icon"><i class="fas fa-blog"></i></div>
                  <div class="atalho-descricao"><span>Blog</span></div>
                </div>
                </a>
              </div>
              
              <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="simulados.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-file-alt"></i></div>
                    <div class="atalho-descricao"><span>Simulados</span></div>
                  </div>
                  </a>
                </div>

           <!--  <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="documentos.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-folder"></i></div>
                    <div class="atalho-descricao"><span>Documentos</span></div>
                  </div>
                  </a>
                </div> -->

           <!--  <div class="col-md-4 col-sm-3 col-xs-12">
              <a href="forum.php">
                  <div class="atalho btn">
                    <div class="atalho-icon"><i class="fa fa-comments"></i></div>
                    <div class="atalho-descricao"><span>Fórum</span></div>
                  </div>
                  </a>
                </div> -->
                
            </div>
            <div class="row">
              <div class="x_panel">
                <div class="x_title"><h4>Gráfico de Vendas</h4></div>
                <div class="x_content">
                  <div class="col-md-10 col-sm-10 col-xs-12">
                    <div id="vendas_echart" style="height:350px;"></div>
                  </div>
                  <div class="col-md-2 col-sm-2 col-xs-12">
                  <form id="form-filter-grafico" class="form">
                    <div class="form-group">
                      <label>Período: </label>
                      <div class="radio">
                        <label>
                          <input type="radio" value="0" name="periodo_opt"> Ano
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" value="1" name="periodo_opt"> Mês
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" value="2" name="periodo_opt"> Dia
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" value="3" name="periodo_opt"> Personalizado
                        </label>
                      </div>
                    </div>
                    <div class="form-group filtro ano collapse">
                      <label class="periodo-tipo">Escolher ano: </label>
                      <select class="periodo-select form-control" name="ano-periodo">
                        <?php
                          //pegando todos os anos validos no banco
                          $result = $mysqli->query("SELECT left(data, 4) as anos from vendas WHERE deletado = 0 GROUP BY left(data, 4)");
                          while($row = $result->fetch_assoc()){
                            echo "<option value='$row[anos]'>$row[anos]</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="form-group filtro mes collapse">
                      <label class="periodo-tipo">Escolher mês: </label>
                      <select class="periodo-select form-control" name="mes-periodo">
                        <option value="01">Janeiro</option>
                        <option value="02">Fevereiro</option>
                        <option value="03">Março</option>
                        <option value="04">Abril</option>
                        <option value="05">Maio</option>
                        <option value="06">Junho</option>
                        <option value="07">Julho</option>
                        <option value="08">Agosto</option>
                        <option value="09">Setembro</option>
                        <option value="10">Outubro</option>
                        <option value="11">Novembro</option>
                        <option value="12">Dezembro</option>
                      </select>
                    </div>
                    <div class="form-group filtro dia collapse">
                      <label class="periodo-tipo">Escolher dia: </label>
                      <select class="periodo-select form-control" name="dia-periodo">
                        <?php
                          for($i = 1; $i <= 31; $i++){
                            echo "<option value='$i'>$i</option>";
                          }
                        ?>
                      </select>
                    </div>
                    <div class="form-group filtro personalizado collapse">
                      <label>Data Inicial:</label>
                      <input type="text" name="data-inicial" class="form-control data">
                      <label>Data Final:</label>
                      <input type="text" name="data-final" class="form-control data">
                    </div>
                  <div class="">
                    <button type="submit" class="btn btn-primary"> Filtrar dados</button>
                  </div>
                </form>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>

    <!-- ECharts -->
    <script src="../vendors/echarts/dist/echarts.min.js"></script>
    <script src="../vendors/echarts/map/js/world.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/index.js"></script>


  </body>
</html>