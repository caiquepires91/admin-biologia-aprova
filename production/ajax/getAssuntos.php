<?php 
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

$query = $mysqli->prepare("SELECT nome as value, nome as label, id FROM assunto");
$query->execute();
$result = $query->get_result();

echo json_encode($result->fetch_all(MYSQLI_ASSOC));

?>