<?php 
// Inicia sessões 
include_once __DIR__ . '/../lib/config.php';
session_start(); 
 
// Verifica se existe os dados da sessão de login 
if ( !isset($_SESSION["id"]) ) {
	header("Location: login.php"); 
    exit; 
}

else //verifica se a sessao expirou por inatividade
    if( $_SESSION['ultima_atividade'] < time()-$_SESSION['timeout'] ) { //Se o usuário ficou inativo durante o tempo selecionado, encerre a sessão
        $mysqli->query("UPDATE sessoes SET encerrada = 1 WHERE id = $_SESSION[id]");
        session_destroy();
        header('Location: login.php?logout=1');
        exit;
    }
    else{ //verifica se a sessão atual foi encerrada
        $result = $mysqli->query("SELECT * FROM sessoes WHERE id = $_SESSION[id] AND encerrada = 0");
        if (!$sessao = $result->fetch_assoc()) {
            session_destroy();
        
            header('Location: login.php?logout=1');
            exit();
        }
        else { //atualiza ultima atividade
            $_SESSION['ultima_atividade'] = time();
            $mysqli->query("UPDATE sessoes SET ultima_atividade = NOW() WHERE id = $_SESSION[id] AND encerrada = 0");
        }
    }

?>