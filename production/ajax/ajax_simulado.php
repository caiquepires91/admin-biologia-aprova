<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';


//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;

    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;

        case 'ler_questoes_simulado':
            ler_questoes_simulado();
        break;

        case 'numero_de_questoes':
            numero_de_questoes();
        break;

        case 'ler_info_simulado':
            ler_info_simulado();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;
    $simulado = getSimulado();
    
    $query = "INSERT INTO `simulado`(`id`, `nome`, `id_categoria`, `data_cadastro`, `data_liberacao`, `data_final`) VALUES (default,?,?,?,?,?)"; 
    try{
        $stmt = $mysqli->prepare($query);
        $stmt->bind_param('sisss', $simulado['nome'], $simulado['id_categoria'], $simulado['data_cadastro'], $simulado['data_liberacao'], $simulado['data_final']);
        $stmt->execute();
        echo json_encode(array("success" => 1, "msg" => "Simulado registrado com sucesso!"));
    }catch(Exception $e){
        echo json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode()));
    }
}

function atualizar(){
    global $mysqli;
    $simulado = getSimulado();
    $id = $_POST['id'];

    $update = "UPDATE simulado SET nome = ?, id_categoria = ?, data_liberacao = ?, data_final = ? WHERE id = ?";
    try{
        $stmt = $mysqli->prepare($update);
        $stmt->bind_param("sissi", $simulado['nome'], $simulado['id_categoria'], $simulado['data_liberacao'], $simulado['data_final'], $id);
        $stmt->execute();
        if($stmt->affected_rows > 0) 
            echo json_encode(array("success" => 1, "msg" => "Simulado atualizado com sucesso!"));
        else
            echo json_encode(array("success" => 2, "msg" => "Não houve alterações no registro!"));
    }catch(Exception $e){
        echo json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode()));
    }
}

function deletar(){
    global $mysqli;
    $id = $_POST['id'];
    $delete = "UPDATE simulado SET deletado = 1 WHERE id = $id";
    if($mysqli->query($delete)){
        if($mysqli->affected_rows > 0){
            echo json_encode(array("success" => 1, "msg" => "Simulado removido do banco de dados!"));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Simulado não encontrado, por favor atualize a página!"));
        }
    }else{
        echo json_encode(array("success" => 0, "msg" => "Erro ao remover simulado!"));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id'];

	$select = "SELECT s.nome, s.id, DATE_FORMAT(s.data_liberacao, '%d/%m/%Y') as data_liberacao, DATE_FORMAT(s.data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(s.data_final, '%d/%m/%Y') as data_final, s.id_categoria, c.nome AS categoria_nome
        FROM simulado s LEFT JOIN categoria c ON c.id = s.id_categoria WHERE s.id = $id";

    if($result = $mysqli->query($select)){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => 1, "simulado" => $row));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Simulado não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_todos(){
    global $mysqli;
    $select = "SELECT s.nome, DATE_FORMAT(s.data_liberacao, '%d/%m/%Y') as data_liberacao, DATE_FORMAT(s.data_cadastro, '%d/%m/%Y') as data_cadastro, DATE_FORMAT(s.data_final, '%d/%m/%Y') as data_final, s.id, s.concluidos AS concluidos_old, s.id_categoria, c.nome AS categoria_nome, (SELECT COUNT(*) AS realizados FROM usuario_simulado WHERE id_simulado = s.id) AS concluidos FROM simulado s LEFT JOIN categoria c ON c.id = s.id_categoria WHERE s.deletado = 0";
    $simulados = array();

    if($result = $mysqli->query($select)){
        while($row = $result->fetch_assoc()){
            $simulados[] = $row;
        }
        echo json_encode($simulados);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_questoes_simulado(){
    global $mysqli;
    $id = $_GET['id'];
    
    $select = "SELECT q.id, q.enunciado, q.codigo, q.id_simulado, DATE_FORMAT(q.data_criacao, '%d/%m/%Y') as data_criacao, a.nome as assunto from questao as q INNER JOIN assunto as a ON q.id_assunto = a.id WHERE q.id_simulado = $id and q.deletado = 0;";;
    $simulados = array();
    if($result = $mysqli->query($select)){
        while($row = $result->fetch_assoc()){
            $simulados[] = $row;
        }
        echo json_encode($simulados);
        
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function numero_de_questoes(){
    global $mysqli;
    $id = $_GET['id'];

    $select = "SELECT count(*) as total FROM questao where id_simulado = $id and deletado = 0";
    if($result = $mysqli->query($select)){
        if($result->num_rows == 1){
            $row = $result->fetch_assoc();
            echo json_encode(array("success" => "1", "total" => (int)$row['total']));
        }else{
            echo json_encode(array("success" => 0, "msg" => "Simulado não encontrado!", "cod" => 404));
        }
    }else{
        echo json_encode(array("success" => 99, "msg" => $mysqli->error, "cod" => $mysqli->errno));
    }
}

function ler_info_simulado(){
    global $mysqli;
    $id = $_GET['id'];
    
    try{
        //quantidade de simulados realizados
        $result1 = $mysqli->query("SELECT COUNT(*) AS realizados FROM usuario_simulado WHERE id_simulado = $id;");
        //numero de acertos por questao
        $result2 = $mysqli->query("SELECT q.codigo, sum(a.isCerta) as certas from usuario_questao as a INNER JOIN questao as q ON q.id = a.id_questao and q.id_simulado = $id and q.deletado = 0 GROUP BY a.id_questao;");
        //numero de questoes corretas
        $result3 = $mysqli->query("SELECT SUM(isCerta) as corretas FROM usuario_questao as q
        INNER JOIN usuario_simulado as a ON a.id_usuario = q.id_usuario and a.id_simulado = $id
        INNER JOIN questao as c ON c.id = q.id_questao and c.id_simulado = $id and c.deletado = 0;");
        //pontos por usuario
        $result4 = $mysqli->query("SELECT id_usuario, sum(isCerta) as pontos FROM `usuario_questao` where id_questao in (SELECT id FROM questao where id_simulado = $id and deletado = 0) group by id_usuario");
        //alunos que deram início ao simulado
        $result5 = $mysqli->query("SELECT a.nome as nome_aluno, DATE_FORMAT(s.data_realizacao, '%d/%m/%Y %H:%i') as data_realizacao FROM usuario_simulado as s, usuario as a WHERE s.id_usuario = a.id AND s.id_simulado = $id;");
        //numero de questoes por simulado
        $result6 = $mysqli->query("SELECT count(*) as nq FROM questao WHERE id_simulado = $id and deletado = 0;");
        
        $dados = array();
        while($row = $result4->fetch_assoc()){
            $dados[] = (int) $row['pontos'];
        }
    
        $retorno["realizados"] = (int)$result1->fetch_assoc()["realizados"];
        $retorno["n_questoes_respondidas"] = (int)$result2->num_rows;
        $retorno["total_questoes_simulado"] = (int) $result6->fetch_assoc()["nq"];
        $retorno["questoes_total_corretas"] = $result2->fetch_all(MYSQLI_ASSOC);
        $retorno["corretas"] = (int)$result3->fetch_assoc()["corretas"];
        $retorno["media"] = $retorno['realizados'] == 0 ? 0 : (int) ($retorno["corretas"]/$retorno["realizados"]);
        $retorno["dados"] = $dados;
        $retorno["atividades_recentes"] = $result5->fetch_all(MYSQLI_ASSOC);
        $retorno["success"] = 1;
    
        echo json_encode($retorno);

    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

//-----------FUNCOES AUXILIARES --------------//

function getSimulado(){
    $simulado['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
    $simulado['data_liberacao'] = filter_input(INPUT_POST, 'data_liberacao', FILTER_SANITIZE_STRING);   
    $simulado['data_final'] = filter_input(INPUT_POST, 'data_final', FILTER_SANITIZE_STRING);
    $simulado['data_cadastro'] = date('Y-m-d');


    if($simulado['nome'] == false || $simulado['nome'] == ""){
        echo json_encode(array("success" => 0, "msg" => "Informe um nome válido!"));
        die();
    }

    $simulado['id_categoria'] = $_POST['id_categoria'];
    if($simulado['id_categoria'] == false || $simulado['id_categoria'] == '0'){
        echo json_encode(array("success" => 0, "msg" => "Selecione uma categoria!"));
        die();
    }

    if(!preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $simulado['data_liberacao'])){ //VER POSSIBILIDADES DE TRATAMENTO DE DATAS
        echo json_encode(array("success" => 0, "msg" => "Informe uma data de liberação válida!"));
        die();
    }
    if($simulado['data_final'] && !preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $simulado['data_final'])){ //VER POSSIBILIDADES DE TRATAMENTO DE DATAS
        echo json_encode(array("success" => 0, "msg" => "Informe uma data final válida!"));
        die();
    }
    $data_ajustado = str_replace('/', '-', $simulado['data_liberacao']);  
    $simulado['data_liberacao'] = date('Y-m-d', strtotime($data_ajustado));
    
    if ($simulado['data_final']) {
        $data_ajustado = str_replace('/', '-', $simulado['data_final']);   
        $simulado['data_final'] = date('Y-m-d', strtotime($data_ajustado));

        if (!isset($_POST['id']) || empty($_POST['id']))
            validaData($simulado['data_final'], $simulado['data_liberacao']);
    }
    else {
        $simulado['data_final'] = null;
    }

    return $simulado;
}

function validaData($dataF,$dataL){

    if($dataF <= date('Y-m-d') || $dataL < date('Y-m-d'))
        die(json_encode(array("success" => 0, "msg" => "Data final e/ou Data de Liberação deve ser maior que a data de hoje!")));

    if($dataF <= $dataL)
        die(json_encode(array("success" => 0, "msg" => "Data final deve ser maior que a data de Liberação!")));
    
    return true;
}
?>