/**
 * JAVASCRIPT FOR ALUNO PAGE
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/

$(document).ready(function(){

    loadTable()

    $(".fm-nome").keyup(function(){
        const regex = /^[a-zA-Z0-9 ãõéáíç\^]+$/
        validaForm(regex , $(this))
    })

    $(".cep-fm").keyup(function(){
        const regex = /^[0-9-]+$/;
        validaForm(regex, $(this))
    })

    $(".fm-email").keyup(function(){
        const regex = /^[a-zA-Z0-9._]+@[a-zA-Z0-9._]+\.[a-zA-Z]{2,4}$/
        validaForm(regex, $(this))
    })

    $(".cpf-fm").keyup(function(){
        const regex = /^[0-9-_\.]+$/
        validaForm(regex, $(this))
        is_cpf($(this).val(), $(this))
    })

    $(".cpf-fm").change(function(){
        insertPassword($(this).val())
        validaForm(/^[a-zA-Z0-9]{6,20}$/, $("#senha-fm"))
    })


    $(".fm-data").keyup(function(){
        const regex = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/
        validaForm(regex, $(this))
    })

    $('.telefone-fm').inputmask({ mask: ["(99) 9999-9999", "(99) 99999-9999"] })

    $(".telefone-fm").keyup(function(){
        const regex = /^\([0-9]{2}\) (([0-9]{5}-[0-9]{4})|([0-9]{4}-[0-9]{4}))$/
        validaForm(regex, $(this))
    })

    $(".senha-fm").keyup(function(){
        const regex = /^[a-zA-Z0-9]{6,20}$/;
        validaForm(regex, $(this))
    })

    $('.reset-form').click(function(){
        $('.has-error').removeClass('has-error')
        $('.has-success').removeClass('has-success')
    })
    
    $('#btn-novo-aluno').click(function(){
        $('#form-cadastro')[0].reset()
        $('#form-cadastro').find('.has-success').removeClass('has-success')
        $('#form-cadastro').find('.has-error').removeClass('has-error')
        loadEstadoCidade('#select_estado', '#select_cidade', null)
    })

    $('#form-cadastro').submit(function(event){
        event.preventDefault()
        let dados_formulario = $('#form-cadastro').serialize()

        $.ajax({
            method: 'post',
            url: `./ajax/ajax_usuario.php`,
            data: `${dados_formulario}&action=novo_aluno`,
            dataType: 'json'
        }).done(res=>{
            success = responseTreatment(res, $('#error_cadastro'), [carregarAlunos], false, $('#modal_cadastro'))
        })
    })
})

function carregarAlunos(){
    $('#alunos').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}


function deleteAluno(id){
    let confirmed = confirm("Deseja remover este registro?")

    if(confirmed == true){
        $.ajax({
            method: 'post',
            url: './ajax/ajax_usuario.php',
            data: {id_usuario: id, action: 'deletar'},
            dataType: 'json'
        }).done(function(result){
            //console.log(result); return
            if(result.success == 1){
                new PNotify({
                    title: 'Aluno Removido!',
                    text: result.msg,
                    type: 'success',
                    styling: 'bootstrap3'
                });
                carregarAlunos()
            }else{
                new PNotify({
                    title: 'Falha ao deletar!',
                    text: result.msg,
                    type: 'danger',
                    styling: 'bootstrap3'
                });
            }
        })
    }

}

function loadEstadoCidade(idEstado, idCidade, estadocidade){

    const selectEstado = $(idEstado)
    const selectCidade = $(idCidade)
    $.ajax({
        method: 'get',
        url: './js/json/estados-cidades.json',
        dataType: 'json'
    }).done(function(result){
        let options = ""
        options = "<option value='0'>Selecione um estado</option>";

        result.estados.forEach(estado => {
            options += '<option value = "'+estado.sigla+'">'+estado.nome+'</option>'
        })
        selectEstado.html(options)

        selectEstado.change(function(){
            if(selectEstado.val() == 0){
                selectEstado.closest('.form-group').addClass('has-error');
                selectEstado.closest('.form-group').removeClass('has-success');
                selectCidade.closest('.form-group').addClass('has-error');
                selectCidade.closest('.form-group').removeClass('has-success');
                return
            }else{
                selectEstado.closest('.form-group').removeClass('has-error');
                selectEstado.closest('.form-group').addClass('has-success');
            }
            var estado = result.estados.find(function(estado){
                return selectEstado.val() === estado.sigla;
            })
            
            let options = ""
            options = "<option value='0'>Selecione uma cidade</option>";

            $.each(estado.cidades, function(key, val){
                    options += "<option value='" + val + "'> " + val + "</option>";
            });

            selectCidade.html(options)
            selectCidade.show()
        })
        if(estadocidade){
            selectEstado.val(estadocidade.estado)
            let options = `<option value='${estadocidade.cidade}'>${estadocidade.cidade}</option>`;
            selectCidade.html(options)
        }
        
        selectCidade.change(function(){
            if(selectCidade.val() == 0){
                selectCidade.closest('.form-group').addClass('has-error');
                selectCidade.closest('.form-group').removeClass('has-success');
                return
            }else{
                selectCidade.closest('.form-group').removeClass('has-error');
                selectCidade.closest('.form-group').addClass('has-success');
            }
        })
        
    })

}

function insertPassword(cpf){
    senha = cpf.match(/\d/g);
    if(senha && senha.length >= 6){
        $('#senha-fm').val("")
        for(i=0;i<6;i++){
            $('#senha-fm').val( $('#senha-fm').val() + senha[i])
            //console.log(senha[i])
        }
    }
}


function loadTable(){
    const table = $('#alunos').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[ 3, "desc" ]],
        
        "ajax": {
            "url":"./ajax/ajax_usuario.php",
            "data": {action: 'ler_alunos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "nome" },
            { "data": "cpf" },
            { "data": "telefone" },
            { "data": "data_cadastro" },
            { "data": "id",
                "render": function(id){
                    let tags = `<a class="btn btn-primary btn-xs" href="./usuario.php?id=${id}" title="Detalhes do Aluno" data-tooltip="tooltip" data-placement="top"><i class="fas fa-eye"></i></a>
                    <button class="btn btn-primary btn-xs" onClick="deleteAluno(${id})" title="Apagar" data-tooltip="tooltip" data-placement="top"><i class="fas fa-trash-alt"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
        ]
    })

}

function is_cpf (c, element) {

    if((c = c.replace(/[^\d]/g,"")).length != 11){
        element.closest('.form-group').addClass('has-error');
        element.closest('.form-group').removeClass('has-success');
        return false;
    }
      
  
    if (c == "00000000000" || c == "11111111111" || c == "22222222222" || c == "33333333333" 
    || c == "44444444444" || c == "55555555555" || c == "66666666666" || c == "77777777777"
    || c == "88888888888" || c == "99999999999"){
        element.closest('.form-group').addClass('has-error');
        element.closest('.form-group').removeClass('has-success');
        return false;
    }
  
    var r;
    var s = 0;
  
    for (i=1; i<=9; i++)
      s = s + parseInt(c[i-1]) * (11 - i);
  
    r = (s * 10) % 11;
  
    if ((r == 10) || (r == 11))
      r = 0;
  
    if (r != parseInt(c[9])){
        element.closest('.form-group').addClass('has-error');
        element.closest('.form-group').removeClass('has-success');
        return false;
    }
  
    s = 0;
  
    for (i = 1; i <= 10; i++)
      s = s + parseInt(c[i-1]) * (12 - i);
  
    r = (s * 10) % 11;
  
    if ((r == 10) || (r == 11))
      r = 0;
  
    if (r != parseInt(c[10])){
        element.closest('.form-group').addClass('has-error');
        element.closest('.form-group').removeClass('has-success');
        return false;
    }
      
    
    element.closest('.form-group').removeClass('has-error');
    element.closest('.form-group').addClass('has-success');
      return true
  }

  //Funções de validação
  function validaForm(regex, element){
    //const regex = /^[a-zA-Z ãõéáíç\^]+$/;
    if(regex.test($(element).val())){
        element.closest('.form-group').removeClass('has-error')
        element.closest('.form-group').addClass('has-success')
        return true
    }else{
        element.closest('.form-group').addClass('has-error')
        element.closest('.form-group').removeClass('has-success')
        return false
    }
  }

