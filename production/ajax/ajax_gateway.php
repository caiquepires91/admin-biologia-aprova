<?php 
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

//----------------METODOS POST ---------------///
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo':
            novo();
        break;

        case 'atualizar':
            atualizar();
        break;

        case 'deletar':
            deletar();
        break;

        case 'alterar_gateway':
            alterar_gateway();
        break;
    }
}

//----------------METODOS GET ---------------///
if(isset($_GET['action'])){
    switch($_GET['action']){
        case 'ler':
            ler();
        break;

        case 'ler_todos':
            ler_todos();
        break;

        case 'ler_objeto':
            ler_objeto();
        break;
    }
}

//-----------FUNCOES AJAX-------------------//

function novo(){
    global $mysqli;
    $dados = validarDados();
    try{
        extract($dados);
        $stmt = $mysqli->prepare("INSERT INTO gateway (id ,nome, liberacao_boleto, liberacao_cartao, taxa_boleto, tarifa_boleto, taxa_cartao, tarifa_cartao, isTaxaFixa, data_cadastro, data_atualizacao, 
        txp1, txp2, txp3, txp4, txp5, txp6, txp7, txp8, txp9, txp10, txp11, txp12) 
        VALUES (default,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("siiddddissdddddddddddd", 
        $nome, $lBoleto, $lCartao, $txBoleto, $trBoleto, $txCartao, $trCartao, $flagFixa, $data_cadastro, $data_atualizacao,
        $txMensal[0],$txMensal[1],$txMensal[2],$txMensal[3],$txMensal[4],$txMensal[5],$txMensal[6],$txMensal[7],$txMensal[8],$txMensal[9],
        $txMensal[10],$txMensal[11]);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array("success" => 1, "msg" => "Gateway registrado com sucesso!"));
    }catch(Exception $e) {
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function atualizar(){
    global $mysqli;
    $dados = validarDados();

    try{
        extract($dados);
        $stmt = $mysqli->prepare("UPDATE gateway SET nome = ?, liberacao_boleto = ?, liberacao_cartao = ?, taxa_boleto = ?,
        tarifa_boleto = ?, taxa_cartao = ?, tarifa_cartao = ?, isTaxaFixa = ?, data_atualizacao = ?, 
        txp1 = ?, txp2 = ?, txp3 = ?, txp4 = ?, txp5 = ?, txp6 = ?, txp7 = ?, txp8 = ?, txp9 = ?, txp10 = ?, txp11 = ?, txp12 = ? WHERE id = ?");
        $stmt->bind_param("siiddddisddddddddddddi", 
        $nome, $lBoleto, $lCartao, $txBoleto, $trBoleto, $txCartao, $trCartao, $flagFixa, $data_atualizacao,
        $txMensal[0],$txMensal[1],$txMensal[2],$txMensal[3],$txMensal[4],$txMensal[5],$txMensal[6],$txMensal[7],$txMensal[8],$txMensal[9],
        $txMensal[10],$txMensal[11], $id);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array("success" => 1, "msg" => "Gateway atualizado com sucesso!"));
    }catch(Exception $e) {
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function deletar(){
    global $mysqli;
    $id = isset($_POST['id'])? filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'GATEWAY inexistente')));
    try{
        $stmt = $mysqli->prepare("UPDATE gateway SET deletado = 1 WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();

        echo json_encode(array('success'=>1, 'msg'=> 'Gateway removido!'));
    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function ler(){
    global $mysqli;
    $id = isset($_GET['id'])? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'GATEWAY inexistente')));
    try{
        $stmt = $mysqli->prepare("SELECT * FROM gateway WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();

        $gateway = $result->fetch_assoc();
        extract($gateway);

            $html = "<div class='gateway-name'><h3>$nome</h3></div>
            <table class='table table-bordered jambo_table' style='width: 100%;'>
            <caption>Boleto</caption>
            <thead>
                <tr>
                    <th>Taxa</th>
                    <th>Tarifa</th>
                    <th>Tempo de liberação</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td>$taxa_boleto %</td>
                   <td>R$ $tarifa_boleto</td>
                   <td>$liberacao_boleto dias</td> 
                </tr>
            </tbody>
            </table>
            <table class='table table-bordered jambo_table' style='width: 100%;'>
            <caption>Cartão</caption>
            <thead>
                <tr>
                    <th>Taxa ";
            $html .= $isTaxaFixa == 1? "Fixa" : "";
            $html .= "</th>
                    <th>Tarifa</th>
                    <th>Tempo de liberação</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                   <td>";
            $html .= $isTaxaFixa == 1? $taxa_cartao : "0.00";
            $html .= "%</td>
                   <td>R$ $tarifa_cartao</td>
                   <td>$liberacao_cartao dias</td> 
                </tr>
            </tbody>
            </table>";
            

            $html .=
            "<table class='table table-striped table-bordered jambo_table' style='width: 100%;'>
                <thead>
                <th>Parcelas</th>
                    <th>Taxas";
                    $html .= $isTaxaFixa == 1? " / mês" : " / transação";
                    $html .="</th>
                    </thead>
                <tbody>
                <tr><td>1x</td><td>";
            $html .= $isTaxaFixa == 0? $taxa_cartao : $txp1;
            $html .= " %</td></tr>
                <tr><td>2x</td><td>$txp2 %</td></tr>
                <tr><td>3x</td><td>$txp3 %</td></tr>
                <tr><td>4x</td><td>$txp4 %</td></tr>
                <tr><td>5x</td><td>$txp5 %</td></tr>
                <tr><td>6x</td><td>$txp6 %</td></tr>
                <tr><td>7x</td><td>$txp7 %</td></tr>
                <tr><td>8x</td><td>$txp8 %</td></tr>
                <tr><td>9x</td><td>$txp9 %</td></tr>
                <tr><td>10x</td><td>$txp10 %</td></tr>
                <tr><td>11x</td><td>$txp11 %</td></tr>
                <tr><td>12x</td><td>$txp12 %</td></tr>
                </tbody>
            </table>";

            echo json_encode(array("success" => 1, "pagina" => $html));

    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=> $e->getMessage(), "cod" => $e->getCode() )));
    }
}

function ler_todos(){
    global $mysqli;
    try{
        $gateways = array();
        $stmt = $mysqli->prepare("SELECT * FROM gateway WHERE deletado = 0");
        $stmt->execute();
        $res = $stmt->get_result();

        while($row = $res->fetch_assoc()){
            $gateways[] = $row;
        }
        echo json_encode($gateways);
        
    }catch(Exception $e){
        die(json_encode(array('success'=>99, 'msg'=>$e->getMessage(), 'cod'=>$e->getCode())));
    }
}

function ler_objeto(){
    global $mysqli;
    $id = isset($_GET['id'])? filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'GATEWAY inexistente')));
    try{
        $stmt = $mysqli->prepare("SELECT * FROM gateway WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $result = $stmt->get_result();
        $taxas = array();
        $gateway = $result->fetch_assoc();
        for($i = 1; $i <=12; $i++){
            $taxas[] = $gateway["txp$i"];
            unset($gateway["txp$i"]);
        }
        $gateway['taxas'] = $taxas;
        
        echo json_encode(array("success" => 1, "gateway" => $gateway));
    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=> $e->getMessage(), "cod" => $e->getCode() )));
    }
}

function alterar_gateway(){
    global $mysqli;
    $id_gateway = isset($_POST['id_gateway'])? filter_input(INPUT_POST, 'id_gateway', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'GATEWAY inexistente')));
    $mysqli->query("UPDATE gateway SET current = '0'"); //DESATIVAR GATEWAY ATUAL
    $mysqli->query("UPDATE gateway SET current = '1' WHERE id = $id_gateway"); //ativando gateway
    $nome = $mysqli->query("SELECT nome FROM gateway WHERE id = $id_gateway");
    echo json_encode(array('success'=>1, 'msg'=>'Gateway alterado com sucesso', 'nome'=>$nome->fetch_assoc()['nome']));
}

//-----------FUNCOES AUXILIARES --------------//
function validarDados(){
    if(!$dados['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)){
        die(json_encode(array('success'=>0, 'msg'=>'Informe um Nome')));
    }

    $dados['lBoleto'] = $_POST['lBoleto'] ? filter_input(INPUT_POST, 'lBoleto', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'Informe o tempo de liberação do boleto')));
    $dados['lCartao'] = $_POST['lCartao'] ? filter_input(INPUT_POST, 'lCartao', FILTER_SANITIZE_NUMBER_INT) : die(json_encode(array('success'=>0, 'msg'=>'Informe o tempo de liberação do cartão')));

    $dados['txBoleto'] = filter_input(INPUT_POST, 'txBoleto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $dados['trBoleto'] = filter_input(INPUT_POST, 'trBoleto', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $dados['txCartao'] = filter_input(INPUT_POST, 'txCartao', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
    $dados['trCartao'] = filter_input(INPUT_POST, 'trCartao', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

    $dados['flagUnico'] = $_POST['isTxUnico'] === "true" ? true : false;
    $dados['flagFixa'] = $_POST['isTxFixa'] === "true" ? true : false;
    $dados['txMensal'] = filter_input(INPUT_POST, 'txMensal', FILTER_DEFAULT, FILTER_FORCE_ARRAY);

    //validando array
    count($dados['txMensal']) == 12 ? true :  die(json_encode(array('success'=>0, 'msg'=>'Preencha todas as taxas de parcelamento')));

    $dados['data_cadastro'] = date('Y-m-d H:i:s');

    $dados['data_atualizacao'] = $dados['data_cadastro'];
    $dados['id'] = $_POST['action'] == 'atualizar' ? filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT) : null;
    if($_POST['action'] == 'atualizar') 
        unset($dados["data_cadastro"]);

    return $dados;
}

?>