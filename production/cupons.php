<?php require __DIR__.'/php/autentica.php'; ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Cupons</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">

        <!-- Datatables -->
        <?php include './componentes/DataTableCSS.php' ?>


    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">


            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-donate"></i> Cupons <small></small></h2>
                    <div class="panel_toolbox">
                    <button id="btn-novo-cupom" class="btn btn-primary" data-toggle="modal" data-target="#modal_cadastro">Cadastrar novo cupom <i class="fas fa-plus-square"></i></button>
                  </div>
                    <div class="clearfix"></div>
                    
                  </div>
                  <div class="x_content">

                    <table id="cupons" class="display table table-hover" style="width:100%">
                      <thead>
                          <tr>
                              <th><i class="fas fa-file-alt"></i> Código</th>
                              <th><i class="fas fa-money-bill-wave"></i> Desconto</th>
                              <th><i class="fas fa-money-bill-wave"></i> Tipo de Desconto</th>
                              <th><i class="fas fa-calendar"></i> Data de Início</th>
                              <th><i class="fas fa-calendar"></i> Data do Fim</th>
                              <th><i class="fas fa-calendar"></i> Data de Cadastro</th>
                              <th><i class="fas fa-file-powerpoint"></i> Plano</th>
                              <th style="width: 65px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                          </tr>
                      </thead>
                      
                  </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->

                  <!-- Small modal Cadastro-->
                  <div id="modal_cadastro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus-square"></i> Cadastrar Cupom</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>
                          <!--Inserir formulario aqui-->
                          <form id="form-cupom" class="form-horizontal form-label-left">
                          <div id="error_cupom">
                          </div>
                          <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                            <label>Cógido do Cupom: </label>
                              <input type="text" class="form-control codigo" id="codigo_cupom" name="cupom-codigo" placeholder="CÓDIGO">
                            <div></div>
                          </div>

                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Valor de Desconto: </label>
                              <input type="text" class="form-control valor" id="valor" name="cupom-valor">
                            <div></div>
                          </div>
                          <div class="clearfix"></div>

                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                            <label>Duração: </label>
                              <input type="text" class="form-control duracao" id="duracao" name="cupom-duracao">
                            <div></div>
                          </div>
                          <div class="form-group">
                          <label> </label>
                          <p>
                              Horas: <input type="radio" class="" name="cupom-duracao-dim" id="dimH" value="0" checked="" required /> 
                              Dias: <input type="radio" class="" name="cupom-duracao-dim" id="dimD" value="1" />
                              Meses: <input type="radio" class="" name="cupom-duracao-dim" id="dimM" value="2" />
                            </p>
                          </div>
                          <div class="clearfix"></div>
                          
                            <div class="modal-footer">
                                <input type="hidden" name="action" value="create">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" id="cadastrar-cupom" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
                  <!-- /modal de leitura -->
                  
            <!-- /modals leitura -->
            <!-- modals update -->
            <div id="modal_update" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-edit"></i> Editar Cupom</h4>
                  </div>
                  <div class="modal-body">
                    <h4>Preencha os campos abaixo</h4>
                    <!--Inserir formulario aqui-->
                    <form id="form-atualizar-cupom" class="form-horizontal form-label-left">
                    <div id="uerror_cupom"></div>
                    
                    <div class="col-md-8 col-sm-8 col-xs-12 form-group">
                            <label>Cógido do Cupom: </label>
                              <input type="text" class="form-control codigo" id="ucodigo_cupom" name="cupom-codigo" placeholder="CÓDIGO">
                            <div></div>
                          </div>

                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Valor de Desconto: </label>
                              <input type="text" class="form-control" id="uvalor" name="cupom-valor">
                            <div></div>
                          </div>

                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Tipo de Desconto: </label>
                              <input type="text" class="form-control" id="utipo_desconto" name="cupom-tipo-desconto">
                            <div></div>
                          </div>

                          
                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Data de Inicio: </label>
                              <input type="text" class="form-control date" id="udata_inicio" name="cupom-data-inicio">
                            <div></div>
                          </div>

                          
                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Data do Fim: </label>
                              <input type="text" class="form-control date" id="udata_fim" name="cupom-data-fim">
                            <div></div>
                          </div>

                          <div class="col-md-4 col-sm-4 col-xs-12 form-group">
                          <label>Plano (id): </label>
                              <input type="text" class="form-control" id="uplano" name="cupom-plano">
                            <div></div>
                          </div>

                          <div class="clearfix"></div>
                      <div class="modal-footer">
                      <input type="hidden" id="id_cupom_update" name="id_cupom">
                      <input type="hidden" name="action" value="update">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                          <button type="submit" id="atualizar-cupom" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/functions.js?v=<?php echo ASSETS_VERSION; ?>"></script>
    <script src="./js/crud_cupons.js?v=<?php echo ASSETS_VERSION; ?>"></script>


  </body>
</html>