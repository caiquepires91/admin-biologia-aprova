/**
 * JAVASCRIPT FOR CATEGORY PAGE
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/

$(document).ready(function(){
    loadTable()
    $('.data.hora').inputmask({ mask: ['99/99/9999 99:99', '99/99/9999 99:99:99'] , placeholder: "__/__/____ 00:00:00"})

    $('#update_modal').on('hide.bs.modal', function (e) {
        $('#form-atualizar-categoria')[0].reset()
        $('#uerror_cadastro').html("");
        $('.has-error').removeClass('has-error')
        $('.has-success').removeClass('has-success')
      })
    
    $('#modal_cadastro').on('hide.bs.modal', function (e) {
        $('#form-categoria')[0].reset()
        $('#uerror_cadastro').html("");
        $('.has-error').removeClass('has-error')
        $('.has-success').removeClass('has-success')
      })

      $('#form-categoria').submit(function(event){
        event.preventDefault()
            let dados = {
                nome_cat: $('#nome-cat').val(),
                data_liberacao: $('#data-liberacao').val(),
                action: 'novo'
            }

            $.post('./ajax/ajax_categoria.php', dados, function(res){
                success = responseTreatment(res, $('#error_cadastro'), [reloadCategorias], false, $('#modal_cadastro'))
            }, 'json');
        return false;
    })
    
    $('#form-atualizar-categoria').submit(function(event){
        event.preventDefault()
            let dados = {
                id_cat: $("#hidden_cat_id").val(),
                nome_cat: $("#unome-cat").val(),
                data_liberacao: $('#udata-liberacao').val(),
                action: 'atualizar'
            }
            $.post('./ajax/ajax_categoria.php', dados, function(res){
                success = responseTreatment(res, $('#uerror_cadastro'), [reloadCategorias], false, $('#update_modal'))
            }, 'json');
        return false;
        })

})

//FUNÇÕES DE CATEGORIAS

function validaNome(){
    var regex = /^[a-zA-Z -çãõêôéáíÂÊÔÃÕ]+$/;
    var nome = $(".nome-form")
    if(regex.test(nome.val())){
        nome.closest('.form-group').removeClass('has-error')
        nome.closest('.form-group').addClass('has-success')
        return true
    }else{
        nome.closest('.form-group').addClass('has-error')
        nome.closest('.form-group').removeClass('has-success')
        return false
    }
}



function validaDesc(){
    var regex2 = /^[a-zA-Z0-9 \.\,\:-çãõêôéáíÂÊÔÃÕ]*$/;
    var desc = $(".desc-form")

    if(regex2.test(desc.val())){
        desc.closest('.form-group').removeClass('has-error')
        desc.closest('.form-group').addClass('has-success')
        return true
    }else{
        desc.closest('.form-group').addClass('has-error')
        desc.closest('.form-group').removeClass('has-success')
        return false
    }
}

function loadTable(){
    const table = $('#categorias').DataTable({
        "processing": true,
        "responsive": true,
        "order": [2, 'asc'],
        "ajax": {
            "method": 'get',
            "url": "./ajax/ajax_categoria.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "nome",
        "render": function(nome){
            let tagName = `<div class="flex-center"><strong>${nome}</strong></div>`
            return tagName
        } },
        {"data": "data_cadastro"},
        {"data": "data_liberacao"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <button class="btn btn-primary btn-xs" onClick="updateCategoria(${id})" data-toggle="modal" data-target="#update_modal" title="Editar Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteCategoria(${id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function reloadCategorias(){
    $('#categorias').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updateCategoria(id){

    const hiddenid = $("#hidden_cat_id");
    hiddenid.val(id);
    $.ajax({
        method: 'get',
        url: './ajax/ajax_categoria.php',
        data: {id_cat: id, action: "ler"},
        dataType: 'json'
    }).done(function(response){
        if(response.success == 1){
            $("#unome-cat").val(response.categoria.nome);
            $("#udata-liberacao").val(response.categoria.data_liberacao);
        }else{
            new PNotify({
                title: 'Falha ao atualizar!',
                text: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
            //reloadCategorias()
        }
    })
}

function deleteCategoria(id){
    let confirmed = confirm("Deseja remover esta Categoria?")
    if(confirmed == true){
        $.post('./ajax/ajax_categoria.php', {id_cat: id, action: "deletar"}, function(res){
            success = responseTreatment(res, false, [reloadCategorias], false, false)
        }, 'json');
    }

}