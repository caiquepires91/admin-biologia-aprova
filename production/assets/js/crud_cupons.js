/**
 * JAVASCRIPT FOR CUPOM PAGE
 * 
*
* Success codes for create and update calls
* 0 = form error
* 1 = success
* 2 = info
* 99 = mysql error
*/

$(document).ready(function(){
    loadTable()

    $(".valor").inputmask( 'currency',{"autoUnmask": true,
    radixPoint:",",
    groupSeparator: ".",
    allowMinus: false,
    prefix: 'R$ ',            
    digits: 2,
    digitsOptional: false,
    rightAlign: true,
    unmaskAsNumber: true,
    });

    $('.codigo').keyup(function(){
        $(this).val($.trim($(this).val().toUpperCase()))
        //$(this).val($(this).val().toUpperCase())
    })

    $('#modal_update').on('hide.bs.modal', function (e) {
        $('#form-atualizar-cupom')[0].reset()
        $('#uerror_cupom').html("");
      })
    
    $('#modal_cadastro').on('hide.bs.modal', function (e) {
        $('#form-cupom')[0].reset()
        $('#uerror_cupom').html("");
      })

      $('#form-cupom').submit(function(event){
        event.preventDefault()
        let dados = {
            "cupom-codigo": $("#form-cupom input[name='cupom-codigo']").val(),
            "cupom-valor": $("#form-cupom input[name='cupom-valor']").val(),
            "cupom-duracao": $("#form-cupom input[name='cupom-duracao']").val(),
            "cupom-duracao-dim": $("#form-cupom input[name='cupom-duracao-dim']:checked").val(),
            "action": "novo"
        };

        $.post('./ajax/ajax_cupom.php', dados, function(res){
            success = responseTreatment(res, $('#error_cupom'), [reloadCupons], false, $('#modal_cadastro'))
        }, 'json');
    })
    
    $('#form-atualizar-cupom').submit(function(event){
        event.preventDefault()
        //let id_cat = $("#hidden_cupom_id").val()]
        let dados = {
            "cupom-codigo": $("#form-atualizar-cupom input[name='cupom-codigo']").val(),
            "cupom-valor": $("#form-atualizar-cupom input[name='cupom-valor']").val(),
            "cupom-duracao": $("#form-atualizar-cupom input[name='cupom-duracao']").val(),
            "cupom-duracao-dim": $("#form-atualizar-cupom input[name='cupom-duracao-dim']:checked").val(),
            "id": $("#form-atualizar-cupom input[name='id_cupom']").val(),
            "action": "atualizar"
        };

        $.post('./ajax/ajax_cupom.php', dados, function(res){
            if(responseTreatment(res, $('#uerror_cupom'), [reloadCupons], false, $('#modal_update')))
                $('#form-atualizar-cupom #dimH').prop("checked", true);
        }, 'json');

    })

})

function loadTable(){
    const table = $('#cupons').DataTable({
        "processing": true,
        "responsive": true,
        "order": [[2, "desc"]],
        "ajax": {
            "method": 'get',
            "url": "./ajax/ajax_cupom.php",
            "data": {action: 'ler_todos'},
            "dataSrc": ""
        },
        "columns": [
            { "data": "codigo"},
            { "data": "desconto",
            "render": function(desconto){
                return desconto ? formata_dinheiro(desconto) : 'Não especificado';
            }},
            { "data": "data_cadastro"},
            { "data": "data_validade"},
            { "data": "id",
                "render": function(id){
                    let tags = `
                    <button class="btn btn-primary btn-xs" onClick="updateCupom(${id})" data-toggle="modal" data-target="#modal_update" title="Editar Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-edit"></i></button>
                    <button class="btn btn-primary btn-xs" onClick="deleteCupom(${id})" title="Excluir Registro" data-tooltip="tooltip" data-placement="top"><i class="fa fa-trash-o"></i></button>`
                    $('[data-tooltip="tooltip"]').tooltip()
                    return tags
                } },
            ]
    })
}

function reloadCupons(){
    $('#cupons').DataTable().ajax.reload()
    $('[data-tooltip="tooltip"]').tooltip()
}

function updateCupom(id){
    const hiddenid = $("#id_cupom_update")
    hiddenid.val(id);
    $.ajax({
        method: 'get',
        url: './ajax/ajax_cupom.php',
        data: {id: id, action: "ler"}
    }).done(function(result){
        let response = JSON.parse(result)
        if(response.success == 1){
            $('#ucodigo_cupom').val(response.cupom.codigo)
            $('#uvalor').val(response.cupom.desconto)
            $('#uduracao').val(response.cupom.duracao)
            if(response.cupom.duracao_unidade == 'hora')
                $('#form-atualizar-cupom #dimH').prop('checked', true)
            else if(response.cupom.duracao_unidade == 'dia')
                $('#form-atualizar-cupom #dimD').prop('checked', true)
            else if(response.cupom.duracao_unidade == 'mes')
                $('#form-atualizar-cupom #dimM').prop('checked', true)

        }else{
            new PNotify({
                title: 'Falha ao atualizar!',
                text: response.msg,
                type: 'danger',
                styling: 'bootstrap3'
            });
        }
    })
}

function deleteCupom(id){
    let confirmed = confirm("Deseja remover este Cupom?")
    if(confirmed == true){
        $.post('./ajax/ajax_cupom.php', {id: id, action: "deletar"}, function(res){
            success = responseTreatment(res, false, [reloadCupons], false, false)
        }, 'json');
    }
}