<?php
require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';
include_once __DIR__ . '/../lib/uploads.php';
include_once __DIR__ . '/../lib/blog.php';

$resposta = ["status" => 1];

//----------------METODOS POST ---------------///
if(isset($_POST['acao']))
    call_user_func($_POST['acao']);

//----------------METODOS GET ---------------///
if(isset($_GET['acao']))
    call_user_func($_GET['acao']);

//retorna resposta
echo json_encode($resposta);

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

function salvar(){
    if(isset($_POST["id"]) && validar_inteiro($_POST["id"]))
        atualizar();
    else
        cadastrar();
}

function cadastrar(){
    global $mysqli;
    global $resposta;

    $mensagem_erro = "";
	$entradas = [
        ['nome' => "Título", 'parametro' => "titulo", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Texto", 'parametro' => "texto", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Autor", 'parametro' => "autor", 'requerido' => FALSE, 'tipo' => "inteiro"],
        ['nome' => "Categoria", 'parametro' => "categoria", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Idioma", 'parametro' => "lingua", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Nota", 'parametro' => "nota", 'requerido' => FALSE, 'tipo' => "texto"],
    ];

	if(!valida_post($entradas, $mensagem_erro)) {
		trigger_error($mensagem_erro, E_USER_ERROR);
    }

    //upload da imagem de exibição
    if (isset($_FILES["img"]) && !empty($_FILES["img"]["name"])) {
        $img = Uploads\salvar_arquivo("" , "post_".time() , $_FILES["img"]["name"], $_FILES["img"]["tmp_name"], ["jpg", "jpeg", "png", "gif"]);
    }else{
        trigger_error("Insira uma imagem de exibição do post.", E_USER_ERROR);
    }

    if(!isset($img) || !$img)
        trigger_error("Ocorreu uma falha ao salvar a imagem de exibição. Por favor, tente novamente!", E_USER_ERROR);

    $args = [
        "titulo" => $_POST["titulo"],
        "texto" => $_POST["texto"],
        "autor" => $_POST["autor"],
        "categoria" => $_POST["categoria"],
        "img" => $img,
        "lingua" => $_POST["lingua"],
        "nota" => $_POST["nota"],
        "data_publicacao" => date('Y-m-d H:i:s'),
        "visualizacoes" => 0
    ];

    if(!$post = Blog\cadastrar($args))
        trigger_error("Não foi possível fazer a postagem. Por favor, tente novamente!", E_USER_ERROR);

    $resposta["id"] = $post["id"];
}

function atualizar(){
    global $mysqli;
    global $resposta;

    $mensagem_erro = "";
	$entradas = [
        ['nome' => "ID do post", 'parametro' => "id", 'requerido' => TRUE, 'tipo' => "inteiro"],
        ['nome' => "Título", 'parametro' => "titulo", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Texto", 'parametro' => "texto", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Autor", 'parametro' => "autor", 'requerido' => FALSE, 'tipo' => "inteiro"],
        ['nome' => "Categoria", 'parametro' => "categoria", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Idioma", 'parametro' => "lingua", 'requerido' => TRUE, 'tipo' => "texto"],
        ['nome' => "Nota", 'parametro' => "nota", 'requerido' => FALSE, 'tipo' => "texto"],
    ];

	if(!valida_post($entradas, $mensagem_erro)) {
		trigger_error($mensagem_erro, E_USER_ERROR);
    }

    if(!$post_atual = Blog\buscar($_POST["id"]))
        trigger_error("Post não encontrado nos registros. Por favor atualize página.", E_USER_ERROR);

    //upload da imagem de exibição
    if (isset($_FILES["img"]) && !empty($_FILES["img"]["name"])) {
        $img = Uploads\salvar_arquivo("assets/images/noticias/","post_".time(), $_FILES["img"]["name"], $_FILES["img"]["tmp_name"], ["jpg", "jpeg", "png", "gif"], $_FILES['img']['error']);
    }else{
        $img = $post_atual["img"];
    }

    if(!isset($img) || !$img)
        trigger_error("Ocorreu uma falha ao salvar a imagem de exibição. Por favor, tente novamente!", E_USER_ERROR);

    $args = [
        "titulo" => $_POST["titulo"],
        "texto" => $_POST["texto"],
        "autor" => $_POST["autor"],
        "categoria" => $_POST["categoria"],
        "img" => $img,
        "lingua" => $_POST["lingua"],
        "nota" => $_POST["nota"],
        "id" => $_POST["id"]
    ];

    if(!$post = Blog\atualizar($args))
        trigger_error("Não foi possível atualizar a postagem. Por favor, tente novamente!", E_USER_ERROR);

    $resposta["id"] = $post["id"];
}

function remover () {
    global $mysqli;
    global $resposta;

    if(!isset($_POST["id_post"]) || !validar_inteiro($_POST["id_post"]))
        trigger_error("O ID do post informado é invalido. Por favor, tente novamente!", E_USER_ERROR);

    $result = $mysqli->query("UPDATE noticias SET deletado = 1 WHERE id = $_POST[id_post]");
    if($mysqli->affected_rows === 0)
        trigger_error("Ocorreu um erro ao processar os dados. Por favor, tente novamente.", E_USER_ERROR);
}

function listar(){
    global $resposta;
    global $mysqli;

    $resposta["noticias"] = Blog\listar();
    
}
