<?php 
require __DIR__.'/php/autentica.php'; 
require __DIR__.'/php/functions.php';

$result = getGateways();

?>


<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  
    <title>Administração - Gateways</title>

    <!-- Bootstrap -->
    <link href="../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="../vendors/fontawesome5/css/all.css" rel="stylesheet">
    <link href="../vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- bootstrap-datepicker -->
    <link href="./js/datepicker/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="../vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="../vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Pnotify -->
    <link href="../vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="../vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>

    <!-- Datatables -->
    <?php include './componentes/DataTableCSS.php' ?>

    <!-- Custom Theme Style -->
    <link href="../build/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/modules/relatorio.css">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
            <?php include './componentes/navTitle.php'?>
            </div>

            <div class="clearfix"></div>

            <br />

            <!-- sidebar menu -->
            <?php include './componentes/SideMenu.php'?>
            <!-- /sidebar menu -->


            <!-- /menu footer buttons -->
            <?php include __DIR__.'/./componentes/menuFooter.php';?>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <?php include __DIR__.'/./componentes/topNavigation.php'?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fas fa-donate"></i> Gateways <small></small></h2>
                    <div class="panel_toolbox">
                    <button id="btn-novo-gateway" class="btn btn-primary" data-toggle="modal" data-target="#modal_cadastro">Cadastrar Gateway <i class="fas fa-plus-square"></i></button>
                  </div>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <!--SELECT ACTIVE GATEWAY -->
                  <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="profile_left">
                    <img class="gateway-img" src="images/icons/gateway_icon.png" alt="icone">
                    <span class="gateway-ativo">Gateway Ativo</span>
                     <h3 id="gateway_ativo_nome"><?php 
                     while($row = current($result))
                     {
                       if($row['current'] == '1'){
                         echo $row['nome']; break;
                         } 
                         next($result);
                         }
                         reset($result);?></h3>
                    <a class="btn btn-success btn-xs gtw-btn" data-toggle="modal" data-target=".alterar_gateway"><i class="fa fa-edit m-right-xs"></i> Alterar</a>
                    </div>
                  </div>

                  <!-- GATEWAY TABLE -->
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <table id="gateways" class="display table table-hover" style="width:100%">
                          <thead class="thead-dark">
                            <tr>
                                <th><i class="fas fa-file-alt"></i> Nome</th>
                                <th><i class="fas fa-calendar"></i> Data Cadastro</th>
                                <th style="width: 75px;"><i class="fas fa-mouse-pointer"></i> Ações</th>
                            </tr>
                          </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

                  <!-- modals -->
                  <div id="modal_cadastro" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus-square"></i> Cadastrar Gateway</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>
                          <!--Inserir formulario aqui-->
                          <form id="form-gateway" class="form-horizontal form-label-left">
                          <div id="error_gateway">
                          </div>

                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Nome: </label>
                          <div class="col-md-10 col-sm-10 col-xs-12">
                              <input type="text" class="form-control nome" name="nome" placeholder="Nome do gateway">
                          </div>


                          <div class="" style="margin-bottom: 7px; margin-top: 7px;">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Boleto: </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <label for="">Taxa: </label>
                              <input type="text" class="form-control liberacao percentage" name="txBoleto">
                              <label for="" class="col-md-12"> </label>
                              <label for="">Tarifa: </label>
                              <input type="text" class="form-control liberacao valor" name="trBoleto">
                              <label for="">Liberação: </label>
                              <input type="text" class="form-control liberacao" name="lBoleto">
                            </div>

                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Cartão: </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <label for="">Taxa: </label>
                                <input type="text" class="form-control liberacao percentage" name="txCartao">
                                <label for="" class="col-md-12">taxa fixa? <input type="checkbox" name="fixa" class="txFixaCk" checked></label>

                              <label for="">Tarifa: </label>
                                <input type="text" class="form-control liberacao valor" name="trCartao">
                              <label for="">Liberação: </label>
                                <input type="text" class="form-control liberacao" name="lCartao">
                            </div>
                          </div>

                          <div class="form-group">
                            <label title="Se 'única' estiver marcado, preencha apenas o primeiro campo" class="control-label col-md-3 col-sm-3 col-xs-12">Taxa de parcelamento <br />única? <input class="unico" type="checkbox" name="unico"></label>

                          <div class="col-md-9 col-sm-9 col-xs-12 mt-7">
                          <div class="clearfix"></div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">1x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p1">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage first">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">2x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">3x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">4x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">5x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">6x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">7x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">8x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">9x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">10x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">11x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">12x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                          </div>
                          </div>
                          <div class="clearfix"></div>
                          
                            <div class="modal-footer">
                                <input type="hidden" name="action" value="create">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
<!-- MODAL LEITURA -->
                  <div id="modal_read" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-eye"></i> Detalhes</h4>
                        </div>
                        <div id="read_modal_body" class="modal-body">

                        <div class="clearfix"></div>
                        </div>
                        <div class="modal-footer">
                          <input type="hidden" name="id_plano" id="id_plano">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>

                      </div>
                    </div>
                  </div>
                  <!-- /modals -->

                  <!-- modals -->
                  <div id="modal_update" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2"><i class="fas fa-plus-square"></i> Cadastrar Gateway</h4>
                        </div>
                        <div class="modal-body">
                          <h4>Preencha os campos abaixo</h4>
                          <!--Inserir formulario aqui-->
                          <form id="form-update-gateway" class="form-horizontal form-label-left">
                          <div id="error_gateway">
                          </div>

                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Nome: </label>
                          <div class="col-md-10 col-sm-10 col-xs-12">
                              <input type="text" class="form-control nome" name="nome" placeholder="Nome do gateway">
                          </div>


                          <div class="" style="margin-bottom: 7px; margin-top: 7px;">
                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Boleto: </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <label for="">Taxa: </label>
                              <input type="text" class="form-control liberacao percentage" name="txBoleto">
                              <label for="" class="col-md-12"> </label>
                              <label for="">Tarifa: </label>
                              <input type="text" class="form-control liberacao valor" name="trBoleto">
                              <label for="">Liberação: </label>
                              <input type="text" class="form-control liberacao" name="lBoleto">
                            </div>

                          <label class="control-label col-md-2 col-sm-2 col-xs-12">Cartão: </label>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                              <label for="">Taxa: </label>
                                <input type="text" class="form-control liberacao percentage" name="txCartao">
                                <label for="" class="col-md-12">taxa fixa? <input type="checkbox" name="fixa" class="txFixaCk" checked></label>

                              <label for="">Tarifa: </label>
                                <input type="text" class="form-control liberacao valor" name="trCartao">
                              <label for="">Liberação: </label>
                                <input type="text" class="form-control liberacao" name="lCartao">
                            </div>
                          </div>

                          <div class="form-group">
                            <label title="Se 'única' estiver marcado, preencha apenas o primeiro campo" class="control-label col-md-3 col-sm-3 col-xs-12">Taxa de parcelamento <br />única? <input class="unico" type="checkbox" name="unico"></label>

                          <div class="col-md-9 col-sm-9 col-xs-12 mt-7">
                          <div class="clearfix"></div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">1x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p1">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage first">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">2x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">3x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">4x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">5x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">6x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">7x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">8x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">9x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">10x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">11x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                            <label class="control-label col-md-1 col-sm-1 col-xs-1 p">12x </label>
                            <div class="col-md-3 col-sm-3 col-xs-11 p">
                                <input type="text" name="taxas[]" required="required" class="form-control percentage">
                            </div>
                          </div>
                          </div>
                          <div class="clearfix"></div>
                          
                            <div class="modal-footer">
                                <input type="hidden" name="action" value="create">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Salvar</button>
                            </div>
                          </form>
                          </div>
                      </div>
                    </div>
                  </div>
                  <!--modal update -->
                  <div class="modal fade alterar_gateway" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel2">Alterar Gateway</h4>
                        </div>
                        <div class="modal-body">
                          <form id="form-alterar-gateway" class="form-horizontal form-label-left">
                          <div class="form-group">
                            <select class="select2_group form-control" id="select_gateway" name="estado">
                            <?php 
                              while($gateway = current($result)){
                                echo "<option value=".$gateway['id'].">".$gateway['nome']."</option>";
                                next($result);
                              }
                            ?>
                            </select>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Aplicar</button>
                        </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <!-- /modals -->
    
        <!-- footer content -->
        <?php include './componentes/footer.php' ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="../vendors/fastclick/lib/fastclick.js"></script>
    <!-- bootstrap-datepicker -->    
    <script src="./js/moment/moment.min.js"></script>
    <script src="./js/datepicker/bootstrap-datepicker.min.js"></script>
    <!-- iCheck -->
    <script src="../vendors/iCheck/icheck.min.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="../vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="../vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="../vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="../vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Select2 -->
    <script src="../vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- mask -->
    <script src="../vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <!-- Autosize -->
    <script src="../vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- PNotify -->
    <script src="../vendors/pnotify/dist/pnotify.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="../vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="../vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    
    <!-- Datatables -->
    <?php include './componentes/DataTableJs.php' ?>

    <!-- Custom Theme Scripts -->
    <script src="../build/js/custom.js"></script>
    <script src="./js/functions.js"></script>
    <script src="./js/crud_gateway.js"></script>


  </body>
</html>