var id_simulado = null;

$(document).ready(function(){

  //guarda o valor do id do simulado
    id_simulado = $('#id').val(); 

    //carrega as questões referentes aquele simulado
    loadQuestoes(id_simulado); 

    //inicializa os campos tinymce
    initTinyMCE(); 

    //mostra na tela de questões o numero da nova questão adicionada
    loadNumberOfQuestion(id_simulado); 

    //atribui os assuntos da tabela para o autocomplete no campo assunto
    getAssuntos(); 

    //Evento que manipula as tabs para editar questao
    $('.nav-tabs li a').click(function(e){
      e.preventDefault()
      if (!$(this).hasClass('editting')) {
          if($('.nav-tabs li.edit').hasClass('active')){
            clearInputs()
            for(var i=1; i<=5; i++) //limpa ids setados
            $(`#form-edit-questao #u_alt${i}`).removeData('id_alternativa')
            $('.nav-tabs li.edit').hide()
          }
      }
    })

    //Tratamento do formulario para criar uma questão nova
    $('#form-questao').submit(function(e){
      e.preventDefault()
      let dados = {
        enunciado: tinymce.get('enunciado').getContent(),
        assunto: $("#form-questao input[name='assunto']").val(),
        assunto_id: $("#form-questao input[name='assunto-id']").val(),
        alt1: tinymce.get('alt1').getContent(),
        alt2: tinymce.get('alt2').getContent(),
        alt3: tinymce.get('alt3').getContent(),
        alt4: tinymce.get('alt4').getContent(),
        alt5: tinymce.get('alt5').getContent(),
        correct: $("#form-questao input[name='correct']:checked").val(),
        id_simulado: id_simulado,
        action: 'novo'
      }

      $.ajax({
        method: 'post',
        url: './ajax/ajax_questao.php',
        data: dados,
        dataType: 'json'	
      }).done(res=>{
        if(responseTreatment(res, false, [reloadQuestoes, getAssuntos, clearInputs], false, false)){
          loadNumberOfQuestion(id_simulado);
        }
      })
    })

    //Tratamento do formulario para editar uma questão nova
    $('#form-edit-questao').submit(function(e){
      e.preventDefault()
      let dados = {
        enunciado: tinymce.get('u_enunciado').getContent(),
        assunto: $("#form-edit-questao input[name='assunto']").val(),
        assunto_id: $("#form-edit-questao input[name='assunto-id']").val(),
        alternativas: [
          {alt: tinymce.get('u_alt1').getContent(), id_alt: $('#u_alt1').data('id_alternativa')? $('#u_alt1').data('id_alternativa') : null},
          {alt: tinymce.get('u_alt2').getContent(), id_alt: $('#u_alt2').data('id_alternativa')? $('#u_alt2').data('id_alternativa') : null},
          {alt: tinymce.get('u_alt3').getContent(), id_alt: $('#u_alt3').data('id_alternativa')? $('#u_alt3').data('id_alternativa') : null},
          {alt: tinymce.get('u_alt4').getContent(), id_alt: $('#u_alt4').data('id_alternativa')? $('#u_alt4').data('id_alternativa') : null},
          {alt: tinymce.get('u_alt5').getContent(), id_alt: $('#u_alt5').data('id_alternativa')? $('#u_alt5').data('id_alternativa') : null},
        ],
        correct: $("#form-edit-questao input[name='correct']:checked").val(),
        id_simulado: id_simulado,
        id_questao: $(this).data('id_questao'),
        action: 'atualizar'
      }

      $.ajax({
        method: 'post',
        url: './ajax/ajax_questao.php',
        data: dados,
        dataType: 'json'	
      }).done(res=>{
        if(responseTreatment(res, false, [reloadQuestoes, getAssuntos, clearInputs], false, false)){
          hideElement($('.nav-tabs li.edit'))
          for(var i=1; i<=5; i++) //limpa ids setados
            $(`#form-edit-questao #u_alt${i}`).removeData('id_alternativa')
        }
      })
    })

  })

    function hideElement(tab){
      tab.hide()
      $('.nav-tabs a[href="#tab_question"]').click()
    }

    function reloadQuestoes(){
      $('#questions').DataTable().ajax.reload()
    }

    function loadQuestoes(id){
      const table = $('#questions').DataTable({
          "processing": true,
          "responsive": true,
          "ajax": {
              "url": "./ajax/ajax_simulado.php",
              "data": {id: id, action: 'ler_questoes_simulado'},
              "dataSrc": ""
          },
          "order": [[ 1, "asc" ]],
          "columns": [
              { "data": "enunciado", "render": function(enunciado){
                novo = enunciado.replace(/\<img.*\>/g,'')
                return `<div class="overflowData">${novo}</div>`
              }},
              { "data": "codigo", "render": function(cod){ return `Q. ${cod}`}},
              { "data": "assunto"},
              { "data": "data_criacao"},
              { "data": "id",
                  "render": function(id){
                      let tags = `
                      <button class="btn btn-primary btn-xs" onClick="readQuestao(${id}, false)" data-toggle="modal" data-target="#ver_questao"><i class="fas fa-eye"></i> Ver</button>
                      <button class="btn btn-primary btn-xs" onClick="editarQuestao(${id})" data-toggle="modal" data-target="#update_modal"><i class="fa fa-pencil"></i> Editar </button>
                      <button class="btn btn-primary btn-xs" onClick="deleteQuestao(${id})" title="Excluir Registro" data-toggle="tooltip" data-placement="top"><i class="fa fa-trash-o"></i> Apagar </button>`
                      return tags
                  }},
              ],
      })
  }

  function getAssuntos(){
    $.getJSON('./ajax/getAssuntos.php', data => {
      $('.autocomplete.assunto')
      .autocomplete({
        source: data,
        change: function(event, ui){
          if(ui.item == null){
            $('.autocompleteId').val(null)
          } else{
            $('.autocompleteId').val(ui.item.id);
          }
        }
    });
    })
  }


function clearInputs(){
  $("form input[type='text']").val("")
  for (var i = 0; i < tinymce.editors.length; i++) {
    tinymce.editors[i].setContent('')
    tinymce.triggerSave()
  }
}

function loadNumberOfQuestion(id){

  $.ajax({
    url: './ajax/ajax_simulado.php',
    data: {id: id_simulado, action: 'numero_de_questoes'},
    dataType: 'json'	
  }).done(res=>{
    if(res.success==1){
      $('#number').html(Number.parseInt(res.total) + 1)
    }
  })
}

function initTinyMCE(){
  tinymce.init({
    selector: 'textarea.tinyEditor',
    paste_data_images: true,
    entity_encoding : "raw",
    language: 'pt_BR',  
    height: 200,
    plugins: [
      "advlist autolink lists link image code imagetools charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    menubar: false,
  toolbar1: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image preview | forecolor backcolor | table fullscreen",
  image_advtab: true,
  content_css: [
    '../vendors/tinymce/css/codepen.min.css',
    '../vendors/tinymce/css/font.css'
  ],
  images_upload_url: './ajax/tinyMCEUpload.php',
  convert_urls: false,
  images_upload_base_path: './uploads/simulado/imgs',

    style_formats:[
      {
        title: "Headers",
        items: [
          {title: "Header 1",format: "h1"},
          {title: "Header 2",format: "h2"},
          {title: "Header 3",format: "h3"},
          {title: "Header 4",format: "h4"},
          {title: "Header 5",format: "h5"},
          {title: "Header 6",format: "h6"}
        ]
      },
      {
          title: "Inline",items: [{title: "Bold",icon: "bold",format: "bold"}, {title: "Italic",icon: "italic",format: "italic"}, 
          {title: "Sublinhado",icon: "underline",format: "underline"}, {title: "Strikethrough",icon: "strikethrough",format: "strikethrough"}, {title: "Superscript",icon: "superscript",format: "superscript"}, {title: "Subscript",icon: "subscript",format: "subscript"}, {title: "Code",icon: "code",format: "code"}]}, 
          {title: "Blocos",items: [{title: "Paragraph",format: "p"}, {title: "Blockquote",format: "blockquote"}, {title: "Div",format: "div"}, {title: "Pre",format: "pre"}]}, 
          {title: "Alinhamento",items: [{title: "À Esquerda",icon: "alignleft",format: "alignleft"}, {title: "Centralizado",icon: "aligncenter",format: "aligncenter"}, {title: "À Direita",icon: "alignright",format: "alignright"}, {title: "Justificado",icon: "alignjustify",format: "alignjustify"}]}, 
          {
              title: "Fonte",
              items: [
                  {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
                  {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
                  {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
                  {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
                  {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
                  {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
                  {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
                  {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
                  {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
                  {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
                  {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
                  {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
                  {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
              ]
          },
  {title: "Tamanho da fonte", items: [
                              {title: '8pt', inline:'span', styles: { fontSize: '12px', 'font-size': '8px' } },
                              {title: '10pt', inline:'span', styles: { fontSize: '12px', 'font-size': '10px' } },
                              {title: '12pt', inline:'span', styles: { fontSize: '12px', 'font-size': '12px' } },
                              {title: '14pt', inline:'span', styles: { fontSize: '12px', 'font-size': '14px' } },
                              {title: '16pt', inline:'span', styles: { fontSize: '12px', 'font-size': '16px' } },
                              {title: '18pt', inline:'span', styles: { fontSize: '18px', 'font-size': '18px' } },
                              {title: '20pt', inline:'span', styles: { fontSize: '20px', 'font-size': '20px' } },
                              {title: '24pt', inline:'span', styles: { fontSize: '24px', 'font-size': '24px' } }
]
}],

    images_upload_handler: function (blobInfo, success, failure) {
      var xhr, formData;
    
      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open('POST', './ajax/tinyMCEUpload.php');
    
      xhr.onload = function() {
          var json;
      
          if (xhr.status != 200) {
            console.log(xhr.responseText);
              failure('HTTP Error: ' + xhr.status);
              return;
          }
      
          json = JSON.parse(xhr.responseText);
          console.log(json);
      
          if (!json || typeof json.location != 'string') {
              failure('Invalid JSON: ' + xhr.responseText);
              return;
          }
      
          success(json.location);
      };
    
      formData = new FormData();
      formData.append('file', blobInfo.blob(), blobInfo.filename());
    
      xhr.send(formData);
  },

  });
}

function readQuestao(id, returnJson){
  body = $('#ver_questao_body')

  $.ajax({
    url: './ajax/ajax_questao.php',
    data: {action: 'ler', id: id},
    dataType: 'json'	
  }).done(res=>{
    if(res.success == 99){
      new PNotify({
        title: res.msg,
        type: 'warning',
        styling: 'bootstrap3'
        });
        return false;
    }
    if(returnJson){
      return res
    }

    html = 
    `<div class="prova-wrapper">
      <div class="prova">
        <div class="enunciado-modal">${res.questao.enunciado}</div>
        <div class="alternativas-modal row">`;
    opcoes = ['a', 'b', 'c', 'd', 'e']
    n = 0
    res.alternativas.forEach( alternativa => {
      html += `<div class="opcao-modal">${opcoes[n++]}) ${alternativa.descricao}</div>`
    })

    html += `</div>
            <div class="alert alert-success fade in" role="alert">
              <strong>Alternativa Correta: </strong> ${res.questao.alternativa_correta}.
          </div>
        </div>
        </div>
        <div class="clearfix"></div>`

    body.html(html)
  })
}

function deleteQuestao(id){
  action = confirm("Você deseja excluir esta questão?")
  if(action){
    $.ajax({
      method: 'post',
      url: './ajax/ajax_questao.php',
      data: {action: 'deletar', id: id},
      dataType: 'json'	
    }).done(res=>{
      responseTreatment(res, false ,[reloadQuestoes], false)
    })
  }
}

function editarQuestao(id){
  //puxa do banco a questao do id
  var response = null;

  $.ajax({
    url: './ajax/ajax_questao.php',
    data: {id: id, action: 'ler'},
    dataType: 'json'	
  }).done(res=>{
    response = res;

    //Quando o botao de editar é clicado, uma nova aba é mostrada onde o usuario pode editar a questao
    $('.nav-tabs li.edit').show()
    $('.nav-tabs li.edit a').click()
    $('.nav-tabs li.edit a').addClass('editting')

    //depois que o html for inserido, inicialize os tinymce

    tinymce.remove('textarea.tinyEditor')
    initTinyMCE()

    //PREENCHER OS TEXTAREAS E INPUTS
    n_alternativa = 1;
    $('#form-edit-questao').data("id_questao", id) //seta o id da questao no formulario
    tinymce.get('u_enunciado').setContent(res.questao.enunciado) //seta o valor do enunciado
    $('#form-edit-questao input[name="assunto"]').val(res.questao.assunto) //seta assunto
    $(`#form-edit-questao input[name="correct"][value="${res.questao.alternativa_correta}"]`).prop('checked', true); //seta alternativa correta
    res.alternativas.forEach( alternativa => { //seta o valor de todas as alternativas
      tinymce.get(`u_alt${n_alternativa}`).setContent(alternativa.descricao)
      $(`#form-edit-questao #u_alt${n_alternativa}`).data("id_alternativa", alternativa.id) //seta o id do banco na alternativa 
      n_alternativa++
    })

    // END PREENCHER TEXTAREAS E INPUTS
    
  })
  
}
