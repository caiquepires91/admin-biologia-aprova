<?php 
error_reporting(E_ALL & ~E_NOTICE);

require __DIR__.'/../php/autentica.php';
include_once __DIR__ . '/../lib/config.php';

require_once __DIR__ . "/../../../lib/emails.php";

//-----------------METODOS POST -------------------------//
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'novo_aluno':
        novo(0);
        break;

        case 'novo_admin':
        novo(1);
        break;

        case 'atualizar':
        atualizar();
        break;

        case 'deletar':
        deletar();
        break;

        case 'alterar_imagem':
        alterar_imagem();
        break;

        case 'alterar_senha':
        alterar_senha();
        break;

        case 'alterar_permissao_imagem':
        alterar_permissao_imagem();
        break;
    }
}

//-----------------METODOS GET -------------------------//
if(isset($_GET['action'])){
    switch($_GET['action']){

        case 'ler':
        ler();
        break;

        case 'ler_alunos':
        ler_todos(0);
        break;

        case 'ler_admins':
        ler_todos(1);
        break;
    }
}

//FUNCOES DO SISTEMA

function novo($tipo_usuario){
    global $mysqli;

    $usuario = validarUsuario();
    extract($usuario);

    if ($tipo_usuario === 0 && empty($cpf))
        die(json_encode(array("success"=>0, "msg"=>"Informe um CPF válido!")));

    // Condições
    $where_usuario = "(cpf = '$cpf' OR email = '$email') AND";

    if (empty($cpf))
        $where_usuario = "(email = '$email') AND";

    $result = $mysqli->query("SELECT nome FROM usuario WHERE $where_usuario admin = $tipo_usuario AND deletado = 0");
    if($result->num_rows > 0) 
        die(json_encode(array("success"=>0, "msg"=>"CPF/E-mail já cadastrado no sistema!")));

    try{
        $mysqli->autocommit(FALSE);
        $perfilstmt = $mysqli->prepare("INSERT INTO perfil(id, image, imgAtiva, imgPermissao) values(default, 'placeholder.png', 1, 1)");
        $perfilstmt->execute();
        $id_perfil = $mysqli->insert_id;
        $perfilstmt->close();

        $data_cadastro = date('Y-m-d');
        $flag_admin = $tipo_usuario;

        $stmt = $mysqli->prepare("INSERT INTO usuario(id,id_perfil,nome, cpf, estado, cidade, cep, email, data_nascimento, telefone, senha, data_cadastro, `admin`) 
        VALUES (default,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("issssssssssi", $id_perfil, $nome, $cpf, $estado, $cidade, $cep, $email, $data_nascimento, $telefone, $senha, $data_cadastro, $flag_admin);
        $stmt->execute();
        $stmt->close();
        $mysqli->autocommit(TRUE);

        // Envia email para o novo usuário, caso seja ALUNO
        if ($tipo_usuario === 0) {
            // Envia email
            $email_conteudo = "
                <h3 style='text-align: center; color: #073982; font-size: 17px; font-weight: bold; margin-top:20px;'>SEJA BEM-VINDO!</h3>
                <p style='text-align: justify; font-size: 12px; line-height: 1.1; padding: 0 40px;'>
                    Ol&aacute;, <b>" . ucfirst(explode(" ", $nome)[0]) . " " . ucfirst(explode(" ", $nome)[count(explode(" ", $nome)) - 1]) . "</b>!<br>
                    <br>
                    N&oacute;s do Curso Somma gostar&iacute;amos de dar as boas-vindas. &Eacute; com grande satisfa&ccedil;&atilde;o que o(a) temos aqui. O Curso Somma &eacute; uma plataforma de ensino <i>online</i> de matemática que surgiu para preparar de forma eficiente <b>VOC&Ecirc;</b>, que quer aprender Matemática de verdade e garantir a sua vaga na Universidade ou passar no t&atilde;o sonhado concurso.</br>
                </p>
                <h4 style='text-align: center; color: #2B2B9C; font-weight: bold;  padding: 0px 40px;'>
                    Esse curso &eacute; tudo que voc&ecirc; deseja para ter sucesso no ENEM, vestibular ou concurso. Esperamos que aproveite ao m&aacute;ximo o que a nossa plataforma tem a oferecer e que tenha um &oacute;timo aprendizado.<br/>
                    <br/>
                    <b>BONS ESTUDOS!</b>
                </h4>
                <br>
            ";
            Emails\mail_utf8($email, "Seja bem-vindo(a) ao Curso Somma!", $email_conteudo);
        }

        echo json_encode(array("success" => 1, "msg" => "Usuário registrado com sucesso!"));
    }catch(Exception $e) {
        $mysqli->rollback();
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function ler(){
    global $mysqli;
    $id = $_GET['id_usuario'];
    $select = "SELECT usuarios.nome, usuarios.admin, usuarios.cpf, usuarios.email, usuarios.cep, usuarios.estado, usuarios.cidade, usuarios.telefone, usuarios.data_nascimento, usuarios.foto FROM usuarios WHERE usuarios.id = ?";
		try{
            $stmt = $mysqli->prepare($select);
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();

            if($result->num_rows > 0) 
                echo json_encode(array("success" => 1, "usuario" => $result->fetch_assoc()));
            else
                echo json_encode(array("success" => 0, "msg" => "Usuário não encontrado!"));
        }catch(Exception $e){
            die(json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno)));
        }
}

function ler_todos($tipo_usuario){
    alert("ler_todos");

    global $mysqli;
    $select = "SELECT DATE_FORMAT(datanascimento, '%d/%m/%Y') as data_nascimento,DATE_FORMAT(created_at, '%d/%m/%Y') as data_cadastro, 
		usuarios.nome, usuarios.cpf, usuarios.estado, usuarios.cidade, usuarios.cep, usuarios.email, usuarios.telefone, usuarios.id, usuarios.admin, usuarios.master
		FROM usuario WHERE deletado = 0 AND admin = $tipo_usuario ORDER BY DATE_FORMAT(created_at, '%d/%m/%Y') DESC";
		
        try{
            $usuarios = array();
            $stmt = $mysqli->prepare($select);
            $stmt->execute();
            $res = $stmt->get_result();

            while($row = $res->fetch_assoc()){
				$usuarios[] = $row;
			}
            echo json_encode($usuarios);
            
        }catch(Exception $e){
            die(json_encode(array('success'=>99, 'msg'=>$e->getMessage(), 'cod'=>$e->getCode())));
        }
}

function atualizar(){
    global $mysqli;
    $usuario = validarUsuario();
    extract($usuario);

    // Busca usuário
    $result = $mysqli->query("SELECT * FROM usuario WHERE id = $id");
    $user = $result->fetch_assoc();

    // Verifica CPF
    if (!$user["admin"] && empty($cpf))
        die(json_encode(array("success"=>0, "msg"=>"Informe um CPF válido!")));

    // Condições
    $where_usuario = "(cpf = '$cpf' OR email = '$email') AND";

    if (empty($cpf))
        $where_usuario = "(email = '$email') AND";
    
    try{
        $stmt = $mysqli->prepare("SELECT id FROM usuario WHERE (cpf = ? OR email = ?) AND admin = $user[admin] AND deletado = 0");
        $stmt->bind_param("ss", $usuario['cpf'], $usuario['email']);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows > 1) die(json_encode(array("success"=>0, "msg"=>"CPF/E-mail já cadastrado no sistema!")));
        if($row = $res->fetch_assoc())
            if(!($row['id'] == $usuario['id'])) die(json_encode(array("success"=>0, "msg"=>"CPF/E-mail já cadastrado no sistema!")));
        $stmt->close();
    }catch(Exception $e) {
        die(json_encode(array("success"=>99, "msg"=>$e->getMessage(), "cod"=>$e->getCode())));
    }

    //atualizando

    $update = 
    'UPDATE usuario SET usuario.nome = ?, usuario.cpf = ?, usuario.estado = ?,
        usuario.cidade = ?, usuario.cep = ?, usuario.email = ?, 
        usuario.data_nascimento = ? , usuario.telefone = ?
        WHERE usuario.id = ?;';
		 
	try{
        $stmt = $mysqli->prepare($update);
        $stmt->bind_param("ssssssssi", $nome, $cpf, $estado, $cidade, $cep, $email, $data_nascimento, $telefone, $id);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array("success" => 1, "msg" => "Usuário atualizado com sucesso!"));

    }catch(Exception $e){
            die(json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno)));
    }
}

function deletar(){
    global $mysqli;

    $id = filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT);
    $delete = "UPDATE usuario SET deletado = 1 WHERE id = $id";
    $select_master = "SELECT id FROM usuario WHERE id = $id AND `admin` = 1 AND `master` = 1";
    try{
        $master = $mysqli->query($select_master);
        if($master->num_rows > 0) die(json_encode(array("success" => 0, "msg" => "Administrador Master não pode ser removido!")));

        $mysqli->query($delete);
        echo json_encode(array('success'=>1, 'msg'=> 'Usuário removido!'));
    }catch(Exception $e){
        die(json_encode(array("success" => 99, "msg" => $e->getMessage(), "cod" => $e->getCode())));
    }
}

function alterar_imagem(){
    global $mysqli;
    $image = validarImagem(); 

    try{
        extract($image);
        $stmt = $mysqli->prepare("UPDATE perfil SET image = ? WHERE id in (SELECT id_perfil FROM usuario WHERE id = ?);");
        $stmt->bind_param("si", $foto, $id);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array("success" => 1, "msg" => "Foto atualizada com sucesso!"));

    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno)));
    }
}

function alterar_senha(){
    global $mysqli;

    $id = $_POST['id'];
    $senha = validaUpdatePwd($id);
    $update = "UPDATE usuario SET usuario.senha = ? WHERE usuario.id = ?";
		try{
            $stmt = $mysqli->prepare($update);
            $stmt->bind_param("si", $senha, $id);
            $stmt->execute();
            $stmt->close();
            echo json_encode(array("success" => 1, "msg" => "Senha alterada!"));

        }catch(Exception $e){
            die(json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno)));
        }
}

function alterar_permissao_imagem(){
    global $mysqli;
    
    $permissao = $_POST['permissao'];
    $valor = $_POST['valor'];
    $id = $_POST['id'];
    try{
        $stmt = $mysqli->prepare("UPDATE perfil SET $permissao = ? WHERE id in (SELECT id_perfil FROM usuario WHERE id = ?);");
        $stmt->bind_param("ii", $valor, $id);
        $stmt->execute();
        $stmt->close();
        echo json_encode(array("success" => 1, "msg" => "Permissão alterada!"));

    }catch(Exception $e){
        die(json_encode(array("success"=>99, "msg"=>$mysqli->error, "cod"=>$mysqli->errno)));
    }
}

//FUNCOES AUXILIARES//

function validarUsuario(){

        $usuario['nome'] = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
        $usuario['cpf'] = filter_input(INPUT_POST, 'cpf', FILTER_SANITIZE_STRING);
        $usuario['email'] = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
        $usuario['estado'] = filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING);
        $data = filter_input(INPUT_POST, 'dataNascimento', FILTER_SANITIZE_STRING);
        $usuario['cidade'] = filter_input(INPUT_POST, 'cidade', FILTER_SANITIZE_STRING);
        $usuario['telefone'] = filter_input(INPUT_POST, 'telefone', FILTER_SANITIZE_STRING);
        $usuario['cep'] = filter_input(INPUT_POST, 'cep', FILTER_SANITIZE_STRING);
        $usuario['id'] = isset($_POST['id_usuario']) ? filter_input(INPUT_POST, 'id_usuario', FILTER_SANITIZE_NUMBER_INT) : null;

        if($usuario['nome'] == false || $usuario['nome'] == ""){
            die(json_encode(array("success" => 0, "msg" => "Informe um nome válido!")));
        }
        if(!preg_match("/^[a-záàâãéèêíïóôõöúçñ ]+$/", mb_strtolower($usuario['nome']))){
            die(json_encode(array("success" => 0, "msg" => "Informe um nome válido!")));
        }
    
        //VALIDANDO CPF E EMAIL
        if($usuario['cpf'] && validaCPF($usuario['cpf']) == false){
            die(json_encode(array("success" => 0, "msg" => "Informe um cpf válido!")));
        }
        
        if($usuario['email'] == false){
            die(json_encode(array("success" => 0, "msg" => "Informe um email válido!")));
        }
        if (!preg_match('/^(?:[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+\.)*[\w\!\#\$\%\&\'\*\+\-\/\=\?\^\`\{\|\}\~]+@(?:(?:(?:[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!\.)){0,61}[a-zA-Z0-9_-]?\.)+[a-zA-Z0-9_](?:[a-zA-Z0-9_\-](?!$)){0,61}[a-zA-Z0-9_]?)|(?:\[(?:(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\.){3}(?:[01]?\d{1,2}|2[0-4]\d|25[0-5])\]))$/', $usuario['email'])) {
            die(json_encode(array("success" => 0, "msg" => "Informe um email válido!")));
        }

        if($usuario['estado'] && $usuario['estado'] == false){
            die(json_encode(array("success" => 0, "msg" => "Selecione um estado!")));
        }

        if($usuario['cidade'] && $usuario['cidade'] == false){
            die(json_encode(array("success" => 0, "msg" => "Selecione uma cidade!")));
        }
    

        if($usuario['cep'] && !preg_match("/^[0-9]{5}-[0-9]{3}$/", $usuario['cep'])){
            die(json_encode(array("success" => 0, "msg" => "Informe um CEP válido!")));
        }

        if($data && (!ValidaData($data) || !preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $data))){
            die(json_encode(array("success" => 0, "msg" => "Informe uma data de nascimento válida!")));
        }
    
        $data_ajustado = $data ? str_replace('/', '-', $data) : " ";   
        $usuario['data_nascimento'] = date('Y-m-d', strtotime($data_ajustado));
    

        if($usuario['telefone'] && !preg_match("/^\([0-9]{2}\) (([0-9]{5}-[0-9]{4})|([0-9]{4}-[0-9]{4}))$/", $usuario['telefone'])) {
            die(json_encode(array("success" => 0, "msg" => "Informe um telefone válido!")));
        }
        
        if($_POST['action'] == 'novo_aluno' || $_POST['action'] == 'novo_admin'){
            $usuario['senha'] = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_STRING);
            if($usuario['senha'] == false){
                die(json_encode(array("success" => 0, "msg" => "Digite uma senha válida!")));
            }
            if(strlen($usuario['senha']) < 6){
                die(json_encode(array("success" => 0, "msg" => "Mínimo de 6 caracteres para a senha!")));
            }
            $usuario['senha'] = md5($usuario['senha']);
        }
    return $usuario;
}

function validarImagem(){
    $id = filter_input(INPUT_POST, 'id_user', FILTER_SANITIZE_NUMBER_INT);

    $image_name = "";
    $target_dir = "../uploads/users/perfil/";
    $img_atual = $target_dir.$_POST['foto_atual'];

    $imageFileType = pathinfo(basename($_FILES["foto_perfil"]["name"]),PATHINFO_EXTENSION);
    $image_name = $id.'.'.$imageFileType;

    $target_file = $target_dir.$image_name;

    if($result = saveImg($target_file, $imageFileType)){
        echo $result;
        die();
    }

    if($image_name != $_POST['foto_atual']) unlink($img_atual);
    $image['foto'] = $image_name;
    $image['id'] = $id;

    return $image;

}

function saveImg($target_file, $type){
    $imageFileType = strtolower($type);
    if(!$_FILES["foto_perfil"]["tmp_name"]){
        return json_encode(array("success" => 0, "msg" => "Selecione uma imagem!"));
    }
    $check = getimagesize($_FILES["foto_perfil"]["tmp_name"]);
    //check se é imagem
    if($check == false) {
        return json_encode(array("success" => 0, "msg" => "Imagem inválida!"));
    }
    // Testar tamanhos de arquivos?
    //if ($_FILES["foto_perfil"]["size"] > 1000000) {
    //    return json_encode(array("success" => 0, "msg" => "A imagem de capa não deve ter mais que 1MB!"));
    //}

    // Formatos permitidos
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        return json_encode(array("success" => 0, "msg" => "Formatos de imagens aceitos: JPG, JPEG, PNG, GIF!"));
    }

    // Se tudo estiver ok

    if (!move_uploaded_file($_FILES["foto_perfil"]["tmp_name"], $target_file)) {
        return json_encode(array("success" => 0, "msg" => "Erro ao fazer upload de arquivo, por favor tente mais tarde!"));
    }
    return false;
}

function validaUpdatePwd($id){
    $senha['new'] = filter_input(INPUT_POST, 'senhaNew', FILTER_SANITIZE_STRING);
    $senha['cfm'] = filter_input(INPUT_POST, 'senhaNewCfm', FILTER_SANITIZE_STRING);

    if($senha['new'] == false){
        echo json_encode(array("success" => 0, "msg" => "Nova senha inválida!"));
        die();
    }

    if($senha['cfm'] == false){
        echo json_encode(array("success" => 0, "msg" => "Senha de confirmação inválida!"));
        die();
    }

    if($senha['new'] != $senha['cfm']){
        echo json_encode(array("success" => 0, "msg" => "Senha de confirmação incorreta!"));
        die();
    }

    return md5($senha['new']);

}

function ValidaData($dat){
	$data = explode("/",$dat); // fatia a string $dat em pedados, usando / como referência
	$d = $data[0];
	$m = $data[1];
	$y = $data[2];

	// verifica se a data é válida!
	// 1 = true (válida)
    // 0 = false (inválida)
    
    if($y > 2010){ //Verifica se tem idade suficiente?*
        return false;
    }
    
	$res = checkdate($m,$d,$y);
	if ($res == 1){
	   return true;
	} else {
	   return false;
	}
}

function cpf_transform($cpf){
    $cpf = preg_replace("/[^0-9]/", "", $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
    return $cpf;
}

function validaCPF($cpf = null) {

	// Verifica se um número foi informado
	if(empty($cpf)) {
		return false;
    }

	// Elimina possivel mascara
	$cpf = cpf_transform($cpf);
	
	// Verifica se o numero de digitos informados é igual a 11 
	if (strlen($cpf) != 11) {
		return false;
	}
	// Verifica se nenhuma das sequências invalidas abaixo 
	// foi digitada. Caso afirmativo, retorna falso
	else if ($cpf == '00000000000' || 
		$cpf == '11111111111' || 
		$cpf == '22222222222' || 
		$cpf == '33333333333' || 
		$cpf == '44444444444' || 
		$cpf == '55555555555' || 
		$cpf == '66666666666' || 
		$cpf == '77777777777' || 
		$cpf == '88888888888' || 
		$cpf == '99999999999') {
		return false;
	 // Calcula os digitos verificadores para verificar se o
	 // CPF é válido
	 } else {   
		
		for ($t = 9; $t < 11; $t++) {
			
			for ($d = 0, $c = 0; $c < $t; $c++) {
				$d += $cpf{$c} * (($t + 1) - $c);
			}
			$d = ((10 * $d) % 11) % 10;
			if ($cpf{$c} != $d) {
				return false;
			}
		}

		return true;
	}
}

?>